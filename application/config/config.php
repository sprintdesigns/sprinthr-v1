<?php
// leave '/' if you don't have base folder. http://example.com/<This is my base folder>/index.php
// example: '/my_folder/my_another_folder/'

define('MAIN_FOLDER', '/sprinthr_clients_project/daichi/');

define('BASE_FOLDER', '/sprinthr_clients_project/daichi/');

define('BASE_FOLDER_EDITOR','sprinthr_clients_project/daichi/files/'); //this is for ckeditor// upload image

define('CLERK_BASE_FOLDER', '/sprinthr_clients_project/daichi/clerk/');

define('CLERK_SCHEDULE_BASE_FOLDER', '/sprinthr_clients_project/daichi/clerk_schedule/');

define('EMPLOYEE_BASE_FOLDER', '/sprinthr_clients_project/daichi/employee/');

define('PAYROLL_BASE_FOLDER', '/sprinthr_clients_project/daichi/payroll/');

define('RECRUITMENT_BASE_FOLDER', '/sprinthr_clients_project/daichi/recruitment/');

define('HR_BASE_FOLDER', '/sprinthr_clients_project/daichi/hr/');

define('MYSQLDUMP_APP_PATH','D:\web\mysql\bin');

define('HIDE_INDEX_PAGE', false);

define('MAINTENANCE_MODE',false); 	// Maintenance Mode

define('INDEX_PAGE', 'index.php');

define('THEME', 'hr');

define('MAIN_THEME', 'hr');

define('ENCRYPTION_KEY', 'webgroundz');

// ==================================================

define('TEMP_USER_FOLDER', $_SERVER['DOCUMENT_ROOT'] . BASE_FOLDER . 'files/temp/users/');

define('USER_PROFILE_IMAGE_FOLDER', BASE_FOLDER . 'hr/files/photo/');

define('DEFAULT_CONTROLLER', 'source');

define('DEFAULT_METHOD', 'plugins');

define('IF_INVALID_CONTROLLER', 'page'); // use this controller if the controller supplied in the url is invalid

define('CONTROLLER_VAR', 'controller');

define('METHOD_VAR', 'method');

// Reflected to _init.js

define('DIALOG_CONTENT_HANDLER', 'dialog-box'); // Automatically created using _init.js

define('CONTENT_BODY_ID', 'content_body'); // The content handler. Must be seen in template.php

define('MAIN_DIALOG_ID', 'main_dialog');

?>