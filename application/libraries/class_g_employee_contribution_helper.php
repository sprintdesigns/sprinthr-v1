<?php
class G_Employee_Contribution_Helper {

    public static function addContribution($employee_id, $salary) {

        $ph = G_Philhealth_Finder::findBySalary($salary);
        if($ph) {
            $philhealth_er = (float) $ph->getCompanyShare();
            $philhealth_ee = (float) $ph->getEmployeeShare();
        }else{
            $philhealth_er = 0;
            $philhealth_ee = 0;
        }
        
        $sss = G_SSS_Finder::findBySalary($salary);
        if($sss) {
            $sss_er = (float) $sss->getCompanyShare();
            $sss_ee = (float) $sss->getEmployeeShare();
        }else{
            $sss_er = 0;
            $sss_ee = 0;
        }

        // OLD
        /*$pagibig = G_Pagibig_Finder::findBySalary($salary);
        $pagibig_er = (float) $pagibig->getCompanyShare();
        $pagibig_ee = (float) $pagibig->getEmployeeShare();*/

        $pagibig = G_Pagibig_Table_Finder::findBySalary($salary);
        $pagibig_er = (float) $pagibig['company_share'];
        $pagibig_ee = (float) $pagibig['employee_share'];

        $contribution = G_Employee_Contribution_Finder::findByEmployeeId($employee_id);
        if (!$contribution) {
            $to_deduct_arr['sss'] = G_Employee_Contribution::YES;
            $to_deduct_arr['philhealth'] = G_Employee_Contribution::YES;
            $to_deduct_arr['pagibig'] = G_Employee_Contribution::YES;

            $c = self::generate($employee_id, $sss_ee, $pagibig_ee, $philhealth_ee, $sss_er, $pagibig_er, $philhealth_er);
            $c->setToDeduct(serialize($to_deduct_arr));
            $c->save();
        }else{
            $c = self::generate($employee_id, $sss_ee, $pagibig_ee, $philhealth_ee, $sss_er, $pagibig_er, $philhealth_er);
            $c->setId($contribution->getId());
            $c->setToDeduct($contribution->getToDeduct());
            $c->save();
        }
    }

    public static function generate($employee_id, $sss_ee, $pagibig_ee, $philhealth_ee, $sss_er, $pagibig_er, $philhealth_er) {
        $cont = new G_Employee_Contribution;
        $cont->setEmployeeId($employee_id);
        $cont->setSssEe($sss_ee);
        $cont->setPagibigEe($pagibig_ee);
        $cont->setPhilhealthEe($philhealth_ee);
        $cont->setSssEr($sss_er);
        $cont->setPagibigEr($pagibig_er);
        $cont->setPhilhealthEr($philhealth_er);
        return $cont;
    }
		
	public static function isIdExist(G_Employee_Contribution $e) {
		$sql = "
			SELECT COUNT(*) as total
			FROM " . G_EMPLOYEE_CONTRIBUTION ."
			WHERE employee_id = ". Model::safeSql($e->getEmployeeId()) ."
		";

		$result = Model::runSql($sql);
		$row    = Model::fetchAssoc($result);
		return $row['total'];
	}

}
?>