<?php
class G_Fp_Attendance_Logs_Helper {

    public static function sqlGetEmployeeLogsByEmployeeCodeAndDate($employee_code = '', $date = '', $fields) {
    	if( !empty($fields) ){
    		$sql_fields = implode(",", $fields);
    	}else{
    		$sql_fields = " * ";
    	}

		$sql = "
            SELECT {$sql_fields}
            FROM ". G_ATTENDANCE_LOG ." 
            WHERE employee_code ='{$employee_code}'
            	AND `date` =" . Model::safeSql($date) . "
            ORDER BY date, type ASC
        ";         
       $records = Model::runSql($sql,true);       
       return $records;
	}

    public static function sqlGetEmployeeLogsByUserIdAndDate($user_id = '', $date = '', $fields) {
        if( !empty($fields) ){
            $sql_fields = implode(",", $fields);
        }else{
            $sql_fields = " * ";
        }

        $sql = "
            SELECT {$sql_fields}
            FROM ". G_ATTENDANCE_LOG ." 
            WHERE user_id =" . Model::safeSql($user_id) . "
                AND `date` =" . Model::safeSql($date) . "
            ORDER BY date, type ASC
        ";  
       $records = Model::runSql($sql,true);       
       return $records;
    }

    public static function sqlIsIdExists($id) {
        $is_exists = false;

        $sql = "
            SELECT COUNT(id) as total
            FROM " . G_ATTENDANCE_LOG ."
            WHERE id = ". Model::safeSql($id) ."
        ";
        $result = Model::runSql($sql);
        $row    = Model::fetchAssoc($result);

        if( $row['total'] > 0 ){
            $is_exists = true;
        }
        
        return $is_exists;
    }
}
?>