<?php 
class G_Notifications_Helper {
	public static function isIdExist(G_Notifications $n) {
		$sql = "
			SELECT COUNT(id) as total
			FROM " . G_NOTIFICATIONS ."
			WHERE id = ". Model::safeSql($n->getId()) ."
		";
		$result = Model::runSql($sql);
		$row    = Model::fetchAssoc($result);
		return $row['total'];
	}
	
	public static function countTotalRecords() {
		$sql = "
			SELECT COUNT(id) as total
			FROM " . G_NOTIFICATIONS ."
		";
		$result = Model::runSql($sql);
		$row    = Model::fetchAssoc($result);
		return $row['total'];
	}
    
    public static function countNotifications() {
        $sql = "
			SELECT COUNT(id) as total
			FROM " . G_NOTIFICATIONS ." 
            WHERE status = ".Model::safeSql(G_Notifications::STATUS_NEW)." 
                AND item > 0
		";
		$result = Model::runSql($sql);
		$row    = Model::fetchAssoc($result);
		return $row['total'];
    }
    
    public static function getNotifications() {
        $sql = "
			SELECT *
			FROM " . G_NOTIFICATIONS ." 
            WHERE item > 0
		";
		$result = Model::runSql($sql,true);
		return $result;
    }
    
    public static function updateNotifications( $event_type_arr = array() ) {


        /*
         * Disable Notification
        */

            $event_type_disable_arr = array(
                'INCOMPLETE_REQUIREMENTS'   => 'Incomplete Requirements',
                'EMPLOYEE_WITH_NO_SCHEDULE' => 'Employee with no schedule',
                'EARLY_TIME_IN'             => 'Employee with early in',
            );

        $disable_n1 = G_Notifications_Finder::findByEventType($event_type_disable_arr['INCOMPLETE_REQUIREMENTS']);
        if($disable_n1) {
            $disable_n1->setStatus(G_Notifications::STATUS_SEEN); 
            $disable_n1->setItem(0);
            $disable_n1->save();
        }   

        $disable_n2 = G_Notifications_Finder::findByEventType($event_type_disable_arr['EMPLOYEE_WITH_NO_SCHEDULE']);
        if($disable_n2) {
            $disable_n2->setStatus(G_Notifications::STATUS_SEEN); 
            $disable_n2->setItem(0);
            $disable_n2->save();
        }  

        /*
         * Disable Notification - end
        */


        //Incomplete Requirements
        $has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['INCOMPLETE_REQUIREMENTS']);
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['INCOMPLETE_REQUIREMENTS']);
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }
        $incomplete_requirements = G_Employee_Requirements_Helper::countEmployeeIncompleteRequirements();
        if($incomplete_requirements > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($incomplete_requirements);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($incomplete_requirements < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($incomplete_requirements);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees have incomplete requirements.' : 'Employee has incomplete requirements.');
        $n->setDescription($description);

        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();
        }
              
        //No Salary Rate
        $has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['NO_SALARY_RATE']);
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['NO_SALARY_RATE']);
            $n->setDescription('');
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }
        $no_salary_rate = G_Employee_Basic_Salary_History_Helper::countEmployeeNoSalaryRate();
        if($no_salary_rate > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($no_salary_rate);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($no_salary_rate < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($no_salary_rate);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees have no salary rate yet.' : 'Employee has no salary rate yet.');
        $n->setDescription($description);

        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();
        }
        
        //End Of Contract
        $has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['END_OF_CONTRACT']);
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['END_OF_CONTRACT']);
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }
        $end_of_contract = G_Employee_Helper::countEmployeeEndOfContract30Days();
        if($end_of_contract > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($end_of_contract);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($end_of_contract < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($end_of_contract);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees reached end of contract this month.' : 'Employee reached end of contract this month.');
        $n->setDescription($description);

        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();
        }
        
        //No Department
        $has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['NO_DEPARTMENT']);
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['NO_DEPARTMENT']);
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }
        $no_department = G_Employee_Subdivision_History_Helper::countEmployeeNoDepartment();
        if($no_department > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($no_department);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($no_department < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($no_department);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees have no assigned department yet.' : 'Employee has no assigned department yet.');
        $n->setDescription($description);

        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();
        }
        
        //No Job Title
        $has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['NO_JOB_TITLE']);
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['NO_JOB_TITLE']);
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }
        $no_job_title = G_Employee_Job_History_Helper::countEmployeeNoJobTitle();
        if($no_job_title > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($no_job_title);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($no_job_title < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($no_job_title);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees have no Job Title yet.' : 'Employee has no Job Title yet.');
        $n->setDescription($description);

        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();
        }
        
        //No Employment Status
        $has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['NO_EMPLOYMENT_STATUS']);
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['NO_EMPLOYMENT_STATUS']);
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }
        $no_employment_status = G_Employee_Job_History_Helper::countEmployeeNoEmploymentStatus();
        if($no_employment_status > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($no_employment_status);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($no_employment_status < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($no_employment_status);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees have no employment status.' : 'Employee has no employment status.');
        $n->setDescription($description);

        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();
        }
        
        //No Employee Status
        $has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['NO_EMPLOYEE_STATUS']);
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['NO_EMPLOYEE_STATUS']);
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }
        $no_employee_status = G_Employee_Helper::countEmployeeNoEmployeeStatus();
        if($no_employee_status > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($no_employee_status);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($no_employee_status < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($no_employee_status);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees have no employee status.' : 'Employee has no employee status.');
        $n->setDescription($description);

        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();
        }
        
        //Tardiness
        $has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['TARDINESS']);
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['TARDINESS']);
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }
        $tardiness = G_Employee_Helper::countEmployeeTardinessByCurrentDate();
        if($tardiness > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($tardiness);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($tardiness < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($tardiness);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees are late today.' : 'Employee is late today.');
        $n->setDescription($description);

        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();
        }
        
        //Incomplete DTR
        $has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['INCOMPLETE_DTR']);
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['INCOMPLETE_DTR']);
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }

        //FOR INCOMPLETE DTR ONLY
        $date_from = date("Y-m-01");
        $date_to   = date("Y-m-t");               
        $count_incomplete_dtr = G_Employee_Helper::sqlCountIncompleteDTR($date_from, $date_to);
        if($count_incomplete_dtr > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($count_incomplete_dtr);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($count_incomplete_dtr < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($count_incomplete_dtr);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees with incomplete DTR this month.' : 'Employee with incomplete DTR this month.');
        $n->setDescription($description);

        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();
        }

        //Employee with no schedule
        $has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['EMPLOYEE_WITH_NO_SCHEDULE']);
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['EMPLOYEE_WITH_NO_SCHEDULE']);
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }
        $employee_with_no_schedule = G_Schedule_Group_Helper::countEmployeeWithNoSchedule();

        if($employee_with_no_schedule > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($employee_with_no_schedule);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($employee_with_no_schedule < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($employee_with_no_schedule);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees with no schedule.' : 'Employee with no schedule.');
        $n->setDescription($description);

        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();
        }

        //Incorrect Shifts
        $has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['EMPLOYEE_INCORRECT_SHIFT']);
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['EMPLOYEE_INCORRECT_SHIFT']);
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }

        $date   = date("Y-m-d");
        $cutoff = new G_Cutoff_Period();
        $period = $cutoff->getCurrentCutoffPeriod($date);
        $date_from = $period['current_cutoff']['start'];
        $date_to   = $period['current_cutoff']['end'];

        $rep = new G_Report();
        $rep->setFromDate($date_from);
        $rep->setToDate($date_to);
        $incorrect_shifts = $rep->allIncorrectShift();

        $total_incorrect_shifts = 0;
        foreach( $incorrect_shifts as $key => $shift ){
            foreach( $shift as $data ){
                $total_incorrect_shifts++;
            }
        }

         if($total_incorrect_shifts > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($total_incorrect_shifts);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($total_incorrect_shifts < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($total_incorrect_shifts);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees with incorrect shifts.' : 'Employee with incorrect shift.');
        $n->setDescription($description);

        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();
        }

        //Work Against Schedule
        /*$has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['WORK_AGAINST_SCHEDULE']);
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['WORK_AGAINST_SCHEDULE']);
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }

        $s_from = date("Y-m-d",strtotime("-1 day"));
        $s_to   = date("Y-m-d");
        $report = new G_Report();
        $report->setFromDate($s_from);
        $report->setToDate($s_to);
        $a_data = $report->summaryWorkAgainstSchedule();
        $i_total_with_work_against_schedule = $a_data['total'];

        if($i_total_with_work_against_schedule > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($i_total_with_work_against_schedule);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($i_total_with_work_against_schedule < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($i_total_with_work_against_schedule);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees with work against schedule.' : 'Employees with work against schedule.');
        $n->setDescription($description);

        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();
        }*/

        //Undertime
        $has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['UNDERTIME']);
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['UNDERTIME']);
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }

        $s_from = date("Y-m-d",strtotime("-1 day"));
        $s_to   = date("Y-m-d");
        $i_total_with_undertime = G_Attendance_Helper::sqlCountTotalWithUndertimeByFromAndToDate($s_from, $s_to);

        if($i_total_with_undertime > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($i_total_with_undertime);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($i_total_with_undertime < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($i_total_with_undertime);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees with undertime.' : 'Employee with undertime.');
        $n->setDescription($description);

        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();
        }

        //Early In
        /*$has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['EARLY_TIME_IN']);
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['EARLY_TIME_IN']);
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }

        $s_from = date("Y-m-d",strtotime("-1 day"));
        $s_to   = date("Y-m-d");
        $i_total_ealy_in = G_Attendance_Helper::sqlCountTotalEarlyInByFromAndToDate($s_from, $s_to);

        if($i_total_ealy_in > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($i_total_ealy_in);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($i_total_ealy_in < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($i_total_ealy_in);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees with early in.' : 'Employee with early in.');
        $n->setDescription($description);

        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();
        }*/

        //No Bank Account
        $has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['NO_BANK_ACCOUNT']);         
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['NO_BANK_ACCOUNT']);
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }
        $no_bank_account = G_Employee_Helper::countEmployeeNoBankAccount();            
        if($no_bank_account > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($no_bank_account);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($no_bank_account < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($no_bank_account);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees have no bank account.' : 'Employee has no bank account.');
        $n->setDescription($description);
        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();            
        }

        //Upcoming Birthday
        $has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['UPCOMING_BIRTHDAY']);
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['UPCOMING_BIRTHDAY']);
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }

        $i_total_upcoming_birthday = G_Employee_Helper::countEmployeeWithUpcomingBirthday();

        if($i_total_upcoming_birthday > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($i_total_upcoming_birthday);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($i_total_upcoming_birthday < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($i_total_upcoming_birthday);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees with upcoming birthday.' : 'Employee with upcoming birthday.');
        $n->setDescription($description);

        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();
        }

        //Birthday Today
        $has_update = false;
        $n = G_Notifications_Finder::findByEventType($event_type_arr['BIRTHDAY_TODAY']);
        if(!$n) {
            $n = new G_Notifications();
            $n->setEventType($event_type_arr['BIRTHDAY_TODAY']);
            $n->setStatus(G_Notifications::STATUS_NEW); 
            $n->setDateCreated(date('Y-m-d H:i:s'));
            $n->setItem(0);
            $has_update = true;
        }

        $i_total_birthday_today = G_Employee_Helper::countEmployeeWithBirthdayToday();

        if($i_total_birthday_today > $n->getItem()) {
            //Update when there's new notification
            $n->setItem($i_total_birthday_today);
            $n->setStatus(G_Notifications::STATUS_NEW);  
            $has_update = true;
        }else if($i_total_birthday_today < $n->getItem()){
            //Update when the item was decreased
            $n->setItem($i_total_birthday_today);
            $has_update = true;
        }

        $description = ($n->getItem() > 1 ? 'Employees with birthday today.' : 'Employee with birthday today.');
        $n->setDescription($description);

        if($has_update) {
            $n->setDateModified(date('Y-m-d H:i:s'));
            $n->save();
        }

        $delete_null = G_Notifications_Finder::findByEventTypeNull();

        if($delete_null) {
            $delete_null->delete();
        }           
    }

    public static function getNotificationItems(G_Notifications $n) {
        $data = null;
        if($n) {
            $event_type_array = $n->getEventTypeArray();
            if($n->getEventType() == $event_type_array['INCOMPLETE_REQUIREMENTS']){
                $data = G_Employee_Requirements_Helper::getEmployeeIncompleteRequirements();
            }elseif($n->getEventType() == $event_type_array['NO_SALARY_RATE']) {
                $data = G_Employee_Basic_Salary_History_Helper::getEmployeeNoSalaryRate();
            }elseif($n->getEventType() == $event_type_array['END_OF_CONTRACT']) {
                $data = G_Employee_Helper::getEmployeeEndOfContractByCurrentMonth();
            }elseif($n->getEventType() == $event_type_array['NO_DEPARTMENT']) {
                $data = G_Employee_Subdivision_History_Helper::getEmployeeNoDepartment();
            }elseif($n->getEventType() == $event_type_array['NO_JOB_TITLE']) {
                $data = G_Employee_Job_History_Helper::getEmployeeNoJobTitle();
            }elseif($n->getEventType() == $event_type_array['NO_EMPLOYMENT_STATUS']) {
                $data = G_Employee_Job_History_Helper::getEmployeeNoEmploymentStatus();
            }elseif($n->getEventType() == $event_type_array['NO_EMPLOYEE_STATUS']) {
                $data = G_Employee_Helper::getEmployeeNoEmployeeStatus();
            }elseif($n->getEventType() == $event_type_array['TARDINESS']) {
                $data = G_Employee_Helper::getEmployeeTardinessByCurrentDate();
            }elseif($n->getEventType() == $event_type_array['INCOMPLETE_DTR']) {
                $query['date_from'] = date("Y-m-01");
                $query['date_to']   = date("Y-m-t");
                $query['remark']    = 'all';
                $data = G_Employee_Helper::getIncompleteTimeInOutData($query);
            }elseif($n->getEventType() == $event_type_array['EMPLOYEE_WITH_NO_SCHEDULE']) {
                $data = G_Schedule_Group_Helper::getEmployeeWithNoSchedule();
            }elseif($n->getEventType() == $event_type_array['UNDERTIME']){
                $s_from = date("Y-m-d",strtotime("-1 day"));
                $s_to   = date("Y-m-d");
                $data = G_Attendance_Helper::sqlEmployeesWithUndertimeByFromAndToDate($s_from, $s_to);
            /*}elseif($n->getEventType() == $event_type_array['EARLY_TIME_IN']){
                $s_from = date("Y-m-d",strtotime("-1 day"));
                $s_to   = date("Y-m-d");
                $data = G_Attendance_Helper::sqlEmployeesWithEarlyInByFromAndToDate($s_from, $s_to);*/
            }elseif($n->getEventType() == $event_type_array['LEAVE_ADDED']){
                $current_year = date("Y");
                $data = G_Employee_Leave_Credit_History_Helper::getAllLeaveCreditHistoryByYear($current_year);
            }elseif($n->getEventType() == $event_type_array['NO_BANK_ACCOUNT']){                
                $data = G_Employee_Helper::employeeWithNoBankAccount();
            }
        }

        return $data;     
    }
				
}
?>