<?php
class G_Payslip_Finder {
	
	public static function findById($id) {
		$sql = "
			SELECT id, payout_date, period_start, period_end, basic_pay, gross_pay, total_earnings, total_deductions, net_pay, earnings, other_earnings, deductions, other_deductions, labels,
				taxable, non_taxable, withheld_tax, month_13th, sss, pagibig, philhealth
			FROM g_employee_payslip
			WHERE id = ". Model::safeSql($id) ."			
			LIMIT 1			
		";
		return self::getObject($sql, $employee);
	}
	
	public static function findAllByEmployeeAndPayoutDateRange(IEmployee $employee, $start_date, $end_date) {
		$sql = "
			SELECT id, payout_date, period_start, period_end, basic_pay, gross_pay, total_earnings, total_deductions, net_pay, earnings, other_earnings, deductions, other_deductions, labels,
				taxable, non_taxable, withheld_tax, month_13th, sss, pagibig, philhealth
			FROM g_employee_payslip
			WHERE employee_id = ". Model::safeSql($employee->getId()) ."
			AND (payout_date >= ". Model::safeSql($start_date) ." AND payout_date <= ". Model::safeSql($end_date) .")
			ORDER BY payout_date		
		";
		return self::getObjects($sql, $employee);
	}
		
	public static function findByEmployeeAndPayoutDate(IEmployee $employee, $payout_date) {
		$sql = "
			SELECT id, payout_date, period_start, period_end, basic_pay, gross_pay, total_earnings, total_deductions, net_pay, earnings, other_earnings, deductions, other_deductions, labels,
				taxable, non_taxable, withheld_tax, month_13th, sss, pagibig, philhealth
			FROM g_employee_payslip
			WHERE employee_id = ". Model::safeSql($employee->getId()) ."
			AND payout_date = ". Model::safeSql($payout_date) ."
			LIMIT 1			
		";
		return self::getObject($sql, $employee);
	}
		
	public static function findByEmployeeAndPeriod(IEmployee $employee, $start_date, $end_date) {
		$sql = "
			SELECT id, payout_date, period_start, period_end, basic_pay, gross_pay, total_earnings, total_deductions, net_pay, earnings, other_earnings, deductions, other_deductions, labels,
				taxable, non_taxable, withheld_tax, month_13th, sss, pagibig, philhealth
			FROM g_employee_payslip
			WHERE employee_id = ". Model::safeSql($employee->getId()) ."
			AND (period_start = ". Model::safeSql($start_date) ." AND period_end = ". Model::safeSql($end_date) .")
			LIMIT 1			
		";	
        		
		return 
		self::getObject($sql, $employee);
	}

	public static function findByEmployeeAndLastSalary(IEmployee $employee) {
		$sql = "
			SELECT id, payout_date, period_start, period_end, basic_pay, gross_pay, total_earnings, total_deductions, net_pay, earnings, other_earnings, deductions, other_deductions, labels,
				taxable, non_taxable, withheld_tax, month_13th, sss, pagibig, philhealth
			FROM g_employee_payslip
			WHERE employee_id = ". Model::safeSql($employee->getId()) ."
			AND basic_pay > 0
			ORDER by period_end DESC
			LIMIT 1			
		";
        		
		return self::getObject($sql, $employee);
	}
	
	public static function findByEmployeeAndDateRange(IEmployee $employee, $start_date, $end_date) {
		$sql = "
			SELECT id, payout_date, period_start, period_end, basic_pay, gross_pay, total_earnings, total_deductions, net_pay, earnings, other_earnings, deductions, other_deductions, labels,
				taxable, non_taxable, withheld_tax, month_13th, sss, pagibig, philhealth
			FROM g_employee_payslip
			WHERE (employee_id = ". Model::safeSql($employee->getId()) .")
			AND (period_start >= " . Model::safeSql($start_date) . " AND period_end <= " . Model::safeSql($end_date) . ")
			LIMIT 1			
		";	
			
		return self::getObject($sql, $employee);
	}
	
	public static function findByPeriod($start_date, $end_date) {
		$sql = "
			SELECT id, payout_date, period_start, period_end, basic_pay, gross_pay, total_earnings, total_deductions, net_pay, earnings, other_earnings, deductions, other_deductions, labels,
				taxable, non_taxable, withheld_tax, month_13th, sss, pagibig, philhealth
			FROM g_employee_payslip
			WHERE (period_start = ". Model::safeSql($start_date) ." AND period_end = ". Model::safeSql($end_date) .")		
		";		
		return self::getObjects($sql);
	}	

	public static function findAllByPeriod($start_date, $end_date, $fields) {
		$sql_fields = " * ";
		if( !empty($fields) ){
			$sql_fields = implode(",", $fields);
		}

		$sql = "
			SELECT {$sql_fields}
			FROM g_employee_payslip
			WHERE (period_end BETWEEN ". Model::safeSql($start_date) ." AND ". Model::safeSql($end_date) .")		
		";		
		
		return self::getObjects($sql);
	}

	public static function findAllByYear($year, $fields) {
		$sql_fields = " * ";
		if( !empty($fields) ){
			$sql_fields = implode(",", $fields);
		}

		$sql = "
			SELECT {$sql_fields}
			FROM g_employee_payslip
			WHERE DATE_FORMAT(period_end,'%Y')=" . Model::safeSql($year) . "
			ORDER BY employee_id ASC	
		";		
		
		return self::getObjects($sql);
	}

	public static function findAllByYearWithOptions($year, $options, $fields) {
		$sql_fields = " p.* ";
		if( !empty($fields) ){
			$sql_fields = implode(",", $fields);
		}

		if( isset($options['add_query']) ){
			$sql_add_query = $options['add_query'];
		}
		
		$sql = "
			SELECT {$sql_fields}
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
			WHERE DATE_FORMAT(p.period_end,'%Y')=" . Model::safeSql($year) . "
			{$sql_add_query}
			ORDER BY p.employee_id ASC	
		";
		
		return self::getObjects($sql);
	}

	public static function findAllByYearWithOptionsCustomAddSelectedEmpAndRemoveJanuary($year, $options, $fields) {
		$sql_fields = " p.* ";
		if( !empty($fields) ){
			$sql_fields = implode(",", $fields);
		}

		if( isset($options['add_query']) ){
			$sql_add_query = $options['add_query'];
		}

		$employee_arr = array(20, 94, 3, 170, 69, 45, 324, 24, 14, 13, 171, 31, 29, 12, 5);
		$included_employee = '"' . implode('","', $employee_arr) . '"';
				
		$sql = "
			SELECT {$sql_fields}
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
			WHERE DATE_FORMAT(p.period_end,'%Y')=" . Model::safeSql($year) . "
			AND DATE_FORMAT(p.period_start,'%m')!=" . Model::safeSql('01') . "
			AND p.employee_id IN ($included_employee)
			{$sql_add_query}
			ORDER BY p.employee_id ASC	
		";		

		//echo $sql;
		
		return self::getObjects($sql);
	}	

	public static function findAllByYearWithOptionsCustomRemoveSelectedEmp($year, $options, $fields) {
		$sql_fields = " p.* ";
		if( !empty($fields) ){
			$sql_fields = implode(",", $fields);
		}

		if( isset($options['add_query']) ){
			$sql_add_query = $options['add_query'];
		}

		$employee_arr = array(20, 94, 3, 170, 69, 45, 324, 24, 14, 13, 171, 31, 29, 12, 5);
		$included_employee = '"' . implode('","', $employee_arr) . '"';
				
		$sql = "
			SELECT {$sql_fields}
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
			WHERE DATE_FORMAT(p.period_end,'%Y')=" . Model::safeSql($year) . "
			AND p.employee_id NOT IN ($included_employee)
			{$sql_add_query}
			ORDER BY p.employee_id ASC	
		";		
		
		//echo '<hr />';
		//echo $sql;
		
		return self::getObjects($sql);
	}		

	public static function findAllByYearAndEmployeeId($year, $employee_id = 0, $fields) {
		$sql_fields = " * ";
		if( !empty($fields) ){
			$sql_fields = implode(",", $fields);
		}

		$sql = "
			SELECT {$sql_fields}
			FROM g_employee_payslip
			WHERE DATE_FORMAT(period_end,'%Y')=" . Model::safeSql($year) . "
				AND employee_id =" . Model::safeSql($employee_id) . "	
		";		
		
		return self::getObjects($sql);
	}

	public static function findByEmployeeIdAndPeriod($employee_id, $start_date, $end_date) {
		$sql = "
			SELECT id, period_start, period_end, employee_id 
			FROM g_employee_payslip
			WHERE employee_id = ". Model::safeSql($employee_id) ."
			AND (period_start = ". Model::safeSql($start_date) ." AND period_end = ". Model::safeSql($end_date) .")
			LIMIT 1			
		";	
        		
		return 
		self::getObject($sql, $employee);
	}	
	
	private static function getObjects($sql, $employee) {
		$result = Model::runSql($sql);
		$total = mysql_num_rows($result);
		if ($total == 0) {
			return false;	
		}
		while ($row = Model::fetchAssoc($result)) {
			$return[$row['id']] = self::newObject($row, $employee);
		}		
		return $return;		
	}
	
	private static function getObject($sql, $employee) {
		$result = Model::runSql($sql);
		$row = Model::fetchAssoc($result);
		if (empty($row)) {
			return false;
		}
		return self::newObject($row, $employee);
	}
	
	private static function newObject($row, $employee) {
		$p = new G_Payslip;
		$p->setId($row['id']);
		$p->setEmployee($employee);
		$p->setPayoutDate($row['payout_date']);
		$p->setPeriod($row['period_start'], $row['period_end']);
		$earnings = ($row['earnings']) ? $p->addEarnings(unserialize($row['earnings'])) : '' ;
		$other_earnings = ($row['other_earnings']) ? $p->addOtherEarnings(unserialize($row['other_earnings'])) : '';
		$deductions = ($row['deductions']) ? $p->addDeductions(unserialize($row['deductions'])) : '';
		$other_deductions = ($row['other_deductions']) ? $p->addOtherDeductions(unserialize($row['other_deductions'])) : '';
		$labels = ($row['labels']) ? $p->addLabels(unserialize($row['labels'])) : '' ;
		
		$p->setEmployeeId($row['employee_id']);
		$p->setBasicPay($row['basic_pay']);
		$p->setGrossPay($row['gross_pay']);
        $p->setTotalEarnings($row['total_earnings']);
        $p->setTotalDeductions($row['total_deductions']);
		$p->setNetPay($row['net_pay']);
		$p->setTaxable($row['taxable']);
        $p->setNonTaxable($row['non_taxable']);
		$p->setWithheldTax($row['withheld_tax']);
		$p->set13thMonth($row['month_13th']);
		$p->setSSS($row['sss']);
		$p->setPagibig($row['pagibig']);
		$p->setPhilhealth($row['philhealth']);
		return $p;
	}
}
?>