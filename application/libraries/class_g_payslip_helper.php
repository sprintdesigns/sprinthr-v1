<?php
/*
	$e = Employee_Factory::get(3);
	$ps = $e->getPayslip('2011-02-06', '2011-02-20');
	$ph = new Payslip_Helper($ps);
	echo $ph->getLabel('prepared_by') .'='. $ph->getValue('prepared_by');
*/

class G_Payslip_Helper {
	protected $payslip; // Instance of Payslip Class
	public $labels;
	protected $earnings;
	protected $deductions;
	
	public function __construct($payslip) {
		$this->payslip = $payslip;		
		$this->arrangeLabels();
	}
	
	public function getLabel($variable) {
		$variable = strtolower($variable);
		return $this->labels[$variable]['label'];	
	}
	
	public function getValue($variable) {
		$variable = strtolower($variable);
		return $this->labels[$variable]['value'];	
	}
	
	public function getSubValue($variable) {
		$variable = strtolower($variable);
		echo $this->payslip['taxable']['value'];
		return $this->labels[$variable];	
	}
	
	public function computeTotalDeductions($deduction_type = '') {		
		$temp_deductions = (is_array($this->payslip->getDeductions())) ? $this->payslip->getDeductions() : array() ;
		$temp_other_deductions = (is_array($this->payslip->getOtherDeductions())) ? $this->payslip->getOtherDeductions() : array() ;			
		$deductions 		   = array_merge($temp_deductions, $temp_other_deductions);		
		foreach ($deductions as $d) {
			if (is_object($d)) {
				if (!empty($deduction_type)) {
					if ($d->getDeductionType() == $deduction_type || $deduction_type == '') {
						$array[$d->getLabel()] += $d->getAmount();
					}
				} else {
					$array[$d->getLabel()] += $d->getAmount();
				}
			}						
		}		
		return Tools::numberFormat(array_sum($array));
	}
	
	public function computeTotalEarnings($earning_type = '') {
		$temp_earnings = (is_array($this->payslip->getEarnings())) ? $this->payslip->getEarnings() : array() ;
		$temp_other_earnings = (is_array($this->payslip->getOtherEarnings())) ? $this->payslip->getOtherEarnings() : array() ;
		
		$earnings = array_merge($temp_earnings, $temp_other_earnings);
		
		foreach ($earnings as $er) {
			if (is_object($er)) {
				if (!empty($earning_type)) {
					if ($er->getEarningType() == $earning_type) {
						$array_earnings[$er->getLabel()] = $er->getAmount();
					}
				} else {
					$array_earnings[$er->getLabel()] = $er->getAmount();
				}
			}
		}
		return Tools::numberFormat(array_sum($array_earnings));
	}		
	
	private function arrangeLabels() {
		if (is_object($this->payslip)) {
			$labels = $this->payslip->getLabels();			
			foreach ($labels as $l) {
				if (is_object($l)) {
					$variable = strtolower($l->getVariable());
					$this->labels[$variable]['label'] = $l->getLabel();
					$this->labels[$variable]['value'] = $l->getValue();
				}
			}
				
			$earnings = $this->payslip->getAllEarnings();	
			foreach ($earnings as $e) {
				if (is_object($e)) {
					$variable = strtolower($e->getVariable());
					$this->labels[$variable]['label'] = $e->getLabel();
					$this->labels[$variable]['value'] = $e->getAmount();
				}
			}
			
			$deductions = $this->payslip->getAllDeductions();			
			foreach ($deductions as $d) {
				if (is_object($d)) {
					$variable = strtolower($d->getVariable());
					$this->labels[$variable]['label'] = $d->getLabel();
					$this->labels[$variable]['value'] = $d->getAmount();
				}
			}
		}	
	}

    public static function generatePayslipByEmployeeIdsPeriod($employee_ids, G_Cutoff_Period $period) {
        $payslips = array();
        foreach ($employee_ids as $employee_id) {
            $e = G_Employee_Finder::findById($employee_id);
            if ($e && $period) {
                $pg = new G_Payslip_Generator($period);
                $pg->setEmployee($e);
                $payslips[] = $pg->generate();
            }
        }
        if ($pg) {
            return $pg->save($payslips);
        }
    }

    public static function sqlGetPreviousEmployeePayslipDetailsByEmployeeId($employee_id = 0, $date_start = '', $date_end = '', $fields){
    	$row = array();

    	if( !empty($fields) ){
    		$sql_fields = implode(",", $fields);
    	}else{
    		$sql_fields = " * ";
    	}

    	$c = new G_Cutoff_Period();
		$previous_cutoff = $c->getPreviousCutOffByDate($date_start);

		if( $previous_cutoff['start_date'] != '' && $previous_cutoff['end_date'] != '' ){
			$period_start = $previous_cutoff['start_date'];
			$period_end   = $previous_cutoff['end_date'];

	    	$sql = "
				SELECT {$sql_fields}
				FROM g_employee_payslip
				WHERE employee_id = ". Model::safeSql($employee_id) ."
				AND period_start =" . Model::safeSql($period_start) . " AND period_end =" . Model::safeSql($period_end) . "
				ORDER BY id DESC
				LIMIT 1			
			";

			$result = Model::runSql($sql);
			$row = Model::fetchAssoc($result);			
		}    	

		return $row;
    }

    public function sqlEmployeesSSSContributionByEmployeeIdsAndByDateRange($employee_ids = '', $date_from = '', $date_to = ''){
    	$sql = "
			SELECT CONCAT(e.lastname, ', ', e.firstname)AS employee_name, CONCAT(p.period_start, ' to ', p.period_end)AS cutoff_period, COALESCE(FORMAT(p.sss,2),0)AS sss_contribution
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
			WHERE p.employee_id IN({$employee_ids})
				AND p.period_start BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . "
			ORDER BY CONCAT(e.lastname, ', ', e.firstname), CONCAT(p.period_start, ' to ', p.period_end) ASC
		";		
		
		$result = Model::runSql($sql,true);		
		return $result;	
    }

    public function sqlEmployeePayslipDataByEmployeeIdAndDateRange($employee_ids = array(), $date_from = '', $date_to = ''){
    	$sql_date_from = date("Y-m-d",strtotime($date_from));
    	$sql_date_to   = date("Y-m-d",strtotime($date_to));
    	$today         = date("Y-m-d");
    	$sql_values = implode(",", $employee_ids);    	
    	$sql = "
			SELECT CONCAT(e.lastname, ', ', e.firstname)AS employee_name, 
				COALESCE(bs.basic_salary,0)AS monthly_salary,
				TIMESTAMPDIFF(MONTH, e.hired_date, '{$today}')AS months_stayed,
				COALESCE(e.number_dependent,0)AS qualified_dependents,
				COALESCE(SUM(p.gross_pay),0)AS gross_amount,
				COALESCE(SUM(p.overtime),0)AS overtime_amount,
				COALESCE(SUM(p.month_13th),0)AS total_13th_month,
				COALESCE(SUM(p.taxable),0)AS taxable_allowance_amount,
				COALESCE(SUM(p.sss),0)AS sss_amount,
				COALESCE(SUM(p.philhealth),0)AS philhealth_amount,
				COALESCE(SUM(p.pagibig),0)AS pagibig_amount,
				COALESCE(SUM(p.non_taxable),0)AS non_taxable_amount,
				COALESCE(SUM(p.withheld_tax),0)AS withheld_tax_amount
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
				LEFT JOIN g_employee_basic_salary_history bs ON p.employee_id = bs.employee_id AND bs.end_date = ''
			WHERE p.employee_id IN({$sql_values})
				AND p.period_start BETWEEN " . Model::safeSql($sql_date_from) . " AND " . Model::safeSql($sql_date_to) . "
			ORDER BY CONCAT(e.lastname, ', ', e.firstname) ASC
		";		
		//echo $sql;
		$result = Model::runSql($sql,true);		
		return $result;	
    }

    public function sqlIndividualEmployeePayslipDataByEmployeeIdAndDateRange($employee_id, $date_from = '', $date_to = ''){
    	$sql_date_from = date("Y-m-d",strtotime($date_from));
    	$sql_date_to   = date("Y-m-d",strtotime($date_to));

    	$sql = "
			SELECT CONCAT(e.lastname, ', ', e.firstname)AS employee_name, 
				COALESCE(bs.basic_salary,0)AS monthly_salary,
				COALESCE(e.number_dependent,0)AS qualified_dependents,
				COALESCE(SUM(p.gross_pay),0)AS gross_amount,
				COALESCE(SUM(p.overtime),0)AS overtime_amount,
				COALESCE(SUM(p.month_13th),0)AS total_13th_month,
				COALESCE(SUM(p.taxable),0)AS taxable_allowance_amount,
				COALESCE(SUM(p.sss),0)AS sss_amount,
				COALESCE(SUM(p.philhealth),0)AS philhealth_amount,
				COALESCE(SUM(p.pagibig),0)AS pagibig_amount,
				COALESCE(SUM(p.non_taxable),0)AS non_taxable_amount,
				COALESCE(SUM(p.withheld_tax),0)AS withheld_tax_amount
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
				LEFT JOIN g_employee_basic_salary_history bs ON p.employee_id = bs.employee_id AND bs.end_date = ''
			WHERE p.employee_id = " . $employee_id . "
				AND p.period_start BETWEEN " . Model::safeSql($sql_date_from) . " AND " . Model::safeSql($sql_date_to) . " 
				LIMIT 1
		";		
		
		$result = Model::runSql($sql,true);		
		$result_data = array();
		foreach($result as $rkey => $rdata) {
			foreach($rdata as $rrkey => $rrdata) {
				$result_data[$rrkey] = $rrdata;
			}

		}

		return $result_data;	
    }    

    public function sqlEmployeesTaxContributionByEmployeeIdsAndByDateRange($employee_ids = '', $date_from = '', $date_to = ''){
    	$sql = "
			SELECT CONCAT(e.lastname, ', ', e.firstname)AS employee_name, CONCAT(p.period_start, ' to ', p.period_end)AS cutoff_period, COALESCE(FORMAT(p.withheld_tax,2),0)AS tax_contribution
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
			WHERE p.employee_id IN({$employee_ids})
				AND p.period_start BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . "
			ORDER BY CONCAT(e.lastname, ', ', e.firstname), CONCAT(p.period_start, ' to ', p.period_end) ASC
		";		
		
		$result = Model::runSql($sql,true);		
		return $result;	
    }

    public function sqlEmployeesPhilhealthContributionByEmployeeIdsAndByDateRange($employee_ids = '', $date_from = '', $date_to = ''){
    	$sql = "
			SELECT CONCAT(e.lastname, ', ', e.firstname)AS employee_name, CONCAT(p.period_start, ' to ', p.period_end)AS cutoff_period, COALESCE(FORMAT(p.philhealth,2),0)AS philhealth_contribution
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
			WHERE p.employee_id IN({$employee_ids})
				AND p.period_start BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . "
			ORDER BY CONCAT(e.lastname, ', ', e.firstname), CONCAT(p.period_start, ' to ', p.period_end) ASC
		";		
		
		$result = Model::runSql($sql,true);		
		return $result;	
    }

    public function sqlEmployeesPagibigContributionByEmployeeIdsAndByDateRange($employee_ids = '', $date_from = '', $date_to = ''){
    	$sql = "
			SELECT CONCAT(e.lastname, ', ', e.firstname)AS employee_name, CONCAT(p.period_start, ' to ', p.period_end)AS cutoff_period, COALESCE(FORMAT(p.pagibig,2),0)AS pagibig_contribution
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
			WHERE p.employee_id IN({$employee_ids})
				AND p.period_start BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . "
			ORDER BY CONCAT(e.lastname, ', ', e.firstname), CONCAT(p.period_start, ' to ', p.period_end) ASC
		";		
		
		$result = Model::runSql($sql,true);		
		return $result;	
    }

    public function sqlAllEmployeesSSSContributionByDateRange($date_from = '', $date_to = '', $add_query){
    	$sql_add_query = '';
    	if( $add_query != '' ){
    		$sql_add_query = $add_query;
    	}

    	$sql = "
			SELECT e.lastname, e.firstname, CONCAT(p.period_start, ' to ', p.period_end)AS cutoff_period, p.sss AS sss_contribution, e.sss_number, es.status,
				sss.company_share, sss.company_ec, e.employee_code,
				(SELECT csA.title FROM `g_company_structure` csA WHERE csA.id = e.section_id ORDER BY csA.id DESC LIMIT 1 )AS section_name,                 
				COALESCE(esh.name,(
                    SELECT name FROM `g_employee_subdivision_history`
                    WHERE employee_id = p.employee_id 
                        AND end_date <> ''
                    ORDER BY end_date DESC
                    LIMIT 1
                ))AS department_name
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
				LEFT JOIN g_settings_employment_status es ON e.employment_status_id = es.id
				LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id AND esh.end_date = ''
				LEFT JOIN p_sss sss ON p.sss = sss.employee_share
			WHERE p.period_start BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . "
				{$sql_add_query}
			ORDER BY CONCAT(e.lastname, ', ', e.firstname), CONCAT(p.period_start, ' to ', p.period_end) ASC
		";		
				
		$result = Model::runSql($sql,true);		
		return $result;	
    }

    public function sqlAllEmployeesYearlyBonusByDateRange($date_from = '', $date_to = '', $add_query){
    	$sql_add_query = '';
    	if( $add_query != '' ){
    		$sql_add_query = $add_query;
    	}

    	$sql = "
			SELECT e.employee_code, e.lastname, e.firstname, CONCAT(p.period_start, ' to ', p.period_end)AS cutoff_period, es.status,
				SUM(p.month_13th)AS total_yearly_bonus,
				COALESCE(ejh.name,(
	                SELECT name FROM `g_employee_job_history`
	                WHERE employee_id = e.id 
	                    AND end_date <> ''
	                ORDER BY end_date DESC 
	                LIMIT 1
                ))AS position,
				(SELECT csA.title FROM `g_company_structure` csA WHERE csA.id = e.section_id ORDER BY csA.id DESC LIMIT 1 )AS section_name,                 
				COALESCE(esh.name,(
                    SELECT name FROM `g_employee_subdivision_history`
                    WHERE employee_id = p.employee_id 
                        AND end_date <> ''
                    ORDER BY end_date DESC
                    LIMIT 1
                ))AS department_name
			FROM g_employee_payslip p
				INNER JOIN g_employee e ON p.employee_id = e.id
				INNER JOIN g_settings_employment_status es ON e.employment_status_id = es.id				
				INNER JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id AND esh.end_date = ''
				INNER JOIN " . G_EMPLOYEE_JOB_HISTORY . " ejh ON esh.employee_id = ejh.employee_id AND ejh.end_date = ''
				INNER JOIN p_sss sss ON p.sss = sss.employee_share
			WHERE p.period_start BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . "
				{$sql_add_query}			
			GROUP BY  p.employee_id
			ORDER BY CONCAT(e.lastname, ', ', e.firstname), CONCAT(p.period_start, ' to ', p.period_end) ASC			
		";		
		
		$result = Model::runSql($sql,true);		
		return $result;	
    }

    public function sqlAllEmployeesPhilhealthContributionByDateRange($date_from = '', $date_to = '', $add_query = ''){
    	$sql_add_query = '';
    	if( $add_query != '' ){
    		$sql_add_query = $add_query;
    	}

    	$sql = "
			SELECT e.lastname, e.firstname, e.middlename, e.birthdate, CONCAT(p.period_start, ' to ', p.period_end)AS cutoff_period, p.philhealth AS philhealth_contribution, e.philhealth_number,es.status,
				ph.company_share, e.employee_code,
				(SELECT csA.title FROM `g_company_structure` csA WHERE csA.id = e.section_id ORDER BY csA.id DESC LIMIT 1 )AS section_name,                 
                COALESCE(esh.name,(
                    SELECT name FROM `g_employee_subdivision_history`
                    WHERE employee_id = p.employee_id 
                        AND end_date <> ''
                    ORDER BY end_date DESC
                    LIMIT 1
                ))AS department_name
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id	
				LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id AND esh.end_date = ''
				LEFT JOIN g_settings_employment_status es ON e.employment_status_id = es.id			
				LEFT JOIN p_philhealth ph ON p.philhealth = ph.employee_share
			WHERE p.period_start BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . "
				{$sql_add_query}
			ORDER BY CONCAT(e.lastname, ', ', e.firstname), CONCAT(p.period_start, ' to ', p.period_end) ASC
		";		
		
		$result = Model::runSql($sql,true);		
		return $result;	
    }

    public function sqlAllEmployeesPagibigContributionByDateRange($date_from = '', $date_to = '', $add_query = ''){
    	$sql_add_query = '';
    	if( $add_query != '' ){
    		$sql_add_query = $add_query;
    	}

    	$sql = "
			SELECT e.lastname, e.firstname, e.middlename, e.extension_name, p.period_start, p.period_end, p.pagibig AS pagibig_contribution, p.labels, e.pagibig_number,es.status,
			(SELECT csA.title FROM `g_company_structure` csA WHERE csA.id = e.section_id ORDER BY csA.id DESC LIMIT 1 )AS section_name, e.employee_code,
                COALESCE(esh.name,(
                    SELECT name FROM `g_employee_subdivision_history`
                    WHERE employee_id = p.employee_id 
                        AND end_date <> ''
                    ORDER BY end_date DESC
                    LIMIT 1
                ))AS department_name
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
				LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id AND esh.end_date = ''
				LEFT JOIN g_settings_employment_status es ON e.employment_status_id = es.id								
			WHERE p.period_start BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . "
				{$sql_add_query}
			ORDER BY CONCAT(e.lastname, ', ', e.firstname), CONCAT(p.period_start, ' to ', p.period_end) ASC
		";		
		
		$result = Model::runSql($sql,true);		
		return $result;	
    }

    public static function sqlProcessedPayslipByDateRange($date_from = '', $date_to = '', $fields = array()) {

		$date_from = date('Y-m-d',strtotime($date_from));
		$date_to   = date('Y-m-d',strtotime($date_to));

		if(!empty($fields)){
			$sql_fields = implode(",",$fields);
		}else{
			$sql_fields = " * ";
		}

		$sql = "
			SELECT {$sql_fields}
			FROM " . G_EMPLOYEE_PAYSLIP ." p					
			WHERE p.period_start =" . Model::safeSql($date_from) . " 
				AND p.period_end =" . Model::safeSql($date_to) . "
			ORDER BY p.id ASC 
		";
		
		$result = Model::runSql($sql,true);		
		return $result;	
	}

	public static function sqlGetAllPayslipDataByYear($year = '', $employee_ids = array(), $fields = array(), $group_by = '') {

		$date_from = date('Y-m-d',strtotime($date_from));
		$date_to   = date('Y-m-d',strtotime($date_to));

		if(!empty($fields)){
			$sql_fields = implode(",",$fields);
		}else{
			$sql_fields = " * ";
		}

		$sql_add_query = "";
		if( !empty($employee_ids) ){
			$string_employee_ids = implode(",", $employee_ids);
			$sql_add_query = "AND p.employee_id IN({$string_employee_ids})";
		}

		$sql = "
			SELECT {$sql_fields}
			FROM " . G_EMPLOYEE_PAYSLIP ." p					
			WHERE DATE_FORMAT(p.period_end,'%Y') =" . Model::safeSql($year) . " 
			{$sql_add_query}			
			{$group_by}				
			ORDER BY id ASC 			
		";
		
		$result = Model::runSql($sql,true);		
		return $result;	
	}

	//Custom
	public static function sqlGetEmployeesPayslipDataByYearAndDateRangeNotIncludedConfiEmployee($year = '', $employee_ids = array(), $range = array(), $fields = array(), $group_by = '') {

		$date_from = date('Y-m-d',strtotime($range['from']));
		$date_to   = date('Y-m-d',strtotime($range['to']));

		if(!empty($fields)){
			$sql_fields = implode(",",$fields);
		}else{
			$sql_fields = " * ";
		}
		
		$sql_add_query = "";
		if( !empty($employee_ids) ){
			$string_employee_ids = implode(",", $employee_ids);
			$sql_add_query = " AND p.employee_id IN({$string_employee_ids})";
		}

		if( !empty($range) ){
			$start_date = date("Y-m-d",strtotime($range['from']));
			$end_date   = date("Y-m-d",strtotime($range['to']));
			$sql_add_query .= " AND p.period_end BETWEEN " . Model::safeSql($start_date) . " AND " . Model::safeSql($end_date);
		}

		$employee_arr = array(20, 94, 3, 170, 69, 45, 324, 24, 14, 13, 171, 31, 29, 12, 5);
		$included_employee = '"' . implode('","', $employee_arr) . '"';
		$sql_add_query .= "AND employee_id NOT IN ($included_employee)";

		$sql = "
			SELECT {$sql_fields}
			FROM " . G_EMPLOYEE_PAYSLIP ." p					
			WHERE DATE_FORMAT(p.period_end,'%Y') =" . Model::safeSql($year) . " 
			{$sql_add_query}			
			{$group_by}				
			ORDER BY id ASC 			
		";

		//echo $sql;
		
		$result = Model::runSql($sql,true);		

		$result_with_key = array(); 

		foreach($result as $rdata) {
			$result_with_key[$rdata['employee_id']] = $rdata;
		}

		return $result_with_key;	

	}

	public static function sqlGetEmployeesPayslipDataByYearAndDateRangeConfiEmployeeAndRemoveJanuary($year = '', $employee_ids = array(), $range = array(), $fields = array(), $group_by = '') {

		$date_from = date('Y-m-d',strtotime($range['from']));
		$date_to   = date('Y-m-d',strtotime($range['to']));

		if(!empty($fields)){
			$sql_fields = implode(",",$fields);
		}else{
			$sql_fields = " * ";
		}
		
		$sql_add_query = "";
		if( !empty($employee_ids) ){
			$string_employee_ids = implode(",", $employee_ids);
			$sql_add_query = " AND p.employee_id IN({$string_employee_ids})";
		}

		if( !empty($range) ){
			$start_date = date("Y-m-d",strtotime($range['from']));
			$end_date   = date("Y-m-d",strtotime($range['to']));
			$sql_add_query .= " AND p.period_end BETWEEN " . Model::safeSql('2016-02-01') . " AND " . Model::safeSql($end_date);
		}

		$employee_arr = array(20, 94, 3, 170, 69, 45, 324, 24, 14, 13, 171, 31, 29, 12, 5);
		$included_employee = '"' . implode('","', $employee_arr) . '"';
		$sql_add_query .= "AND employee_id IN ($included_employee)";

		$sql = "
			SELECT {$sql_fields}
			FROM " . G_EMPLOYEE_PAYSLIP ." p					
			WHERE DATE_FORMAT(p.period_end,'%Y') =" . Model::safeSql($year) . " 
			{$sql_add_query}			
			{$group_by}				
			ORDER BY id ASC 			
		";

		//echo $sql;
		
		$result = Model::runSql($sql,true);			

		$result_with_key = array(); 

		foreach($result as $rdata) {
			$result_with_key[$rdata['employee_id']] = $rdata;
		}
				
		return $result_with_key;	

	}

	public static function sqlGetEmployeesPayslipOtherDeduction($eid, $range, $year = '') {
		$date_from = date('Y-m-d',strtotime($range['from']));
		$date_to   = date('Y-m-d',strtotime($range['to']));

		$sql_fields = " p.other_deductions ";

		$sql_add_query = "";

		if( !empty($range) ){
			$start_date = date("Y-m-d",strtotime($range['from']));
			$end_date   = date("Y-m-d",strtotime($range['to']));
			$sql_add_query .= " AND p.period_end BETWEEN " . Model::safeSql($start_date) . " AND " . Model::safeSql($end_date);
			$sql_add_query .= " AND p.employee_id = " . Model::safeSql($eid);
		}

		$sql = "
			SELECT {$sql_fields}
			FROM " . G_EMPLOYEE_PAYSLIP ." p					
			WHERE DATE_FORMAT(p.period_end,'%Y') =" . Model::safeSql($year) . " 
			{$sql_add_query}			
			{$group_by}				
			ORDER BY id ASC 			
		";
		
		$result = Model::runSql($sql,true);		
		return $result;			
	}

	public static function sqlGetEmployeesPayslipDataByYearAndDateRange($year = '', $employee_ids = array(), $range = array(), $fields = array(), $group_by = '') {

		$date_from = date('Y-m-d',strtotime($range['from']));
		$date_to   = date('Y-m-d',strtotime($range['to']));

		if(!empty($fields)){
			$sql_fields = implode(",",$fields);
		}else{
			$sql_fields = " * ";
		}

		$sql_add_query = "";
		if( !empty($employee_ids) ){
			$string_employee_ids = implode(",", $employee_ids);
			$sql_add_query = " AND p.employee_id IN({$string_employee_ids})";
		}

		if( !empty($range) ){
			$start_date = date("Y-m-d",strtotime($range['from']));
			$end_date   = date("Y-m-d",strtotime($range['to']));
			$sql_add_query .= " AND p.period_end BETWEEN " . Model::safeSql($start_date) . " AND " . Model::safeSql($end_date);
		}

		$sql = "
			SELECT {$sql_fields}
			FROM " . G_EMPLOYEE_PAYSLIP ." p					
			WHERE DATE_FORMAT(p.period_end,'%Y') =" . Model::safeSql($year) . " 
			{$sql_add_query}			
			{$group_by}				
			ORDER BY id ASC 			
		";

		//echo $sql;
		
		$result = Model::runSql($sql,true);		
		return $result;	
	}

    public function sqlAllEmployeesTaxContributionByDateRange($date_from = '', $date_to = ''){
    	$sql = "
			SELECT CONCAT(e.lastname, ', ', e.firstname)AS employee_name, CONCAT(p.period_start, ' to ', p.period_end)AS cutoff_period, COALESCE(FORMAT(p.withheld_tax,2),0)AS tax_contribution
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
			WHERE p.period_start BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . "
			ORDER BY CONCAT(e.lastname, ', ', e.firstname), CONCAT(p.period_start, ' to ', p.period_end) ASC
		";		
		
		$result = Model::runSql($sql,true);		
		return $result;	
    }

    public function sqlAllEmployeesPhilhealthContributionByDateRange_depre($date_from = '', $date_to = ''){
    	$sql = "
			SELECT CONCAT(e.lastname, ', ', e.firstname)AS employee_name, CONCAT(p.period_start, ' to ', p.period_end)AS cutoff_period, COALESCE(FORMAT(p.philhealth,2),0)AS philhealth_contribution
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
			WHERE p.period_start BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . "
			ORDER BY CONCAT(e.lastname, ', ', e.firstname), CONCAT(p.period_start, ' to ', p.period_end) ASC
		";		
		
		$result = Model::runSql($sql,true);		
		return $result;	
    }

    public function sqlAllEmployeesPagibigContributionByDateRange_depre($date_from = '', $date_to = ''){
    	$sql = "
			SELECT CONCAT(e.lastname, ', ', e.firstname)AS employee_name, CONCAT(p.period_start, ' to ', p.period_end)AS cutoff_period, COALESCE(FORMAT(p.pagibig,2),0)AS pagibig_contribution
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
			WHERE p.period_start BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . "
			ORDER BY CONCAT(e.lastname, ', ', e.firstname), CONCAT(p.period_start, ' to ', p.period_end) ASC
		";		
		
		$result = Model::runSql($sql,true);		
		return $result;	
    }

    public function sqlEmployeesSSSContributionByDepartmentIdsAndByDateRange($department_ids = '', $date_from = '', $date_to = ''){
    	$sql = "
			SELECT CONCAT(e.lastname, ', ', e.firstname)AS employee_name, CONCAT(p.period_start, ' to ', p.period_end)AS cutoff_period, COALESCE(FORMAT(p.sss,2),0)AS sss_contribution
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
			WHERE e.department_company_structure_id IN({$department_ids})
				AND p.period_start BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . "
			ORDER BY CONCAT(e.lastname, ', ', e.firstname), CONCAT(p.period_start, ' to ', p.period_end) ASC
		";		
		
		$result = Model::runSql($sql,true);		
		return $result;	
    }

    public function sqlEmployeesTaxContributionByDepartmentIdsAndByDateRange($department_ids = '', $date_from = '', $date_to = ''){
    	$sql = "
			SELECT CONCAT(e.lastname, ', ', e.firstname)AS employee_name, CONCAT(p.period_start, ' to ', p.period_end)AS cutoff_period, COALESCE(FORMAT(p.withheld_tax,2),0)AS tax_contribution
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
			WHERE e.department_company_structure_id IN({$department_ids})
				AND p.period_start BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . "
			ORDER BY CONCAT(e.lastname, ', ', e.firstname), CONCAT(p.period_start, ' to ', p.period_end) ASC
		";		
		
		$result = Model::runSql($sql,true);		
		return $result;	
    }

    public function sqlEmployeesPhilhealthContributionByDepartmentIdsAndByDateRange($department_ids = '', $date_from = '', $date_to = ''){
    	$sql = "
			SELECT CONCAT(e.lastname, ', ', e.firstname)AS employee_name, CONCAT(p.period_start, ' to ', p.period_end)AS cutoff_period, COALESCE(FORMAT(p.philhealth,2),0)AS philhealth_contribution
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
			WHERE e.department_company_structure_id IN({$department_ids})
				AND p.period_start BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . "
			ORDER BY CONCAT(e.lastname, ', ', e.firstname), CONCAT(p.period_start, ' to ', p.period_end) ASC
		";		
		
		$result = Model::runSql($sql,true);		
		return $result;	
    }

    public function sqlEmployeesPagibigContributionByDepartmentIdsAndByDateRange($department_ids = '', $date_from = '', $date_to = ''){
    	$sql = "
			SELECT CONCAT(e.lastname, ', ', e.firstname)AS employee_name, CONCAT(p.period_start, ' to ', p.period_end)AS cutoff_period, COALESCE(FORMAT(p.pagibig,2),0)AS pagibig_contribution
			FROM g_employee_payslip p
				LEFT JOIN g_employee e ON p.employee_id = e.id
			WHERE e.department_company_structure_id IN({$department_ids})
				AND p.period_start BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . "
			ORDER BY CONCAT(e.lastname, ', ', e.firstname), CONCAT(p.period_start, ' to ', p.period_end) ASC
		";		
		
		$result = Model::runSql($sql,true);		
		return $result;	
    }
    
	public static function countByEmployeeAndPeriod(IEmployee $employee, $start_date, $end_date) {
		$sql = "
			SELECT count(*) as total
			FROM g_employee_payslip
			WHERE employee_id = ". Model::safeSql($employee->getId()) ."
			AND (period_start = ". Model::safeSql($start_date) ." AND period_end = ". Model::safeSql($end_date) .")
			LIMIT 1			
		";
		$result = Model::runSql($sql);
		$row = Model::fetchAssoc($result);
		return $row['total'];
	}
	
	public static function countPeriod($start_date, $end_date) {
		$sql = "
			SELECT count(*) as total
			FROM g_employee_payslip
			WHERE (period_start = ". Model::safeSql($start_date) ." AND period_end = ". Model::safeSql($end_date) .")
			LIMIT 1		
		";
		$result = Model::runSql($sql);
		$row = Model::fetchAssoc($result);
		return $row['total'];	
	}
	
	public static function getExistingPeriod() {
		$sql = "
			SELECT p.period_start, p.period_end
			FROM g_employee_payslip p
			GROUP BY p.period_start, p.period_end
			ORDER BY p.period_start DESC
		";
		$result = Model::runSql($sql);
		$counter = 0;
		while ($row = Model::fetchAssoc($result)) {
			$return[$counter]['start'] = $row['period_start'];
			$return[$counter]['end'] = $row['period_end'];
			$counter++;
		}
		return $return;
	}
	
	public static function getPeriods() {
		$sql = "
			SELECT p.id, p.period_start, p.period_end, p.payout_date, is_lock
			FROM g_cutoff_period p
			GROUP BY p.period_start, p.period_end
			ORDER BY p.period_start DESC
		";
		$result = Model::runSql($sql);
		$counter = 0;
		while ($row = Model::fetchAssoc($result)) {
			$return[$counter]['id']      = $row['id'];
			$return[$counter]['start']   = $row['period_start'];
			$return[$counter]['end']     = $row['period_end'];
			$return[$counter]['is_lock'] = $row['is_lock'];
			$counter++;
		}
		return $return;
	}
	
	public static function getPayrollPeriodsByYearTag($year_tag) {
		$sql = "
			SELECT p.id, p.period_start, p.period_end, p.payout_date, is_lock
			FROM g_cutoff_period p
			WHERE p.year_tag =" . Model::safeSql($year_tag) . "
			GROUP BY p.period_start, p.period_end
			ORDER BY p.period_start DESC
		";
		$result = Model::runSql($sql);
		$counter = 0;
		while ($row = Model::fetchAssoc($result)) {
			$return[$counter]['id']      = $row['id'];
			$return[$counter]['start']   = $row['period_start'];
			$return[$counter]['end']     = $row['period_end'];
			$return[$counter]['is_lock'] = $row['is_lock'];
			$counter++;
		}
		return $return;
	}
	
	public static function getPeriodPayoutDate($from, $to) {
		$sql = "
			SELECT p.payout_date
			FROM g_cutoff_period p
			WHERE period_start = ". Model::safeSql($from) ."
			AND period_end = ". Model::safeSql($to) ."
			LIMIT 1
		";
		$result = Model::runSql($sql);
		$row = Model::fetchAssoc($result);
		return $row['payout_date'];
	}
	
	public static function updatePayslip(IEmployee $e, $start_date, $end_date) {
		//$p = self::generatePayslip($e, $start_date, $end_date);
		//return $p->save();
        $c = G_Company_Factory::get();
        $period = G_Cutoff_Period_Finder::findByPeriod($start_date, $end_date);
        return $c->generatePayslipByEmployee($e, $start_date, $end_date, $period->getCutoffNumber());
	}
	
	public static function updatePayslipByEmployeeAndDate($e, $date) {
		$c = G_Cutoff_Period_Finder::findByDate($date);
		if ($c) {
			$start_date = $c->getStartDate();	
			$end_date = $c->getEndDate();
			$ps = self::generatePayslip($e, $start_date, $end_date);	
			return $ps->save();			
		}		
	}	
	
	public static function updatePayslipIfExistByEmployeeAndDate($e, $date) {
		$c = G_Cutoff_Period_Finder::findByDate($date);
		if ($c) {
			$start_date = $c->getStartDate();	
			$end_date = $c->getEndDate();			
			$p = G_Payslip_Finder::findByEmployeeAndPeriod($e, $start_date, $end_date);
			if ($p) {
				$ps = self::generatePayslip($e, $start_date, $end_date);	
				return $ps->save();			
			}
		}		
	}
	
	public static function updatePayslipsByPeriod($start_date, $end_date) {
		$c = G_Cutoff_Period_Finder::findByPeriod($start_date, $end_date);				
		$employees = G_Employee_Finder::findAllActiveByDate($start_date);
		foreach ($employees as $id => $e) {
			$p = G_Payslip_Helper::generatePayslip($e, $start_date, $end_date);
			if ($p) {
				if ($c) {
					$payout_date = $c->getPayoutDate();	
					$p->setPayoutDate($payout_date);
				}
				$p->save();
				//Add Earnings
					$ea = G_Employee_Earnings_Helper::addEarningsToPayslip($e,$start_date,$end_date);					
				//
				
				//Add Deductions
					$de = G_Employee_Deductions_Helper::addDeductionsToPayslip($e,$start_date,$end_date);					
				//
				
				//Add Benefits
					$period['start_date'] = $start_date;
					$period['end_date']   = $end_date;	
									
					$be = new G_Employee_Benefit();	
					$be->addToPayslip($e,$period);
				//
				
				//Add Loans
					$loans = G_Employee_Loan_Helper::addLoansToPaySlip($e,$start_date,$end_date);
				//
			}
		}
						
	}	
	
	// DEPRECATED
	public static function generatePayslipsByPeriod($start_date, $end_date) {
		$employees = G_Employee_Finder::findAllActiveByDate($start_date);
		foreach ($employees as $id => $e) {
			$p = G_Payslip_Helper::generatePayslip($e, $start_date, $end_date);
			if ($p) {
				$p->save();
			} else {
				continue;	
			}
		}		
	}

    /*
     * Deprecated - Use G_Company_Factory::generatePayslipByEmployee()
     */
	public static function generatePayslip(IEmployee $e, $start_date, $end_date) {
        $c = G_Company_Factory::get();
        $period = G_Cutoff_Period_Finder::findByPeriod($start_date, $end_date);        
        $p = $c->generatePayslipByEmployee($e, $start_date, $end_date, $period->getCutoffNumber());
        return $p;
	}
	
 	public static function getAllPayslipsByPeriodGroupByEmployee($from, $to) {		
		$sql = "
			SELECT e.id, p.period_start, p.period_end, p.payout_date, p.basic_pay, p.gross_pay, p.net_pay, p.earnings, p.other_earnings, p.deductions, p.other_deductions, p.labels,
					p.taxable, p.withheld_tax, p.month_13th, p.sss, p.pagibig, p.philhealth, p.total_earnings, p.total_deductions
			FROM
			 	(
					SELECT p2.id, p2.employee_id, p2.period_start, p2.period_end, p2.payout_date, p2.basic_pay, p2.gross_pay, p2.net_pay, p2.earnings, p2.other_earnings, p2.deductions, p2.other_deductions, p2.labels,
							p2.taxable, p2.withheld_tax, p2.month_13th, p2.sss, p2.pagibig, p2.philhealth, p2.total_earnings, p2.total_deductions
					FROM ". G_EMPLOYEE_PAYSLIP ." p2
					WHERE 
					( p2.period_start = ". Model::safeSql($from) ." AND p2.period_end = ". Model::safeSql($to) .")
					GROUP BY p2.employee_id
				) AS p
			INNER JOIN ". EMPLOYEE." e ON e.id = p.employee_id						
			ORDER BY e.lastname
		";		
		$result = Model::runSql($sql);
		while ($row = Model::fetchAssoc($result)) {
			$records[$row['id']] = $row;
		}	
		return $records;
	}
    /*
     * @param array $a Array instance of G_Attendance
     */
    public static function computeLegalAmountDepre($attendance, $rate, $daily_amount, $hourly_amount) {                
        $amount             = 0;
        $omit_100 = 100; // if legal, additional 200% pay to employee. we will compute it from 100% because the other 100% is in the regular pay already
        $holiday_legal_rate = $rate->getHolidayLegal() - $omit_100;
        
        foreach ($attendance as $a) {
            $employee_id = $a->getEmployeeId();
            $date        = $a->getDate();
            $e = new G_Employee();
            $e->setId($employee_id);
            $salary = $e->getSalary($date);
            
            if( !empty($salary) && $salary->getType() == 'Monthly' ){                
                $holiday_legal_divisor = $holiday_legal_rate;
            }else{
                $holiday_legal_divisor = 100;
            }
            
            if ($a->isPresent() && $a->isHoliday() && !$a->isRestday()) {
                $h = $a->getHoliday();
                if ( !empty($h) && $h->isLegal()) {
                    $t = $a->getTimesheet();
                    $hours_worked = $t->getTotalHoursWorked();
                   /* if ($hours_worked >= 8) {
                        $hours_worked = 8;
                    }*/
                    if ($a->isOfficialBusiness()) {
                        $hours_worked = 8;
                    }                    
                    $hours_worked = Tools::numberFormat($hours_worked, 2);
                    $temp_amount = (float) Tools::numberFormat($hours_worked * $hourly_amount * ($holiday_legal_rate/$holiday_legal_divisor), 2);
                    $amount = $amount + $temp_amount;
                }
            }
        }
        return $amount;
    }

    public static function computeLegalAmount($attendance, $rate, $daily_amount, $hourly_amount, $custom_ot = array()) {                
        $amount             = 0;
        $omit_100 = 100; // if legal, additional 200% pay to employee. we will compute it from 100% because the other 100% is in the regular pay already
        $holiday_legal_rate = $rate->getHolidayLegal() - $omit_100;
        $total_hrs_worked   = 0;
        $new_amount         = 0;        
        $deductible_breaktime = 0;
        //$hourly_amount * ($rate->getHolidayLegal() / 100) * $num_hrs;
        foreach ($attendance as $a) {
        	$is_with_custom_ot = false;
            $employee_id = $a->getEmployeeId();
            $date        = $a->getDate();
            $t 			 = $a->getTimesheet(); 
            $e = new G_Employee();
            $e->setId($employee_id);
            $salary = $e->getSalary($date);

            //Custom OT
            if( !empty($custom_ot) && isset($custom_ot[$a->getDate()]) && $custom_ot[$a->getDate()]['day_type'] == G_Custom_Overtime::DAY_TYPE_HOLIDAY && $t->getTotalHoursWorked() > 0 && Tools::isValidTime($custom_ot[$a->getDate()]['start_time']) && Tools::isValidTime($custom_ot[$a->getDate()]['end_time']) ){
            	$timestamp_start = $custom_ot[$a->getDate()]['start_time'];
            	$timestamp_end   = $custom_ot[$a->getDate()]['end_time'];

	            $emp = G_Employee_Finder::findById($a->getEmployeeId());
	            $day_type[]               = "applied_to_legal_holiday";
	            $schedule['schedule_in']  = $timestamp_start; // $t->getScheduledDateIn();
	            $schedule['schedule_out'] = $timestamp_end; // $t->getScheduledDateOut();
	            $schedule['actual_in']    = $t->getTimeIn();
	            $schedule['actual_out']   = $t->getTimeOut();   
	            $deductible_breaktime     = $emp->getTotalBreakTimeHrsDeductible($schedule, $day_type);  

            	if( $timestamp_start > $timestamp_end ){
            		$date_start = $a->getDate() . " " . $custom_ot[$a->getDate()]['start_time'];
            		//$date_end   = date("Y-m-d",strtotime("+1 day", $a->getDate())) . " " . $custom_ot[$a->getDate()]['end_time'];

            		$date = new DateTime($a->getDate());
                    $date->modify('+1 day');
                    $date_end = $date->format('Y-m-d');
                    $date_end = $date_end . " " . $custom_ot[$a->getDate()]['end_time'];
            	}else{
            		$date_start = $a->getDate() . " " . $custom_ot[$a->getDate()]['start_time'];
            		$date_end   = $a->getDate() . " " . $custom_ot[$a->getDate()]['end_time'];
            	}
            	$custom_ot_hours_worked = Tools::computeHoursDifferenceByDateTime($date_start, $date_end) - $deductible_breaktime;
            	$is_with_custom_ot = true;   
            }
            
            if( !empty($salary) && $salary->getType() == 'Monthly' ){                
                $holiday_legal_divisor = $holiday_legal_rate;
            }else{
                $holiday_legal_divisor = 100;
            }

            if( $a->isHoliday() && !$a->isOfficialBusiness() ){
            	$h = $a->getHoliday();
            	if( $h && $h->isLegal() ){            		
            		$employee_id   = $a->getEmployeeId();
                    $previous_date = date("Y-m-d",strtotime("-1 days",strtotime($date)));                    
                    $fields        = array("ea.is_present","ea.is_paid","ea.is_leave","ea.is_ob");
                    $yesterday	   = G_Attendance_Helper::sqlGetNearestRegularDay($employee_id, $date); //Get previous date attendance 

					$cutoff_period = G_Cutoff_Period_Helper::sqlGetCurrentCutoffPeriod($date);                    
                    if( ($a->isPresent() && $yesterday['is_present']) || ($a->isPresent() && $yesterday['is_leave'] == 1 && $yesterday['is_paid'] == 1) || ($a->isPresent() && $yesterday['is_ob'] == 1 && $yesterday['is_paid'] == 1)  || ($a->isPresent() && $a->isPaid()) ){ //if both previous date and holiday is present or on leave with pay before holiday and present on holiday - double pay						
                    	//$hours_worked = 8;                    	
                    	$hours_worked = $t->getTotalHoursWorked();
                    	if($is_with_custom_ot){
                    		$hours_worked = $custom_ot_hours_worked;
                    	}

                    	$new_amount  += $hourly_amount * (($rate->getHolidayLegal() - $omit_100) / 100) * $hours_worked;    
                    	
                    }elseif((!$a->isPresent() && $yesterday['is_present']) && ($previous_date >= $cutoff_period['period_start']) && ($previous_date <= $cutoff_period['period_end'])) {
                    	
                    	//$hours_worked = 8;
                    	$hours_worked = $t->getTotalScheduleHours();
                    	if($is_with_custom_ot){
                    		$hours_worked = $custom_ot_hours_worked;
                    	}
                    	//$new_amount  += number_format($hourly_amount * (($rate->getHolidayLegal() - $omit_100) / 100) * $hours_worked,2);                        
                    	$new_amount += 0;
                    }else{ //Regular paid if absent before holiday and present on holiday or vice versa                    	                    	
                    
                    	if( $t ){                        	
                    		$required_schedule_total_hrs = $t->getTotalScheduleHours();
                    		$hours_worked = $t->getTotalHoursWorked();
                    		if($is_with_custom_ot){
	                    		$hours_worked = $custom_ot_hours_worked;
	                    	}
                    		/*$hours_worked 				 = $t->totalHrsWorkedBaseOnSchedule();
                    		if( $hours_worked > $required_schedule_total_hrs ){ //Limit total hours worked base on schedule required total hrs work
	                    		$hours_worked = 8;
	                    	}*/
	                    	//$new_amount  += $hourly_amount * $hours_worked;   
	                    	$new_amount += 0;   
                    	}
                    }
            	}
            }          
        }
        return $new_amount;
    }

    public static function computeLegalAmountDoublePay($attendance, $rate, $daily_amount, $hourly_amount, $custom_ot = array()) {                
        $amount             = 0;
        $omit_100 = 100; // if legal, additional 200% pay to employee. we will compute it from 100% because the other 100% is in the regular pay already
        $holiday_legal_rate = $rate->getHolidayLegal();        
        $total_hrs_worked   = 0;
        $new_amount         = 0;
        //$hourly_amount * ($rate->getHolidayLegal() / 100) * $num_hrs;
        foreach ($attendance as $a) {
        	$is_with_custom_ot = false;
            $employee_id = $a->getEmployeeId();
            $date        = $a->getDate();
            $t 			 = $a->getTimesheet();    
            
            $e = new G_Employee();
            $e->setId($employee_id);
            $salary = $e->getSalary($date);

            if( !empty($custom_ot) && isset($custom_ot[$a->getDate()]) && $custom_ot[$a->getDate()]['day_type'] == G_Custom_Overtime::DAY_TYPE_HOLIDAY && $t->getTotalHoursWorked() > 0 && Tools::isValidTime($custom_ot[$a->getDate()]['start_time']) && Tools::isValidTime($custom_ot[$a->getDate()]['end_time']) ){
            	$timestamp_start = $custom_ot[$a->getDate()]['start_time'];
            	$timestamp_end   = $custom_ot[$a->getDate()]['end_time'];
            	if( $timestamp_start > $timestamp_end ){
            		$date_start = $a->getDate() . " " . $custom_ot[$a->getDate()]['start_time'];
            		//$date_end   = date("Y-m-d",strtotime("+1 day", $a->getDate())) . " " . $custom_ot[$a->getDate()]['end_time'];
            		$date = new DateTime($a->getDate());
                    $date->modify('+1 day');
                    $date_end = $date->format('Y-m-d');
                    $date_end = $date_end . " " . $custom_ot[$a->getDate()]['end_time'];
            	}else{
            		$date_start = $a->getDate() . " " . $custom_ot[$a->getDate()]['start_time'];
            		$date_end   = $a->getDate() . " " . $custom_ot[$a->getDate()]['end_time'];
            	}
            	$custom_ot_hours_worked = Tools::computeHoursDifferenceByDateTime($date_start, $date_end);
            	$is_with_custom_ot = true;   
            }
            
            if( !empty($salary) && $salary->getType() == 'Monthly' ){                
                $holiday_legal_divisor = $holiday_legal_rate;
            }else{
                $holiday_legal_divisor = 100;
            }

            if( $a->isHoliday() && !$a->isOfficialBusiness() ){
            	$h = $a->getHoliday();
            	if( $h && $h->isLegal() ){            		
            		$employee_id   = $a->getEmployeeId();
                    $previous_date = date("Y-m-d",strtotime("-1 days",strtotime($date)));                    
                    $fields        = array("ea.is_present","ea.is_paid","ea.is_leave","ea.is_ob");
                    //$yesterday	   = G_Attendance_Helper::sqlEmployeeAttendanceByDate($employee_id, $previous_date); //Get previous date attendance
                    $yesterday	   = G_Attendance_Helper::sqlGetNearestRegularDay($employee_id, $date); //Get previous date attendance                    
                                     
                    //if( ($a->isPresent() && $yesterday['is_present']) || ($a->isPresent() && $yesterday['is_leave'] ==1 && $yesterday['is_paid'] == 1) || ($a->isPresent() && $yesterday['is_ob'] == 1 && $yesterday['is_paid'] == 1) ){ //if both previous date and holiday is present or on leave with pay before holiday and present on holiday - double pay                    	                    	
                    if( ($a->isPresent() && $yesterday['is_present']) || ($a->isPresent() && $yesterday['is_leave'] == 1 && $yesterday['is_paid'] == 1) || ($a->isPresent() && $yesterday['is_ob'] == 1 && $yesterday['is_paid'] == 1)){ //if both previous date and holiday is present or on leave with pay before holiday and present on holiday - double pay
                    	//$hours_worked = 8;                     	                    	
                    	$hours_worked = $t->getTotalScheduleHours();
                    	//$new_amount  += number_format(  $hourly_amount * (($rate->getHolidayLegal()) / 100) * $hours_worked,2); 

                    	if( $is_with_custom_ot ){
                    		$hours_worked = $custom_ot_hours_worked;
                    	}

                    	$new_amount += ($hours_worked * $hourly_amount) * ( $rate->getHolidayLegal() / 100 );
                    }elseif( (!$a->isPresent() && $yesterday['is_present']) || (!$a->isPresent() && $yesterday['is_leave'] == 1 && $yesterday['is_paid'] == 1) || ($a->isPresent() && $yesterday['is_ob'] == 1 && $yesterday['is_paid'] == 1) || $yesterday['is_restday'] ){
                    	//$hours_worked = $t->getTotalScheduleHours();                    	
                    	$hours_worked = 8;
                    	if( $is_with_custom_ot ){
                    		$hours_worked = $custom_ot_hours_worked;
                    	}
                    	$new_amount  += $hourly_amount * $hours_worked;   
                    }else{ //Regular paid if absent before holiday and present on holiday or vice versa                    	                    	         	
                    	if( $t && $a->isPresent() ){                         	
                    		$required_schedule_total_hrs = $t->getTotalScheduleHours();
                    		$hours_worked = $t->getTotalHoursWorked();
                    		/*$hours_worked 				 = $t->totalHrsWorkedBaseOnSchedule();
                    		if( $hours_worked > $required_schedule_total_hrs ){ //Limit total hours worked base on schedule required total hrs work
	                    		$hours_worked = 8;
	                    	}*/
	                    	if( $is_with_custom_ot ){
	                    		$hours_worked = $custom_ot_hours_worked;
	                    	}
	                    	$new_amount  += $hourly_amount * $hours_worked;   
	                    	//$new_amount += 0;   
                    	}
                    }
            	}
            }
        }
        return $new_amount;
    }

    public static function computeLegalOvertimeAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $amount = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && $a->isHoliday()) {
                $h = $a->getHoliday();
                if (!empty($h) && $h->isLegal()) {
                    $t = $a->getTimesheet();
                    $holiday_legal_ot_hours = $t->getLegalOvertimeHours() + $t->getLegalOvertimeExcessHours();
                    $holiday_legal_rate     = $rate->getHolidayLegal() - 100;
                    $holiday_legal_ot_rate  = $rate->getHolidayLegalOvertime();

                    $temp_amount = (float) Tools::numberFormat(($holiday_legal_ot_hours * ($hourly_amount * ($holiday_legal_rate/100))) * ($holiday_legal_ot_rate/100), 2);
                    $amount = $amount + $temp_amount;
                }
            }
        }    
        return $amount;
    }

    /*
     * @param array $a Array instance of G_Attendance
     */
    public static function computeSpecialAmountDepre($attendance, $rate, $daily_amount, $hourly_amount) {    	
        $amount = 0;

        $omit_100 = 100; // if legal, additional 130% pay to employee. we will compute it from 30% because the other 100% is in the regular pay already
        $holiday_special_rate = $rate->getHolidaySpecial() - $omit_100;
    	
        foreach ($attendance as $a) {
            if ($a->isPresent() && ($a->isHoliday())) {
                $h = $a->getHoliday();
                if ( !empty($h) && $h->isSpecial()) {
                    $t = $a->getTimesheet();
                    $hours_worked = $t->getTotalHoursWorked();

                    if ($a->isOfficialBusiness()) {
                        $hours_worked = 8;
                    }

                    if ($hours_worked >= 8) {
                        $hours_worked = 8;
                    }                    
                    
                    $hours_worked = Tools::numberFormat($hours_worked, 2);
                    $multiplier = ($holiday_special_rate/100);    	
                    $temp_amount = (float) Tools::numberFormat($hours_worked * $hourly_amount * ($holiday_special_rate/100), 2);
                    $amount = $amount + $temp_amount;
                }
            }
        }
        return $amount;
    }

    public static function computeSpecialAmount($attendance, $rate, $daily_amount, $hourly_amount, $custom_ot = array()) {    	
        $amount = 0;

        $omit_100 = 100; // if legal, additional 130% pay to employee. we will compute it from 30% because the other 100% is in the regular pay already
        $holiday_special_rate = $rate->getHolidaySpecial() - $omit_100;
        //$holiday_special_rate = $rate->getHolidaySpecial();
        $new_hourly_rate      = $hourly_amount * ( $rate->getHolidaySpecial() / 100 );                    
        foreach ($attendance as $a) {        	
            //if ( ($a->isPresent() && $a->isHoliday()) && (!$a->isRestday() && !$a->isOfficialBusiness())) {            	
        	if ( ($a->isPresent() && $a->isHoliday()) && !$a->isRestday() ) {          	
                $h = $a->getHoliday();
                $t = $a->getTimesheet();                           
                if ( !empty($h) && $h->isSpecial()) {
                    if( !empty($custom_ot) && isset($custom_ot[$a->getDate()]) && $custom_ot[$a->getDate()]['day_type'] == G_Custom_Overtime::DAY_TYPE_HOLIDAY && $t->getTotalHoursWorked() > 0 ){                    	

                    	$timestamp_start = $custom_ot[$a->getDate()]['start_time'];
                    	$timestamp_end   = $custom_ot[$a->getDate()]['end_time'];
                    	if( $timestamp_start > $timestamp_end ){
                    		$date_start = $a->getDate() . " " . $custom_ot[$a->getDate()]['start_time'];
                    		//$date_end   = date("Y-m-d",strtotime("+1 day " . $a->getDate())) . " " . $custom_ot[$a->getDate()]['end_time'];
                    		$date = new DateTime($a->getDate());
	                        $date->modify('+1 day');
	                        $date_end = $date->format('Y-m-d');
	                        $date_end = $date_end . " " . $custom_ot[$a->getDate()]['end_time'];
                    	}else{
                    		$date_start = $a->getDate() . " " . $custom_ot[$a->getDate()]['start_time'];
                    		$date_end   = $a->getDate() . " " . $custom_ot[$a->getDate()]['end_time'];
                    	}

                    	$ceiling_hours_worked_holiday_special_amount = 0;
                    	$ceiling_hours_worked_holiday_special_amount = ( $t->totalHrsWorkedBaseOnSchedule() / 2 ) + $t->getTotalDeductibleBreaktimeHours();

                    	$hours_worked = Tools::computeHoursDifferenceByDateTime($date_start, $date_end);   

                    	if($hours_worked >= $ceiling_hours_worked_holiday_special_amount) {
                    		$hours_worked = $hours_worked - $t->getTotalDeductibleBreaktimeHours();
                    	} else {
                    		$hours_worked = $hours_worked;
                    	}

                    }else{

                    	$t = $a->getTimesheet();
                    	$hours_worked = $t->getTotalHoursWorked();
                    	if ($a->isOfficialBusiness()) {
	                        $hours_worked = 8;
	                    }
                    }

                    if ($hours_worked >= 8) {
                        $hours_worked = 8;
                    }

                    $hours_worked = Tools::numberFormat($hours_worked, 2);
                    $total_hrs_worked += $hours_worked; 	                    
                }
            }          
        }    

        //echo $total_hrs_worked . 'x' . $hourly_amount . 'x' . '(' . $rate->getHolidaySpecial() . ' / ' .  100 . ')';

        $new_amount = (float) Tools::numberFormat($total_hrs_worked * $hourly_amount * ( $rate->getHolidaySpecial() / 100), 2);         
        return $new_amount;
    }

    public static function computeSpecialAmountNoOmit($attendance, $rate, $daily_amount, $hourly_amount, $custom_ot = array()) {    	
        $amount = 0;

        $omit_100 = 100; // if legal, additional 130% pay to employee. we will compute it from 30% because the other 100% is in the regular pay already
        //$holiday_special_rate = $rate->getHolidaySpecial() - $omit_100;
        $holiday_special_rate = $rate->getHolidaySpecial();
        $new_hourly_rate      = $hourly_amount * ( $rate->getHolidaySpecial() / 100 );        
        foreach ($attendance as $a) {
        	if ($a->isPresent() && ($a->isHoliday()) && !$a->isOfficialBusiness()) {
            //if ($a->isPresent() && ($a->isHoliday()) && !$a->isRestday() && !$a->isOfficialBusiness()) {
                $h = $a->getHoliday();
                $t = $a->getTimesheet();
                if ( !empty($h) && $h->isSpecial()) {                    
                	if( !empty($custom_ot) && isset($custom_ot[$a->getDate()]) && $custom_ot[$a->getDate()]['day_type'] == G_Custom_Overtime::DAY_TYPE_HOLIDAY && $t->getTotalHoursWorked() > 0 && Tools::isValidTime($custom_ot[$a->getDate()]['start_time']) && Tools::isValidTime($custom_ot[$a->getDate()]['end_time']) ){
                    	$timestamp_start = $custom_ot[$a->getDate()]['start_time'];
                    	$timestamp_end   = $custom_ot[$a->getDate()]['end_time'];
                    	if( $timestamp_start > $timestamp_end ){
                    		$date_start = $a->getDate() . " " . $custom_ot[$a->getDate()]['start_time'];
                    		//$date_end   = date("Y-m-d",strtotime("+1 day", $a->getDate())) . " " . $custom_ot[$a->getDate()]['end_time'];
                    		$date = new DateTime($a->getDate());
	                        $date->modify('+1 day');
	                        $date_end = $date->format('Y-m-d');
	                        $date_end = $date_end . " " . $custom_ot[$a->getDate()]['end_time'];
                    	}else{
                    		$date_start = $a->getDate() . " " . $custom_ot[$a->getDate()]['start_time'];
                    		$date_end   = $a->getDate() . " " . $custom_ot[$a->getDate()]['end_time'];
                    	}
                    	$hours_worked = Tools::computeHoursDifferenceByDateTime($date_start, $date_end);   
                    }else{                    	
	                    $hours_worked = $t->getTotalHoursWorked();
	                    if ($hours_worked >= 8) {
	                        $hours_worked = 8;
	                    }
	                    if ($a->isOfficialBusiness()) {
	                        $hours_worked = 8;
	                    }
                    }
                    $hours_worked = Tools::numberFormat($hours_worked, 2);
                    $total_hrs_worked += $hours_worked; 	                    
                }
            }
        }        

        //$new_amount = (float) Tools::numberFormat($total_hrs_worked * $new_hourly_rate * ( $holiday_special_rate / 100), 2);        
        //$new_amount = (float) Tools::numberFormat($total_hrs_worked * ( $holiday_special_rate / 100) * $hourly_amount, 2);
        $new_amount = (float) Tools::numberFormat($total_hrs_worked * $hourly_amount * ( $rate->getHolidaySpecial() / 100), 2);     
        return $new_amount;
    }

    public static function computeRestDaySpecialAmount($attendance, $rate, $daily_amount, $hourly_amount, $custom_ot = array()) {
        $amount = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && $a->isHoliday() && $a->isRestday()) {
                $h = $a->getHoliday();                
                if ( !empty($h) && $h->isSpecial()) {
                    $t = $a->getTimesheet();

                    if($t->getScheduledTimeIn() != '' && $t->getScheduledTimeIn() != '') {
                    
	                    $hours_worked = $t->getTotalHoursWorked();
	                    if ($a->isOfficialBusiness()) {
	                        $hours_worked = 8;
	                    }

	                    if( !empty($custom_ot) && isset($custom_ot[$a->getDate()])  && $t->getTotalHoursWorked() > 0 && Tools::isValidTime($custom_ot[$a->getDate()]['start_time']) && Tools::isValidTime($custom_ot[$a->getDate()]['end_time']) ){

	                        $timestamp_start = $custom_ot[$a->getDate()]['start_time'];
	                        $timestamp_end   = $custom_ot[$a->getDate()]['end_time'];

	                        $e = G_Employee_Finder::findById($a->getEmployeeId());
	                        $day_type[] = "applied_to_restday";

	                        $schedule['schedule_in']  = $timestamp_start;
	                        $schedule['schedule_out'] = $timestamp_end;
	                        $schedule['actual_in']    = $t->getTimeIn();
	                        $schedule['actual_out']   = $t->getTimeOut();   
	                        $deductible_breaktime     = $e->getTotalBreakTimeHrsDeductible($schedule, $day_type);

	                        if( $timestamp_start > $timestamp_end ){
	                            $date_start = $a->getDate() . " " . $custom_ot[$a->getDate()]['start_time'];
	                            $date_end   = date("Y-m-d",strtotime("+1 day", $a->getDate())) . " " . $custom_ot[$a->getDate()]['end_time'];
	                        }else{
	                            $date_start = $a->getDate() . " " . $custom_ot[$a->getDate()]['start_time'];
	                            $date_end   = $a->getDate() . " " . $custom_ot[$a->getDate()]['end_time'];
	                        }
	                        $hours_worked = Tools::computeHoursDifferenceByDateTime($date_start, $date_end) - $deductible_breaktime;
	                        if( $hours_worked >= $t->totalHrsWorkedBaseOnSchedule() ) {
	                            $hours_worked = $t->totalHrsWorkedBaseOnSchedule(); 
	                        }   

	                    }

                    }

                    $holiday_special_rate = $rate->getHolidaySpecialRestday() / 100;
                    $hours_worked = (float) $hours_worked;
                    $hourly_amount = $hourly_amount * $holiday_special_rate;

                    $temp_amount = (float) Tools::numberFormat($hours_worked * $hourly_amount, 2);
                    $amount = $amount + $temp_amount;
                }
            }
        }
        return $amount;
    }

    public static function computeRestDayLegalOvertimeAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $amount = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && $a->isHoliday() && $a->isRestday()) {
                $h = $a->getHoliday();
                if ( !empty($h) && $h->isLegal()) {
                    $t = $a->getTimesheet();
                    $hours_worked = $t->getTotalHoursWorked();

                    /*if ($hours_worked >= 8) {
                        $hours_worked = 8;
                    }*/
                    if ($a->isOfficialBusiness()) {
                        $hours_worked = 8;
                    }

                    $holiday_legal_rate = $rate->getHolidayLegalRestday() / 100;
                    $hours_worked = (float) $hours_worked;
                    $hourly_amount = $hourly_amount * $holiday_legal_rate;

                    $temp_amount = (float) Tools::numberFormat($hours_worked * $hourly_amount, 2);
                    $amount = $amount + $temp_amount;
                }
            }
        }
        return $amount;
    }

    public static function computeSpecialOvertimeAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $amount = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && $a->isHoliday()) {
                $h = $a->getHoliday();
                if ( !empty($h) && $h->isSpecial()) {
                    $t = $a->getTimesheet();
                    $holiday_special_ot_hours = $t->getSpecialOvertimeHours() + $t->getSpecialOvertimeExcessHours();
                    $holiday_special_rate = $rate->getHolidaySpecial();
                    $holiday_special_ot_rate = $rate->getHolidaySpecialOvertime();
                    $holiday_special_ot_hours = Tools::numberFormat($holiday_special_ot_hours, 2);
                    $temp_amount = (float) Tools::numberFormat(($holiday_special_ot_hours * ($hourly_amount * ($holiday_special_rate/100))) * ($holiday_special_ot_rate/100), 2);
                    $amount = $amount + $temp_amount;
                }
            }
        }
        return $amount;
    }

    public static function computeRestDaySpecialOvertimeAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $amount = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && $a->isHoliday() && $a->isRestday()) {
                $h = $a->getHoliday();
                if ( !empty( $h ) && $h->isSpecial()) {

                	$t = $a->getTimesheet();
                	if($t->getScheduledTimeIn() != '' && $t->getScheduledTimeIn() != '') {

	                    $ot_hours = $t->getRestDaySpecialOvertimeHours() + $t->getRestDaySpecialOvertimeExcessHours();
	                    $day_rate = $rate->getHolidaySpecialRestday() / 100;
	                    $ot_rate = $rate->getHolidaySpecialRestdayOvertime() / 100;

	                    $hourly_amount = $hourly_amount * $day_rate * $ot_rate;

	                    $temp_amount = (float) Tools::numberFormat($ot_hours * $hourly_amount, 2);
	                    $amount = $amount + $temp_amount;

                	}

                }
            }
        }
        return $amount;
    }


    public static function computeRestDayLegalAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $amount = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && $a->isHoliday() && $a->isRestday()) {
                $h = $a->getHoliday();
                if ( !empty($h) && $h->isLegal()) {
                    $t = $a->getTimesheet();
                    $hours_worked = $t->getTotalHoursWorked();

                    /*if ($hours_worked >= 8) {
                        $hours_worked = 8;
                    }*/
                    if ($a->isOfficialBusiness()) {
                        $hours_worked = 8;
                    }

                    $holiday_legal_rate = $rate->getHolidayLegalRestday() / 100;
                    $hours_worked = (float) $hours_worked;
                    $hourly_amount = $hourly_amount * $holiday_legal_rate;

                    $temp_amount = (float) Tools::numberFormat($hours_worked * $hourly_amount, 2);
                    $amount = $amount + $temp_amount;
                }
            }
        }
        return $amount;
    }

    public static function computeRestDayLegalOvertimeAmountDepre02($attendance, $rate, $daily_amount, $hourly_amount) {
        $amount = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && $a->isHoliday() && $a->isRestday()) {
                $h = $a->getHoliday();
                if ( !empty($h) && $h->isLegal()) {
                    $t = $a->getTimesheet();
                    $ot_hours = $t->getRestDayLegalOvertimeHours() + $t->getRestDayLegalOvertimeExcessHours();
                    $day_rate = $rate->getHolidayLegalRestday() / 100;
                    $ot_rate = $rate->getHolidayLegalRestdayOvertime() / 100;

                    $hourly_amount = $hourly_amount * $day_rate * $ot_rate;

                    $temp_amount = (float) Tools::numberFormat($ot_hours * $hourly_amount, 2);
                    $amount = $amount + $temp_amount;
                }
            }
        }
        return $amount;
    }

    public static function computeRestDaySpecialOvertimeNightShiftAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $amount = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && $a->isHoliday() && $a->isRestday()) {
                $h = $a->getHoliday();
                if ( !empty($h) && $h->isSpecial()) {

                    $t = $a->getTimesheet();
                    if($t->getScheduledTimeIn() != '' && $t->getScheduledTimeIn() != '') {

						$ot_hours = $t->getRestDaySpecialOvertimeNightShiftHours() + $t->getRestDaySpecialOvertimeNightShiftExcessHours();
	                    $day_rate = $rate->getHolidaySpecialRestday() / 100;
	                    $ot_rate = $rate->getHolidaySpecialRestdayOvertime() / 100;
	                    $ns_rate = ($rate->getNightShiftDiff() - 100) / 100;

	                    $hourly_amount = $hourly_amount * $day_rate * $ot_rate;

	                    $temp_amount = (float) Tools::numberFormat($ot_hours * $hourly_amount * $ns_rate, 2);
	                    $amount = $amount + $temp_amount;

                    }

                }
            }
        }
        return $amount;
    }

    public static function computeRestDayLegalOvertimeNightShiftAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $amount = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && $a->isHoliday() && $a->isRestday()) {
                $h = $a->getHoliday();
                if ( !empty($h) && $h->isLegal()) {
                    $t = $a->getTimesheet();
                    $ot_hours = $t->getRestDayLegalOvertimeNightShiftHours() + $t->getRestDayLegalOvertimeNightShiftExcessHours();

                    $day_rate = $rate->getHolidayLegalRestday() / 100;
                    $ot_rate = $rate->getHolidayLegalRestdayOvertime() / 100;
                    $ns_rate = ($rate->getNightShiftDiff() - 100) / 100;

                    $hourly_amount = $hourly_amount * $day_rate * $ot_rate;

                    $temp_amount = (float) Tools::numberFormat($ot_hours * $hourly_amount * $ns_rate, 2);
                    $amount = $amount + $temp_amount;
                }
            }
        }
        return $amount;
    }

    public static function computeRestDayAmount($attendance, $rate, $daily_amount, $hourly_amount, $custom_ot = array()) {
        $amount = 0;
        foreach ($attendance as $a) {
        	 $t = $a->getTimesheet();
            if ($a->isPresent() && $a->isRestday() && !$a->isHoliday() && !$a->isOfficialBusiness() && ( $t->getScheduledDateIn() != '' && $t->getScheduledDateOut() != '' ) ) {
            	if( !empty($custom_ot) && isset($custom_ot[$a->getDate()]) && $custom_ot[$a->getDate()]['day_type'] == G_Custom_Overtime::DAY_TYPE_RESTDAY && Tools::isValidTime($custom_ot[$a->getDate()]['start_time']) && Tools::isValidTime($custom_ot[$a->getDate()]['end_time']) ){
            		$timestamp_start = $custom_ot[$a->getDate()]['start_time'];
                	$timestamp_end   = $custom_ot[$a->getDate()]['end_time'];
                	if( $timestamp_start > $timestamp_end ){
                		$date_start = $a->getDate() . " " . $custom_ot[$a->getDate()]['start_time'];
                		$date = new DateTime($a->getDate());
                        $date->modify('+1 day');
                        $date_end = $date->format('Y-m-d');
                        $date_end = $date_end . " " . $custom_ot[$a->getDate()]['end_time'];

                		//$date_end   = date("Y-m-d",strtotime("+1 day", $a->getDate())) . " " . $custom_ot[$a->getDate()]['end_time'];
                	}else{
                		$date_start = $a->getDate() . " " . $custom_ot[$a->getDate()]['start_time'];
                		$date_end   = $a->getDate() . " " . $custom_ot[$a->getDate()]['end_time'];
                	}
                	$restday_hours = Tools::computeHoursDifferenceByDateTime($date_start, $date_end); 

                	$day_type = array();
                    if( $a->isRestday() ){
                        $day_type[] = "applied_to_restday";
                    }else{
                        $day_type[] = "applied_to_regular_day";
                    }
                    
                    $schedule['schedule_in']  = $t->getScheduledDateIn() . " " . $t->getScheduledTimeIn();
                    $schedule['schedule_out'] = $t->getScheduledDateOut() . " " . $t->getScheduledTimeOut();
                    $schedule['actual_in']    = $t->getTimeIn();
                    $schedule['actual_out']   = $t->getTimeOut();
                    $e = new G_Employee();
                    $e->setId($a->getEmployeeId());
                    $deductible_breaktime     = $e->getTotalBreakTimeHrsDeductible($schedule, $day_type); 
                    if($restday_hours > $deductible_breaktime) {
                        $restday_hours = $restday_hours - $deductible_breaktime;
                    }                      
                      
            	}else{
            		$t = $a->getTimesheet();
	                //$restday_hours = $t->getTotalHoursWorked();
	                $restday_hours = $t->totalHrsWorkedBaseOnSchedule();
	                /*if ($restday_hours > 8) {
	                    $restday_hours = 8;
	                }*/
	                if ($a->isOfficialBusiness()) {
	                    $restday_hours = 8;
	                }


	                if( strtotime($t->getTimeIn()) > strtotime($t->getScheduledTimeIn()) ){                        
	                    $in_diff = Tools::computeHoursDifferenceByDateTime($t->getScheduledTimeIn(), $t->getTimeIn()); 
	                    $restday_hours -= $in_diff;
	                }	
            	}
                	

                $restday_rate = $rate->getRestDay();
                //echo "Rest Day :{$restday_rate}";
                //echo "/ Rest HRS :{$restday_hours}";
                //echo "/ Hrly Rate :{$hourly_amount}";
                $restday_hours = Tools::numberFormat($restday_hours, 2);
                $temp_amount = (float) Tools::numberFormat($restday_hours * $hourly_amount * ($restday_rate/100), 2);
                $amount = $amount + $temp_amount;
            }
        }
        return $amount;
    }

   	/*public static function computeRegularAmount($attendance, $daily_amount, $hourly_amount) {

        $amount = 0;                     
        foreach ($attendance as $a) {
        	$employee_id = $a->getEmployeeId();        	
            //if ($a->isPresent() && $a->isRestday() && !$a->isHoliday()) {
        	$employee_id = $a->getEmployeeId();        	
           	if ($a->isPaid() && $a->isPresent() && !$a->isRestday()) { // march 4, 2015 - let           		        		
                $t = $a->getTimesheet();
                $regular_hours = $t->getTotalHoursWorked();
                if ($regular_hours > 8) {
                    $regular_hours = 8;
                }
                if ($a->isOfficialBusiness()) {
                    $regular_hours = 8;
                }

                $regular_hours = Tools::numberFormat($regular_hours, 2);
                $temp_amount = (float) Tools::numberFormat($regular_hours * $hourly_amount, 2);
                $amount = $amount + $temp_amount;
            }
        }
        return $amount;
    }*/

    public static function computeRegularAmount($attendance, $daily_amount, $hourly_amount) {

        $amount    = 0;      
        $total_hrs = 0;               
        foreach ($attendance as $a) {
        	$employee_id = $a->getEmployeeId();        	
            //if ($a->isPresent() && $a->isRestday() && !$a->isHoliday()) {
        	$employee_id = $a->getEmployeeId();        	
           	if ($a->isPaid() && $a->isPresent() && !$a->isRestday()) { // march 4, 2015 - let           		        		
                $t = $a->getTimesheet();
                $regular_hours = $t->getTotalHoursWorked() + $t->getTotalOvertimeHours();
                $date = $a->getDate();
                //echo "Date : {$date} / Employee ID : {$employee_id} / Total HRS Worked : {$regular_hours}<br>";

                /*if ($regular_hours > 8) {
                    $regular_hours = 8;
                }*/
                if ($a->isOfficialBusiness()) {
                    $regular_hours = 8;
                }
                
                $regular_hours = Tools::numberFormat($regular_hours, 2);
                $total_hrs    += $regular_hours;                               
            }
        }

        //echo "Total HRS : {$total_hrs} / Employee ID {$employee_id}<br>";
        $amount = (float) Tools::numberFormat($total_hrs * $hourly_amount, 2);
        return $amount;
    }

    public static function computeRestDayOvertimeAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $amount = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && $a->isRestday() && !$a->isHoliday() && !$a->isOfficialBusiness() ) {
                $t = $a->getTimesheet();

                $rd_rate = $rate->getRestDay() / 100;
                $rd_ot_rate = $rate->getRestDayOvertime() / 100;

                $rd_ot_hours = $t->getRestDayOvertimeHours() + $t->getRestDayLegalOvertimeExcessHours();

                $temp_amount = (float) Tools::numberFormat(($rd_ot_hours * ($hourly_amount * $rd_rate)) * $rd_ot_rate, 2);
                $amount = $amount + $temp_amount;
            }
        }
        return $amount;
    }

    public static function computeRegularOvertimeAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $amount = 0;  
        $hrly_amount = $hourly_amount;      
        foreach ($attendance as $a) {
            if ($a->isPresent() && !$a->isRestday() && !$a->isHoliday()) {                
                $t = $a->getTimesheet();
                $ot_rate = $rate->getRegularOvertime() / 100;
                $ot_hours = $t->getRegularOvertimeHours() + $t->getRegularOvertimeExcessHours();
                $total_ot_hours += $ot_hours;
                //echo $ot_hours . "<br />";
                $hourly_amount = $hourly_amount * $ot_rate;                
                $temp_amount = (float) Tools::numberFormat($ot_hours * $hourly_amount, 2);
                $amount = $amount + $temp_amount;
            }
        }

        //echo $total_ot_hours . 'x ' . $hrly_amount . ' x ' . $ot_rate;
                
        $new_amount = (float) Tools::numberFormat($total_ot_hours * $hrly_amount * $ot_rate,2);
        //echo "New Total Amount : {$new_amount}";
        
        return $new_amount;
    }

    /**
     * Compute Special Rate Overtime 
     *
     * @param attendance array
     * @param rate float
     * @return float
    */
    public static function computeSpecialRateRegularOvertimeAmount($attendance, $rate) {
        $amount = 0;          
        foreach ($attendance as $a) {
            if ($a->isPresent() && !$a->isRestday() && !$a->isHoliday()) {                
                $t = $a->getTimesheet();                
                $ot_hours = $t->getRegularOvertimeHours() + $t->getRegularOvertimeExcessHours();
                $total_ot_hours += $ot_hours;                                
            }
        }
                
        $ot_amount = (float) Tools::numberFormat($total_ot_hours * $rate,2);
        //echo "New Total Amount : {$new_amount}";
        
        
        return $ot_amount;
    }

    public static function computeRestDayOvertimeNightShiftAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $total = 0;
        foreach ($attendance as $a) {
            $t = $a->getTimesheet();
            if ($a->isPresent() && $a->isRestday() && !$a->isHoliday()) {
                if ($t) {
                    $rd_rate = $rate->getRestDay() / 100;
                    $ot_rate = $rate->getRestDayOvertime() / 100;
                    $ns_rate = ($rate->getNightShiftDiff() - 100) / 100;

                    $hourly_amount = ($hourly_amount * $rd_rate * $ot_rate);
                    $ot_hours = (float) $t->getRestDayOvertimeNightShiftHours() + $t->getRestDayOvertimeNightShiftExcessHours();

                    $temp_total = (float) Tools::numberFormat($ot_hours * $hourly_amount * $ns_rate , 2);
                    $total = $total + $temp_total;
                }
            }
        }
        return $total;
    }

    public static function computeRegularOvertimeNightShiftAmountDepre($attendance, $rate, $daily_amount, $hourly_amount) {    	
        $total = 0;
        $rate_per_hour = $hourly_amount;
        foreach ($attendance as $a) {
            if ($a->isPresent() && !$a->isRestday() && !$a->isHoliday()) {
                $t = $a->getTimesheet();
                if ($t) {
                    $ot_rate = $rate->getRegularOvertime() / 100;
                    $ns_rate = ($rate->getNightShiftDiff() - 100) / 100;
                    //Utilities::displayArray($a);                                        
                    $hourly_amount = ($hourly_amount * $ot_rate);
                    $ot_hours = (float) $t->getRegularOvertimeNightShiftHours() + $t->getRegularOvertimeNightShiftExcessHours();    
                    $total_ot_hrs += $ot_hours;       
                    //echo "OT Hours : {$ot_hours} / Hourly Amount : {$hourly_amount} / Rate : {$ns_rate} <br>";         
                    $temp_total = (float) Tools::numberFormat($ot_hours * $hourly_amount * $ns_rate , 2);
                    $total = $total + $temp_total;                    
                }
            }
        }    

        return $total;
    }

    public static function computeRegularOvertimeNightShiftAmount($attendance, $rate, $daily_amount, $hourly_amount) {    	
        $total = 0;
        $rate_per_hour   = $hourly_amount; 
        $nightshift_rate = ($rate->getNightShiftDiff() - 100) / 100;       
        foreach ($attendance as $a) {
            if ($a->isPresent() && !$a->isRestday() && !$a->isHoliday()) {
                $t = $a->getTimesheet();
                if ($t) {                  
                	$ot_hours = (float) $t->getRegularOvertimeNightShiftHours() + $t->getRegularOvertimeNightShiftExcessHours();     
                    $total_ot_hrs += $ot_hours;       
                }
            }
        }    

        $ot_rate = ($rate->getRegularOvertimeNightShiftDifferential() / 100); 
        $total = ($rate_per_hour * $nightshift_rate) * ($rate->getRegularOvertimeNightShiftDifferential() / 100) * $total_ot_hrs; 

        return $total;
    }

    public static function computeRegularNightShiftAmount($attendance, $rate, $daily_amount, $hourly_amount) {    	
        $total = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && !$a->isRestday() && !$a->isHoliday()) {
                $t = $a->getTimesheet();
                if ($t) {
                    $ns_hours = $t->getNightShiftHours();                    
                    $ns_rate = ($rate->getNightShiftDiff() - 100) / 100;
                    $temp_total = (float) Tools::numberFormat($hourly_amount * $ns_rate * $ns_hours, 2);
                    $total = $total + $temp_total;
                }
            }
        }
        return $total;
    }

    public static function computeRestDayNightShiftAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $total = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && $a->isRestday() && !$a->isHoliday() && !$a->isOfficialBusiness()) {
                $t = $a->getTimesheet();
                if ($t) {
                    $ns_hours = $t->getNightShiftHours();

                    $rd_rate = $rate->getRestDay() / 100;
                    $ns_rate = ($rate->getNightShiftDiff() - 100) / 100;

                    $temp_total = (float) Tools::numberFormat($hourly_amount * $rd_rate * $ns_rate * $ns_hours, 2);
                    $total = $total + $temp_total;
                }
            }
        }
        return $total;
    }

    public static function computeRestDaySpecialNightShiftAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $total = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && $a->isRestday() && $a->isHoliday()) {
                $h = $a->getHoliday();
                if ($h && $h->isSpecial()) {
                    $t = $a->getTimesheet();
                    if ($t) {
                        $ns_hours = $t->getNightShiftHours();

                        $rd_rate = $rate->getHolidaySpecialRestday() / 100;
                        $ns_rate = ($rate->getNightShiftDiff() - 100) / 100;

                        $temp_total = (float) Tools::numberFormat($hourly_amount * $rd_rate * $ns_rate * $ns_hours, 2);
                        $total = $total + $temp_total;
                    }
                }
            }
        }
        return $total;
    }

    public static function computeRestDayLegalNightShiftAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $total = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && $a->isRestday() && $a->isHoliday()) {
                $h = $a->getHoliday();
                if ($h && $h->isLegal()) {
                    $t = $a->getTimesheet();
                    if ($t) {
                        $ns_hours = $t->getNightShiftHours();

                        $rd_rate = $rate->getHolidayLegalRestday() / 100;
                        $ns_rate = ($rate->getNightShiftDiff() - 100) / 100;

                        $temp_total = (float) Tools::numberFormat($hourly_amount * $rd_rate * $ns_rate * $ns_hours, 2);
                        $total = $total + $temp_total;
                    }
                }
            }
        }
        return $total;
    }

    public static function computeSpecialNightShiftAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $total = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && !$a->isRestday() && $a->isHoliday()) {
                $h = $a->getHoliday();
                if ($h && $h->isSpecial()) {
                    $t = $a->getTimesheet();
                    if ($t) {
                        $ns_hours = $t->getNightShiftHours();

                        $day_rate = $rate->getHolidaySpecial() / 100;
                        $ns_rate = ($rate->getNightShiftDiff() - 100) / 100;

                        $temp_total = (float) Tools::numberFormat($hourly_amount * $day_rate * $ns_rate * $ns_hours, 2);
                        $total = $total + $temp_total;
                    }
                }
            }
        }
        return $total;
    }

    public static function computeLegalNightShiftAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $total = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && !$a->isRestday() && $a->isHoliday()) {
                $h = $a->getHoliday();
                if ($h && $h->isLegal()) {
                    $t = $a->getTimesheet();
                    if ($t) {
                        $ns_hours = $t->getNightShiftHours();

                        $day_rate = $rate->getHolidayLegal() / 100;
                        $ns_rate = ($rate->getNightShiftDiff() - 100) / 100;

                        $temp_total = (float) Tools::numberFormat($hourly_amount * $day_rate * $ns_rate * $ns_hours, 2);
                        $total = $total + $temp_total;
                    }
                }
            }
        }
        return $total;
    }

    public static function computeLegalOvertimeNightShiftAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $total = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && $a->isHoliday() && !$a->isRestday()) {
                $h = $a->getHoliday();
                if ($h && $h->isLegal()) {
                    $t = $a->getTimesheet();
                    if ($t) {
                        $day_rate = $rate->getHolidayLegal() / 100;
                        $ot_rate = $rate->getHolidayLegalOvertime() / 100;
                        $ns_rate = ($rate->getNightShiftDiff() - 100) / 100;

                        $hourly_amount = ($hourly_amount * $day_rate * $ot_rate);
                        $ot_hours = (float) $t->getLegalOvertimeNightShiftHours() + $t->getLegalOvertimeNightShiftExcessHours();

                        $temp_total = (float) Tools::numberFormat($ot_hours * $hourly_amount * $ns_rate , 2);
                        $total = $total + $temp_total;
                    }
                }
            }
        }
        return $total;
    }

    public static function computeSpecialOvertimeNightShiftAmount($attendance, $rate, $daily_amount, $hourly_amount) {
        $total = 0;
        foreach ($attendance as $a) {
            if ($a->isPresent() && $a->isHoliday() && !$a->isRestday()) {
                $h = $a->getHoliday();
                if ($h && $h->isSpecial()) {
                    $t = $a->getTimesheet();
                    if ($t) {
                        $day_rate = $rate->getHolidaySpecial() / 100;
                        $ot_rate = $rate->getHolidaySpecialOvertime() / 100;
                        $ns_rate = ($rate->getNightShiftDiff() - 100) / 100;

                        $hourly_amount = ($hourly_amount * $day_rate * $ot_rate);
                        $ot_hours = (float) $t->getSpecialOvertimeNightShiftHours() + $t->getSpecialOvertimeNightShiftExcessHours();

                        $temp_total = (float) Tools::numberFormat($ot_hours * $hourly_amount * $ns_rate , 2);
                        $total = $total + $temp_total;
                    }
                }
            }
        }
        return $total;
    }

    public static function wrapPayslipSection($payslip_array,$section,$template) {

    	$wrap_earnings_array 			= array();
    	$wrap_loan_leave_balance_array 	= array();
    	$wrap_deduction_array			= array();
    	$wrap_other_earnings_deduction  = array();
    	$wrap_breakdown_array			= array();
    	$wrap_all_arrays                = array();

    	$emp_earnings 			= unserialize($payslip_array['earnings']);
    	$emp_other_earnings 	= unserialize($payslip_array['other_earnings']);
    	$emp_deduction 			= unserialize($payslip_array['deductions']);
    	$emp_other_deductions	= unserialize($payslip_array['other_deductions']);
    	$emp_labels			 	= unserialize($payslip_array['labels']);

    	//echo '<pre>';
    	//print_r($emp_earnings);
    	//print_r($emp_other_earnings);
    	//print_r($emp_deduction);
    	//print_r($emp_other_deductions);
    	//echo '</pre>';

    	if($section == 'earnings')  {
			
			foreach ($emp_earnings as $earnings) {
			  $variable 							   = strtolower($earnings->getVariable());
			  $wrap_earnings_array[$variable]['label'] = $earnings->getLabel();
			  $wrap_earnings_array[$variable]['value'] = $earnings->getAmount();
			}

			$earning_from_deduct_tardi = $emp_deduction[0]; //tardines or late			
			$wrap_earnings_array[$earning_from_deduct_tardi->getVariable()]['label'] = $earning_from_deduct_tardi->getLabel();
			$wrap_earnings_array[$earning_from_deduct_tardi->getVariable()]['value'] = $earning_from_deduct_tardi->getAmount();

			$earning_from_deduct_undertime = $emp_deduction[1]; //undertime
			$wrap_earnings_array[$earning_from_deduct_undertime->getVariable()]['label'] = $earning_from_deduct_undertime->getLabel();
			$wrap_earnings_array[$earning_from_deduct_undertime->getVariable()]['value'] = $earning_from_deduct_undertime->getAmount();

			$earning_from_deduct_absent = $emp_deduction[2]; //absent
			$wrap_earnings_array[$earning_from_deduct_absent->getVariable()]['label'] = $earning_from_deduct_absent->getLabel();
			$wrap_earnings_array[$earning_from_deduct_absent->getVariable()]['value'] = $earning_from_deduct_absent->getAmount();
	    	
	    	$wrap_arrays = $wrap_earnings_array;
    	}elseif($section == 'deductions') {

    		foreach($emp_deduction as $deduction) {
			  $variable_deduction   							  = strtolower($deduction->getVariable());
			  $wrap_deduction_array[$variable_deduction]['label'] = $deduction->getLabel();
			  $wrap_deduction_array[$variable_deduction]['value'] = $deduction->getAmount();    			
    		}

    		foreach($emp_other_deductions as $other_deduction) {
			  $variable_other_deduction   								= strtolower( preg_replace('/\s+/', '_', $other_deduction->getLabel())  );
			  $wrap_deduction_array[$variable_other_deduction]['label'] = $other_deduction->getLabel();
			  $wrap_deduction_array[$variable_other_deduction]['value'] = $other_deduction->getAmount();     			
    		}   

			$wrap_arrays = $wrap_deduction_array;

    	}elseif($section == 'loan_leave_balance') {

    	}elseif($section == 'other_earnings_deductions') {

    	}elseif($section == 'breakdown') {

    	}

    	return $wrap_arrays;
    }

    public static function computeEmployeeYearlyPayslipBreakdown($employee_id) {
		$yearS = mktime(0, 0, 0, 1, 1,  date('Y') );
		$yearE = mktime(0, 0, 0, 12, 31,  date('Y') );

		$start_date = date('Y-m-d',$yearS);
		$end_data   = date('Y-m-d',$yearE);

    	$sql = "
			SELECT 
				SUM(p.basic_pay) as y_basic_pay, SUM(p.gross_pay) as y_gross_pay, SUM(p.total_earnings) as y_total_earnings, 
				SUM(p.total_deductions) as y_total_deductions, SUM(p.net_pay) as y_net_pay, SUM(p.taxable) as y_taxable, 
				SUM(p.non_taxable) as y_non_taxable, SUM(p.withheld_tax) as y_withheld_tax, SUM(p.month_13th) as y_month_13th, 
				SUM(p.sss) as y_sss, SUM(p.pagibig) as y_pagibig, SUM(p.philhealth) as y_philhealth, SUM(p.overtime) as y_overtime, 
				SUM(p.tardiness_amount) as y_tardiness_amount
			FROM " . G_EMPLOYEE_PAYSLIP . " p
			WHERE p.employee_id = " . Model::safeSql($employee_id) . "
				AND (p.period_start BETWEEN " . Model::safeSql($start_date) . " AND " . Model::safeSql($end_data) . " OR p.period_end BETWEEN " . Model::safeSql($start_date) . " AND " . Model::safeSql($end_data) . ")
			LIMIT 1
		";
		
		$result = Model::runSql($sql);
		$row    = Model::fetchAssoc($result);		
		return $row;			
    }

    public static function computeEmployeeYearlyPayslipBreakdownByEndDate($employee_id, $end_date = '') {
    	if( $end_date == '' ){
    		$sql_end_date = date("Y-m-d");
    	}else{
    		$sql_end_date = $end_date;
    	}

    	$sql_year = date("Y",strtotime($sql_end_date));

    	$sql = "
			SELECT 
				SUM(p.basic_pay) as y_basic_pay, SUM(p.gross_pay) as y_gross_pay, SUM(p.total_earnings) as y_total_earnings, 
				SUM(p.total_deductions) as y_total_deductions, SUM(p.net_pay) as y_net_pay, SUM(p.taxable) as y_taxable, 
				SUM(p.non_taxable) as y_non_taxable, SUM(p.withheld_tax) as y_withheld_tax, SUM(p.month_13th) as y_month_13th, 
				SUM(p.sss) as y_sss, SUM(p.pagibig) as y_pagibig, SUM(p.philhealth) as y_philhealth, SUM(p.overtime) as y_overtime, 
				SUM(p.tardiness_amount) as y_tardiness_amount
			FROM " . G_EMPLOYEE_PAYSLIP . " p
			WHERE p.employee_id = " . Model::safeSql($employee_id) . "
				AND p.period_end <= " . Model::safeSql($sql_end_date) . "
				AND YEAR(p.period_end) =" . Model::safeSql($sql_year) . "
			LIMIT 1
		";		
		$result = Model::runSql($sql);
		$row    = Model::fetchAssoc($result);		
		return $row;			
    }

    public function sqlGetPayslipDataByDateRange($date_from = '', $date_to = '', $fields = array()){
    	$sql_fields = " * ";
		if( !empty($fields) ){
			$sql_fields = implode(",", $fields);
		}

    	$sql = "
			SELECT {$sql_fields}
			FROM g_employee_payslip				
			WHERE period_start BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . "							
		";		

		$result = Model::runSql($sql,true);		
		return $result;	
    }

    public function sqlGetPayslipDataByYear( $year ){
    	$sql_fields = " * ";
		if( !empty($fields) ){
			$sql_fields = implode(",", $fields);
		}

    	$sql = "
			SELECT *
			FROM g_employee_payslip				
			WHERE DATE_FORMAT(period_start,'%Y')
		";		
		
		$result = Model::runSql($sql,true);		
		return $result;	
    }

	public static function sqlGetPayslipIdsByPeriod($start_date, $end_date) {
		$sql = "
			SELECT id, payout_date, period_start, period_end
			FROM g_employee_payslip
			WHERE (period_start = ". Model::safeSql($start_date) ." AND period_end = ". Model::safeSql($end_date) .")		
		";		
		//return self::getObjects($sql);
		$result = Model::runSql($sql,true);		
		return $result;	
	}    

	public static function isIdExist(G_Payslip $p) {
		$sql = "
			SELECT COUNT(*) as total
			FROM g_employee_payslip
			WHERE id = ". Model::safeSql($p->getId()) ."
		";
		$result = Model::runSql($sql);
		$row    = Model::fetchAssoc($result);
		return $row['total'];
	}	
}
?>