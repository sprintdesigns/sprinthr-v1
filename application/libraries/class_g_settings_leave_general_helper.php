<?php
class G_Settings_Leave_General_Helper {
	
	public static function isIdExist(G_Settings_Leave_General $u) {
		$sql = "
			SELECT COUNT(*) as total
			FROM " . G_SETTINGS_LEAVE_GENERAL ."
			WHERE id = ". Model::safeSql($u->getId()) ."
		";
		$result = Model::runSql($sql);
		$row    = Model::fetchAssoc($result);
		return $row['total'];
	}
	
	public static function countTotalRecords() {
		$sql = "
			SELECT COUNT(*) as total
			FROM " . G_SETTINGS_LEAVE_GENERAL ."
		";
		
		$result = Model::runSql($sql);
		$row    = Model::fetchAssoc($result);
		return $row['total'];
	}

}
?>