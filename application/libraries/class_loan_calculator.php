<?php
class Loan_Calculator {
	protected $start_date;
	protected $loan_amount = 0;
	protected $deduction_type; //Monthly, Bi-monthly
	protected $months_to_pay = 0;
	protected $interest_rate;	
	protected $cutoff_period;
	
	public function __construct( $amount = 0) {
		if( $amount > 0 ){
			$this->loan_amount = $amount;
		}
	}
	
	public function setStartDate($value) {
		$date_format = date("Y-m-d",strtotime($value));
		$this->start_date = $date_format;	
	}

	public function setLoanAmount($value) {
		$this->loan_amount = $value;
	}

	public function setDeductionType($value){
		$this->deduction_type = $value;
	}

	public function setCutoffPeriod($value = ''){
		$this->cutoff_period = $value;
	}

	public function setMonthsToPay($value = 0) {
		$this->months_to_pay = $value;
	}

	public function setInterestRate($value) {
		$this->interest_rate = $value;
	}

	/*
	 * @param int 0 = by month / 1 = by cutoff - for bi-monthly
	*/
	public function getEndDate( $month_cutoff = 0 ) {	
		$end_date = '';
		if( $this->deduction_type != '' ){
			switch ($this->deduction_type) {
				case G_Employee_Loan::BI_MONTHLY:		
					if( $month_cutoff == 1 ){
						$end_date = $this->endDateByCutoff();//Will use number of cutoffs for counting dates
					}else{
						$end_date = $this->endDateBiMonthly();//Will use number of months for counting dates
					}					
					break;
				case G_Employee_Loan::MONTHLY:				
					$end_date = $this->endDateMonthly();
					break;			
			}			
		}

		return $end_date;
	}

	public function endDateByCutoff() {
		$new_date = '';
		if( $this->start_date != '' ){
			$fields = array("cut_off");
			$default_pay_period = G_Settings_Pay_Period_Helper::sqlDefaultPayPeriod($fields);
			$cutoff_data = explode(",", $default_pay_period['cut_off']);    
			$cutoff_a    = explode("-", $cutoff_data[0]);
			$cutoff_b    = explode("-", $cutoff_data[1]);		

			$start_date_day = date("d",strtotime($this->start_date));
			if( in_array($start_date_day, $cutoff_a) ){
				$start_pattern = 'a';				
			}else{
				$start_pattern = 'b';				
			}

			$new_date = $this->start_date;
			$x        = 0;

			while( $x <= $this->months_to_pay ){				
				if( $start_pattern == 'b' ){
					$new_date = Tools::convertDateToCutoffPattern($new_date,$cutoff_b,1);	
					$start_pattern = '';				
					$x++;
				}else{
					$new_date_a = Tools::convertDateToCutoffPattern($new_date,$cutoff_a,1);						
					$new_date   = $new_date_a;
					$x++;
					if( $x >= $this->months_to_pay ){						
						break;
					}		

					$new_date_b = Tools::convertDateToCutoffPattern($new_date,$cutoff_b,1);
					$new_date   = $new_date_b;
					$x++;
					if( $x >= $this->months_to_pay ){						
						break;
					}
				}					
				$new_date = date("Y-m-d",strtotime("first day of +1 month",strtotime($new_date)));									
			}			
		}

		return $new_date;
	}

	public function getStartDate( $period = array() ){
		$loan_start_date  = '';
		if( !empty($period) ){
			$i_year     = $period['year'];
			$s_cutoff   = $period['cutoff'];
			$i_month    = date("m",strtotime($i_year . "-" . $period['month'] . "-" . "01"));								

			$fields = array("cut_off");
			$default_pay_period = G_Settings_Pay_Period_Helper::sqlDefaultPayPeriod($fields);
			$cutoff_data = explode(",", $default_pay_period['cut_off']);    
			$cutoff_a    = explode("-", $cutoff_data[0]);
			$cutoff_b    = explode("-", $cutoff_data[1]);		

			switch( $s_cutoff ){
				case 'a':						
				$loan_start_date = $cutoff_a[1];
				$loan_start_date = "{$i_year}-{$i_month}-{$loan_start_date}";

				break;
				case 'b':
					$start_date_day   = $cutoff_b[1];							
					if( $i_month == 2 && $start_date_day > 28 ){
						$feb_date = date("Y-m-t",strtotime("{$i_year}-{$i_month}-01"));
						$loan_start_date = $feb_date;
					}elseif( $start_date_day == 31 ){
						$new_date = date("Y-m-t",strtotime("{$i_year}-{$i_month}-01"));
						$loan_start_date = $new_date;
					}else{
						$loan_start_date = "{$i_year}-{$i_month}-{$start_date_day}";
					}
					break;
			}
		}

		return $loan_start_date;
	}

	private function endDateMonthly() {
		$fields = array("cut_off");
		$default_pay_period = G_Settings_Pay_Period_Helper::sqlDefaultPayPeriod($fields);
		$cutoff_data = explode(",", $default_pay_period['cut_off']);    
		$cutoff_a    = explode("-", $cutoff_data[0]);
		$cutoff_b    = explode("-", $cutoff_data[1]);

		//Will use end date
		$start_date_a = $cutoff_a[1];
		$start_date_b = $cutoff_b[1];

		$plus_months = $this->months_to_pay - 1;		
		$start_date_day = date("d",strtotime($this->start_date));

		if( $start_date_day > $start_date_a ){
			$new_start_date_day = $start_date_b;
		}else{
			$new_start_date_day = $start_date_a;
		}

		$end_date = date("Y-m-d",strtotime("first day of +{$plus_months} month",strtotime($this->start_date))); //Use first day to not skip february
		$end_date_month = date("m",strtotime($end_date));

		if( $end_date_month == 2 && $start_date_day > 28 ){
			$end_date = date("Y-m-t",strtotime($end_date));
		}else{					
			$end_date = date("Y-m-{$new_start_date_day}",strtotime($end_date));			
		}
		return $end_date;
	}

	private function endDateBiMonthly() {		
		$start_date_day = date("d",strtotime($this->start_date));
		$fields = array("cut_off");
		$default_pay_period = G_Settings_Pay_Period_Helper::sqlDefaultPayPeriod($fields);
		$cutoff_data = explode(",", $default_pay_period['cut_off']);    
		$cutoff_a    = explode("-", $cutoff_data[0]);
		$cutoff_b    = explode("-", $cutoff_data[1]);

		if( in_array($start_date_day, $cutoff_a) ){ //if start date day falls within cutoff a get cutoff b end period 
			$new_day     = $cutoff_b[1];
			$plus_months = $this->months_to_pay - 1;
		}elseif( in_array($start_date_day, $cutoff_b) ){
			$new_day     = $cutoff_a[1];
			$plus_months = $this->months_to_pay;		
		}else{
			if( $start_date_day > $cutoff_a[1] ){
				$new_day     = $cutoff_b[1];
			}else{
				$new_day     = $cutoff_a[1];
			}
			$plus_months = $this->months_to_pay;	
		}

		$datetime = new DateTime($this->start_date);
		if( $datetime->format("d") > $cutoff_a[1] ){
			$new_day =  $cutoff_a[1];			
		}
		
		$new_start_date = strtotime(date("Y-m-01",strtotime($this->start_date)));
		$new_start_date = date("Y-m-d",strtotime("first day of +{$plus_months} month",$new_start_date));		
		$new_start_date_day = date("m",strtotime($new_start_date));
		if( $new_start_date_day == 2 && $new_day > 28 ){			
			$end_date = date("Y-m-t",strtotime($new_start_date));
		}else{
			$end_date     =  date("Y-m-{$new_day}",strtotime($new_start_date));
		}
		
		return $end_date;
	}

	public function expectedLoanEndDate() {
		$end_date        = '';
		$a_cutoff_period = explode(",", $this->cutoff_period);
		$count_cutoff    = 0;
		foreach( $a_cutoff_period as $period ){
			if( trim($period) != '' ){
				$count_cutoff++;
			}
		}

		$total_cutoff = $this->months_to_pay * $count_cutoff;
		$cutoff_year  = date("Y",strtotime($this->start_date));
		$fields = array("payout_date");
		if( $count_cutoff == 2 ){						
			$periods = G_Cutoff_Period_Helper::sqlCutoffPeriodsByYearTagAndStartPeriod($cutoff_year, $this->start_date, $fields, $total_cutoff);
		}elseif( $count_cutoff == 1 ){			
			$cutoff_number = trim($a_cutoff_period[0]);
			$periods = G_Cutoff_Period_Helper::sqlCutoffPeriodsByYearTagAndStartPeriodAndCutoffNumber($cutoff_year, $this->start_date, $cutoff_number, $fields, $total_cutoff);
		}
		
		$last_data = end($periods);
		if( !empty($last_data) ){
			$end_date = $last_data['payout_date'];
		}

		return $end_date;
	}

	public function totalAmountToPay() {		
		$total_amount_to_pay = $this->loan_amount + ($this->loan_amount * ($this->interest_rate/100));
		return $total_amount_to_pay;

	}

	private function monthlyDue() {
		$monthly_due         = 0;
		$total_amount_to_pay = $this->totalAmountToPay();
		if( $this->deduction_type == G_Employee_Loan::BI_MONTHLY ){			
			//$monthly_due = $total_amount_to_pay / ($this->months_to_pay * 2);
			$monthly_due = ($total_amount_to_pay / $this->months_to_pay);
		}elseif( $this->deduction_type == G_Employee_Loan::MONTHLY ){
			$monthly_due = ($total_amount_to_pay / $this->months_to_pay);
		}
		return $monthly_due;
	}

	public function expectedDue() {
		$amount_to_pay = self::totalAmountToPay();
		$expected_due  = 0;
		if( $this->cutoff_period != '' && $amount_to_pay > 0 && $this->months_to_pay > 0 ){						
			$a_cutoff_period = explode(",", $this->cutoff_period);
			$count_cutoff = 0;
			foreach( $a_cutoff_period as $period ){
				if( trim($period) != '' ){
					$count_cutoff++;
				}
			}

			$expected_due = $amount_to_pay / ($this->months_to_pay * $count_cutoff);
		}

		return $expected_due;
	}

	public function computeLoanDepre() {
		$data = array();		
		if( !empty($this->start_date) && !empty($this->loan_amount) && !empty($this->deduction_type) && !empty($this->months_to_pay) ){

			$data['end_date']            = $this->getEndDate();
			$data['total_amount_to_pay'] = number_format($this->totalAmountToPay(),2, ".", "");
			$data['monthly_due']         = number_format($this->monthlyDue(),2, ".", "");
			$data['is_valid']			 = true;
		}else{
			$data['end_date']            = '';
			$data['total_amount_to_pay'] = 0;
			$data['monthly_due']         = 0;
			$data['is_valid']			 = false;
		}

		return $data;
	}

	public function computeGovernmentLoan($period = array()) {
		$data = array();				
		if( !empty($period) && $this->loan_amount > 0 && $this->months_to_pay > 0 && $this->deduction_type != '' ){		
			switch ($this->deduction_type) {
				case G_Employee_Loan::BI_MONTHLY:

					break;
				case G_Employee_Loan::MONTHLY:		

					break;
				default:
					$data['start_date']			 = '';
					$data['end_date']            = '';
					$data['total_amount_to_pay'] = 0;					
					$data['is_valid']			 = false;
					break;
			}	
		}

		return $data;
	}

	public function computeLoan($period = array()){
		$data = array();				
		if( !empty($period) && $this->loan_amount > 0 && $this->months_to_pay > 0 && $this->deduction_type != '' ){					
			switch ($this->deduction_type) {
				case G_Employee_Loan::BI_MONTHLY:					
					$this->start_date = $this->getStartDate($period);
					$data['start_date']			 = $this->start_date;
					$data['end_date'] 		     = $this->getEndDate(1);
					$data['total_amount_to_pay'] = number_format($this->totalAmountToPay(),2, ".", "");
					$data['monthly_due']         = number_format($this->monthlyDue(),2, ".", "");
					$data['is_valid']			 = true;
					break;
				case G_Employee_Loan::MONTHLY:												
					$i_year     = $period['year'];
					$s_cutoff   = $period['cutoff'];
					$i_month    = date("m",strtotime($i_year . "-" . $period['month'] . "-" . "01"));								

					$fields = array("cut_off");
					$default_pay_period = G_Settings_Pay_Period_Helper::sqlDefaultPayPeriod($fields);
					$cutoff_data = explode(",", $default_pay_period['cut_off']);    
					$cutoff_a    = explode("-", $cutoff_data[0]);
					$cutoff_b    = explode("-", $cutoff_data[1]);		

					switch( $s_cutoff ){
						case 'a':												
						$this->start_date = $this->getStartDate($period);

						$end_date = $this->getEndDate();
						$end_date_day   = date("d",strtotime($end_date));
						$end_date_month = date("m",strtotime($end_date));

						break;
						case 'b':
							$start_date_day   = $cutoff_b[1];		
							$this->start_date = $this->getStartDate($period);	

							$end_date 		= $this->getEndDate();
							$end_date_day   = date("d",strtotime($end_date));
							$end_date_month = date("m",strtotime($end_date));
							
							if( $start_date_day > 28 && $end_date_month == 2 ){
								$end_date = date("Y-m-t",strtotime($end_date));
							}else{								
								$end_date = date("Y-m-{$start_date_day}",strtotime($end_date));
							}
							break;
					}

					$data['start_date']			 = $this->start_date;
					$data['end_date'] 		     = $end_date;
					$data['total_amount_to_pay'] = number_format($this->totalAmountToPay(),2, ".", "");
					$data['monthly_due']         = number_format($this->monthlyDue(),2, ".", "");
					$data['is_valid']			 = true;
					break;
				default:
					$data['start_date']			 = '';
					$data['end_date']            = '';
					$data['total_amount_to_pay'] = 0;
					$data['monthly_due']         = 0;
					$data['is_valid']			 = false;
					break;
			}
		}		
		return $data;
	}
}
?>