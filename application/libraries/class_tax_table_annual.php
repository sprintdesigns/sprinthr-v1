<?php
class Tax_Table_Annual {

	public function __construct() {	
	
	}

	public function getTaxTable() {
		$tax_table[0] = array(
			'from' => 0,
			'to' => 10000,
			'first' => 0,
			'percent' => 5,
			'excess' => 0
		);
		$tax_table[1] = array(
			'from' => 10001,
			'to' => 30000,
			'first' => 500,
			'percent' => 10,
			'excess' => 10000
		);
		$tax_table[2] = array(
			'from' => 30001,
			'to' => 70000,
			'first' => 2500,
			'percent' => 15,
			'excess' => 30000
		);
		$tax_table[3] = array(
			'from' => 70001,
			'to' => 140000,
			'first' => 8500,
			'percent' => 20,
			'excess' => 70000
		);
		$tax_table[4] = array(
			'from' => 140001,
			'to' => 250000,
			'first' => 22500,
			'percent' => 25,
			'excess' => 140000
		);
		$tax_table[5] = array(
			'from' => 250001,
			'to' => 500000,
			'first' => 50000,
			'percent' => 30,
			'excess' => 250000
		);
		$tax_table[6] = array(
			'from' => 500001,
			'to' => 1000000,
			'first' => 125000,
			'percent' => 32,
			'excess' => 500000
		);

		return $tax_table;
	}
	
	public function getAnnualTaxBracket( $taxable_income = 0 ) {
		$return    = array();
		$tax_table = $this->getTaxTable();
		foreach( $tax_table as $tax ){
			$from = $tax['from'];
			$to   = $tax['to'];
			if( $from <= $taxable_income && $to >= $taxable_income ){
				$return = $tax;
				return $return;				
			}
		}

		return $return;
	}
}
?>