/* Declaration */
var auto_load_notification;

$(function(){
	//_count_new_notifications();
});

/*
 * Activate autoload of notification with 60 seconds interval = 60000ms
 * when the user is active in the browser.
 */
$(window).focus(function(){	
	clearInterval(auto_load_notification);
	auto_load_notification = setInterval(function(){_count_new_notifications()}, 100000);		
});

/*
 * Deactivate autoload of notification when the user leave the browser - Idle
 * This will avoid resource overload. 
 */
$(window).blur(function(){
	clearInterval(auto_load_notification);
});

function _count_new_notifications() {
	//$.active returns the number of active Ajax requests.
	if($.active == 0) {
		$.get(base_url+'notifications/_count_new_notifications',{},
		function(o){	
			if(o){
				if(o.new_notifications > 0)
					$('#noti_count').html(o.new_notifications);
				else{
					$('#noti_count').html('');
				}
			}	
		},"json");	
	}
	
}

function loadNotificationList(){
	$('#notification_list_wrapper').html(loading_image + ' Loading...');
    $.get(base_url + 'notifications/_load_notification_list',{},
        function(o){
            $('#notification_list_wrapper').html(o);
    });
}

function loadViewNotificationItemList(notification_id){
	$('#view_notification_item_list_wrapper').html(loading_image + ' Loading...');
    $.post(base_url + 'notifications/_load_view_notification_item_list',{notification_id:notification_id},
        function(o){
            $('#view_notification_item_list_wrapper').html(o);
    });
}

function updateLeaveCreditNotification(employee_credits_list) {
	/*$("#leave_credit_notication").html(loading_image);
	$.post(base_url + 'notifications/_leave_credit_update_notication',{},function(o) {
		$("#leave_credit_notication").html(o);							 
	})*/
	
	var html_head = "<p><strong>Leave credit of the following employee has been updated</strong></p>";
	
	$("#leave_credit_notication").html(html_head + employee_credits_list);	
	
	var $dialog = $('#leave_credit_notication');
	 $dialog.dialog("destroy");
	 
	var $dialog = $('#leave_credit_notication');
	$dialog.dialog({
		title: 'Leave Credit Notication',
		resizable: false,
		position: [480,50],
		width: 400,
		modal: false,
		close: function() {
				   $dialog.dialog("destroy");
				   $dialog.hide($.validationEngine.closePrompt('.formError',true));	
				   //load_my_messages_list();				
				}	
					
		}).dialogExtend({
        "maximize" : false,
		"minimize"  : true,
        "dblclick" : "maximize",
        "icons" : { "maximize" : "ui-icon-arrow-4-diag" }
      }).show();		
}