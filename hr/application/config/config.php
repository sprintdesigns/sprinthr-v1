<?php
// leave '/' if you don't have base folder. http://example.com/<This is my base folder>/index.php
// example: '/my_folder/my_another_folder/'

define('MAIN_FOLDER','/sprinthr_clients_project/daichi/');

define('BASE_FOLDER', '/sprinthr_clients_project/daichi/hr/');

define('CLERK_BASE_FOLDER', '/sprinthr_clients_project/daichi/clerk/');

define('CLERK_SCHEDULE_BASE_FOLDER', '/sprinthr_clients_project/daichi/clerk_schedule/');

define('EMPLOYEE_BASE_FOLDER', '/sprinthr_clients_project/daichi/employee/');

define('PAYROLL_BASE_FOLDER', '/sprinthr_clients_project/daichi/payroll/');

define('RECRUITMENT_BASE_FOLDER', '/sprinthr_clients_project/daichi/recruitment/');

define('HELP_FOLDER',MAIN_FOLDER . 'manual/');

//leave '/' if you don't have folder
define('LIBRARY_FOLDER', '/'); //this is for folder library in root directory

define('BASE_FOLDER_EDITOR', 'sprinthr_clients_project/daichi/hr/files/'); //this is for ckeditor// upload image

define('HIDE_INDEX_PAGE', false);

define('MAINTENANCE_MODE', false); 	// Maintenance Mode

define('INDEX_PAGE', 'index.php');

define('THEME', 'default');

define('MAIN_THEME','hr'); // this is for css

define('ENCRYPTION_KEY', 'webgroundz');

define('PHOTO_FOLDER',BASE_FOLDER.'files/photo/');

define('FILES_FOLDER',BASE_FOLDER.'files/files/');


// ==================================================

define('TEMP_USER_FOLDER', $_SERVER['DOCUMENT_ROOT'] . MAIN_FOLDER . 'files/temp/users/');

define('USER_PROFILE_IMAGE_FOLDER', MAIN_FOLDER . 'hr/files/photo/');

define('DEFAULT_CONTROLLER', 'dashboard');

define('DEFAULT_METHOD', 'index');

define('IF_INVALID_CONTROLLER', 'page'); // use this controller if the controller supplied in the url is invalid

define('CONTROLLER_VAR', 'controller');

define('METHOD_VAR', 'method');

// ====================================================
// Reflected to _init.js

define('CONTENT_BODY_ID', '_content-body_'); // The content handler. Must be seen in template.php

?>