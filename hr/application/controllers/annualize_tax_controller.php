<?php
class Annualize_Tax_Controller extends Controller
{
	function __construct()
	{	
		parent::__construct();
		
		Loader::appStyle('style.css');
		Loader::appMainScript('tax.js');		
		Loader::appMainUtilities();

		$this->sprintHdrMenu(G_Sprint_Modules::PAYROLL, 'earnings_deductions');
		//$this->redirectNoAccessModule(G_Sprint_Modules::PAYROLL, 'earnings_deductions');
		$data = G_Cutoff_Period_Helper::isPeriodLock($_GET['hpid']);		
		if($_GET['hpid']){
			$this->var['is_period_lock'] = $_SESSION['sprint_hr']['is_period_lock'] = $data;
		}else{			
			$this->var['is_period_lock'] = $_SESSION['sprint_hr']['is_period_lock'];
		}
		
		if($_GET['from'] && $_GET['to'] && $_GET['hpid']){
			$this->var['download_url']    = url('reports/download_earnings?from=' . $_GET['from'] . '&to=' . $_GET['to'] . '&hpid=' . $_GET['hpid']);
            $this->var['period_selected'] = '<small style="font-size:15px;">Period: <b>' . Tools::convertDateFormat($_GET['from']) . ' </b> to <b>' . Tools::convertDateFormat($_GET['to']) . '</b></small>';
		}
		
		$this->eid                  = $this->global_user_eid;
		$this->company_structure_id = $this->global_user_ecompany_structure_id;				
		$this->c_date  				= Tools::getCurrentDateTime('Y-m-d H:i:s','Asia/Manila');
		$this->default_method       = 'index';					
		$this->var['leave']         = 'selected';			
		$this->var['employee']      = 'selected';
		$this->var['eid']           = $this->eid;	
		$this->var['departments']   = G_Company_Structure_Finder::findByParentID(Utilities::decrypt($this->global_user_ecompany_structure_id));		
		$this->validatePermission(G_Sprint_Modules::PAYROLL,'earnings_deductions','');				
	}

	function index()
	{			
		Jquery::loadMainInlineValidation2();
		Jquery::loadMainJqueryFormSubmit();	
		Jquery::loadMainTipsy();
		Jquery::loadMainJqueryDatatable();
		Jquery::loadMainTextBoxList();	
		
		$this->var['recent'] = 'class="selected"';				
		$this->var['module'] = 'tax_annualization'; 		
		
		$year = date("Y");
        $this->var['start_year']  = 2015;        
		$this->var['location']    = 'tax_annualization';		
        $this->var['cutoff_id']   = $eid;   
		$this->var['page_title']  = "Annualize Tax";			
		$this->var['period']      = $period;			
		$this->view->setTemplate('payroll/template.php');
		$this->view->render('annualized_tax/annualized_tax.php',$this->var);
	}

	function process_annual_tax()
	{
		$this->var['module'] = 'earnings'; 		
		
		$year = date("Y");
        $this->var['start_year']  = 2015;        
		$this->var['location']    = 'earnings';		          
		$this->var['page_title']  = "Annualized Tax";							
		$this->view->setTemplate('payroll/template.php');
		$this->view->render('annualized_tax/annualized_tax.php',$this->var);
	}

	function process_tax()
	{
		Jquery::loadMainInlineValidation2();
		Jquery::loadMainJqueryFormSubmit();	
		Jquery::loadMainTipsy();		

		$year = date("Y");
		$c    = G_Cutoff_Period_Finder::findAllByYear($year);        		

		$this->var['recent'] = 'class="selected"';				
		$this->var['module'] = 'earnings'; 				
		$this->var['cutoff_periods'] = $c;
		$this->var['token']			 = Utilities::createFormToken();
        $this->var['start_year']     = 2014;
        $this->var['end_year']		 = date("Y");
        $this->var['months']         = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');  		
		$this->var['page_title']     = "Process Annual Tax";					
		$this->view->setTemplate('payroll/template.php');
		$this->view->render('annualized_tax/forms/annualize_tax.php',$this->var);
	}
	
	function _process_annual_tax()
	{
		$data   = $_POST;		
		$year   = date('Y');
		$bonus  = new G_Yearly_Bonus();
		$bonus->setMonthStart($data['start_month']);
		$bonus->setMonthEnd($data['end_month']);			

		if( $data['use-import-file'] == 'Yes' ){	
		 	$yearly_bonus_data = ['year' => $year, 'cutoff' => $data['cutoff_period'], 'file' => $_FILES['yearly_bonus_file']['tmp_name']];					
			$json   = $bonus->importYearlyBonus($yearly_bonus_data);
		}else{			
			$yearly_bonus_data = ['year' => $year, 'cutoff' => $data['cutoff_period'], 'action' => 2, 'selected' => array()];					
			$json   = $bonus->processYearlyBonus($yearly_bonus_data);
		}

		echo json_encode($json);
	}

	function _process_tax()
	{
		$data = $_POST;

		$tax = new G_Annualize_Tax();
		$tax->setYear(date("Y"));	
		$tax->setCutoffPeriod($data['cutoff_period']);			

		if( date("Y") == 2016 ) {
			$json = $tax->setDefaultFromAndEndDate()->validate($cutoff_period)->annualizeTaxCustomize();
			//$json = $tax->setDefaultFromAndEndDate()->validate($cutoff_period)->annualizeTax();
		} else {
			$json = $tax->setDefaultFromAndEndDate()->validate($cutoff_period)->annualizeTax();
		}
		
		echo json_encode($json);
	}

	function _load_annualize_tax_by_year() 
	{
		$year = $_GET['year'];
		$e    = new G_Employee();
		$data = $e->getAnnualizedTaxByYear($year);	
			
		$this->var['tax_data'] = $data;		
		$this->view->render('annualized_tax/_annualized_tax_list_dt.php',$this->var);
	}
}
?>