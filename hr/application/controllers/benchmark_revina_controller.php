<?php
class Benchmark_Revina_Controller extends Controller
{
	function __construct() {
		parent::__construct();
        Loader::appMainUtilities();
		$this->c_date = Tools::getCurrentDateTime('Y-m-d H:i:s','Asia/Manila');
	}

    function _import_leave_credit() { //fromt attendance controller
    	ob_start();
        ini_set("memory_limit", "999M");
        set_time_limit(999999999999999999999);
        $file = $_FILES['file']['tmp_name'];
        $covered_year = $_POST['covered_year'];

        $l = new G_Employee_Leave_Available_Importer($file);
        $l->setYear($covered_year);
        $is_imported = $l->importLeaveCredit(); //function import

        if ($is_imported) {
            $return['is_imported'] = true;
            $return['message'] = 'Leave credits have been successfully added.';
        } else {
            $return['is_imported'] = false;
            $return['message'] = 'There was a problem importing the leave credits. Please contact the administrator.';
        }
        ob_clean();
		ob_end_flush();
        echo json_encode($return);
    }	

    function test_settings_leave_credit()
    {
        echo 'Test Class';
        //find and delete
        $slv = G_Settings_Leave_Credit_Finder::findById(1);        
        $slv->delete();

        exit;
        //find and update
        $slv = G_Settings_Leave_Credit_Finder::findById(1);
        $slv->setEmploymentYears('2st');
        $slv->setDefaultCredit(10);
        $slv->setLeaveId(2);       
        $slv->setEmploymentStatusId(9);
        $slv->setIsArchived("Yes"); 
        $update = $slv->save();        

        echo '<pre>';
        print_r($update);
        echo '</pre>';
        exit;

        //save
        $slv = new G_Settings_Leave_Credit();
        $slv->setEmploymentYears('1st');
        $slv->setDefaultCredit(5);
        $slv->setLeaveId(1);       
        $slv->setEmploymentStatusId(2);        
        $slv->setIsArchived("No"); 
        $saved = $slv->save();

        echo '<pre>';
        print_r($saved);
        echo '</pre>';
    }

    function test_settings_leave_general()
    {
        exit;
        echo "Test Leave General";
        $slv = G_Settings_Leave_General_Finder::findById(3);
        $slv->delete();
        exit;
        $slv = G_Settings_Leave_General_Finder::findById(1);
        $slv->setConvertLeaveCriteria(8);
        $slv->setLeaveId(2);       
        $saved = $slv->save();
        echo '<pre>';
        print_r($slv);
        echo '</pre>';
        exit;
        $slv = new G_Settings_Leave_General();
        $slv->setConvertLeaveCriteria(3);
        $slv->setLeaveId(1);       
        $saved = $slv->save();

        echo '<pre>';
        print_r($saved);
        echo '</pre>';        
    }

    function test_update_employee_leave_credit()
    {
        echo 'Employee Leave Credit Test <hr />';

        $gela = new G_Employee_Leave_Available();
        $gela->getAllEmployeesEntitledForLeaveIncrease()->saveAllEmployeeWithLeaveIncrease();
    }

    function test_all_update_employee_leave_credit()
    {
        //echo 'Employee Leave Credit With General Rule<hr />';
        $slg = new G_Settings_Leave_General();
        $apply_credits = $slg->getAllUnusedLeaveCreditLastYear()->applyGeneralRule()->applyCredits();   

        Utilities::displayArray($apply_credits);
    }

    function test_payslip_template()
    {
        echo 'Payslip Template Test <hr />';

        $d_template = G_Payslip_Template_Helper::defaultTemplate();
        echo '<pre>';
        print_r($d_template);
        echo '</pre>';
        exit;
        $template = G_Payslip_Template_Finder::findById(1);

        echo '<pre>';
        print_r($template);
        echo '</pre>';
        exit;

        $slv = new G_Payslip_Template();
        $slv->setTemplateName("Template 03");
        $slv->setIsDefault(G_Payslip_Template::IS_DEFAULT_NO);       
        $saved = $slv->save();

        echo '<pre>';
        print_r($saved);
        echo '</pre>';        
    }

    function test_clear_template() {
        echo 'Test Reset Template <hr />';
        $gpt = G_Payslip_Template_Finder::findById(2);
        $gpt->setIsDefault(G_Payslip_Template::IS_DEFAULT_YES);       

        echo '<pre>';
        print_r($gpt);
        echo '</pre>';

        $gpt->clearDefaultTemplate()->save();

    }

    function test_employee_new_class() {
        echo 'Test Employee Leave Request <hr />';

        $el = new G_Employee_Leave_Request();
        $el->checkGeneralRule();
        if(!empty($el->default_general_rule) || $el->default_general_rule > 0) {
            echo 'My General Rule';
        } else {
            echo 'Walang General Rule';
        }
    }

    function test_generate_payslip_array() {
        echo 'Generate Payslip Array <hr />';
        
        $p = new G_Payslip(); 

        $p->wrapPayslipArray();   
    }

    function employeeLeaveCreditHistory()
    {
        echo 'Employee Leave Credit History <hr />';

        $eid = 7;
        $leave_id = 7;
        $added = 99;
        $h = new G_Employee_Leave_Credit_History();
        $h->setEmployeeId($eid);
        $h->setLeaveId($leave_id);
        $h->setCreditsAdded($added);
        $return = $h->addToHistory();

        Utilities::displayArray($return);
    }  

    function update_attendance() {
        echo 'Update Attendance <hr />';
        $emp_id = 67;
        $from = "2016-08-30";
        $to   = "2016-08-30";
        //$is_updated = G_Attendance_Helper::updateAttendanceByPeriod($from, $to);
        $is_updated = G_Attendance_Helper::updateAttendanceByEmployeeIdPeriod($emp_id, $from, $to);
        if($is_updated) {
            echo 'Successfully';
        } else {
            echo 'Not';
        }
    }  

    function test_tardiness_with_breakin_breakout()
    {
        echo 'Tardiness with Breakin/Breakout <hr />';

        $scheduled_time_in = '07:30:00';
        $scheduled_time_out = '17:00:00';
        $actual_time_in = '07:49:00';
        $actual_time_out = '18:56:00';      
        
        if (Tools::isTimeNightShift($scheduled_time_in)) {
            $break_time_in = '00:00:00';
            $break_time_out = '01:00:00';
        } else {
            $break_time_in = '12:00:00';
            $break_time_out = '12:45:00';
        }
        $l = new Late_Calculator;
        $l->setGracePeriod(0);
        $l->setBreakTimeIn($break_time_in);
        $l->setBreakTimeOut($break_time_out);           
        $l->setScheduledTimeIn($scheduled_time_in);
        $l->setScheduledTimeOut($scheduled_time_out);
        $l->setActualTimeIn($actual_time_in);
        $l->setActualTimeOut($actual_time_out);
        echo $late_hours = $l->computeLateHours();
        
        echo '<pre>';
        print_r($l);      
        echo '<pre />';

        echo '<hr />';
        $employee_id = 6;
        $date_from   = '2016-06-21';
        $date_to     = '2016-06-21';

        $breaktime_late = G_Employee_Breaktime_Helper::getLateHoursByEmployeeIdPeriod($employee_id, $date_from, $date_to);
        echo $breaktime_late;
        echo '<br />';
        echo round($breaktime_late,2);
    }

    function checkBirthdayLeave() {
        echo 'Check for Birthday Leave <hr />';
    }

    function testClassEmployeeStatusHistory()
    {
        echo "Employee Status History <hr />";

        $h = G_Employee_Status_History_Finder::findById(2);
        $h->delete();
        exit;

        //$h = new G_Employee_Status_History();
        $h = G_Employee_Status_History_Finder::findById(2);
        $h->setEmployeeId(111);
        $h->setEmployeeStatusId(111);
        $h->setStatus('InactiveUp');
        $h->setStartDate('Update');
        $h->setEndDate('testup');
        $return = $h->save();        
    }

    function checkInactiveEmployeeHistory()
    {
        echo 'Inactive Employee History Finder <hr />';

        $start_date = "2016-07-5";
        $end_date   = "2016-07-15";

        $inactive = G_Employee_Status_History_Finder::findInactiveEmployeeInBetweenDates($start_date, $end_date);

        echo '<br />';

        foreach($inactive as $pkey => $pkeyd) {
            $employee_id = $pkeyd->getEmployeeId();

            echo $employee_id;
            echo '<br />';
        }        

        echo '<pre>';
        print_r($inactive);
        echo '</pre>';
    }

    function checkAttendance()
    {
        echo 'Check Attendance <hr />';

        $e = G_Employee_Finder::findById(179);
        $date = "2016-08-04";
        $update = G_Attendance_Helper::updateAttendance($e, $date);

    }    

    function test_nighshift_calculator() {
        echo 'test nightshift<hr />';
        $ns = new Nightshift_Calculator;
        $ns->setScheduledTimeIn('00:30:00');
        $ns->setScheduledTimeOut('04:00:00');
        //$ns->setOvertimeIn($overtime_in);
        //$ns->setOvertimeOut($overtime_out);
        $ns->setActualTimeIn('00:28:00');
        $ns->setActualTimeOut('04:15:00');
        $ns_hours = $ns->compute();
       
        print_r($ns_hours);
    }    

    function test() {
        echo 'test break in out<hr />';
        $e = G_Employee_Finder::findById(67);
        $a = G_Attendance_Finder::findByEmployeeAndDate($e, '2016-08-30');

        $break = G_Employee_Breaktime_Finder::findByEmployeeIdAndDate(67,'2016-08-30');

        echo '<pre>';
        print_r($break);
        echo '</pre>';

    }

    function udateNotification() {
        echo 'Update Notification <hr />';
        $end_of_contract = G_Employee_Helper::countEmployeeEndOfContract30Days();
        echo $end_of_contract;
    }
	
}
?>