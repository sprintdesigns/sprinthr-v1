<?php
class Notifications_Controller extends Controller
{
	function __construct()
	{
		parent::__construct();
		$this->sprintHdrMenu(G_Sprint_Modules::HR, 'reports');

		$this->login();
		Loader::appStyle('style.css');
		$this->company_structure_id = $_SESSION['sprint_hr']['company_structure_id'];
		$this->c_date  				= Tools::getCurrentDateTime('Y-m-d H:i:s','Asia/Manila');
		$this->validatePermission(G_Sprint_Modules::HR,'reports','reports_notifications');
	}

	function index()
	{
		$this->var['page_title'] = 'Notification';
		$this->view->setTemplate('template_notifications.php');
		$this->view->render('notifications/index.php',$this->var);    
	}

	function view_notification() {
		if(!empty($_GET['hash']) && !empty($_GET['eid'])){

			$eid = $_GET['eid'];
			$hid = $_GET['hash'];
			
			Utilities::verifyHash(Utilities::decrypt($eid),$hid);				
			$id = Utilities::decrypt($eid);						
			$n  = G_Notifications_Finder::findById($id);

			if($n){
				// Update status to SEEN
				if($n->getStatus() == G_Notifications::STATUS_NEW) {					
					$n->setStatus(G_Notifications::STATUS_SEEN);
					$n->save();
				}
				
				Jquery::loadMainJqueryDatatable();

				$this->var['n'] = $n;
				$this->var['page_title'] = 'Notification';
				$this->view->setTemplate('template_notifications.php');
				$this->view->render('notifications/view_notification.php',$this->var);  
			}else{
				redirect("notifications");
			}
		}else{
			redirect("notifications");
		}
	}

	function ini_user_modal() {
		//Cutoff Period
		$cp = new G_Cutoff_Period();
		$cutoff_periods = $cp->getAllCutOffPeriods();
		$this->var['cutoff_periods'] = $cutoff_periods;

		$this->view->noTemplate();
		$this->view->render('notifications/_ajax_ini_notifications.php',$this->var);  
	}

	function generate_excel() {
		if( !empty($_GET['eid'])){
			$id = Utilities::decrypt($_GET['eid']);			
		
			$n = G_Notifications_Finder::findById($id);
			if($n){				
				$filename = strtolower($n->getEventType());
				$filename = str_replace(' ','_',$filename);
				$excel_filename = "$filename.xls";
				$filename = "_{$filename}_excel.php";
				
				$this->var['report_name'] = $n->getEventType();
				$this->var['filename']    = $excel_filename; 
				$this->var['data']        = $n->getNotificationItems();
				$this->view->noTemplate();
				$this->view->render('notifications/excel/'.$filename,$this->var);  
			}else{
				redirect("notifications");
			}
		}else{
			redirect("notifications");
		}
	}

	function get_link() {
		if(!empty($_GET['module']) && !empty($_GET['emp_code'])) {
			$e = G_Employee_Finder::findByEmployeeCode($_GET['emp_code']);
			if($e) {
				$eid = Utilities::encrypt($e->getId());
				$hash = Utilities::createHash($e->getId());

				if($_GET['module'] == 'tardiness') {
					G_Cutoff_Period_Helper::addNewPeriod();

			        $now = date('Y-m-d');
			        $p = G_Cutoff_Period_Finder::findByDate($now);
			        if ($p) {
			            $hpid = Utilities::encrypt($p->getId());
			            $from_date = $p->getStartDate();
			            $to_date = $p->getEndDate();
			        }
					redirect('attendance/show_attendance?employee_id='.$eid.'&hash='.$hash.'&from='.$from_date.'&to='.$to_date);
				}elseif($_GET['module'] == 'incomplete_dtr') {
					$now = date('Y-m-d');
					$from_date = date('Y-m-01',strtotime($now));
					$to_date = date('Y-m-t',strtotime($now));
					redirect('attendance/attendance_logs_period?hpid='.$eid.'&from='.$from_date.'&to='.$to_date);
				}elseif($_GET['module'] == 'employee_with_no_schedule') {
					redirect('schedule/show_employee_schedule?eid='.$eid.'&hash='.$hash);
				}elseif($_GET['module'] == 'employee_with_incorrect_shift'){
					$from = $_GET['from'];
					$to   = $_GET['to'];
					redirect('attendance/show_attendance?employee_id='.$eid.'&hash='.$hash.'&from='.$from.'&to='.$to);
				}else{
					redirect('employee/profile?eid='.$eid.'&hash='.$hash.'#'.$_GET['module']);
				}

				
			}else{
				redirect('notifications');
			}
		}else{
			redirect('notifications');
		}
	}

	function _count_new_notifications() {
		$n = new G_Notifications();
        $n->updateNotifications();
        $return['new_notifications'] = $n->countNotifications();
        
        echo json_encode($return);
	}

	function _load_notification_list() {
		sleep(1);

		$n = new G_Notifications();
        $n->updateNotifications();        
        $this->var['employee'] 		= $n->getNotifications(G_Notifications::TYPE_EMPLOYEE);
        $this->var['attendance'] 	= $n->getNotifications(G_Notifications::TYPE_ATTENDANCE);
        $this->var['schedule'] 		= $n->getNotifications(G_Notifications::TYPE_SCHEDULE);
        $this->view->noTemplate();
		$this->view->render('notifications/_ajax_notification_list.php',$this->var);  
	}

	function _load_view_notification_item_list()
	{		
		sleep(1);
		$nid = Utilities::decrypt($_POST['notification_id']);
		$n   = G_Notifications_Finder::findById($nid);
		if($n) {
			$filename = strtolower($n->getEventType());
			$filename = str_replace(' ','_',$filename);
			$filename = "_{$filename}_dt.php";
			$this->var['n'] = $n;

			//FOR INCOMPLETE DTR ONLY
			$query['date_from'] = date("Y-m-01");
			$query['date_to'] 	= date("Y-m-t");
			$query['remark'] 	= 'all';
			if( $nid == 10 || $nid == 9 ){
				$this->var['inc_dtr_data'] = G_Employee_Helper::getIncompleteTimeInOutData($query);
			}

			//FOR NO BANK ACCOUNT
			if( $nid == 14 ){
					$this->var['employees'] = G_Employee_Helper::employeeWithNoBankAccount();
			}

			$this->view->render('notifications/'.$filename,$this->var);
		}	
	}

	function ajax_load_incomplete_requirements_list_dt()
	{
		Utilities::ajaxRequest();
		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE);
		$dt->setSQL("
			SELECT e.id, er.id as requirement_id, CONCAT(' <a target=\"_blank\" href=" . url("notifications/get_link?module=requirements&emp_code=' , e.employee_code , '") . "  > ', e.lastname , ', ' , e.firstname , '</a>') as employee_name, esh.name as department_name, er.requirements as incomplete_requirement
            FROM " . EMPLOYEE . " e
            	LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id 
            	LEFT JOIN " . G_EMPLOYEE_REQUIREMENTS . " er ON e.id = er.employee_id 		
		");		
		$dt->setCountSQL("SELECT COUNT(e.id) as c FROM " . EMPLOYEE . " e LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id LEFT JOIN " . G_EMPLOYEE_REQUIREMENTS . " er ON e.id = er.employee_id ");	
		$dt->setCondition(" e.id NOT IN (SELECT employee_id FROM ". G_EMPLOYEE_REQUIREMENTS ." WHERE is_complete = 1)");
		$dt->setColumns('employee_name,department_name,incomplete_requirement');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"incomplete_requirement" => "er.requirements LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"department_name" => "esh.name LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}

	function ajax_load_no_salary_rate_list_dt()
	{
		Utilities::ajaxRequest();
		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE);
		$dt->setSQL("
			SELECT e.id, CONCAT(' <a target=\"_blank\" href=" . url("notifications/get_link?module=compensation&emp_code=' , e.employee_code , '") . "  > ', e.lastname , ', ' , e.firstname , '</a>') as employee_name, esh.name as department_name, DATE_FORMAT(e.hired_date,'%M %d, %Y') as hired_date, e.employee_status_id
            FROM " . EMPLOYEE . " e
            	LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id 		
		");		
		$dt->setCountSQL("SELECT COUNT(e.id) as c FROM " . EMPLOYEE . " e LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id ");	
		$dt->setCondition(" e.employee_status_id = 1 AND e.id NOT IN (SELECT employee_id FROM " . G_EMPLOYEE_BASIC_SALARY_HISTORY . " WHERE end_date >= IF(end_date = '','',NOW()) ) ");
		$dt->setColumns('employee_name,department_name,hired_date');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"department_name" => "esh.name LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"hired_date" => "DATE_FORMAT(e.hired_date,'%M %d, %Y') LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}
	
	function ajax_load_end_of_contract_list_dt()
	{
		Utilities::ajaxRequest(); 
		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE);
		$dt->setSQL("
			SELECT e.id, CONCAT(' <a target=\"_blank\" href=" . url("notifications/get_link?module=employment_status&emp_code=' , e.employee_code , '") . "  > ', e.lastname , ', ' , e.firstname , '</a>') as employee_name, esh.name as department_name, DATE_FORMAT(e.hired_date,'%M %d, %Y') as hired_date,
				DATE_FORMAT(esh.end_date ,'%M %d, %Y') as date_end_of_contract
            FROM " . EMPLOYEE . " e
            	LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id 		
		");		
		$dt->setCountSQL("SELECT COUNT(e.id) as c FROM " . EMPLOYEE . " e LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id ");	

		$start_date = date("Y-m-01");
		$end_date = date("Y-m-d", strtotime("+30 days"));
		$dt->setCondition(" (esh.end_date >= '{$start_date}' AND esh.end_date <= '{$end_date}') AND esh.type = ". Model::safeSql(G_Employee_Subdivision_History::DEPARTMENT). " AND e.employee_status_id = ". Model::safeSql(4). " ");
		$dt->setColumns('employee_name,department_name,hired_date,date_end_of_contract');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"department_name" => "esh.name LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"hired_date" => "DATE_FORMAT(e.hired_date,'%M %d, %Y') LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"date_end_of_contract" => "DATE_FORMAT(esh.end_date,'%M %d, %Y') LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}

	function ajax_load_no_department_list_dt()
	{
		Utilities::ajaxRequest();
		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE);
		$dt->setSQL("
			SELECT e.id, CONCAT(' <a target=\"_blank\" href=" . url("notifications/get_link?module=employment_status&emp_code=' , e.employee_code , '") . "  > ', e.lastname , ', ' , e.firstname , '</a>') as employee_name, DATE_FORMAT(e.hired_date,'%M %d, %Y') as hired_date
            FROM " . EMPLOYEE . " e	
		");		
		$dt->setCountSQL("SELECT COUNT(e.id) as c FROM " . EMPLOYEE . " e ");	
		$dt->setCondition(" e.employee_status_id = 1 AND e.id NOT IN (SELECT employee_id FROM " . G_EMPLOYEE_SUBDIVISION_HISTORY . " WHERE end_date >= IF( end_date = '', '', NOW( ) ) AND type = ". Model::safeSql(G_Employee_Subdivision_History::DEPARTMENT). ")");
		$dt->setColumns('employee_name,hired_date');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"hired_date" => "DATE_FORMAT(e.hired_date,'%M %d, %Y') LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}

	function ajax_load_no_job_title_list_dt()
	{
		Utilities::ajaxRequest();
		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE);
		$dt->setSQL("
			SELECT e.id, CONCAT(' <a target=\"_blank\" href=" . url("notifications/get_link?module=employment_status&emp_code=' , e.employee_code , '") . "  > ', e.lastname , ', ' , e.firstname , '</a>') as employee_name, DATE_FORMAT(e.hired_date,'%M %d, %Y') as hired_date, esh.name as department_name
            FROM " . EMPLOYEE . " e	
            LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id 	
		");		
		$dt->setCountSQL("SELECT COUNT(e.id) as c FROM " . EMPLOYEE . " e LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id ");	
		$dt->setCondition(" e.id NOT IN (SELECT employee_id FROM " . G_EMPLOYEE_JOB_HISTORY . " WHERE end_date >= IF( end_date = '', '', '' ) ) ");
		$dt->setColumns('employee_name,department_name,hired_date');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"department_name" => "esh.name LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"hired_date" => "DATE_FORMAT(e.hired_date,'%M %d, %Y') LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}

	function ajax_load_no_employment_status_list_dt()
	{
		Utilities::ajaxRequest();
		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE);
		$dt->setSQL("
			SELECT e.id, CONCAT(' <a target=\"_blank\" href=" . url("notifications/get_link?module=employment_status&emp_code=' , e.employee_code , '") . "  > ', e.lastname , ', ' , e.firstname , '</a>') as employee_name, DATE_FORMAT(e.hired_date,'%M %d, %Y') as hired_date, esh.name as department_name
            FROM " . EMPLOYEE . " e	
            LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id 	
		");		
		$dt->setCountSQL("SELECT COUNT(e.id) as c FROM " . EMPLOYEE . " e LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id ");	
		$dt->setCondition(" e.id NOT IN (SELECT employee_id FROM " . G_EMPLOYEE_JOB_HISTORY . " WHERE employment_status <> '' ) ");
		$dt->setColumns('employee_name,department_name,hired_date');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"department_name" => "esh.name LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"hired_date" => "DATE_FORMAT(e.hired_date,'%M %d, %Y') LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}

	function ajax_load_no_employee_status_list_dt()
	{
		Utilities::ajaxRequest();
		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE);
		$dt->setSQL("
			SELECT e.id, CONCAT(' <a target=\"_blank\" href=" . url("notifications/get_link?module=employment_status&emp_code=' , e.employee_code , '") . "  > ', e.lastname , ', ' , e.firstname , '</a>') as employee_name, DATE_FORMAT(e.hired_date,'%M %d, %Y') as hired_date, esh.name as department_name
            FROM " . EMPLOYEE . " e	
            LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id 	
		");		
		$dt->setCountSQL("SELECT COUNT(e.id) as c FROM " . EMPLOYEE . " e LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id ");	
		$dt->setCondition(" e.employee_status_id = 0 ");
		$dt->setColumns('employee_name,department_name,hired_date');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"department_name" => "esh.name LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"hired_date" => "DATE_FORMAT(e.hired_date,'%M %d, %Y') LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}

	function ajax_load_employee_with_undertime_list_dt()
	{
		Utilities::ajaxRequest();

		$s_from = date("Y-m-d",strtotime("-1 day"));
        $s_to   = date("Y-m-d");

		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE);
		/*$dt->setSQL("
			SELECT a.id, CONCAT(e.firstname, ' ', e.lastname)AS employee_name, DATE_FORMAT(a.date_attendance,'%M %d, %Y') date_attendance, CONCAT(a.scheduled_date_in, ' ', a.scheduled_time_in)AS scheduled_in, CONCAT(a.scheduled_date_out, ' ', a.scheduled_time_out)AS scheduled_out,
				CONCAT(a.actual_date_in, ' ', a.actual_time_in)AS actual_in, CONCAT(a.actual_date_out, ' ', a.actual_time_out)AS actual_out,
				a.undertime_hours
            FROM " . G_EMPLOYEE_ATTENDANCE . " a 	
            	LEFT JOIN " . EMPLOYEE . " e ON a.employee_id = e.id 
		");*/
		$dt->setSQL("
			SELECT a.id, CONCAT(e.firstname, ' ', e.lastname)AS employee_name, DATE_FORMAT(a.date_attendance,'%M %d, %Y') date_attendance, a.scheduled_time_in, a.scheduled_time_out,a.actual_time_in, a.actual_time_out,a.undertime_hours
            FROM " . G_EMPLOYEE_ATTENDANCE . " a 	
            	LEFT JOIN " . EMPLOYEE . " e ON a.employee_id = e.id 
		");		
		$dt->setCountSQL("SELECT COUNT(a.id) as c FROM " . G_EMPLOYEE_ATTENDANCE . " a LEFT JOIN " . EMPLOYEE . " e ON a.employee_id = e.id ");	
		$dt->setCondition("((a.undertime_hours <> 0 OR a.undertime_hours <> '' )AND (STR_TO_DATE(CONCAT(a.scheduled_date_out, ' ', a.scheduled_time_out), '%Y-%m-%d %H:%i:%s') > STR_TO_DATE(CONCAT(a.actual_date_out, ' ', a.actual_time_out), '%Y-%m-%d %H:%i:%s'))) AND a.date_attendance BETWEEN " . Model::safeSql($s_from) . " AND " . Model::safeSql($s_to));
		//$dt->setColumns('employee_name,date_attendance,scheduled_in,scheduled_out,actual_in,actual_out,undertime_hours');	
		$dt->setColumns('employee_name,date_attendance,scheduled_time_in,scheduled_time_out,actual_time_in,actual_time_out,undertime_hours');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}

	function ajax_load_employee_with_yearly_leave_increase_dt()
	{
		Utilities::ajaxRequest();

		$current_year = date("Y");

		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE_LEAVE_CREDIT_HISTORY);		
		$dt->setSQL("
			SELECT lc.id, CONCAT(e.firstname, ' ', e.lastname)AS employee_name, l.name AS leave_type, lc.credits_added
            FROM " . EMPLOYEE_LEAVE_CREDIT_HISTORY . " lc 	
            	LEFT JOIN " . EMPLOYEE . " e ON lc.employee_id = e.id 
            	LEFT JOIN " . G_LEAVE . " l ON lc.leave_id = l.id
		");		
		$dt->setCountSQL("SELECT COUNT(lc.id) as c FROM " . EMPLOYEE_LEAVE_CREDIT_HISTORY . " lc LEFT JOIN " . EMPLOYEE . " e ON lc.employee_id = e.id LEFT JOIN " . G_LEAVE . " l ON lc.leave_id = l.id");	
		$dt->setCondition("DATE_FORMAT(lc.date_added,'%Y') = " . Model::safeSql($current_year));		
		$dt->setColumns('employee_name,leave_type,credits_added');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"leave_type" => "l.name LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"></div>'
		));		
		echo $dt->constructDataTableRightTools();
	}

	function ajax_load_employee_with_early_in_list_dt()
	{
		Utilities::ajaxRequest();

		$s_from = date("Y-m-d",strtotime("-1 day"));
        $s_to   = date("Y-m-d");

		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE);
		/*$dt->setSQL("
			SELECT a.id, CONCAT(e.firstname, ' ', e.lastname)AS employee_name, DATE_FORMAT(a.date_attendance,'%M %d, %Y') date_attendance, CONCAT(a.scheduled_date_in, ' ', a.scheduled_time_in)AS scheduled_in, CONCAT(a.scheduled_date_out, ' ', a.scheduled_time_out)AS scheduled_out,
				CONCAT(a.actual_date_in, ' ', a.actual_time_in)AS actual_in, CONCAT(a.actual_date_out, ' ', a.actual_time_out)AS actual_out,
				a.undertime_hours
            FROM " . G_EMPLOYEE_ATTENDANCE . " a 	
            	LEFT JOIN " . EMPLOYEE . " e ON a.employee_id = e.id 
		");*/
		$dt->setSQL("
			SELECT a.id, CONCAT(e.firstname, ' ', e.lastname)AS employee_name, DATE_FORMAT(a.date_attendance,'%M %d, %Y') date_attendance, a.scheduled_time_in, a.scheduled_time_out,a.actual_time_in, a.actual_time_out
            FROM " . G_EMPLOYEE_ATTENDANCE . " a 	
            	LEFT JOIN " . EMPLOYEE . " e ON a.employee_id = e.id 
		");		
		$dt->setCountSQL("SELECT COUNT(a.id) as c FROM " . G_EMPLOYEE_ATTENDANCE . " a LEFT JOIN " . EMPLOYEE . " e ON a.employee_id = e.id ");	
		$dt->setCondition("STR_TO_DATE(CONCAT(a.scheduled_date_in, ' ', a.scheduled_time_in), '%Y-%m-%d %H:%i:%s') >
                        STR_TO_DATE(CONCAT(a.actual_date_in, ' ', a.actual_time_in), '%Y-%m-%d %H:%i:%s') AND a.date_attendance BETWEEN " . Model::safeSql($s_from) . " AND " . Model::safeSql($s_to));
		//$dt->setColumns('employee_name,date_attendance,scheduled_in,scheduled_out,actual_in,actual_out,undertime_hours');	
		$dt->setColumns('employee_name,date_attendance,scheduled_time_in,scheduled_time_out,actual_time_in,actual_time_out');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}

	function ajax_load_employee_with_no_bank_account_dt()
	{
		Utilities::ajaxRequest();
		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE);		
		$dt->setSQL("
			SELECT e.id, CONCAT(' <a target=\"_blank\" href=" . url("notifications/get_link?module=profile&emp_code=' , e.employee_code , '") . "  > ', e.lastname , ', ' , e.firstname , '</a>') as employee_name
            FROM " . EMPLOYEE . " e 	            	
		");		
		$dt->setCountSQL("SELECT COUNT(e.id) as c FROM " . EMPLOYEE . " e");	
		$dt->setCondition("
			NOT EXISTS(
				SELECT null 
				FROM " . G_EMPLOYEE_DIRECT_DEPOSIT . " dd 
			  WHERE dd.employee_id = e.id
			)
		");		
		$dt->setColumns('employee_name');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}

	function ajax_load_tardiness_list_dt()
	{
		Utilities::ajaxRequest();
		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE);
		$dt->setSQL("
			SELECT e.id, CONCAT(' <a target=\"_blank\" href=" . url("notifications/get_link?module=tardiness&emp_code=' , e.employee_code , '") . "  > ', e.lastname , ', ' , e.firstname , '</a>') as employee_name, ea.late_hours, esh.name as department_name
            FROM " . EMPLOYEE . " e	
            LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id 	
            LEFT JOIN " . G_EMPLOYEE_ATTENDANCE . " ea ON ea.employee_id = e.id
		");		
		$dt->setCountSQL("SELECT COUNT(e.id) as c FROM " . EMPLOYEE . " e LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id LEFT JOIN " . G_EMPLOYEE_ATTENDANCE . " ea ON ea.employee_id = e.id ");	
		$dt->setCondition(" ea.date_attendance = CURDATE() AND ea.late_hours <> ''  ");
		$dt->setColumns('employee_name,department_name,late_hours');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"department_name" => "esh.name LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}

	function ajax_load_incomplete_dtr_list_dt()
	{
		Utilities::ajaxRequest();
		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(G_EMPLOYEE_ATTENDANCE);
		$dt->setSQL("
			SELECT e.id, CONCAT(' <a target=\"_blank\" href=" . url("notifications/get_link?module=incomplete_dtr&emp_code=' , e.employee_code , '") . "  > ', e.lastname , ', ' , e.firstname , '</a>') as employee_name, esh.name as department_name, 
				DATE_FORMAT(ea.date_attendance,'%M %d, %Y') as date_attendance, 
				ea.actual_time_in, ea.actual_time_out
            FROM " . G_EMPLOYEE_ATTENDANCE . " ea	
            LEFT JOIN " . EMPLOYEE . " e ON ea.employee_id = e.id
            LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id 	   
		");		
		$dt->setCountSQL("SELECT COUNT(e.id) as c FROM " . G_EMPLOYEE_ATTENDANCE . " ea	LEFT JOIN " . EMPLOYEE . " e ON ea.employee_id = e.id LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id ");	
		$dt->setCondition(" MONTH(ea.date_attendance) = MONTH(NOW()) AND ((ea.actual_time_in = '' AND ea.actual_time_out <> '') OR (ea.actual_time_out = '' AND ea.actual_time_in <> '')) ");
		$dt->setColumns('employee_name,department_name,date_attendance,actual_time_in,actual_time_out');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"department_name" => "esh.name LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"date_attendance" => "DATE_FORMAT(ea.date_attendance,'%M %d, %Y') LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}

	function ajax_load_employee_with_no_schedule_list_dt()
	{
		Utilities::ajaxRequest();
		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE);
		$dt->setSQL("
			SELECT e.id, CONCAT(' <a target=\"_blank\" href=" . url("notifications/get_link?module=employee_with_no_schedule&emp_code=' , e.employee_code , '") . "  > ', e.lastname , ', ' , e.firstname , '</a>') as employee_name, esh.name as department_name
            FROM " . EMPLOYEE . " e	
            LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id 	
		");		
		$dt->setCountSQL("SELECT COUNT(e.id) as c FROM " . EMPLOYEE . " e LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id ");	
		$dt->setCondition(" 
				e.department_company_structure_id NOT IN (
            		SELECT employee_group_id
					FROM ". G_EMPLOYEE_GROUP_SCHEDULE ." es, ". G_SCHEDULE_GROUP ." g
					WHERE es.schedule_group_id = g.id
					AND es.employee_group = ". Model::safeSql(ENTITY_GROUP) ."
		            AND g.end_date >= NOW()
                ) AND e.id NOT IN (
            		SELECT employee_group_id
					FROM ". G_EMPLOYEE_GROUP_SCHEDULE ." es, ". G_SCHEDULE_GROUP ." g
					WHERE es.schedule_group_id = g.id
					AND es.employee_group = ". Model::safeSql(ENTITY_EMPLOYEE) ."
		            AND g.end_date >= NOW() )
		 ");
		$dt->setColumns('employee_name,department_name');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"department_name" => "esh.name LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}

	/*
	 * Deprecated Date: 01/31/2017
	*/
	function ajax_load_employee_with_incorrect_shift_list_dtDepre()
	{
		Utilities::ajaxRequest();

		$date   = date("Y-m-d",strtotime($this->c_date));
        $cutoff = new G_Cutoff_Period();
        $period = $cutoff->getCurrentCutoffPeriod($date);
        $date_from = $period['current_cutoff']['start'];
        $date_to   = $period['current_cutoff']['end'];

		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE);
		$dt->setSQL("
			SELECT e.employee_code, CONCAT(' <a target=\"_blank\" href=" . url("notifications/get_link?module=employee_with_incorrect_shift&from=" . $date_from . "&to=" . $date_to . "&emp_code=' , e.employee_code , '") . "  > ', e.lastname , ', ' , e.firstname , '</a>') as employee_name,DATE_FORMAT(ea.date_attendance,'%M %d, %Y')AS date_attendance, esh.name as department_name 
			FROM " . G_EMPLOYEE_ATTENDANCE . " ea 
				LEFT JOIN " . EMPLOYEE . " e ON ea.employee_id = e.id 
				LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON ea.employee_id = esh.employee_id
		");		
		$dt->setCondition("(ea.date_attendance BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . ") AND (ea.is_present = 1 AND ea.is_restday = 1 AND esh.end_date = '') OR ( ea.scheduled_date_out <> '' AND ea.actual_date_in <> '' AND STR_TO_DATE(CONCAT(ea.scheduled_date_out, ' ', ea.scheduled_time_out), '%Y-%m-%d %H:%i:%s') <= STR_TO_DATE(CONCAT(ea.actual_date_in, ' ', ea.actual_time_in), '%Y-%m-%d %H:%i:%s' AND esh.end_date = '') )");
		$dt->setColumns('employee_name,department_name,date_attendance');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"department_name" => "esh.name LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}

	/*$sql = "SELECT e.employee_code, CONCAT(e.firstname, ', ', e.lastname)AS employee_name, DATE_FORMAT(ea.date_attendance,'%M %d, %Y')AS date_attendance 
				FROM g_employee_attendance ea LEFT JOIN g_employee e ON ea.employee_id = e.id 
				WHERE (ea.date_attendance BETWEEN '2017-01-16' AND '2017-01-31') 
				AND (ea.is_present = 1 AND ea.is_restday = 1 AND (ea.scheduled_date_in = '' OR ea.scheduled_date_out = '' ) ) 
				OR ( (ea.date_attendance BETWEEN '2017-01-16' AND '2017-01-31') 
				AND ea.scheduled_date_out <> '' AND ea.actual_date_in <> '' 
				AND STR_TO_DATE(CONCAT(ea.scheduled_date_out, ' ', ea.scheduled_time_out), '%Y-%m-%d %H:%i:%s') <= STR_TO_DATE(CONCAT(ea.actual_date_in, ' ', ea.actual_time_in), '%Y-%m-%d %H:%i:%s') ) 
				ORDER BY ea.date_attendance ASC ";*/

	function ajax_load_employee_with_incorrect_shift_list_dt()
	{
		Utilities::ajaxRequest();

		$date   = date("Y-m-d",strtotime($this->c_date));
        $cutoff = new G_Cutoff_Period();
        $period = $cutoff->getCurrentCutoffPeriod($date);
        $date_from = $period['current_cutoff']['start'];
        $date_to   = $period['current_cutoff']['end'];

		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE);
		$dt->setSQL("
			SELECT e.employee_code, CONCAT(' <a target=\"_blank\" href=" . url("notifications/get_link?module=employee_with_incorrect_shift&from=" . $date_from . "&to=" . $date_to . "&emp_code=' , e.employee_code , '") . "  > ', e.lastname , ', ' , e.firstname , '</a>') as employee_name,DATE_FORMAT(ea.date_attendance,'%M %d, %Y')AS date_attendance, esh.name as department_name 
			FROM " . G_EMPLOYEE_ATTENDANCE . " ea 
			LEFT JOIN " . EMPLOYEE . " e ON ea.employee_id = e.id 
			LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON ea.employee_id = esh.employee_id
		");		
		$dt->setCountSQL("
			SELECT e.employee_code, CONCAT(' <a target=\"_blank\" href=" . url("notifications/get_link?module=employee_with_incorrect_shift&from=" . $date_from . "&to=" . $date_to . "&emp_code=' , e.employee_code , '") . "  > ', e.lastname , ', ' , e.firstname , '</a>') as employee_name,DATE_FORMAT(ea.date_attendance,'%M %d, %Y')AS date_attendance, esh.name as department_name 
			FROM " . G_EMPLOYEE_ATTENDANCE . " ea 
			LEFT JOIN " . EMPLOYEE . " e ON ea.employee_id = e.id 
			LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON ea.employee_id = esh.employee_id
		");
		$dt->setCondition("
				esh.end_date = ''
				AND (ea.date_attendance BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to) . ")
			 	AND (ea.is_present = 1 AND ea.is_restday = 1 AND (ea.scheduled_date_in = '' OR ea.scheduled_date_out = '' ) )
			 	OR ( (ea.date_attendance BETWEEN " . Model::safeSql($date_from) . " AND " . Model::safeSql($date_to)  . ") 
			 	AND ea.scheduled_date_out <> '' AND ea.actual_date_in <> '' 
			 	AND STR_TO_DATE(CONCAT(ea.scheduled_date_out, ' ', ea.scheduled_time_out), '%Y-%m-%d %H:%i:%s') <= STR_TO_DATE(CONCAT(ea.actual_date_in, ' ', ea.actual_time_in), '%Y-%m-%d %H:%i:%s') ) 
		");

		$dt->setColumns('employee_name,department_name,date_attendance');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"department_name" => "esh.name LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}	

	function ajax_load_employee_upcoming_birthday_list_dt()
	{
		Utilities::ajaxRequest();
		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE);
		
		/*
		$dt->setSQL("
			SELECT e.id, CONCAT(e.lastname , ', ' , e.firstname , '</a>') as employee_name, DATE_FORMAT(e.birthdate,'%M %d, %Y') as birthdate, esh.name as department_name
            FROM " . EMPLOYEE . " e	
            LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id 	
		");		
		$dt->setCountSQL("SELECT COUNT(e.id) as c FROM " . EMPLOYEE . " e LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id ");
		*/	


		$dt->setSQL("
			SELECT e.id, CONCAT(e.lastname , ', ' , e.firstname , '</a>') as employee_name, DATE_FORMAT(e.birthdate,'%M %d, %Y') as birthdate, 
			COALESCE((
                    SELECT name FROM `g_employee_subdivision_history` esh
                    WHERE employee_id = e.id 
                    ORDER BY end_date ASC
                    LIMIT 1
                ))AS department_name
            FROM " . EMPLOYEE . " e	
            LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id 	
		");		

		//$dt->setCountSQL("SELECT COUNT(e.id) as c FROM " . EMPLOYEE . " e LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id ");	
		$dt->setCountSQL("SELECT COUNT(e.id) as c FROM " . EMPLOYEE . " e LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id ");	
		
		$start_date = date("m-d", strtotime("+1 day"));
		$end_date = date("m-d", strtotime("+30 days"));

		$dt->setCondition(" DATE_FORMAT(e.birthdate,'%m-%d') >= '{$start_date}' AND DATE_FORMAT(e.birthdate,'%m-%d') <= '{$end_date}' AND e.employee_status_id = 1 ");
		$dt->setColumns('employee_name,department_name,birthdate');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"department_name" => "esh.name LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"birthdate" => "DATE_FORMAT(e.birthdate,'%M %d, %Y') LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setSQLGroupBy('GROUP BY employee_name');
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}

	function ajax_load_employee_birthday_today_list_dt()
	{
		Utilities::ajaxRequest();
		$dt = new Main_Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(EMPLOYEE);
		$dt->setSQL("
			SELECT e.id, CONCAT(e.lastname , ', ' , e.firstname , '</a>') as employee_name, esh.name as department_name
            FROM " . EMPLOYEE . " e	
            LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id 	
		");		
		$dt->setCountSQL("SELECT COUNT(e.id) as c FROM " . EMPLOYEE . " e LEFT JOIN " . G_EMPLOYEE_SUBDIVISION_HISTORY . " esh ON e.id = esh.employee_id ");	
		
		$date = date("m-d");

		$dt->setCondition(" DATE_FORMAT(e.birthdate,'%m-%d') = '{$date}' AND e.employee_status_id = 1");
		$dt->setColumns('employee_name,department_name');	
		$dt->setPreDefineSearch(
			array(				
				"employee_name" => "e.lastname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%' OR e.firstname LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"department_name" => "esh.name LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'",
				"birthdate" => "DATE_FORMAT(e.birthdate,'%M %d, %Y') LIKE '%" . addslashes($_REQUEST['sSearch']) . "%'"
			)
		); 	 
		$dt->setSQLGroupBy('GROUP BY employee_name');
		$dt->setOrder('ASC');
		$dt->setSort(0);							
		$dt->setCustomColumn(
				array(		
						1 => '<div class=\"i_container\"><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></div>',		
						2 => '<div class=\"btn-group pull-right\"><a class=\"btn dropdown-toggle\" href=\"#\">Action <span class=\"caret\"></span></a><ul class=\"dropdown-menu\"><li><a href=\"' . url("company/edit_company?eid=id") . '\"><i class=\"icon-edit\"></i> Edit Company</a></li><li><a href=\"' . url("company/emp_list?eid=id") . '\"><i class=\"icon-list\"></i> View participants List</a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_archive_btn\" ><i class=\"icon-trash\"></i> Archive </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"company_mark_as_blacklisted_btn\" ><i class=\"icon-tag\"></i> Mark as Blacklisted </a></li><li><a href=\"javascript:void(0);\" id=\"e_id\" class=\"view_company_attended_btn\" ><i class=\"icon-list\"></i>  View Program Attended</a></li></ul></div>'
		));
		//echo "<pre>"; print_r($dt);
		echo $dt->constructDataTableRightTools();
	}

	function _leave_credit_update_notication()
	{
		$this->view->render('notifications/_leave_credit_update_notication.php',$this->var);  		
	}	

}
?>