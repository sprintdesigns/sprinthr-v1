<?php
class OB_Controller extends Controller
{
	function __construct()
	{	
		parent::__construct();

		Loader::appStyle('style.css');
		Loader::appMainScript('ob_admin.js');
		Loader::appMainScript('ob_admin_base.js');		
		Loader::appMainUtilities();

		$this->sprintHdrMenu(G_Sprint_Modules::HR, 'attendance');
		
		if($_GET['hpid']){
			$this->var['is_period_lock'] = $_SESSION['sprint_hr']['is_period_lock'] = G_Cutoff_Period_Helper::isPeriodLock($_GET['hpid']);
		}else{			
			$this->var['is_period_lock'] = $_SESSION['sprint_hr']['is_period_lock'];
		}
		
		if($_GET['from'] && $_GET['to'] && $_GET['hpid']){
			$this->var['download_url']    = url('reports/download_ob_request?from=' . $_GET['from'] . '&to=' . $_GET['to'] . '&hpid=' . $_GET['hpid']);
			$this->var['period_selected'] = '<small style="font-size:15px;">Period: <b>' . Tools::convertDateFormat($_GET['from']) . ' </b> to <b>' . Tools::convertDateFormat($_GET['to']) . '</b></small>';
		}
		
		$this->eid                  = $this->global_user_eid;
		$this->company_structure_id = $this->global_user_ecompany_structure_id;
		$this->c_date  				= Tools::getCurrentDateTime('Y-m-d H:i:s','Asia/Manila');
		$this->default_method       = 'index';					
		$this->var['leave']         = 'selected';			
		$this->var['employee']      = 'selected';
		$this->var['eid']           = $this->eid;	

		$this->var['departments']   = G_Company_Structure_Finder::findByParentID(Utilities::decrypt($this->global_user_ecompany_structure_id));

		$this->validatePermission(G_Sprint_Modules::HR,'attendance','');
	}
	
	function index()
	{			
		Jquery::loadMainInlineValidation2();
		Jquery::loadMainJqueryFormSubmit();		
		
		$this->var['pendings']   = 'class="selected"';				
		$this->var['module'] 	 = 'ob'; 		
				
		$period['to']   = $_GET['to'];
		$period['from'] = $_GET['from'];
		$period['hpid'] = $_GET['hpid'];
		
		$eid  = $_GET['hpid'];

		$btn_request_ob_config = array(
    		'module'				=> 'hr',
    		'parent_index'			=> 'attendance',
    		'child_index'			=> 'official_business',
    		'href' 					=> 'javascript:void(0);',
    		'onclick' 				=> 'javascript:show_add_ob_request_form("'.$period['from'].'","'.$period['to'].'");',
    		'id' 					=> 'add_ob_button',
    		'class' 				=> 'add_button',
    		'icon' 					=> '',
    		'additional_attribute'	=> '',
    		'caption' 				=> '<strong>+</strong><b>Request OB</b>'
    		); 

    	$btn_import_ob_config = array(
    		'module'				=> 'hr',
    		'parent_index'			=> 'attendance',
    		'child_index'			=> 'official_business',
    		'href' 					=> '#',
    		'onclick' 				=> 'javascript:importOBRequest("'.$period['from'].'","'.$period['to'].'");',
    		'id' 					=> 'import_undertime',
    		'class' 				=> 'add_button pull-right',
    		'icon' 					=> '<i class="icon-arrow-left"></i>',
    		'additional_attribute'	=> '',
    		'caption' 				=> 'Import OB'
    		); 
		
		$this->var['permission_action'] 	= $this->validatePermission(G_Sprint_Modules::HR,'attendance','official_business');
		$this->var['btn_request_ob'] 		= G_Button_Builder::createAnchorTagWithPermissionValidation($this->global_user_hr_actions, $btn_request_ob_config);
        $this->var['btn_import_ob'] 		= G_Button_Builder::createAnchorTagWithPermissionValidation($this->global_user_hr_actions, $btn_import_ob_config);
		
		if($eid){	
			Jquery::loadMainTipsy();
			Jquery::loadMainJqueryDatatable();
			Jquery::loadMainTextBoxList();
			$this->var['eid'] 		  = $eid;
			$this->var['period']      = $period;			
			$this->var['page_title']  = 'Official Business Requests ';
			$this->view->setTemplate('template_leftsidebar.php');

            $cutoff_id = Utilities::decrypt($_GET['hpid']);
            //$previous_cutoff = G_Cutoff_Period_Finder::findPreviousByCutoffId($cutoff_id);
            //$next_cutoff = G_Cutoff_Period_Finder::findNextByCutoffId($cutoff_id);
            $from_date = $_GET['from'];
       		$to_date   = $_GET['to'];

            $c  = new G_Cutoff_Period();
			$c->setId($cutoff_id);
			$next_cutoff_data = $c->getNextCutOff();
			$previous_cutoff_data = $c->getPreviousCutOff();

			$next_from = $next_cutoff_data['period_start'];
			$next_to   = $next_cutoff_data['period_end'];
			$next_id   = Utilities::encrypt($next_cutoff_data['id']);
			$this->var['next_cutoff_link'] = url("ob?from={$next_from}&to={$next_to}&hpid={$next_id}");
			if( !empty($next_from) ){
				$this->var['next_cutoff_link'] = url("ob?from={$next_from}&to={$next_to}&hpid={$next_id}");
			}else{
				$this->var['next_cutoff_link'] = url("ob?from={$from_date}&to={$to_date}&hpid={$eid}");
			}

			$previous_from = $previous_cutoff_data['period_start'];
			$previous_to   = $previous_cutoff_data['period_end'];
			$previous_id   = Utilities::encrypt($previous_cutoff_data['id']);			
			if( !empty($previous_from) ){
				$this->var['previous_cutoff_link'] = url("ob?from={$previous_from}&to={$previous_to}&hpid={$previous_id}");
			}else{
				$this->var['previous_cutoff_link'] = url("ob?from={$from_date}&to={$to_date}&hpid={$eid}");
			}

            /*if ($previous_cutoff) {
                $previous_from = $previous_cutoff->getStartDate();
                $previous_to = $previous_cutoff->getEndDate();
                $previous_id = Utilities::encrypt($previous_cutoff->getId());
                $this->var['previous_cutoff_link'] = url("ob?from={$previous_from}&to={$previous_to}&hpid={$previous_id}");
            }

            if ($next_cutoff) {
                $next_from = $next_cutoff->getStartDate();
                $next_to = $next_cutoff->getEndDate();
                $next_id = Utilities::encrypt($next_cutoff->getId());
                $this->var['next_cutoff_link'] = url("ob?from={$next_from}&to={$next_to}&hpid={$next_id}");
            }*/

			$this->view->render('ob/index.php',$this->var);
		}else{
			//$this->var['periods'] 	 = G_Payslip_Helper::getPeriods();
			//$this->var['page_title'] = 'Official Business Requests ';
			//$this->view->setTemplate('template.php');
			//$this->view->render('ob/payroll_period.php',$this->var);

            $now = date('Y-m-d');
            $p = G_Cutoff_Period_Finder::findByDate($now);
            if ($p) {
                $hpid = Utilities::encrypt($p->getId());
                $from_date = $p->getStartDate();
                $to_date = $p->getEndDate();
            }

            redirect("ob/period?from={$from_date}&to={$to_date}&hpid={$hpid}");
		}
	}
	
	function approved()
	{			
		Jquery::loadMainInlineValidation2();
		Jquery::loadMainJqueryFormSubmit();
		Jquery::loadMainTipsy();
		Jquery::loadMainJqueryDatatable();
		Jquery::loadMainTextBoxList();
		
		$this->var['approved']   = 'class="selected"';				
		$this->var['module'] 	 = 'ob'; 		
		
		$period['to']   = $_GET['to'];
		$period['from'] = $_GET['from'];
		$period['hpid'] = $_GET['hpid'];
		
		$eid  = $_GET['hpid'];
		
		if($eid){		
			$this->var['eid'] 		   = $eid;
			$this->var['period']       = $period;				
			$this->var['page_title']  = 'Official Business Requests ';
			$this->view->setTemplate('template_leftsidebar.php');

            $cutoff_id = Utilities::decrypt($_GET['hpid']);
            $previous_cutoff = G_Cutoff_Period_Finder::findPreviousByCutoffId($cutoff_id);
            $next_cutoff = G_Cutoff_Period_Finder::findNextByCutoffId($cutoff_id);

            if ($previous_cutoff) {
                $previous_from = $previous_cutoff->getStartDate();
                $previous_to = $previous_cutoff->getEndDate();
                $previous_id = Utilities::encrypt($previous_cutoff->getId());
                $this->var['previous_cutoff_link'] = url("ob/approved?from={$previous_from}&to={$previous_to}&hpid={$previous_id}");
            }

            if ($next_cutoff) {
                $next_from = $next_cutoff->getStartDate();
                $next_to = $next_cutoff->getEndDate();
                $next_id = Utilities::encrypt($next_cutoff->getId());
                $this->var['next_cutoff_link'] = url("ob/approved?from={$next_from}&to={$next_to}&hpid={$next_id}");
            }

			$this->view->render('ob/approved.php',$this->var);
		}else{
			redirect('ob');	
		}
	}

	function disapproved()
	{			
		Jquery::loadMainInlineValidation2();
		Jquery::loadMainJqueryFormSubmit();
		Jquery::loadMainTipsy();
		Jquery::loadMainJqueryDatatable();
		Jquery::loadMainTextBoxList();
		
		$this->var['disapproved'] = 'class="selected"';				
		$this->var['module'] 	  = 'ob'; 		
		
		$period['to']   = $_GET['to'];
		$period['from'] = $_GET['from'];
		$period['hpid'] = $_GET['hpid'];
		
		$eid  = $_GET['hpid'];
		
		if($eid){		
			$this->var['eid'] 		   = $eid;
			$this->var['period']       = $period;				
			$this->var['page_title']  = 'Official Business Requests ';
			$this->view->setTemplate('template_leftsidebar.php');

            $cutoff_id = Utilities::decrypt($_GET['hpid']);
            $previous_cutoff = G_Cutoff_Period_Finder::findPreviousByCutoffId($cutoff_id);
            $next_cutoff = G_Cutoff_Period_Finder::findNextByCutoffId($cutoff_id);

            if ($previous_cutoff) {
                $previous_from = $previous_cutoff->getStartDate();
                $previous_to = $previous_cutoff->getEndDate();
                $previous_id = Utilities::encrypt($previous_cutoff->getId());
                $this->var['previous_cutoff_link'] = url("ob/disapproved?from={$previous_from}&to={$previous_to}&hpid={$previous_id}");
            }

            if ($next_cutoff) {
                $next_from = $next_cutoff->getStartDate();
                $next_to = $next_cutoff->getEndDate();
                $next_id = Utilities::encrypt($next_cutoff->getId());
                $this->var['next_cutoff_link'] = url("ob/disapproved?from={$next_from}&to={$next_to}&hpid={$next_id}");
            }

			$this->view->render('ob/disapproved.php',$this->var);
		}else{
			redirect('ob');	
		}
	}
	
	function archives()
	{			
		Jquery::loadMainInlineValidation2();
		Jquery::loadMainJqueryFormSubmit();
		Jquery::loadMainTipsy();
		Jquery::loadMainJqueryDatatable();
		Jquery::loadMainTextBoxList();
		
		$this->var['archives']   = 'class="selected"';				
		$this->var['module'] 	 = 'ob'; 		
		
		$period['to']   = $_GET['to'];
		$period['from'] = $_GET['from'];
		$period['hpid'] = $_GET['hpid'];
		
		$eid  = $_GET['hpid'];
		
		if($eid){		
			$this->var['eid'] 		   = $eid;
			$this->var['period']       = $period;				
			$this->var['page_title']  = 'Official Business Requests ';
			$this->view->setTemplate('template_leftsidebar.php');
			$this->view->render('ob/archives.php',$this->var);
		}else{
			redirect('ob');	
		}
	}
	
	function html_import_ob() {
		$this->view->setTemplate('template_blank.php');
		$this->view->render('ob/html/html_import_ob.php', $this->var);	
	}	
	
	function import_ob_request()
	{
		ob_start();
		ini_set("memory_limit", "999M");
		set_time_limit(999999999999999999999);
		
		$file 	 = $_FILES['ob_file']['tmp_name'];
		$ob = new G_OB_Import($file);
		$ob->setCompanyStructureId(Utilities::decrypt($this->company_structure_id));
		
		$is_imported = $ob->import();		
		
		if ($is_imported) {
			$return['is_imported'] = true;
			$return['message']     = 'OB request has been successfully imported.';	
		} else {
			$return['is_imported'] = false;
			$return['message']     = 'There was a problem importing ob request. Please contact the administrator.';
		}
		ob_clean();
		ob_end_flush();
		echo json_encode($return);		
	}	
	
	function ajax_add_new_request() 
	{
		sleep(1);
		$this->var['from']		 = $_POST['from'];
		$this->var['to']		 = $_POST['to'];	 
		$this->var['token']		 = Utilities::createFormToken();		
		$this->var['page_title'] = 'Add New Request';		
		$this->view->render('ob/form/add_request.php',$this->var);		
	}

	function _load_get_employee_request_approvers() {
		
		if(!empty($_POST)) {
			$employee  = G_Employee_Finder::findById(Utilities::decrypt($_POST['h_employee_id']));
			if($employee) {				
				$employee_id = $employee->getId();
				$gra = new G_Request_Approver();
				$gra->setEmployeeId($employee_id);
				$approvers = $gra->getEmployeeRequestApprovers();

				$this->var['approvers'] = $approvers;		
				$this->view->render('ob/form/_show_request_approvers.php',$this->var);
			}		
		}
	}
	
	function ajax_edit_ob_request() 
	{
		$gobr = G_Employee_Official_Business_Request_Finder::findById(Utilities::decrypt($_POST['eid']));
		if($gobr){
			$e = G_Employee_Finder::findById($gobr->getEmployeeId());
			if( $e ){
				$this->var['employee_name'] = $e->getLastname() . ', ' . $e->getFirstname();
				$this->var['eid']       = Utilities::encrypt($e->getId());
				$this->var['gobr']	    = $gobr;
				$this->var['from']		= $_POST['date_from'];
				$this->var['to']		= $_POST['date_to'];
				$this->var['token']		= Utilities::createFormToken();
				$this->var['page_title']= 'Edit Earning';		
				$this->view->render('ob/form/ajax_edit_request.php',$this->var);
			}else{
				echo "<div class=\"alert alert-error\">Employee Record not found</div><br />";
			}
		}
	}
	
	function ajax_import_ob_request() 
	{	
		$this->var['action']	 = url('ob/import_earnings');			
		$this->view->render('ob/form/ajax_import_ob_request.php',$this->var);
	}
	
	function ajax_get_employees_autocomplete() 
	{
		$q = Model::safeSql(strtolower($_GET["search"]), false);
		
		if ($q != '') {
			$employees = G_Employee_Finder::searchByFirstnameAndLastname($q);
			
			foreach ($employees as $e) {
				$response[] = array(Utilities::encrypt($e->getId()), $e->getFullname(), null);
			}
		}
		
		if(count($response) == 0) {
			$response = '';
		}
		header('Content-type: application/json');
		echo json_encode($response);		
	}
	
	function _with_selected_action() 
	{
		if(!empty($_POST)) {
			$mArray = $_POST['dtChk'];
			foreach($mArray as $key => $value) {
			$d++;
			$gobr = G_Employee_Official_Business_Request_Finder::findById($value);		
                if($gobr){
                    if($_POST['chkAction'] == 'ob_approve'){
                        $gobr->approve();

                        /*$date_start = $gobr->getDateStart();
                        $date_end = $gobr->getDateEnd();
                        $employee_id = $gobr->getEmployeeId();
                        $e = G_Employee_Finder::findById($employee_id);
                        if ($e) {
                            G_Attendance_Helper::updateAttendanceByEmployeeAndPeriod($e, $date_start, $date_end);
                        }*/

                        /*
                        $start_date = $gobr->getDateStart();
                        $end_date	= $gobr->getDateEnd();

                        $employee 	= G_Employee_Finder::findById($gobr->getEmployeeId());
                        $dates 		= Tools::getBetweenDates($start_date, $end_date);
                        foreach ($dates as $date) {
                            $a = G_Attendance_Finder::findByEmployeeAndDate($employee, $date);
                            if (!$a) {
                                $a = new G_Attendance;
                            }
                            $a->setDate($date);
                            $a->setAsPaid();
                            $a->setAsPresent();
                            $a->recordToEmployee($employee);
                        }*/

                        $json['message']    = 'Successfully <b>approved</b> ' . $d . ' record(s)';

                    }elseif($_POST['chkAction'] == 'ob_archive'){
                        $gobr->archive();
                        $json['message']    = 'Successfully <b>archived</b> ' . $d . ' record(s)';

                    }elseif($_POST['chkAction'] == 'ob_disapprove'){
                        $gobr->disapprove();

                        /*$date_start = $gobr->getDateStart();
                        $date_end = $gobr->getDateEnd();
                        $employee_id = $gobr->getEmployeeId();
                        $e = G_Employee_Finder::findById($employee_id);
                        if ($e) {
                            G_Attendance_Helper::updateAttendanceByEmployeeAndPeriod($e, $date_start, $date_end);
                        }*/

                        $json['message']    = 'Successfully <b>disapproved</b> ' . $d . ' record(s)';

                    }elseif($_POST['chkAction'] == 'ob_restore'){
                        $gobr->restore_archived();
                        $json['message']    = 'Successfully <b>restored</b> ' . $d . ' archived record(s)';

                    }else {

                    }
                }
            }
		}
		
		$json['is_success'] = 1;
		$json['eid']        = $_POST['eid'];
			
		echo json_encode($json);
	}

	function _save_ob_request()
	{
		Utilities::verifyFormToken($_POST['token']);		
		if($_POST['employee_id']){			
			if($_POST['ob_request_id']){
				$gobr = G_Employee_Official_Business_Request_Finder::findById(Utilities::decrypt($_POST['ob_request_id']));
			}else{
				$gobr = new G_Employee_Official_Business_Request();				
			}

            G_Employee_Official_Business_Request_Helper::addNewRequest(Utilities::decrypt($_POST['employee_id']), $this->c_date, $_POST['ob_date_from'], $_POST['ob_date_to'], $_POST['comments']);
			
			$employee 	= G_Employee_Finder::findById(Utilities::decrypt($_POST['employee_id']));
            $obr = $employee->getOfficialBusinessRequest($_POST['ob_date_from']);

            if( $obr ){
            	$request_id   = $obr->getId();
            	$approvers    = $_POST['approvers'];
				$requestor_id = Utilities::decrypt($_POST['employee_id']);
				$request_type = G_Request::PREFIX_OFFICIAL_BUSSINESS;

				$r = new G_Request();
		        $r->setRequestorEmployeeId($requestor_id);
		        $r->setRequestId($request_id);
		        $r->setRequestType($request_type);
		        $r->saveEmployeeRequest($approvers); //Save request approvers
            }

			$json['is_success'] = 1;			
			$json['message']    = 'Record was successfully saved.';			
						
		}else {
			$json['is_success'] = 0;
			$json['message']    = 'Error in sql';
		}
		$json['from'] = $_POST['date_from'];		
		$json['to']	  = $_POST['date_to'];
		echo json_encode($json);
	}

	function _update_ob_request()
	{
		Utilities::verifyFormToken($_POST['token']);		
		if($_POST['employee_id']){			
			if($_POST['ob_request_id']){
				$gobr = G_Employee_Official_Business_Request_Finder::findById(Utilities::decrypt($_POST['ob_request_id']));
				if( $gobr ){	
					$start_date = date("Y-m-d",strtotime($_POST['ob_date_from']));	
					$end_date   = date("Y-m-d",strtotime($_POST['ob_date_to']));			
            		$comment    = $_POST['comments'];

            		$gobr->setDateStart($start_date);
            		$gobr->setDateEnd($end_date);
            		$gobr->setComments($comment);            		
            		$gobr->save();

					$json['is_success'] = 1;			
					$json['message']    = 'Record was successfully saved.';		
				}else{
					$json['is_success'] = 0;
					$json['message']    = 'Record not found';
				}
			}
						
		}else {
			$json['is_success'] = 0;
			$json['message']    = 'Error in sql';
		}
		$json['from'] = $_POST['date_from'];		
		$json['to']	  = $_POST['date_to'];
		echo json_encode($json);
	}
	
	function _save_ob_request_depre()
	{
		Utilities::verifyFormToken($_POST['token']);		
		if($_POST['employee_id']){			
			if($_POST['ob_request_id']){
				$gobr = G_Employee_Official_Business_Request_Finder::findById(Utilities::decrypt($_POST['ob_request_id']));
			}else{
				$gobr = new G_Employee_Official_Business_Request();				
			}

            G_Employee_Official_Business_Request_Helper::addNewRequest(Utilities::decrypt($_POST['employee_id']), $this->c_date, $_POST['ob_date_from'], $_POST['ob_date_to'], $_POST['comments']);
			
			$json['is_success'] = 1;			
			$json['message']    = 'Record was successfully saved.';
			
			
			if($_POST['is_approved'] == G_Employee_Official_Business_Request::YES) {
				$employee 	= G_Employee_Finder::findById(Utilities::decrypt($_POST['employee_id']));
                $obr = $employee->getOfficialBusinessRequest($_POST['ob_date_from']);
                if ($obr) {
                    $obr->approve();
                }

				/*$dates 		= Tools::getBetweenDates($start_date, $end_date);
				foreach ($dates as $date) {
					$a = G_Attendance_Finder::findByEmployeeAndDate($employee, $date);
					if (!$a) {
						$a = new G_Attendance;
					}
					$a->setDate($date);
					$a->setAsPaid();
					$a->setAsPresent();
					$a->recordToEmployee($employee);
				}*/
			}
		}else {
			$json['is_success'] = 0;
			$json['message']    = 'Error in sql';
		}
		$json['from'] = $_POST['date_from'];		
		$json['to']	  = $_POST['date_to'];
		echo json_encode($json);
	}

	function ajax_view_ob_request_approvers() 
	{
		$date  		 = date("Y-m-d"); 		
		$cp          = new G_Cutoff_Period();
		$cutoff_data = $cp->getCurrentCutoffPeriod($date);				

		$request_id  = Utilities::decrypt($_GET['eid']);
		$approvers   = new G_Request();
		$approvers->setRequestId($request_id);
		$data = $approvers->getObRequestApproversStatus();

		if( $data['total_approvers'] > 0 ){	
			$this->var['eid']        = $_GET['eid'];		
			$this->var['is_lock']    = $cutoff_data['is_lock'];			
			$this->var['total_approvers'] = $data['total_approvers'];
			$this->var['approvers']  = $data['approvers'];	
			$this->var['token']		 = Utilities::createFormToken();			
			$this->var['page_title'] = 'Leave Request Approvers';		
			$this->view->render('ob/form/view_ob_request_approvers.php',$this->var);
		}else{
			echo "<div class=\"alert alert-error\">No approvers set for selected request</div><br />";
		}
	}

	function _update_ob_request_approvers(){    	
    	Utilities::verifyFormToken($_POST['token']);    	

    	$data = $_POST['approvers'];    	    	
    	$id   = Utilities::decrypt($_POST['eid']);
    	$json['is_success'] = false;
		$json['message']    = "Cannot save record";

    	if( !empty($data) ){
    		$date 		  = $this->c_date;
    		$request_type = G_Request::PREFIX_OFFICIAL_BUSSINESS;
    		$r = new G_Request();
    		$r->setRequestId($id);
    		$r->setActionDate($date);
    		$r->setRequestType($request_type);
			$json = $r->updateRequestApproversDataById($data);
			$r->updateRequestStatus();
    	}

    	echo json_encode($json);
    }
	
	function _approve_ob_request()
	{
		
		if($_POST['eid']){
			$gobr = G_Employee_Official_Business_Request_Finder::findById(Utilities::decrypt($_POST['eid']));
			if($gobr){
				$gobr->approve();

                $date_start = $gobr->getDateStart();
                $date_end = $gobr->getDateEnd();
                $employee_id = $gobr->getEmployeeId();
                $e = G_Employee_Finder::findById($employee_id);
                if ($e) {
                    G_Attendance_Helper::updateAttendanceByEmployeeAndPeriod($e, $date_start, $date_end);
                }
				
				/*
				$start_date = $gobr->getDateStart();		
				$end_date	= $gobr->getDateEnd();
				
				$employee 	= G_Employee_Finder::findById($gobr->getEmployeeId());
				$dates 		= Tools::getBetweenDates($start_date, $end_date);
				foreach ($dates as $date) {
					$a = G_Attendance_Finder::findByEmployeeAndDate($employee, $date);
					if (!$a) {
						$a = new G_Attendance;
					}
					$a->setDate($date);
					$a->setAsPaid();
					$a->setAsPresent();
					$a->recordToEmployee($employee);
				}*/
				
				
				$json['is_success'] = 1;			
				$json['message']    = 'Record was successfully updated.';
			}else{
				$json['is_success'] = 0;
				$json['message']    = 'Record not found...';
			}
		}else{
			$json['is_success'] = 0;
			$json['message']    = 'Error in sql';
		}
		
		$json['eid'] = $_POST['eid'];
		echo json_encode($json);
	}
	
	function _disapprove_ob_request()
	{
		if($_POST['eid']){
			$gobr = G_Employee_Official_Business_Request_Finder::findById(Utilities::decrypt($_POST['eid']));
			if($gobr){
				$gobr->hr_disapprove();

                $date_start = $gobr->getDateStart();
                $date_end = $gobr->getDateEnd();
                $employee_id = $gobr->getEmployeeId();
                $e = G_Employee_Finder::findById($employee_id);
                if ($e) {
                    G_Attendance_Helper::updateAttendanceByEmployeeAndPeriod($e, $date_start, $date_end);
                }

				$json['is_success'] = 1;			
				$json['message']    = 'Record was successfully updated.';
			}else{
				$json['is_success'] = 0;
				$json['message']    = 'Record not found...';
			}
		}else{
			$json['is_success'] = 0;
			$json['message']    = 'Error in sql';
		}
		
		$json['eid'] = $_POST['eid'];
		echo json_encode($json);
	}
	
	function _archive_ob_request()
	{
		if($_POST['eid']){
			$gobr = G_Employee_Official_Business_Request_Finder::findById(Utilities::decrypt($_POST['eid']));
			if($gobr){
				$gobr->archive();
				$json['is_success'] = 1;			
				$json['message']    = 'Record was successfully sent to archive.';
			}else{
				$json['is_success'] = 0;
				$json['message']    = 'Record not found...';
			}
		}else{
			$json['is_success'] = 0;
			$json['message']    = 'Error in sql';
		}
		
		$json['eid'] = $_POST['eid'];
		echo json_encode($json);
	}
	
	function _restore_ob_request()
	{
		if($_POST['eid']){
			$gobr = G_Employee_Official_Business_Request_Finder::findById(Utilities::decrypt($_POST['eid']));
			if($gobr){
				$gobr->restore_archived();
				$json['is_success'] = 1;			
				$json['message']    = 'Archive record was successfully restored.';
			}else{
				$json['is_success'] = 0;
				$json['message']    = 'Record not found...';
			}
		}else{
			$json['is_success'] = 0;
			$json['message']    = 'Error in sql';
		}
		
		$json['eid'] = $_POST['eid'];
		echo json_encode($json);
	}	
	
	function _load_ob_list_dt() 
	{
		$this->var['permission_action'] = $this->validatePermission(G_Sprint_Modules::HR,'attendance','official_business');
		$this->var['from'] = $_POST['from'];
		$this->var['to']   = $_POST['to'];
		$this->view->render('ob/_ob_pending_list_dt.php',$this->var);
	}
	
	function _load_approved_ob_list_dt() 
	{
		$this->var['permission_action'] = $this->validatePermission(G_Sprint_Modules::HR,'attendance','official_business');
		$this->var['from'] = $_POST['from'];
		$this->var['to']   = $_POST['to'];
		$this->view->render('ob/_ob_approved_list_dt.php',$this->var);
	}

	function _load_disapproved_ob_list_dt() 
	{
		$this->var['permission_action'] = $this->validatePermission(G_Sprint_Modules::HR,'attendance','official_business');
		$this->var['from'] = $_POST['from'];
		$this->var['to']   = $_POST['to'];
		$this->view->render('ob/_ob_disapproved_list_dt.php',$this->var);
	}
	
	function _load_archived_ob_list_dt() 
	{
		$this->var['permission_action'] = $this->validatePermission(G_Sprint_Modules::HR,'attendance','official_business');
		$this->var['from'] = $_POST['from'];
		$this->var['to']   = $_POST['to'];
		$this->view->render('ob/_ob_archived_list_dt.php',$this->var);
	}
	
	function _load_server_ob_pending_list_dt() 
	{
		$permission_action = $this->validatePermission(G_Sprint_Modules::HR,'attendance','official_business');

		Utilities::ajaxRequest();		
		//$sqlcond 	 = "  AND jbh.end_date = ''";
		$sqlcond 	.= ' AND date_start BETWEEN ' . Model::safeSql($_GET['from']) . ' AND ' . Model::safeSql($_GET['to']);
		
		$dt = new Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(G_EMPLOYEE_OFFICIAL_BUSINESS_REQUEST);
		$dt->setCustomField(array('emp_name' => 'firstname,lastname'));
		$dt->setJoinTable("LEFT JOIN " . EMPLOYEE . " e");			
		$dt->setJoinFields(G_EMPLOYEE_OFFICIAL_BUSINESS_REQUEST . ".employee_id = e.id LEFT JOIN " . G_EMPLOYEE_JOB_HISTORY . " jbh ON e.id = jbh.employee_id AND jbh.end_date = ''");
		$dt->setCondition(' ' . G_EMPLOYEE_OFFICIAL_BUSINESS_REQUEST . '.company_structure_id = ' . Utilities::decrypt($this->global_user_ecompany_structure_id) . ' AND is_archive = "' . G_Employee_Official_Business_Request::NO . '" AND is_approved="' . G_Employee_Official_Business_Request::STATUS_PENDING . '"' . $sqlcond);
		$dt->setColumns('date_start,date_end');
		$dt->setOrder('ASC');
		$dt->setStartIndex(0);
		$dt->setSort(0);
		if($permission_action == Sprint_Modules::PERMISSION_02) {
			if($_SESSION['sprint_hr']['is_period_lock'] == G_Cutoff_Period::NO){
				$dt->setNumCustomColumn(1);
				$dt->setCustomColumn(	
				array(
				'1' => '<div class=\"i_container\"><ul class=\"dt_icons\"><li><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></li><li><a title=\"Edit\" id=\"edit\" class=\"ui-icon ui-icon-pencil g_icon\" href=\"javascript:void(0);\" onclick=\"javascript:editOBRequest(\'e_id\');\"></a></li><li><a title=\"Approvers\" id=\"edit\" class=\"ui-icon ui-icon-person g_icon\" href=\"javascript:void(0);\" onclick=\"javascript:viewObRequestApprovers(\'e_id\');\"></a></li><li><a title=\"Approve\" id=\"edit\" class=\"ui-icon ui-icon-check g_icon\" href=\"javascript:void(0);\" onclick=\"javascript:approveOBRequest(\'e_id\',1);\"></a></li><li><a title=\"Disapproved\" id=\"edit\" class=\"ui-icon ui-icon-close g_icon\" href=\"javascript:void(0);\" onclick=\"javascript:disapproveOBRequest(\'e_id\',1);\"></a></li><li><a title=\"Send to Archive\" id=\"delete\" class=\"ui-icon ui-icon-trash g_icon\" href=\"javascript:void(0);\" onclick=\"javascript:archivePendingOBRequest(\'e_id\')\"></a></li></ul></div>'));
			}else {
				$dt->setNumCustomColumn(0);	
			}
		}else{
			$dt->setNumCustomColumn(0);
		}
		echo $dt->constructDataTable();
	}
	
	function _load_server_ob_approved_list_dt() 
	{
		$permission_action = $this->validatePermission(G_Sprint_Modules::HR,'attendance','official_business');

		Utilities::ajaxRequest();		
		$sqlcond 	 = "  AND jbh.end_date = ''";
		$sqlcond 	.= ' AND date_start BETWEEN ' . Model::safeSql($_GET['from']) . ' AND ' . Model::safeSql($_GET['to']);
		
		$dt = new Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(G_EMPLOYEE_OFFICIAL_BUSINESS_REQUEST);
		$dt->setCustomField(array('emp_name' => 'firstname,lastname','job_name'=>'jbh.name'));
		$dt->setJoinTable("LEFT JOIN " . EMPLOYEE . " e");			
		$dt->setJoinFields(G_EMPLOYEE_OFFICIAL_BUSINESS_REQUEST . ".employee_id = e.id LEFT JOIN " . G_EMPLOYEE_JOB_HISTORY . " jbh ON " . G_EMPLOYEE_OFFICIAL_BUSINESS_REQUEST . ".employee_id = jbh.employee_id");
		$dt->setCondition(' ' . G_EMPLOYEE_OFFICIAL_BUSINESS_REQUEST . '.company_structure_id = ' . Utilities::decrypt($this->global_user_ecompany_structure_id) . ' AND is_archive = "' . G_Employee_Official_Business_Request::NO . '" AND is_approved="' . G_Employee_Official_Business_Request::STATUS_APPROVED . '"' . $sqlcond);
		$dt->setColumns('date_start,date_end');
		$dt->setOrder('ASC');
		$dt->setStartIndex(0);
		$dt->setSort(0);
		if($permission_action == Sprint_Modules::PERMISSION_02) {
			if($_SESSION['sprint_hr']['is_period_lock'] == G_Cutoff_Period::NO){
				$dt->setNumCustomColumn(1);
				$dt->setCustomColumn(	
				array(
				//'1' => '<div class=\"i_container\"><ul class=\"dt_icons\"><li><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></li><li><a title=\"Disapprove\" id=\"edit\" class=\"ui-icon ui-icon-close g_icon\" href=\"javascript:void(0);\" onclick=\"javascript:disapproveOBRequest(\'e_id\');\"></a></li><li><a title=\"Send to Archive\" id=\"delete\" class=\"ui-icon ui-icon-trash g_icon\" href=\"javascript:void(0);\" onclick=\"javascript:archiveApprovedOBRequest(\'e_id\')\"></a></li></ul></div>'));
	                '1' => '<div class=\"i_container\"><ul class=\"dt_icons\"><li><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></li><li><a title=\"Disapprove\" id=\"edit\" class=\"ui-icon ui-icon-close g_icon\" href=\"javascript:void(0);\" onclick=\"javascript:disapproveOBRequest(\'e_id\',2);\"></a></li></ul></div>'));
			}else {
				$dt->setNumCustomColumn(0);	
			}
		}else{
			$dt->setNumCustomColumn(0);	
		}
		echo $dt->constructDataTable();
	}

	function _load_server_ob_disapproved_list_dt() 
	{
		$permission_action = $this->validatePermission(G_Sprint_Modules::HR,'attendance','official_business');

		Utilities::ajaxRequest();		
		$sqlcond 	 = "  AND jbh.end_date = ''";
		$sqlcond 	.= ' AND date_start BETWEEN ' . Model::safeSql($_GET['from']) . ' AND ' . Model::safeSql($_GET['to']);
		
		$dt = new Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(G_EMPLOYEE_OFFICIAL_BUSINESS_REQUEST);
		$dt->setCustomField(array('emp_name' => 'firstname,lastname','job_name'=>'jbh.name'));
		$dt->setJoinTable("LEFT JOIN " . EMPLOYEE . " e");			
		$dt->setJoinFields(G_EMPLOYEE_OFFICIAL_BUSINESS_REQUEST . ".employee_id = e.id LEFT JOIN " . G_EMPLOYEE_JOB_HISTORY . " jbh ON e.id = jbh.employee_id");
		$dt->setCondition(' ' . G_EMPLOYEE_OFFICIAL_BUSINESS_REQUEST . '.company_structure_id = ' . Utilities::decrypt($this->global_user_ecompany_structure_id) . ' AND is_archive = "' . G_Employee_Official_Business_Request::NO . '" AND is_approved="' . G_Employee_Official_Business_Request::STATUS_DISAPPROVED . '"' . $sqlcond);
		$dt->setColumns('date_start,date_end');
		$dt->setOrder('ASC');
		$dt->setStartIndex(0);
		$dt->setSort(0);
		if($permission_action == Sprint_Modules::PERMISSION_02) {
			if($_SESSION['sprint_hr']['is_period_lock'] == G_Cutoff_Period::NO){
				$dt->setNumCustomColumn(1);
				$dt->setCustomColumn(	
				array(
				//'1' => '<div class=\"i_container\"><ul class=\"dt_icons\"><li><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></li><li><a title=\"Disapprove\" id=\"edit\" class=\"ui-icon ui-icon-close g_icon\" href=\"javascript:void(0);\" onclick=\"javascript:disapproveOBRequest(\'e_id\');\"></a></li><li><a title=\"Send to Archive\" id=\"delete\" class=\"ui-icon ui-icon-trash g_icon\" href=\"javascript:void(0);\" onclick=\"javascript:archiveApprovedOBRequest(\'e_id\')\"></a></li></ul></div>'));
	                '1' => '<div class=\"i_container\"><ul class=\"dt_icons\"><li><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></li><li><a title=\"Approve\" id=\"edit\" class=\"ui-icon ui-icon-check g_icon\" href=\"javascript:void(0);\" onclick=\"javascript:approveOBRequest(\'e_id\',2);\"></a></li></ul></div>'));
			}else {
				$dt->setNumCustomColumn(0);	
			}
		}else{
			$dt->setNumCustomColumn(0);	
		}
		echo $dt->constructDataTable();
	}
	
	function _load_server_ob_archived_list_dt() 
	{
		$permission_action = $this->validatePermission(G_Sprint_Modules::HR,'attendance','official_business');

		Utilities::ajaxRequest();		
		$sqlcond 	 = "  AND jbh.end_date = ''";
		$sqlcond 	.= ' AND date_start BETWEEN ' . Model::safeSql($_GET['from']) . ' AND ' . Model::safeSql($_GET['to']);
		
		$dt = new Datatable();
		$c  = $_GET['iDisplayStart'];
		$dt->setPagination(1);
		$dt->setStart(1);
		$dt->setStartIndex(0);
		$dt->setDbTable(G_EMPLOYEE_OFFICIAL_BUSINESS_REQUEST);
		$dt->setCustomField(array('emp_name' => 'firstname,lastname','job_name'=>'jbh.name'));
		$dt->setJoinTable("LEFT JOIN " . EMPLOYEE . " e");			
		$dt->setJoinFields(G_EMPLOYEE_OFFICIAL_BUSINESS_REQUEST . ".employee_id = e.id LEFT JOIN " . G_EMPLOYEE_JOB_HISTORY . " jbh ON e.id = jbh.employee_id");
		$dt->setCondition(' ' . G_EMPLOYEE_OFFICIAL_BUSINESS_REQUEST . '.company_structure_id = ' . Utilities::decrypt($this->global_user_ecompany_structure_id) . ' AND is_archive = "' . G_Employee_Official_Business_Request::YES . '"' . $sqlcond);
		$dt->setColumns('date_start,date_end');
		$dt->setOrder('ASC');
		$dt->setStartIndex(0);
		$dt->setSort(0);
		if($permission_action == Sprint_Modules::PERMISSION_02) {
			if($_SESSION['sprint_hr']['is_period_lock'] == G_Cutoff_Period::NO){
				$dt->setNumCustomColumn(1);
				$dt->setCustomColumn(	
				array(
				'1' => '<div class=\"i_container\"><ul class=\"dt_icons\"><li><input type=\"checkbox\" name=\"dtChk[]\" onclick=\"javascript:enableDisableWithSelected();\" value=\"id\"></li><li><a title=\"Restore Archived\" id=\"edit\" class=\"ui-icon ui-icon-refresh g_icon\" href=\"javascript:void(0);\" onclick=\"javascript:restoreArchivedOBRequest(\'e_id\');\"></a></li></ul></div>'));
			}else {
				$dt->setNumCustomColumn(0);	
			}
		}else{
			$dt->setNumCustomColumn(0);	
		}
		echo $dt->constructDataTable();
	}
}
?>