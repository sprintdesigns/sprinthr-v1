<?php
class Payroll_Register_Controller extends Controller
{
	function __construct()
	{		
        $this->module = 'payroll';

		parent::__construct();
        Loader::appMainScript('payslip_base.js');
        Loader::appMainScript('payslip.js');
        Loader::appMainUtilities();
		Loader::appStyle('style.css');

		$this->sprintHdrMenu(G_Sprint_Modules::PAYROLL, 'payroll');
		$this->generateCurrentCutOffPeriod();
		//$this->redirectNoAccessModule(G_Sprint_Modules::PAYROLL, 'payroll');

		$this->var['payroll_register'] = 'selected';

		$this->validatePermission(G_Sprint_Modules::PAYROLL,'payroll','');
	}

	function index() {
		Jquery::loadMainTipsy();
		Jquery::loadMainInlineValidation2();
		Jquery::loadMainJqueryFormSubmit();
		
		$this->var['page_title'] = 'Payroll Register';
		//$this->var['action'] = url('attendance/manage');
		$this->var['periods'] = G_Payslip_Helper::getPeriods();
		$this->view->setTemplate('payroll/template.php');
		$this->view->render('payroll_register/index.php',$this->var);		
	}

	function generation() {
		Jquery::loadMainTipsy();
		Jquery::loadMainInlineValidation2();
		Jquery::loadMainJqueryFormSubmit();
		Jquery::loadMainBootStrapDropDown();

        Loader::appMainScript('settings_base.js');
        Loader::appMainScript('attendance_base.js');
        Loader::appMainScript('attendance.js');

        $month = (int) $_GET['month'];
        $year  = (int) $_GET['year'];  

        if( $month <= 0 ) {
           $month = Tools::getGmtDate('n');
        }

        if ($year <= 0) {
            $year = Tools::getGmtDate('Y');
        } 

        $sv = new G_Cutoff_Period();
        //$sv->generateCutoffPeriodByMonthAndYear( $month, $year );
        //$periods = $sv->getValidCutOffPeriodsByMonthAndYear($month, $year);        
        $periods = $sv->getMonthCutoffPeriods($month, $year);        
        //$periods = G_Cutoff_Period_Finder::findByMonthYear($month, $year);        
        /*if( empty($periods) ){
        	$cutoff  = new G_Cutoff_Period();
			$data    = $cutoff->generateCutoffPeriodByMonthAndYear( $month, $year );
			$periods = $sv->getValidCutOffPeriodsByMonthAndYear($month, $year);
        }*/
        
        $btns_dl_payroll       = array();
		$btns_dl_payslip       = array();
		$btns_view_payslip     = array();
		$btns_generate_payroll = array();
		$btns_lock_unlock      = array();

        foreach( $periods as $p ){
        	$period_encrypted_id  = Utilities::encrypt($p->getId());
		    $start_date 		  = $p->getStartDate();
		    $end_date 			  = $p->getEndDate();
		    $cutoff_character     = $p->getCutoffCharacter();
		    $cutoff_number	      = $p->getCutoffNumber();
		    $is_locked 		      = $p->isLocked();
		    $is_payroll_generated = $p->isPayrollGenerated();

		    if( $is_locked ){		    	
		    	$action  = "javascript:unlockPeriod('{$period_encrypted_id}')"; 
		    	$class   = "red_button";
		    	$caption = "Unlock";
		    }else{
		    	$action  = "javascript:lockPeriod('{$period_encrypted_id}')"; 
		    	$class   = "blue_button";
		    	$caption = "Lock";		    	
		    }

		    $btn_lock_unlock_config = array(
				'module'				=> 'payroll',
	    		'parent_index'			=> 'payroll',
	    		'child_index'			=> '',
	    		'required_permission'	=> Sprint_Modules::PERMISSION_02,        		
	    		'event' 				=> 'onmouseup',
	    		'action' 				=> $action,
	    		'id' 					=> '',
	    		'class' 				=> $class,
	    		'icon' 					=> '',
	    		'additional_attribute' 	=> '',
	    		'wrapper_start'			=> '',
	    		'wrapper_end'			=> '',
	    		'caption' 				=> $caption
			); 

			$btns_lock_unlock[$p->getId()] = G_Button_Builder::createButtonWithPermissionValidation($this->global_user_payroll_actions, $btn_lock_unlock_config);

			$employee_access = $this->validatePermission(G_Sprint_Modules::HR,'employees','employee_access');
			$has_option = false;
			if($employee_access == Sprint_Modules::PERMISSION_05) {
				$q = "both";
				$has_option = true;
			}elseif($employee_access == Sprint_Modules::PERMISSION_06) {
				$q = "confidential";
				$has_option = false;
			}elseif($employee_access == Sprint_Modules::PERMISSION_07) {
				$q = "non-confidential";
				$has_option = false;
			}else{
				$q = "both";
				$has_option = true;
			}

			if( $is_payroll_generated ){							
				//$action  = "location.href='" . url("payslip/download_payslip?from={$start_date}&to={$end_date}'");
				$action  = url("payroll_reports/payroll_management#payslip");
				$caption = "Download Payslip";         
				/*if($has_option) {
					$action = url("payslip/download_payslip?from={$start_date}&to={$end_date}");
					$wrapper_start = '<div class="btn-group" style="display:inline;">';
					$dropdown_btn_wrapper = '<ul class="dropdown-menu " style="margin-top:20px;margin-left:4px;"><li><a class="" href="'.$action.'&q=confidential"><i class=""></i> Download Confidential </a></li><li><a class="" id="" href="'.$action.'&q=non-confidential"><i class=""></i> Download Non-Confidential </a></li><li><a class="" id="" href="'.$action.'&q=both"><i class=""></i> Download Both </a></li></ul></div>';
					$caption_caret = ' <span class="icon icon-chevron-down icon-white"></span>';
					$action = "javscript:void(0);";
				}else{
					$wrapper_start = '';
					$dropdown_btn_wrapper = '';
					$caption_caret = '';
				}*/

				$wrapper_start = '';
				$dropdown_btn_wrapper = '';
				$caption_caret = '';

				$btn_dl_payslip_config = array(
					'module'				=> 'payroll',
		    		'parent_index'			=> 'payroll',
		    		'child_index'			=> '',
		    		'required_permission'	=> Sprint_Modules::PERMISSION_01,        		
		    		'event' 				=> 'onmouseup',
		    		//'action' 				=> $action,
		    		'href' 					=> $action,
		    		'id' 					=> '',
		    		//'class' 				=> 'blue_button dropdown-toggle',
		    		'class' 				=> 'blue_button',
		    		'icon' 					=> '',
		    		'additional_attribute' 	=> '',
		    		'wrapper_start'			=> $wrapper_start,
		    		'wrapper_end'			=> $dropdown_btn_wrapper,
		    		'caption' 				=> $caption . $caption_caret
				); 				

				//$action  = "location.href='" . url("payroll/payroll_download_payroll_register?from={$start_date}&to={$end_date}'");
				$action  = url("payroll_reports/payroll_management#payroll_register");
				$caption = "Download Payroll Register";
				/*if($has_option) {
					$action = url("payroll/payroll_download_payroll_register?from={$start_date}&to={$end_date}");
					$wrapper_start = '<div class="btn-group" style="display:inline;">';
					$dropdown_btn_wrapper = '<ul class="dropdown-menu " style="margin-top:20px;margin-left:4px;"><li><a class="" href="'.$action.'&q=confidential"><i class=""></i> Download Confidential </a></li><li><a class="" id="" href="'.$action.'&q=non-confidential"><i class=""></i> Download Non-Confidential </a></li><li><a class="" id="" href="'.$action.'&q=both"><i class=""></i> Download Both </a></li></ul></div>';
					$caption_caret = ' <span class="icon icon-chevron-down icon-white"></span>';
					$action = "javscript:void(0);";
				}else{
					$wrapper_start = '';
					$dropdown_btn_wrapper = '';
					$caption_caret = '';
				}*/    
				$wrapper_start = '';
				$dropdown_btn_wrapper = '';
				$caption_caret = '';

				$btn_dl_payroll_config = array(
					'module'				=> 'payroll',
		    		'parent_index'			=> 'payroll',
		    		'child_index'			=> '',
		    		'required_permission'	=> Sprint_Modules::PERMISSION_01,        		
		    		'event' 				=> 'onmouseup',
		    		//'action' 				=> $action,
		    		'href' 					=> $action,
		    		'id' 					=> '',
		    		//'class' 				=> 'blue_button dropdown-toggle',
		    		'class' 				=> 'blue_button',
		    		'icon' 					=> '',
		    		'additional_attribute' 	=> '',
		    		'wrapper_start'			=> $wrapper_start,
		    		'wrapper_end'			=> $dropdown_btn_wrapper,
		    		'caption' 				=> $caption . $caption_caret
				); 
				
				$action  = "location.href='" . url("payslip/manage?from={$start_date}&to={$end_date}'");
				$caption = "View Payslip";         
				$btn_view_payslip_config = array(
					'module'				=> 'payroll',
		    		'parent_index'			=> 'payroll',
		    		'child_index'			=> '',
		    		'required_permission'	=> Sprint_Modules::PERMISSION_01,        		
		    		'event' 				=> 'onmouseup',
		    		'action' 				=> $action,
		    		'id' 					=> '',
		    		'class' 				=> 'blue_button',
		    		'icon' 					=> '',
		    		'additional_attribute' 	=> '',
		    		'wrapper_start'			=> '',
		    		'wrapper_end'			=> '',
		    		'caption' 				=> $caption
				); 	

				$action  = "location.href='" . url("payslip/processed_payroll?from={$start_date}&to={$end_date}'");
				$caption = "Processed Payroll";
				if($has_option) {
					$action = url("payslip/processed_payroll?from={$start_date}&to={$end_date}");
					$wrapper_start = '<div class="btn-group" style="display:inline;">';
					$dropdown_btn_wrapper = '<ul class="dropdown-menu " style="margin-top:20px;margin-left:4px;"><li><a class="" href="'.$action.'&q=confidential"><i class=""></i> View Confidential </a></li><li><a class="" id="" href="'.$action.'&q=non-confidential"><i class=""></i> View Non-Confidential </a></li><li><a class="" id="" href="'.$action.'&q=both"><i class=""></i> View All </a></li></ul></div>';
					$caption_caret = ' <span class="icon icon-chevron-down icon-white"></span>';
					$action = "javscript:void(0);";
				}else{
					$wrapper_start = '';
					$dropdown_btn_wrapper = '';
					$caption_caret = '';
				}    

				$btn_processed_payroll_config = array(
					'module'				=> 'payroll',
		    		'parent_index'			=> 'payroll',
		    		'child_index'			=> '',
		    		'required_permission'	=> Sprint_Modules::PERMISSION_01,        		
		    		'event' 				=> 'onmouseup',
		    		'action' 				=> $action,
		    		'id' 					=> '',
		    		'class' 				=> 'blue_button dropdown-toggle',
		    		'icon' 					=> '',
		    		'additional_attribute' 	=> '',
		    		'wrapper_start'			=> $wrapper_start,
		    		'wrapper_end'			=> $dropdown_btn_wrapper,
		    		'caption' 				=> $caption . $caption_caret
				); 

				$btns_dl_payslip[$p->getId()]   		= G_Button_Builder::createAnchorTagWithPermissionValidation($this->global_user_payroll_actions, $btn_dl_payslip_config);
				$btns_dl_payroll[$p->getId()]   		= G_Button_Builder::createAnchorTagWithPermissionValidation($this->global_user_payroll_actions, $btn_dl_payroll_config);
				$btns_view_payslip[$p->getId()] 		= G_Button_Builder::createButtonWithPermissionValidation($this->global_user_payroll_actions, $btn_view_payslip_config);
				$btn_processed_payroll[$p->getId()]   	= G_Button_Builder::createButtonWithPermissionValidation($this->global_user_payroll_actions, $btn_processed_payroll_config);

			}

			if( !$is_locked || !$is_payroll_generated ){				
				$action = "javascript:generatePayslipByMonthCutoffNumberYear({$month},{$cutoff_number},{$year},'{$q}')";     
				$caption = "Generate Payroll";  

				if($has_option) {
					$conf_action = "javascript:generatePayslipByMonthCutoffNumberYear({$month},{$cutoff_number},{$year},'confidential')"; 
					$non_conf_action = "javascript:generatePayslipByMonthCutoffNumberYear({$month},{$cutoff_number},{$year},'non-confidential')"; 
					$both_action = "javascript:generatePayslipByMonthCutoffNumberYear({$month},{$cutoff_number},{$year},'both')"; 
					$wrapper_start = '<div class="btn-group" style="display:inline;">';
					$dropdown_btn_wrapper = '<ul class="dropdown-menu " style="margin-top:20px;margin-left:4px;"><li><a class="" href="'.$conf_action.'"><i class=""></i> Generate Confidential </a></li><li><a class="" id="" href="'.$non_conf_action.'"><i class=""></i> Generate Non-Confidential </a></li><li><a class="" id="" href="'.$both_action.'"><i class=""></i> Generate Both </a></li></ul></div>';
					$caption_caret = ' <span class="icon icon-chevron-down icon-white"></span>';
					$action = "javscript:void(0);";
				}else{
					$wrapper_start = '';
					$dropdown_btn_wrapper = '';
					$caption_caret = '';
				} 

				$btn_generate_payroll_config = array(
					'module'				=> 'payroll',
		    		'parent_index'			=> 'payroll',
		    		'child_index'			=> '',
		    		'required_permission'	=> Sprint_Modules::PERMISSION_02,        		
		    		'event' 				=> 'onmouseup',
		    		'action' 				=> $action,
		    		'id' 					=> '',
		    		'class' 				=> 'blue_button dropdown-toggle',
		    		'icon' 					=> '',
		    		'additional_attribute' 	=> '',
		    		'wrapper_start'			=> $wrapper_start,
		    		'wrapper_end'			=> $dropdown_btn_wrapper,
		    		'caption' 				=> $caption . $caption_caret
				); 
				$btns_generate_payroll[$p->getId()] = G_Button_Builder::createButtonWithPermissionValidation($this->global_user_payroll_actions, $btn_generate_payroll_config);
			}
        }

        $this->var['month'] 		= $month;
        $this->var['year'] 			= $year;
        $this->var['current_year']  = Tools::getGmtDate('Y');
        $this->var['previous_year'] = (int) Tools::getGmtDate('Y') - 1;
        $this->var['page_title']    = 'Payroll Generation';
        $this->var['months']        = Tools::getMonthNames();
        $this->var['periods']       = $periods;
        $this->var['btns_dl_payroll']       = $btns_dl_payroll;
		$this->var['btns_dl_payslip']       = $btns_dl_payslip;
		$this->var['btns_view_payslip']     = $btns_view_payslip;
		$this->var['btns_generate_payroll'] = $btns_generate_payroll;
		$this->var['btn_processed_payroll'] = $btn_processed_payroll;
		$this->var['btns_lock_unlock']      = $btns_lock_unlock;

        $this->view->setTemplate('payroll/template.php');
		$this->view->render('payroll_register/generation.php', $this->var);
	}
	
	function history() {
		Jquery::loadMainTipsy();
		
		$this->var['page_title'] = 'Payroll History';
		$this->var['periods'] = G_Payslip_Helper::getPeriods();
		$this->view->setTemplate('payroll/template.php');

        $this->var['months'] = Tools::getMonthNames();
		$this->view->render('payroll_register/history.php',$this->var);		
	}

	function _load_generate_payroll_option() {
		if(!empty($_POST['month']) && !empty($_POST['cutoff_number']) && !empty($_POST['year']) ){
			$this->var['data']	= $_POST;
			$this->view->render('payroll_register/forms/generate_payroll_form.php',$this->var);
		}
	}

	function generate_payroll_option() {
		if(!empty($_GET['month']) && !empty($_GET['cutoff_number']) && !empty($_GET['year']) ){
			Jquery::loadMainJqueryDatatable();
			Jquery::loadMainTipsy();
			Jquery::loadMainInlineValidation2();
			Jquery::loadMainJqueryFormSubmit();
			Jquery::loadMainSelect2();

			$employee_access = $this->validatePermission(G_Sprint_Modules::HR,'employees','employee_access');
			if($employee_access == Sprint_Modules::PERMISSION_05) {
				$additional_qry = "";	
			}elseif($employee_access == Sprint_Modules::PERMISSION_06) {
				if(strtolower($employee_access) != strtolower($_GET['q']." Employees")) {
					redirect("payroll_register/generation");
				}	
				$additional_qry = " AND e.is_confidential = 1  ";	
			}elseif($employee_access == Sprint_Modules::PERMISSION_07) {
				if(strtolower($employee_access) != strtolower($_GET['q']." Employees")) {
					redirect("payroll_register/generation");
				}	
				$additional_qry = " AND e.is_confidential = 0  ";	
			}else{
				$additional_qry = "";	
			}

			if($employee_access == Sprint_Modules::PERMISSION_05 || $employee_access == 1) {
				if($_GET['q'] == "confidential") {
					$additional_qry = " AND e.is_confidential = 1  ";	
				}elseif($_GET['q'] == "non-confidential"){
					$additional_qry = " AND e.is_confidential = 0  ";
				}else{
					$additional_qry = "";
				}
			}

			$period = G_Cutoff_Period_Finder::findByYearMonthAndCutoffNumber($_GET['year'], $_GET['month'], $_GET['cutoff_number']);        
	        if ($period) {
	            //Utilities::displayArray($period);
	            $e = new G_Employee();
	            $payroll_employee_details = $e->getProcessedAndUnprocessedEmployeePayrollByCutoff($period->getStartDate(),$period->getEndDate(), $additional_qry );
	            $this->var['payroll_employee_details'] = $payroll_employee_details;
	        }	       

			$company_structure_id = $this->global_user_ecompany_structure_id;
			$b = G_Company_Branch_Finder::findMain();
			$this->var['departments'] = G_Company_Structure_Finder::findAllDepartmentsIsNotArchiveByBranchIdAndParentId($b->getId(),Utilities::decrypt($company_structure_id));
			$this->var['data']	= $_GET;
			$this->var['page_title']    = 'Generate Payroll';
			$this->view->setTemplate('payroll/template.php');
			$this->view->render('payroll_register/forms/generate_payroll_form.php',$this->var);
		}else{
			redirect('payroll_register/generation');
		}
	}

	function _load_employee_select() {
		if( $_POST['department_id'] ){
			if($_POST['q'] == "confidential") {
				$additional_qry = " AND is_confidential = 1  ";	
			}elseif($_POST['q'] == "non-confidential"){
				$additional_qry = " AND is_confidential = 0 ";
			}else{
				$additional_qry = "";
			}

			$fields = array("id,CONCAT(lastname,' ', firstname, ' ',middlename) as fullname");
			if($_POST['department_id'] == "all") {
				$this->var['employees'] =$x= G_Employee_Helper::sqlGetAllEmployees($fields,$additional_qry);	
			}else{
				$this->var['employees'] =$x= G_Employee_Helper::sqlGetAllEmployeeByDepartmentId(Utilities::decrypt($_POST['department_id']), $fields, $additional_qry);	
			}
		}

		$this->view->render('payroll_register/forms/_employee_select.php',$this->var);
	}

	function _load_selected_employee() {
		$selected_employee = explode(",", $_POST['selected_employee_id']);
		$selected_employee = array_values(array_filter(array_unique($selected_employee)));
		foreach($selected_employee as $key => $value) {
			$ids[] = Utilities::decrypt($value); 
		}
		$selected_employee = implode(",",$ids);
		$fields = array("id,CONCAT(lastname,' ', firstname, ' ',middlename) as fullname");
		$this->var['employees'] = G_Employee_Helper::sqlMultipleEmployeeDetailsById($selected_employee, $fields);	
		$this->view->render('payroll_register/forms/_selected_employee_dt.php',$this->var);
		
	}

	function _remove_from_selected_employee() {
		$selected_employee = explode(",", $_POST['selected_employee_id']);
		$selected_employee = array_values(array_filter(array_unique($selected_employee)));
		foreach($selected_employee as $key => $value) {
			if($value != $_POST['employee_id']) {
				$new_ids[] = $value; 
			}
			
		}
		$return['new_selected_employee_id'] = implode(",",$new_ids);
		echo json_encode($return);
		
	}
}
?>