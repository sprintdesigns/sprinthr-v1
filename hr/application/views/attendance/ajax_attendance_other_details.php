<style>
.tbl-other-details{border:none;}
.tbl-other-details td{border: none;}
.tbl-other-details td.field_label{width:176px;}
.lbl-attendance-other-details{background-color:#198cc9;color:#ffffff;padding-left: 10px;margin:0 0 6px;}
ul.list-attendance-other-details{list-style: none;}
ul.list-attendance-other-details li{display: inline-block; margin-left:24px; padding:6px;width:40%;background-color:#e3e3e3;margin-bottom: 32px}
</style>
<ul class="list-attendance-other-details">
<?php if( !empty($schedule) ) { ?>
    <li>
        <h3 class="lbl-attendance-other-details">Assigned Schedule</h3>
        <table class="tbl-other-details">
        <?php foreach($schedule as $key => $value){ ?>    
            <?php if($value != "0"){ ?>         
                <tr><td class="field_label"><?php echo $key; ?></td><td>: <?php echo $value; ?></td></tr>    
            <?php } ?>
        <?php } ?>
        </table>
    </li>
<?php } ?>

<?php if( !empty($breaktime) ) { ?>
    <li>
        <h3 class="lbl-attendance-other-details">Breaktime Schedule</h3>
        <table class="tbl-other-details">
        <?php foreach($breaktime as $key => $value){ ?>
            <tr>
                <td class="field_label" colspan="1">Sched: </td>
                <td class="field_label" colspan="1"><?php echo $value; ?></td>
            </tr>                  
        <?php } ?>   
        <?php if(!empty($break)) {?>
            <?php foreach($break as $bkey => $bkey_d) { ?> 
                <?php if($bkey == 'time_in') { ?>
                    <tr>
                        <td class="field_label" colspan="1">Time In: </td>
                        <td class="field_label" colspan="1"><?php echo $bkey_d; ?></td>
                    </tr>
                <?php } ?>
                <?php if($bkey == 'time_out') { ?>
                    <tr>
                        <td class="field_label" colspan="1">Time Out: </td>
                        <td class="field_label" colspan="1"><?php echo $bkey_d; ?></td>
                    </tr>
                <?php } ?>            
            <?php } ?>  
        <?php } ?>                    
        </table>
    </li>
<?php } ?>

<?php if( !empty($attendance) ) { ?>    
    <li>
        <h3 class="lbl-attendance-other-details">Attendance</h3>
        <table class="tbl-other-details">
        <?php foreach($attendance as $key => $value){ ?> 
            <?php if($value != "0"){ ?>             
                <tr><td class="field_label"><?php echo $key; ?></td><td>: <?php echo $value; ?></td></tr>        
            <?php } ?>
        <?php } ?>
        </table>
    </li>
<?php } ?>

<?php if( !empty($holiday) ) { ?>
    <li>
        <h3 class="lbl-attendance-other-details">Holiday</h3>
        <table class="tbl-other-details">
        <?php foreach($holiday as $key => $value){ ?>     
            <?php if($value != "0"){ ?>         
                <tr><td class="field_label"><?php echo $key; ?></td><td>: <?php echo $value; ?></td></tr>        
            <?php } ?>
        <?php } ?>
        </table>
    </li>
<?php } ?>

<?php if( !empty($tardiness) ) { ?>
    <li>
        <h3 class="lbl-attendance-other-details">Tardiness</h3>
        <table class="tbl-other-details">
        <?php foreach($tardiness as $key => $value){ ?>  
            <?php if($value != "0"){ ?>                  
                <tr><td class="field_label"><?php echo $key; ?></td><td>: <?php echo $value; ?></td></tr>        
            <?php } ?>
        <?php } ?>
        </table>
    </li>
<?php } ?>

<?php if( !empty($overtime) ) { ?>
    <li>
        <h3 class="lbl-attendance-other-details">Overtime</h3>
        <table class="tbl-other-details">
        <?php foreach($overtime as $key => $value){ ?>       
            <?php if($value != "0"){ ?>      
                <tr><td class="field_label"><?php echo $key; ?></td><td>: <?php echo $value; ?></td></tr>    
            <?php } ?>
        <?php } ?>
        </table>
    </li>
<?php } ?>
</ul>
