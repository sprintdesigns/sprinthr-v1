<script>
function countChecked()
{       
    var inputs     = document.withSelectedAction.elements['dtChk[]'];
    var is_checked = false;
    var cnt        = 0;
    var theForm = document.withSelectedAction;
    for (i=0; i<theForm.elements.length; i++) {         
        if (theForm.elements[i].name=='dtChk[]')
            is_checked = theForm.elements[i].checked;
            if(is_checked){                              
                cnt++;
            }
    }
    
    return cnt;

}

function chkUnchk()
{
    var check_uncheck = document.withSelectedAction.elements['check_uncheck'];
    if(check_uncheck.checked == 1) {    
        $('#check_uncheck').attr('title', 'Uncheck All');                                   
        //$("#chkAction").removeAttr('disabled');
        var status = 1; 
    } else { 
        $('#check_uncheck').attr('title', 'Check All');                                 
        //$("#chkAction").attr('disabled',true);
        var status = 0;
    }
    
    var c = 0;
    var theForm = document.withSelectedAction;
    for (i=0; i<theForm.elements.length; i++) {        
        if (theForm.elements[i].name=='dtChk[]') {
            theForm.elements[i].checked = status;
            c++;
        }
    }

    if(c > 0 && status == 1) {
        $("#chkAction").removeAttr('disabled');
    }else{
        $("#chkAction").attr('disabled',true);
    }
}

</script>
<?php include('includes/_wrappers.php'); ?>
<form name="withSelectedAction" id="withSelectedAction">
<input type="hidden" id="from_period" name="from_period" value="<?php echo $from_period; ?>"/>
<input type="hidden" id="to_period" name="to_period" value="<?php echo $to_period; ?>" />

<div class="break-bottom inner_top_option">
	<div id="detailscontainer" class="detailscontainer_blue details_highlights">
        <div class="earnings_period_selected">
            <div class="overtime_title_period"><?php echo $period_selected; ?>
                [ Go to:
                <?php if ($previous_cutoff_link != ''):?>
                    <a href="<?php echo $previous_cutoff_link;?>">Previous Cutoff</a>
                <?php else:?>
                    Previous Cutoff
                <?php endif;?>
                |
                <?php if ($next_cutoff_link != ''):?>
                    <a href="<?php echo $next_cutoff_link;?>">Next Cutoff</a>
                <?php else:?>
                    Next Cutoff
                <?php endif;?>
                ]</div>
        </div>
    </div>
    <div class="select_dept display-inline-block right-space">
        <strong>Select Department:</strong> <select id="cmb_dept_id" onchange="javascript:load_approved_leave_list_dt(this.value);">
            <option value="" selected="selected">All</option>
            <?php foreach($departments as $d){ ?>
                <option value="<?php echo $d->getId(); ?>"><?php echo $d->getTitle(); ?></option>
            <?php } ?>
        </select>
    </div>
    <?php if($download_url){ ?>
        <a id="import_undertime" class="gray_button vertical-middle" href="<?php echo $download_url; ?>"><i class="icon-excel icon-custom"></i> <b>Download Leave</b></a>
    <?php } ?>
    <div class="clear"></div>
</div>
<div class="break-bottom">
    <?php if($is_period_lock == G_Cutoff_Period::NO){ ?>
    <div class="datatable_withselect display-inline-block right-space">
        <select disabled="disabled" name="chkAction" id="chkAction" onchange="javascript:pendingLeaveWithSelectedAction(this.value);">
            <option value="">With Selected:</option>
            <option value="disapprove">Disapprove</option>                   
        </select>
    </div> 
    <?php } ?>    
    <div class="clear"></div>
</div>
<div id="leave_list_dt_wrapper" class="dtContainer"></div>
</form>
<div id="import_leave_wrapper" style="display:none">
<?php include 'form/import_leave.php'; ?>
</div>
<script>
	$(function() { load_within_date_range_approved_leave_list_dt(0,'<?php echo $from_period; ?>','<?php echo $to_period; ?>'); });
</script>
