<script>
var jq = jQuery.noConflict();
$(function(){
    $('#generatePayrollForm').validationEngine({scroll:false});     
    $('#generatePayrollForm').ajaxForm({
        success:function(o) {
            //closeTheDialog();
            
            if(o.is_success){
                var query = window.location.search;
                //location.href= base_url + 'payroll_register/generation'+ query;
                dialogOkBox(o.message,{ok_url:"payroll_register/generation"+query});  
            }else{
               dialogOkBox(o.message,{});   
            }
            
        },
        dataType:'json',
        beforeSubmit: function() {
            closeDialog('#' + DIALOG_CONTENT_HANDLER);
            showLoadingDialog('Processing...');        
            return true;
        }
    }); 

    $("#generatePayrollForm")[0].reset();
    $(".filter-select").prop("disabled",true);
    $("#all_employee").click(function(){
        if($(this).is(":checked")) {
            $("#department_id").val("all");
            $(".filter-select").prop("disabled",true);
            $("#selected_employee_id").val("");    
            loadEmployeeByDepartmentId("null");
            $('#employee_wrapper').html(""); 
            $(".hide-show-tr").fadeOut(1000);
        }else{
            $(".filter-select").removeProp("disabled");
            loadEmployeeByDepartmentId("all");
            var selected_employee_id = $("#selected_employee_id").val();
            $(".hide-show-tr").fadeIn(1000);
            loadSelectedEmployees(selected_employee_id);
        }
        //$("#department_id").trigger("change");
    });

    $("#department_id").change(function(){
        var department_id = $(this).val();
        loadEmployeeByDepartmentId(department_id);
    });

    //$("#toggle-content").hide();
    $("#toggle-btn").click(function(){
        //$("#toggle-content").slideToggle(500);

    });
});

function loadEmployeeByDepartmentId(department_id) {
    var q = $("#q").val();
    $("#employee-select-wrapper").html(loading_image);
    $.post(base_url + 'payroll_register/_load_employee_select',{department_id:department_id,q:q},function(o) {
        $('#employee-select-wrapper').html(o);       
    }); 
}

function loadSelectedEmployees(selected_employee_id) {
    $.post(base_url + 'payroll_register/_load_selected_employee',{selected_employee_id:selected_employee_id},function(o) {
        $('#employee_wrapper').html(o);    
        jq("#employee_id").select2("val", "");
        $("#loading-msg").html("");
    }); 
}
</script>
<table class="table">
    <tr class="info">
        <th colspan="3">Payroll Details</th>
    </tr>
    <tr class="info">
        <td width="33%">
            Date : <b><?php echo Tools::getMonthString($data['month']); ?> <?php echo $data['year'];?></b>
        </td>
        <td width="33%">
            Cutoff : <b><?php echo $data['cutoff_number'].($data['cutoff_number'] == 1 ? "st" : "nd"); ?> </b>
        </td>
        <td width="33%">
            Type : <b><?php echo ucfirst($data['q']); ?></b>
        </td>
    </tr>
    <tr class="info">
        <td width="33%">
            Processed Payroll : <b><?php echo $payroll_employee_details['processed_payroll'];?> Employee(s)</b> 
        </td>
        <td width="33%">
            Unprocessed Payroll : <b><?php echo $payroll_employee_details['unprocessed_payroll'];?> Employee(s)</b>
        </td>
        <td width="33%">
            Total Employees : <b><?php echo $payroll_employee_details['total_employees'];?> Employee(s)</b>
        </td>
    </tr>
</table>


<!-- <div id="form_main" class="inner_form popup_form"> -->
<div id="form_main" class="">
    <form name="generatePayrollForm" id="generatePayrollForm" method="post" action="<?php echo url('payslip/generate_payslip'); ?>">    
    <input type="hidden" id="month" name="month" value="<?php echo $data['month']; ?>" />
    <input type="hidden" id="cutoff_number" name="cutoff_number" value="<?php echo $data['cutoff_number']; ?>" />
    <input type="hidden" id="year" name="year" value="<?php echo $data['year']; ?>" />
    <input type="hidden" id="q" name="q" value="<?php echo $data['q']; ?>" />
    <input type="hidden" id="selected_employee_id" name="selected_employee_id" value="" />
    <div id="form_default">
       
    <a href="javascript:void(0);" id="toggle-btn" style="text-decoration:none;">
        <h3 id="" style="padding: 2px 2px 2px 15px; background-color: gray; color: rgb(255, 255, 255);">Payroll Options <i class="icon icon-chevron-down icon-white"></i></h3>
    </a>
    <div id="toggle-content">
        <table width="100%"> 
            <tr>
                <td >&nbsp;</td>
                <td >
                    <input checked="checked" type="checkbox" style="vertical-align:text-bottom;"  name="all_employee" id="all_employee"> 
                    <label style="display:inline-flex;padding:5px;" for="all_employee">  All Employees </label>
                </td>
            </tr>
            <tr>
                <td width="25%" class="">Department </td>
                <td width="75%" class="">
                    <select class=" filter-select" id="department_id" name="department_id" >
                      <option value="all">All</option>
                      <?php foreach($departments as $d) { ?>
                        <option value="<?php echo Utilities::encrypt($d->id);?>"><?php echo $d->title;?></option>
                      <?php } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td width="25%" class="">Employee </td>
                <td width="75%" class="" valign="top" >
                    <div id="employee-select-wrapper">
                        <select class=" filter-select" id="employee_id" name="employee_id" >
                            <option value="all">All</option>
                        </select>
                    </div>              
                </td>
            </tr>
            <tr>
                <td width="25%" class="">&nbsp;</td>
                <td width="75%" class="">
                    <input style="vertical-align:sub;" type="checkbox" name="remove_resigned" class="" id="remove_resigned" /> <label style="display:inline-flex;" for="remove_resigned">Remove Resigned Employee</label>
                    &nbsp;&nbsp;<input style="vertical-align: sub;" type="checkbox" name="remove_terminated" class="" id="remove_terminated" /> <label style="display:inline-flex;" for="remove_terminated">Remove Terminated Employee</label>
                </td>
            </tr>   
            <tr ><td colspan="2">&nbsp;</td></tr> 
           </table>
           <div class="hide-show-tr" style="display:none;">

                    <div style="width:100%;" id="employee_wrapper"></div>
            </div>
        </div>
    </div>
    <div id="form_default" class="form_action_section">
        <table width="100%">
            <tr>

                <td>
                    <div class="pull-right">
                        <input value="Confirm" id="edit_schedule_submit" class="curve blue_button" type="submit">&nbsp;
                        <!-- <a href="javascript:void(0);" onclick="javascript:closeDialogBox('#_dialog-box_','#editGroupTeam');">Cancel</a> -->
                    </div>
                </td>
            </tr>
        </table>
    </div>    
    </form>
</div>