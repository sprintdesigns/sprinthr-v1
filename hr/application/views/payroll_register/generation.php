<style>
.blue_button{vertical-align: middle;}    
</style>
<script>
$(function(){
    var jq = jQuery.noConflict();
    jq('.dropdown-toggle').dropdown();  
})
</script>
<form method="get">
Go to
<select name="month">
    <?php foreach ($months as $key => $m):?>
        <option value="<?php echo ($key + 1);?>" <?php echo (($key + 1) == $month) ? 'selected="selected"' : '';?>><?php echo $m;?></option>
    <?php endforeach;?>
</select>
<select name="year">
    <option value="<?php echo $current_year;?>" <?php echo ($current_year == $year) ? 'selected="selected"' : '';?>><?php echo $current_year;?></option>
    <option value="<?php echo $previous_year;?>" <?php echo ($previous_year == $year) ? 'selected="selected"' : '';?>><?php echo $previous_year;?></option>
</select> <button class="blue_button" type="submit">Go</button>
</form>
<br><br>
<?php foreach ($periods as $p):?>
    <?php  
        $start_date = $p->getStartDate();
        $end_date = $p->getEndDate();
        $cutoff_character = $p->getCutoffCharacter();
    ?>
    <div style="border:1px solid #CCCCCC; margin-bottom:10px; padding: 10px">
        <div style="font-size: 20px; font-weight:bold"><?php echo Tools::getMonthString($month);?> - <?php echo $cutoff_character;?> <span style="font-size:13px">(<?php echo Tools::convertDateFormat($start_date);?> - <?php echo Tools::convertDateFormat($end_date);?>)</span></div><br>
            <?php 
                echo $btns_lock_unlock[$p->getId()]; 
                echo $btns_dl_payslip[$p->getId()];
                echo $btns_dl_payroll[$p->getId()];
                echo $btns_generate_payroll[$p->getId()];
                echo $btns_view_payslip[$p->getId()];
                echo $btn_processed_payroll[$p->getId()];
            ?>
    </div>
<?php endforeach;?>