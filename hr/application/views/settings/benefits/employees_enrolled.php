<script>
$(function(){
  load_employees_enrolled_to_benefit('<?php echo $eid; ?>');
  $("#back-benefit-btn").click(function(){
    showBenefitsContainer();
  });
});
</script>
<div class="pull-right">
  <a id="back-benefit-btn" class="btn" href="javascript:void(0);"><b>Back</b></a>
</div>
<p style="font-size:12px;">Employees enrolled to <b><?php echo $benefit_name; ?></b></p>

<div class="yui-skin-sam">
    <div id="employees-enrolled-to-benefit-dt"></div>
</div>

