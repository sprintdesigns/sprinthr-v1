<table width="100%" border="1">
<thead>
	<th width="11%">Name</th>
    <th width="31%">Payday Breakdown</th>
    <th width="15%">Base salary credit</th>
    <th style="text-align:center;" width="13%">Is Taxable</th>
    <th style="text-align:center;" width="7%">Status</th>
    <th></th>
</thead>
<?php foreach($deductions as $d): ?>
<?php $b = explode(':',$d['breakdown']); ?>
    <tr>
        <td><?php echo $d['name']; ?></td>
        <td><?php echo $b[0] . '% (1st Cut-Off) : ' . $b[1] . '% (2nd Cut-Off)'; ?></td>
        <td style="text-align:center;"><?php echo $salary_credit_options[$d['salary_credit']]; ?></td>
        <td style="text-align:center;"><?php echo $d['is_taxable']; ?></td>    
        <td style="text-align:center;"><?php echo ($d['is_active'] == G_Settings_Deduction_Breakdown::YES ? 'Active' : 'Inactive'); ?></td>
        <td valign="middle">
            <div class="i_container">
                <ul class="dt_icons" style="margin:0 42px;"> 
                    <li>
                        <a id="tipsy" class="ui-icon ui-icon-pencil" href="javascript:void(0)" onclick="javascript:editDeductionBreakdown('<?php echo $d['id'];?>');" title="Edit"></a>
                    </li>
                    <?php if($d['is_active'] == G_Settings_Deduction_Breakdown::YES) { ?> 
                        <li>   
                            <a id="tipsy" class="ui-icon ui-icon-close" href="javascript:void(0)" onclick="javascript:_deactivateDeductionBreakdown('<?php echo $d['id'];?>')" title="Deactivate"></a>
                        </li>
                    <?php } else { ?>
                        <li>
                            <a id="tipsy" class="ui-icon ui-icon-check" href="javascript:void(0)" onclick="javascript:_activateDeductionBreakdown('<?php echo $d['id'];?>')" title="Activate"></a>
                        </li>
                    <?php } ?>                               
                </ul>
            </div>      
        </td>
    </tr>
<?php endforeach; ?>
</table>
<script>
$('.dt_icons #tipsy').tipsy({gravity: 's'});
</script>
