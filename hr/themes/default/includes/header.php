<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/png" href="<?php echo BASE_FOLDER; ?>themes/<?php echo THEME;?>/themes-images/favicon.ico">
<title>SprintHR &laquo; <?php echo strip_tags($page_title);?> <?php echo strip_tags($title);?></title>
<link rel="stylesheet" href="<?php echo MAIN_FOLDER; ?>themes/<?php echo MAIN_THEME; ?>/bootstrap/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo MAIN_FOLDER; ?>themes/<?php echo MAIN_THEME; ?>/bootstrap/bootstrap.css" />
<?php echo $meta_tags;?>
<?php Loader::get();?>
<link rel="stylesheet" href="<?php echo MAIN_FOLDER; ?>themes/<?php echo MAIN_THEME; ?>/fonts.css" />
</head>
<body>
<script language="javascript">
	if (typeof lockScreen == "function") {
		lockScreen();
	}
</script>
<script type="text/javascript">
 var sync_attendance_interval = <?php echo $hdr_settings_sync_interval; ?>;

$(function() {      
    var button   = $('#dropButton');
    var box      = $('#dropBox');

    <?php echo $ini_modal_script; ?>

    //var form = $('#contDiv');
    button.removeAttr('href');
    button.mouseup(function(login) {
        box.toggle();
        button.toggleClass('active');
    });

    $(this).mouseup(function(login) {
        if(!($(login.target).parent('#dropButton').length > 0)) {
            button.removeClass('active');
            box.hide();
        }
    });

    function iniUserModal() {    
        blockPopUp(); 
        $("#<?php echo $ini_modal_wrapper; ?>").html(loading_image);
        $.post(base_url + 'initial_settings/ini_pay_period',{},function(o) {
            $("#<?php echo $ini_id_wrapper; ?>").html(o);                               
        })
        
        var $dialog = $('#<?php echo $ini_id_wrapper; ?>');
        $dialog.dialog("destroy");
        
        
        var $dialog = $('#<?php echo $ini_id_wrapper; ?>');
        $dialog.dialog({
            title: 'Initial Setup : Pay Period',
            resizable: false,
            position: [480,70],
            width: 340,        
            modal: true,
            close: function() {
                       $dialog.dialog("destroy");                      
                       disablePopUp();          
                    }   
                        
            }).dialogExtend({
            "maximize" : false,
            "minimize"  : false       
          }).show();        
    }

    var employee_credits_list = "";
    <?php foreach($employee_with_leave_increase as $key => $value)  { ?>
        employee_credits_list += '<?php echo $value; ?> <br>'; 
    <?php } ?>

    <?php if($is_credit_upgraded) { ?>
        updateLeaveCreditNotification(employee_credits_list);
    <?php } ?>
});
</script>

<!-- NOTIFICATIONS -->
<script src="<?php echo MAIN_FOLDER; ?>application/scripts/notifications.js" type="text/javascript"></script>
<script src="<?php echo MAIN_FOLDER; ?>application/scripts/attendance_sync.js" type="text/javascript"></script>

<script src="<?php echo MAIN_FOLDER; ?>application/scripts/sync_data.js" type="text/javascript"></script>
<div id="sync_data_modal"></div>
<div id="leave_credit_notication"></div>
<?php echo $ini_modal_wrapper; ?>
<div id="wrapper">
	<div id="header_wrapper">
        <div id="header_container">        	
            <div id="header" class="clearfix">
                <div class="logo_container">
                	<a href="<?php echo url('schedule');?>">                    	
                    	<img src="<?php echo MAIN_FOLDER; ?>themes/<?php echo MAIN_THEME; ?>/themes-images/logo.png" border="0" alt="SprintHR" />
                    </a>
                </div>                
                <?php include('profile_info.php');?>
                <?php include('menu.php');?>
            </div><!-- #header -->
        </div><!-- #header_container -->
    </div><!-- #header_wrapper -->
    <div id="wrapcontainer">
    	<div id="container">
        	<div class="contshad contshadcor lefttop"></div>
            <div class="contshad contshadcor righttop"></div>
            <div class="contshad contshadcor leftbottom"></div>
            <div class="contshad contshadcor rightbottom"></div>
            <div class="contshad leftside"></div>
            <div class="contshad rightside"></div>
            <div class="contshad topside"></div>
            <div class="contshad bottomside"></div>