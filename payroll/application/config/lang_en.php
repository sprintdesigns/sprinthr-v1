<?php
$GLOBALS['lang'] = array (
	'app_short_name'                     => 'Apps',
	'app_full_name'                      => 'Apps'
);

$GLOBALS['lang_general'] = array(
	'loading_message'					=> 'Loading...'		
);

$GLOBALS['lang_module_names'] = array (
	MODULE_USERS                        => 'User Administration'		
);


$GLOBALS['lang_general'] = array(
	'title'					=> 'Gleent Human Resources',
	'copyright_statement'	=> '&copy; 2012 Gleent Incorporated. All Rights Reserved.',
	'footer_title'			=> 'Krikel Framework Version 3'
);


$GLOBALS['hr']['marital_status'] = array(
	SINGLE		=> 'Single',
	MARRIED 	=> 'Married',
	SEPARATED	=> 'Separated',
	WIDOWED		=> 'Widowed'
);


//default applicant status
$GLOBALS['hr']['application_status'] = array(
	APPLICATION_SUBMITTED 	=> 	'Application Submitted',
	INTERVIEW				=>	'Interview',
	JOB_OFFERED				=>	'Job Offered',
	OFFER_DECLINED			=>	'Offer Declined',
	REJECTED				=>	'Rejected',
	HIRED					=>	'Applicant Passed'
	
);


//default applicant event
$GLOBALS['hr']['application_event'] = array(
	INTERVIEW_EVENT 		=> 	'Interview',
	OFFER_JOB_EVENT			=>	'Offer a Job',
	DECLINED_OFFER_EVENT	=>	'Decline Offer',
	REJECTED_EVENT			=>	'Reject',
	HIRED_EVENT				=>	'Hired'
);


//default employee status
$GLOBALS['hr']['employee_status'] = array(
	PROBATIONARY 		=> 	'Probationary',
	REGULAR				=>	'Regular / Full Time',
	PART_TIME			=>	'Part Time',
	AWOL				=>	'AWOL',
	RESIGNED			=>	'Resigned',
	TERMINATED			=>	'Terminated',
	SUSPENDED			=>	'Suspended'
	
);

$GLOBALS['language'] = array(
	'ENGLISH'	=> 'English',
	'Tagalog'	=> 'Tagalog'
);


$GLOBALS['hr']['requirements'] = array(
	'Required 2x2 Picture'	=> '',
	'Medical'				=> '',
	'SSS'					=> '',
	'Tin'					=> ''
);


$GLOBALS['hr']['performance_rate'] = array(
	RATE_1				=> 'Does not Meet Minimum Standards',
	RATE_2				=> 'Needs Improvement',
	RATE_3				=> 'Meets Expectations',
	RATE_4				=> 'Exceeds Expectation',
	RATE_5				=> 'Outstanding'
);

?>