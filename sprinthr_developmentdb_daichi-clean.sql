-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 09, 2017 at 09:13 AM
-- Server version: 5.5.25a
-- PHP Version: 5.5.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sprinthr_developmentdb_daichi`
--

-- --------------------------------------------------------

--
-- Table structure for table `g_access_rights`
--

DROP TABLE IF EXISTS `g_access_rights`;
CREATE TABLE IF NOT EXISTS `g_access_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `rights` varchar(240) COLLATE utf8_unicode_ci NOT NULL COMMENT 'serialize',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_allowed_ip`
--

DROP TABLE IF EXISTS `g_allowed_ip`;
CREATE TABLE IF NOT EXISTS `g_allowed_ip` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(50) NOT NULL,
  `employee_id` bigint(20) NOT NULL,
  `date_modified` varchar(50) NOT NULL,
  `date_created` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_anviz_connection`
--

DROP TABLE IF EXISTS `g_anviz_connection`;
CREATE TABLE IF NOT EXISTS `g_anviz_connection` (
  `id` int(11) NOT NULL,
  `terminal_no` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `device_name` varchar(32) NOT NULL,
  `firmware` varchar(32) NOT NULL,
  `ip_address` varchar(32) NOT NULL,
  `mask` varchar(32) NOT NULL,
  `gateway` varchar(32) NOT NULL,
  `server_ip` varchar(32) NOT NULL,
  `mac_address` varchar(32) NOT NULL,
  `status` varchar(32) NOT NULL,
  `total_users` int(11) NOT NULL,
  `total_logs` int(11) NOT NULL,
  `total_fingerprint` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `g_anviz_raw_logs`
--

DROP TABLE IF EXISTS `g_anviz_raw_logs`;
CREATE TABLE IF NOT EXISTS `g_anviz_raw_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `time` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `machine_id` int(11) NOT NULL,
  `backup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_anviz_users`
--

DROP TABLE IF EXISTS `g_anviz_users`;
CREATE TABLE IF NOT EXISTS `g_anviz_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `employee_code` varchar(32) NOT NULL,
  `device_employee_id` int(11) NOT NULL,
  `fullname` varchar(32) NOT NULL,
  `nickname` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `verification_type` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `card1` int(11) NOT NULL,
  `card2` int(11) NOT NULL,
  `card3` int(11) NOT NULL,
  `kgroup` int(11) NOT NULL,
  `sync` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_applicant`
--

DROP TABLE IF EXISTS `g_applicant`;
CREATE TABLE IF NOT EXISTS `g_applicant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `employee_id` int(11) DEFAULT NULL COMMENT 'if hired',
  `company_structure_id` int(11) NOT NULL,
  `job_vacancy_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `application_status_id` int(11) NOT NULL COMMENT '0=application submitted,  1=interview,  2=offered a job, 3=declined offer, 4=reject, 5=hired',
  `lastname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `middlename` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `extension_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `marital_status` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `birth_place` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `province` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `home_telephone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email_address` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `qualification` text COLLATE utf8_unicode_ci NOT NULL,
  `sss_number` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `tin_number` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `pagibig_number` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `philhealth_number` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `applied_date_time` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `hired_date` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `rejected_date` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'if applicant did not passed, or failed or declined or delinquent',
  `resume_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `resume_path` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `application_status_id` (`application_status_id`),
  KEY `job_id` (`job_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_applicant_attachment`
--

DROP TABLE IF EXISTS `g_applicant_attachment`;
CREATE TABLE IF NOT EXISTS `g_applicant_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '346kb',
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'doc, docx, pdf',
  `date_attached` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'hr admin name',
  `screen` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'personal details,\r\n employment details, qualification',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_applicant_education`
--

DROP TABLE IF EXISTS `g_applicant_education`;
CREATE TABLE IF NOT EXISTS `g_applicant_education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `institute` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `course` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `year` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `gpa_score` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `attainment` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_applicant_examination`
--

DROP TABLE IF EXISTS `g_applicant_examination`;
CREATE TABLE IF NOT EXISTS `g_applicant_examination` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `passing_percentage` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `exam_code` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `schedule_date` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `date_taken` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'pending, failed, passed, rescheduled',
  `result` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `questions` text COLLATE utf8_unicode_ci NOT NULL,
  `time_duration` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'day:hour:minute',
  `scheduled_by` int(11) NOT NULL COMMENT 'employee_id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_applicant_language`
--

DROP TABLE IF EXISTS `g_applicant_language`;
CREATE TABLE IF NOT EXISTS `g_applicant_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `language` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fluency` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'writing, speaking,reading',
  `competency` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'poor,basic,good, mother tongue',
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_applicant_license`
--

DROP TABLE IF EXISTS `g_applicant_license`;
CREATE TABLE IF NOT EXISTS `g_applicant_license` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `license_type` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Engineer License etc',
  `license_number` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `issued_date` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `expiry_date` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_applicant_logs`
--

DROP TABLE IF EXISTS `g_applicant_logs`;
CREATE TABLE IF NOT EXISTS `g_applicant_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(180) CHARACTER SET latin1 NOT NULL,
  `country` varchar(200) CHARACTER SET latin1 NOT NULL,
  `firstname` varchar(180) CHARACTER SET latin1 NOT NULL,
  `lastname` varchar(180) CHARACTER SET latin1 NOT NULL,
  `email` varchar(180) CHARACTER SET latin1 NOT NULL,
  `password` varchar(150) CHARACTER SET latin1 NOT NULL,
  `status` varchar(50) CHARACTER SET latin1 NOT NULL,
  `date_time_created` varchar(110) CHARACTER SET latin1 NOT NULL,
  `date_time_validated` varchar(110) CHARACTER SET latin1 NOT NULL,
  `link` text CHARACTER SET latin1 NOT NULL,
  `is_password_change` varchar(3) CHARACTER SET latin1 NOT NULL DEFAULT 'No' COMMENT '''Yes'' or ''No''',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_applicant_medical_history`
--

DROP TABLE IF EXISTS `g_applicant_medical_history`;
CREATE TABLE IF NOT EXISTS `g_applicant_medical_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `applicant_id` bigint(20) NOT NULL,
  `date` varchar(50) NOT NULL,
  `chief_complaint` text NOT NULL,
  `medical_diagnosis` text NOT NULL,
  `treatment` text NOT NULL,
  `confined` varchar(5) NOT NULL,
  `confined_from` varchar(50) NOT NULL,
  `confined_to` varchar(50) NOT NULL,
  `physicians_name` varchar(150) NOT NULL,
  `clinic_or_hospital_name` text NOT NULL,
  `attachment` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_applicant_profile`
--

DROP TABLE IF EXISTS `g_applicant_profile`;
CREATE TABLE IF NOT EXISTS `g_applicant_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `applicant_log_id` int(11) NOT NULL,
  `lastname` varchar(20) CHARACTER SET latin1 NOT NULL,
  `firstname` varchar(20) CHARACTER SET latin1 NOT NULL,
  `middlename` varchar(20) CHARACTER SET latin1 NOT NULL,
  `extension_name` varchar(64) CHARACTER SET latin1 NOT NULL,
  `birthdate` date NOT NULL,
  `gender` varchar(20) CHARACTER SET latin1 NOT NULL,
  `marital_status` varchar(128) CHARACTER SET latin1 NOT NULL,
  `home_telephone` varchar(30) CHARACTER SET latin1 NOT NULL,
  `mobile` varchar(30) CHARACTER SET latin1 NOT NULL,
  `birth_place` varchar(128) CHARACTER SET latin1 NOT NULL,
  `address` varchar(250) CHARACTER SET latin1 NOT NULL,
  `city` varchar(30) CHARACTER SET latin1 NOT NULL,
  `province` varchar(250) CHARACTER SET latin1 NOT NULL,
  `zip_code` varchar(30) CHARACTER SET latin1 NOT NULL,
  `sss_number` varchar(64) CHARACTER SET latin1 NOT NULL,
  `tin_number` varchar(64) CHARACTER SET latin1 NOT NULL,
  `philhealth_number` varchar(64) CHARACTER SET latin1 NOT NULL,
  `pagibig_number` varchar(30) CHARACTER SET latin1 NOT NULL,
  `resume_name` varchar(250) CHARACTER SET latin1 NOT NULL,
  `resume_path` varchar(250) CHARACTER SET latin1 NOT NULL,
  `photo` varchar(150) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_applicant_requirements`
--

DROP TABLE IF EXISTS `g_applicant_requirements`;
CREATE TABLE IF NOT EXISTS `g_applicant_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `requirements` text COLLATE utf8_unicode_ci NOT NULL,
  `is_complete` varchar(15) COLLATE utf8_unicode_ci NOT NULL COMMENT '1=complete, 0=incomplete',
  `date_updated` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `applicant_id` (`applicant_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_applicant_skills`
--

DROP TABLE IF EXISTS `g_applicant_skills`;
CREATE TABLE IF NOT EXISTS `g_applicant_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `skill` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `years_experience` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_applicant_training`
--

DROP TABLE IF EXISTS `g_applicant_training`;
CREATE TABLE IF NOT EXISTS `g_applicant_training` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `cost` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_applicant_work_experience`
--

DROP TABLE IF EXISTS `g_applicant_work_experience`;
CREATE TABLE IF NOT EXISTS `g_applicant_work_experience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `company` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `job_title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `comment` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_attendance_rate`
--

DROP TABLE IF EXISTS `g_attendance_rate`;
CREATE TABLE IF NOT EXISTS `g_attendance_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nightshift_rate` float NOT NULL COMMENT '%',
  `regular_overtime` float NOT NULL COMMENT '% (example: 125%)',
  `nightshift_overtime` float NOT NULL COMMENT '%',
  `restday` float NOT NULL COMMENT '%',
  `restday_overtime` float NOT NULL COMMENT '%',
  `holiday_special` float NOT NULL COMMENT '%',
  `holiday_special_overtime` float NOT NULL COMMENT '%',
  `holiday_legal` float NOT NULL COMMENT '%',
  `holiday_legal_overtime` float NOT NULL COMMENT '%',
  `holiday_special_restday` float NOT NULL COMMENT '%',
  `holiday_special_restday_overtime` float NOT NULL COMMENT '%',
  `holiday_legal_restday` float NOT NULL COMMENT '%',
  `holiday_legal_restday_overtime` float NOT NULL COMMENT '%',
  `regular_overtime_nightshift_differential` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `g_attendance_rate`
--

INSERT INTO `g_attendance_rate` (`id`, `nightshift_rate`, `regular_overtime`, `nightshift_overtime`, `restday`, `restday_overtime`, `holiday_special`, `holiday_special_overtime`, `holiday_legal`, `holiday_legal_overtime`, `holiday_special_restday`, `holiday_special_restday_overtime`, `holiday_legal_restday`, `holiday_legal_restday_overtime`, `regular_overtime_nightshift_differential`) VALUES
(1, 110, 125, 125, 130, 130, 130, 130, 300, 130, 150, 130, 260, 130, 125);

-- --------------------------------------------------------

--
-- Table structure for table `g_audit_trail`
--

DROP TABLE IF EXISTS `g_audit_trail`;
CREATE TABLE IF NOT EXISTS `g_audit_trail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(150) CHARACTER SET latin1 NOT NULL,
  `action` varchar(150) CHARACTER SET latin1 NOT NULL,
  `event_status` varchar(20) CHARACTER SET latin1 NOT NULL,
  `details` varchar(255) CHARACTER SET latin1 NOT NULL,
  `audit_date` varchar(50) CHARACTER SET latin1 NOT NULL,
  `ip_address` varchar(100) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_break_time_schedule`
--

DROP TABLE IF EXISTS `g_break_time_schedule`;
CREATE TABLE IF NOT EXISTS `g_break_time_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_in` time NOT NULL,
  `schedule_out` time NOT NULL,
  `break_in` time NOT NULL,
  `break_out` time NOT NULL,
  `total_hrs_break` float NOT NULL DEFAULT '0',
  `total_hrs_to_deduct` int(2) NOT NULL,
  `to_deduct` int(2) NOT NULL DEFAULT '0' COMMENT '0 - not to deduct in total hrs worked / 1 - deduct to total hrs worked',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_break_time_schedule_details`
--

DROP TABLE IF EXISTS `g_break_time_schedule_details`;
CREATE TABLE IF NOT EXISTS `g_break_time_schedule_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `header_id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  `obj_type` varchar(2) NOT NULL COMMENT 'a = all, e = employee, d = department',
  `break_in` time NOT NULL,
  `break_out` time NOT NULL,
  `to_deduct` int(2) NOT NULL DEFAULT '0' COMMENT '0 = no / 1 = yes',
  `applied_to_legal_holiday` int(2) NOT NULL,
  `applied_to_special_holiday` int(2) NOT NULL,
  `applied_to_restday` int(2) NOT NULL,
  `applied_to_regular_day` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_break_time_schedule_header`
--

DROP TABLE IF EXISTS `g_break_time_schedule_header`;
CREATE TABLE IF NOT EXISTS `g_break_time_schedule_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_in` time NOT NULL,
  `schedule_out` time NOT NULL,
  `break_time_schedules` text NOT NULL,
  `applied_to` text NOT NULL,
  `date_start` varchar(80) NOT NULL,
  `date_created` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_company_branch`
--

DROP TABLE IF EXISTS `g_company_branch`;
CREATE TABLE IF NOT EXISTS `g_company_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `province` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `location_id` int(40) NOT NULL,
  `is_archive` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `g_company_branch`
--

INSERT INTO `g_company_branch` (`id`, `company_structure_id`, `name`, `province`, `city`, `address`, `zip_code`, `phone`, `fax`, `location_id`, `is_archive`) VALUES
(1, 1, 'Main', 'Cabuyao', 'Laguna', 'Sundrel Bldg Brgy Sala', '4025', '5024826', '5024826', 0, 'No');

-- --------------------------------------------------------

--
-- Table structure for table `g_company_info`
--

DROP TABLE IF EXISTS `g_company_info`;
CREATE TABLE IF NOT EXISTS `g_company_info` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(6) NOT NULL,
  `address` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `tin_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sss_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `philhealth_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pagibig_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `company_logo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `g_company_info`
--

INSERT INTO `g_company_info` (`id`, `company_structure_id`, `address`, `phone`, `fax`, `address1`, `city`, `address2`, `state`, `zip_code`, `tin_number`, `sss_number`, `philhealth_number`, `pagibig_number`, `remarks`, `company_logo`) VALUES
(1, 1, 'Your Address', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `g_company_structure`
--

DROP TABLE IF EXISTS `g_company_structure`;
CREATE TABLE IF NOT EXISTS `g_company_structure` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `company_branch_id` int(6) NOT NULL DEFAULT '0',
  `title` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Branch / Department / Group / Team',
  `parent_id` int(6) NOT NULL DEFAULT '0',
  `is_archive` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `g_company_structure`
--

INSERT INTO `g_company_structure` (`id`, `company_branch_id`, `title`, `description`, `type`, `parent_id`, `is_archive`) VALUES
(1, 1, 'Company Name', 'Company Description', '', 0, 'No'),
(2, 1, 'Facilities', '', 'Department', 1, 'No'),
(3, 1, 'Finance', '', 'Department', 1, 'No'),
(4, 1, 'General Affairs', '', 'Department', 1, 'No'),
(5, 1, 'Human Resources', '', 'Department', 1, 'No'),
(6, 1, 'Information Technology', '', 'Department', 1, 'No'),
(7, 1, 'Logistics', '', 'Department', 1, 'No'),
(8, 1, 'Equipment', '', 'Department', 1, 'No'),
(9, 1, 'Production', '', 'Department', 1, 'No'),
(10, 1, 'Technical', '', 'Department', 1, 'No'),
(11, 1, 'Quality Control', '', 'Department', 1, 'No');

-- --------------------------------------------------------

--
-- Table structure for table `g_converted_leaves`
--

DROP TABLE IF EXISTS `g_converted_leaves`;
CREATE TABLE IF NOT EXISTS `g_converted_leaves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(30) NOT NULL,
  `leave_id` int(11) NOT NULL,
  `year` int(5) NOT NULL,
  `total_leave_converted` int(11) NOT NULL,
  `amount` float(11,2) NOT NULL COMMENT 'Non taxable amount',
  `date_converted` date NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_custom_overtime`
--

DROP TABLE IF EXISTS `g_custom_overtime`;
CREATE TABLE IF NOT EXISTS `g_custom_overtime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `day_type` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_cutoff_period`
--

DROP TABLE IF EXISTS `g_cutoff_period`;
CREATE TABLE IF NOT EXISTS `g_cutoff_period` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year_tag` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `period_start` date NOT NULL,
  `period_end` date NOT NULL,
  `payout_date` date NOT NULL,
  `cutoff_number` int(11) NOT NULL,
  `salary_cycle_id` int(11) NOT NULL,
  `is_lock` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No' COMMENT 'Yes / No',
  `is_payroll_generated` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `g_cutoff_period`
--

INSERT INTO `g_cutoff_period` (`id`, `year_tag`, `period_start`, `period_end`, `payout_date`, `cutoff_number`, `salary_cycle_id`, `is_lock`, `is_payroll_generated`) VALUES
(1, '2017', '2017-12-01', '2017-12-15', '2017-12-20', 1, 2, 'No', 'No'),
(2, '2017', '2017-12-16', '2017-12-31', '2017-12-05', 2, 2, 'No', 'No'),
(3, '2017', '2017-11-01', '2017-11-15', '2017-11-20', 1, 2, 'No', 'No'),
(4, '2017', '2017-11-16', '2017-11-30', '2017-11-05', 2, 2, 'No', 'No'),
(5, '2017', '2017-10-01', '2017-10-15', '2017-10-20', 1, 2, 'No', 'No'),
(6, '2017', '2017-10-16', '2017-10-31', '2017-10-05', 2, 2, 'No', 'No'),
(7, '2017', '2017-09-01', '2017-09-15', '2017-09-20', 1, 2, 'No', 'No'),
(8, '2017', '2017-09-16', '2017-09-30', '2017-09-05', 2, 2, 'No', 'No'),
(9, '2017', '2017-08-01', '2017-08-15', '2017-08-20', 1, 2, 'No', 'No'),
(10, '2017', '2017-08-16', '2017-08-31', '2017-08-05', 2, 2, 'No', 'No'),
(11, '2017', '2017-07-01', '2017-07-15', '2017-07-20', 1, 2, 'No', 'No'),
(12, '2017', '2017-07-16', '2017-07-31', '2017-07-05', 2, 2, 'No', 'No'),
(13, '2017', '2017-06-01', '2017-06-15', '2017-06-20', 1, 2, 'No', 'No'),
(14, '2017', '2017-06-16', '2017-06-30', '2017-06-05', 2, 2, 'No', 'No'),
(15, '2017', '2017-05-01', '2017-05-15', '2017-05-20', 1, 2, 'No', 'No'),
(16, '2017', '2017-05-16', '2017-05-31', '2017-05-05', 2, 2, 'No', 'No'),
(17, '2017', '2017-04-01', '2017-04-15', '2017-04-20', 1, 2, 'No', 'No'),
(18, '2017', '2017-04-16', '2017-04-30', '2017-04-05', 2, 2, 'No', 'No'),
(19, '2017', '2017-03-01', '2017-03-15', '2017-03-20', 1, 2, 'No', 'No'),
(20, '2017', '2017-03-16', '2017-03-31', '2017-03-05', 2, 2, 'No', 'No'),
(21, '2017', '2017-02-01', '2017-02-15', '2017-02-20', 1, 2, 'No', 'No'),
(22, '2017', '2017-02-16', '2017-02-28', '2017-02-05', 2, 2, 'No', 'No'),
(23, '2017', '2017-01-01', '2017-01-15', '2017-01-20', 1, 2, 'No', 'No'),
(24, '2017', '2017-01-16', '2017-01-31', '2017-01-05', 2, 2, 'No', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `g_daily_time_record`
--

DROP TABLE IF EXISTS `g_daily_time_record`;
CREATE TABLE IF NOT EXISTS `g_daily_time_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `employee_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_entry` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `time_entry` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_eeo_job_category`
--

DROP TABLE IF EXISTS `g_eeo_job_category`;
CREATE TABLE IF NOT EXISTS `g_eeo_job_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `category_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `g_eeo_job_category`
--

INSERT INTO `g_eeo_job_category` (`id`, `company_structure_id`, `category_name`, `description`) VALUES
(1, 1, 'Officials and managers', 'Occupations requiring administrative and managerial personnel who set broad policies, exercise overall responsibility for execution of these policies, and direct individual departments or special phases of a firm''s operations. Includes: officials, executives, middle management, plant managers, department managers, and superintendents, salaried supervisors who are members of management, purchasing agents and buyers, railroad conductors and yard masters, ship captains, mates and other officers, farm operators and managers, and kindred workers.'),
(2, 1, 'Professionals', 'Occupations requiring either college graduation or experience of such kind and amount as to provide a comparable background. Includes: accountants and auditors, airplane pilots and navigators, architects, artists, chemists, designers, dietitians, editors, engineers, lawyers, librarians, mathematicians, natural scientists, registered professional nurses, personnel and labor relations specialists, physical scientists, physicians, social scientists, teachers, surveyors and kindred workers.'),
(3, 1, 'Technicians', 'Occupations requiring a combination of basic scientific knowledge and manual skill which can be obtained through 2 years of post high school education, such as is offered in many technical institutes and junior colleges, or through equivalent on-the-job training. Includes: computer programmers, drafters, engineering aides, junior engineers, mathematical aides, licensed, practical or vocational nurses, photographers, radio operators, scientific assistants, technical illustrators, technicians (medical, dental, electronic, physical science), and kindred workers.'),
(4, 1, 'Sales', 'Occupations engaging wholly or primarily in direct selling. Includes: advertising agents and sales workers, insurance agents and brokers, real estate agents and brokers, stock and bond sales workers, demonstrators, sales workers and sales clerks, grocery clerks, and cashiers/checkers, and kindred workers.'),
(5, 1, 'Office and clerical', 'Includes all clerical-type work regard-less of level of difficulty, where the activities are predominantly nonmanual though some manual work not directly involved with altering or transporting the products is included. Includes: bookkeepers, collectors (bills and accounts), messengers and office helpers, office machine operators (including computer), shipping and receiving clerks, stenographers, typists and secretaries, telegraph and telephone operators, legal assistants, and kindred workers.'),
(6, 1, 'Craft Workers (skilled)', 'Manual workers of relatively high skill level having a thorough and comprehensive knowledge of the processes involved in their work. Exercise considerable independent judgment and usually receive an extensive period of training. Includes: the building trades, hourly paid supervisors and lead operators who are not members of management, mechanics and repairers, skilled machining occupations, compositors and typesetters, electricians, engravers, painters (construction and maintenance), motion picture projectionists, pattern and model makers, stationary engineers, tailors and tailoresses, arts occupations, handpainters, coaters, bakers, decorating occupations, and kindred workers.'),
(7, 1, 'Operatives (semiskilled)', 'Workers who operate machine or processing equipment or perform other factory-type duties of intermediate skill level which can be mastered in a few weeks and require only limited training. Includes: apprentices (auto mechanics, plumbers, bricklayers, carpenters, electricians, machinists, mechanics, building trades, metalworking trades, printing trades, etc.), operatives, attendants (auto service and parking), blasters, chauffeurs, delivery workers, sewers and stitchers, dryers, furnace workers, heaters, laundry and dry cleaning operatives, milliners, mine operatives and laborers, motor operators, oilers and greasers (except auto), painters (manufactured articles), photographic process workers, truck and tractor drivers, knitting, looping, taping and weaving machine operators, welders and flamecutters, electrical and electronic equipment assemblers, butchers and meatcutters, inspectors, testers and graders, handpackers and packagers, and kindred workers.'),
(8, 1, 'Laborers (unskilled)', 'Workers in manual occupations which generally require no special training who perform elementary duties that may be learned in a few days and require the application of little or no independent judgment. Includes: garage laborers, car washers and greasers, groundskeepers and gardeners, farmworkers, stevedores, wood choppers, laborers performing lifting, digging, mixing, loading and pulling operations, and kindred workers.'),
(9, 1, 'Service workers', 'Workers in both protective and non-protective service occupations. Includes: attendants (hospital and other institutions, professional and personal service, including nurses aides, and orderlies), barbers, charworkers and cleaners, cooks, counter and fountain workers, elevator operators, firefighters and fire protection, guards, door-keepers, stewards, janitors, police officers and detectives, porters, waiters and waitresses, amusement and recreation facilities attendants, guides, ushers, public transportation attendants, and kindred workers.');

-- --------------------------------------------------------

--
-- Table structure for table `g_email_buffer`
--

DROP TABLE IF EXISTS `g_email_buffer`;
CREATE TABLE IF NOT EXISTS `g_email_buffer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sent_from` varchar(80) CHARACTER SET latin1 NOT NULL COMMENT 'sender name, web email (Administrator, admin@gleent.com)',
  `email_address` varchar(50) CHARACTER SET latin1 NOT NULL,
  `sent_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `subject` varchar(100) CHARACTER SET latin1 NOT NULL,
  `message` text CHARACTER SET latin1 NOT NULL,
  `attachment` varchar(100) CHARACTER SET latin1 NOT NULL,
  `is_sent` varchar(10) CHARACTER SET latin1 NOT NULL COMMENT 'Yes / No',
  `is_archive` varchar(10) CHARACTER SET latin1 NOT NULL COMMENT 'Yes / No',
  `error_message` text CHARACTER SET latin1 NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee`
--

DROP TABLE IF EXISTS `g_employee`;
CREATE TABLE IF NOT EXISTS `g_employee` (
  `id` bigint(30) NOT NULL AUTO_INCREMENT,
  `hash` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `employee_device_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `employee_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `company_structure_id` int(11) NOT NULL,
  `department_company_structure_id` int(11) NOT NULL DEFAULT '2',
  `employment_status_id` int(11) NOT NULL,
  `employee_status_id` int(11) NOT NULL,
  `eeo_job_category_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `photo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `salutation` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `middlename` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `extension_name` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `nickname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `marital_status` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `nationality` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `number_dependent` int(11) NOT NULL,
  `sss_number` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `tin_number` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `pagibig_number` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `philhealth_number` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `hired_date` date NOT NULL,
  `leave_date` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'resignation, terminated, endo dates',
  `resignation_date` date NOT NULL,
  `endo_date` date NOT NULL,
  `terminated_date` date NOT NULL,
  `inactive_date` date NOT NULL,
  `e_is_archive` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `is_tax_exempted` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `is_confidential` int(1) NOT NULL COMMENT '0 = No, 1 = Yes',
  `year_working_days` int(11) NOT NULL,
  `week_working_days` varchar(110) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id-fname-lname-code` (`id`,`firstname`,`lastname`,`employee_code`),
  KEY `firstname-lastname` (`firstname`,`lastname`,`id`),
  KEY `id-first-last-code` (`id`,`firstname`,`lastname`,`employee_code`),
  KEY `firstname` (`firstname`),
  KEY `lastname` (`lastname`),
  KEY `employee_code` (`employee_code`),
  KEY `id` (`id`,`hash`,`firstname`,`lastname`,`employee_code`),
  KEY `company_structure_id` (`company_structure_id`),
  KEY `nationality` (`nationality`),
  KEY `id-employee_code` (`id`,`employee_code`),
  KEY `employee_status_id` (`employee_status_id`),
  KEY `hired_date-leave_date` (`hired_date`,`leave_date`),
  KEY `hired_date` (`hired_date`),
  KEY `leave_date` (`leave_date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `g_employee`
--

INSERT INTO `g_employee` (`id`, `hash`, `employee_device_id`, `employee_code`, `company_structure_id`, `department_company_structure_id`, `employment_status_id`, `employee_status_id`, `eeo_job_category_id`, `section_id`, `photo`, `salutation`, `lastname`, `firstname`, `middlename`, `extension_name`, `nickname`, `birthdate`, `gender`, `marital_status`, `nationality`, `number_dependent`, `sss_number`, `tin_number`, `pagibig_number`, `philhealth_number`, `hired_date`, `leave_date`, `resignation_date`, `endo_date`, `terminated_date`, `inactive_date`, `e_is_archive`, `is_tax_exempted`, `is_confidential`, `year_working_days`, `week_working_days`) VALUES
(1, '467344b48341565c719df0c16c0a8bb9', '', '1', 1, 5, 0, 0, 0, 0, '', '', 'HR', 'HR', '', '', '', '0000-00-00', '', '', '', 0, '', '', '', '', '2013-01-01', '', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'No', 'No', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_annualize_tax`
--

DROP TABLE IF EXISTS `g_employee_annualize_tax`;
CREATE TABLE IF NOT EXISTS `g_employee_annualize_tax` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) NOT NULL,
  `year` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `gross_income_tax` float(11,2) NOT NULL,
  `less_personal_exemption` float(11,2) NOT NULL,
  `taxable_income` float(11,2) NOT NULL,
  `tax_due` float(11,2) NOT NULL,
  `tax_withheld_payroll` float(11,2) NOT NULL,
  `tax_refund_payable` float(11,2) NOT NULL,
  `cutoff_start_date` date NOT NULL,
  `cutoff_end_date` date NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_attachment`
--

DROP TABLE IF EXISTS `g_employee_attachment`;
CREATE TABLE IF NOT EXISTS `g_employee_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `filename` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '346kb',
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'doc, docx, pdf',
  `date_attached` date NOT NULL,
  `added_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'hr admin name',
  `screen` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'personal details, employment details, qualification',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_attendance`
--

DROP TABLE IF EXISTS `g_employee_attendance`;
CREATE TABLE IF NOT EXISTS `g_employee_attendance` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(11) NOT NULL,
  `date_attendance` date NOT NULL,
  `is_present` tinyint(1) NOT NULL,
  `is_paid` tinyint(1) NOT NULL,
  `is_restday` tinyint(1) NOT NULL,
  `is_holiday` tinyint(1) NOT NULL,
  `is_ob` tinyint(1) NOT NULL,
  `is_leave` smallint(1) NOT NULL,
  `leave_id` int(11) NOT NULL,
  `is_suspended` tinyint(1) NOT NULL,
  `holiday_id` int(11) NOT NULL,
  `holiday_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `holiday_type` tinyint(4) NOT NULL COMMENT '1 = legal, 2 = special',
  `scheduled_time_in` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `scheduled_time_out` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `scheduled_date_in` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `scheduled_date_out` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `total_schedule_hours` float NOT NULL,
  `actual_time_in` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `actual_time_out` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `actual_date_in` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `actual_date_out` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `overtime_time_in` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `overtime_time_out` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `overtime_date_in` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `overtime_date_out` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `total_overtime_hours` float NOT NULL,
  `early_overtime_in` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `early_overtime_out` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `total_hours_worked` float NOT NULL COMMENT 'based from actual time in and out',
  `night_shift_hours` float NOT NULL,
  `night_shift_overtime_hours` float NOT NULL,
  `night_shift_overtime_excess_hours` float NOT NULL,
  `night_shift_hours_special` float NOT NULL,
  `night_shift_hours_legal` float NOT NULL,
  `holiday_hours_special` float NOT NULL,
  `holiday_hours_legal` float NOT NULL,
  `overtime_hours` float NOT NULL,
  `overtime_excess_hours` float NOT NULL,
  `regular_overtime_hours` float NOT NULL,
  `regular_overtime_excess_hours` float NOT NULL,
  `regular_overtime_nightshift_hours` float NOT NULL,
  `regular_overtime_nightshift_excess_hours` float NOT NULL,
  `restday_overtime_hours` float NOT NULL,
  `restday_overtime_excess_hours` float NOT NULL,
  `restday_overtime_nightshift_hours` float NOT NULL,
  `restday_overtime_nightshift_excess_hours` float NOT NULL,
  `restday_legal_overtime_hours` float NOT NULL,
  `restday_legal_overtime_excess_hours` float NOT NULL,
  `restday_legal_overtime_ns_hours` float NOT NULL,
  `restday_legal_overtime_ns_excess_hours` float NOT NULL,
  `restday_special_overtime_hours` float NOT NULL,
  `restday_special_overtime_excess_hours` float NOT NULL,
  `restday_special_overtime_ns_hours` float NOT NULL,
  `restday_special_overtime_ns_excess_hours` float NOT NULL,
  `legal_overtime_hours` float NOT NULL,
  `legal_overtime_excess_hours` float NOT NULL,
  `legal_overtime_ns_hours` float NOT NULL,
  `legal_overtime_ns_excess_hours` float NOT NULL,
  `special_overtime_hours` float NOT NULL,
  `special_overtime_excess_hours` float NOT NULL,
  `special_overtime_ns_hours` float NOT NULL,
  `special_overtime_ns_excess_hours` float NOT NULL,
  `late_hours` float NOT NULL,
  `undertime_hours` float NOT NULL,
  `total_breaktime_deductible_hours` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id-date_attendance` (`employee_id`,`date_attendance`),
  KEY `date_attendance` (`date_attendance`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Triggers `g_employee_attendance`
--
DROP TRIGGER IF EXISTS `triggerUpdateFpLogsOnInsert`;
DELIMITER //
CREATE TRIGGER `triggerUpdateFpLogsOnInsert` AFTER INSERT ON `g_employee_attendance`
 FOR EACH ROW BEGIN
				UPDATE g_fp_attendance_log
				SET is_transferred = 1
				WHERE `date` = NEW.date_attendance AND user_id = NEW.employee_id AND is_transferred = 0;
			END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `triggerUpdateFpLogsOnUpdate`;
DELIMITER //
CREATE TRIGGER `triggerUpdateFpLogsOnUpdate` AFTER UPDATE ON `g_employee_attendance`
 FOR EACH ROW BEGIN
				UPDATE g_fp_attendance_log
				SET is_transferred = 1
				WHERE date = NEW.date_attendance AND user_id = NEW.employee_id AND is_transferred = 0;
			END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_attendance_correction_request`
--

DROP TABLE IF EXISTS `g_employee_attendance_correction_request`;
CREATE TABLE IF NOT EXISTS `g_employee_attendance_correction_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date_applied` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `date_in` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `time_in` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `time_out` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `correct_date_in` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `correct_time_in` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `correct_time_out` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `is_approved` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Pending / Approved / Disapproved',
  `is_archive` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Yes / No',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_basic_salary_history`
--

DROP TABLE IF EXISTS `g_employee_basic_salary_history`;
CREATE TABLE IF NOT EXISTS `g_employee_basic_salary_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `job_salary_rate_id` int(11) NOT NULL,
  `type` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'monthly_rate, daily_rate, hourly_rate',
  `basic_salary` float NOT NULL,
  `pay_period_id` int(11) NOT NULL COMMENT 'frequency rate: example Bi-Monthly or Monthly',
  `start_date` date NOT NULL,
  `end_date` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_benefits`
--

DROP TABLE IF EXISTS `g_employee_benefits`;
CREATE TABLE IF NOT EXISTS `g_employee_benefits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `obj_id` int(11) NOT NULL,
  `obj_type` varchar(20) CHARACTER SET latin1 NOT NULL,
  `benefit_id` int(11) NOT NULL,
  `apply_to_all` varchar(25) CHARACTER SET latin1 NOT NULL,
  `date_created` varchar(150) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_benefits_main`
--

DROP TABLE IF EXISTS `g_employee_benefits_main`;
CREATE TABLE IF NOT EXISTS `g_employee_benefits_main` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_structure_id` smallint(6) NOT NULL,
  `employee_department_id` bigint(20) NOT NULL,
  `benefit_id` int(11) NOT NULL,
  `applied_to` varchar(50) CHARACTER SET latin1 NOT NULL,
  `description` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `criteria` text COLLATE utf8_unicode_ci,
  `custom_criteria` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `company_structure_id` (`company_structure_id`),
  KEY `id` (`id`),
  KEY `employee_department_id` (`employee_department_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_branch_history`
--

DROP TABLE IF EXISTS `g_employee_branch_history`;
CREATE TABLE IF NOT EXISTS `g_employee_branch_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `company_branch_id` int(11) NOT NULL,
  `branch_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Null it means Current',
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `company_branch_id` (`company_branch_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `g_employee_branch_history`
--

INSERT INTO `g_employee_branch_history` (`id`, `employee_id`, `company_branch_id`, `branch_name`, `start_date`, `end_date`) VALUES
(1, 1, 1, 'Main', '2013-01-01', '');

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_change_schedule_request`
--

DROP TABLE IF EXISTS `g_employee_change_schedule_request`;
CREATE TABLE IF NOT EXISTS `g_employee_change_schedule_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date_applied` datetime NOT NULL,
  `date_start` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `date_end` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `time_in` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `time_out` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `change_schedule_comments` text COLLATE utf8_unicode_ci NOT NULL,
  `is_approved` int(11) NOT NULL,
  `is_archive` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_contact_details`
--

DROP TABLE IF EXISTS `g_employee_contact_details`;
CREATE TABLE IF NOT EXISTS `g_employee_contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `province` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `home_telephone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `work_telephone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `work_email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `other_email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_contribution`
--

DROP TABLE IF EXISTS `g_employee_contribution`;
CREATE TABLE IF NOT EXISTS `g_employee_contribution` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `sss_ee` float NOT NULL,
  `pagibig_ee` float NOT NULL,
  `philhealth_ee` float NOT NULL,
  `sss_er` float NOT NULL,
  `pagibig_er` float NOT NULL,
  `philhealth_er` float NOT NULL,
  `to_deduct` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `g_employee_contribution`
--

INSERT INTO `g_employee_contribution` (`id`, `employee_id`, `sss_ee`, `pagibig_ee`, `philhealth_ee`, `sss_er`, `pagibig_er`, `philhealth_er`, `to_deduct`) VALUES
(1, 1, 36.3, 0, 100, 83.7, 100, 100, 'a:3:{s:3:"sss";s:3:"Yes";s:10:"philhealth";s:3:"Yes";s:7:"pagibig";s:3:"Yes";}');

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_deductions`
--

DROP TABLE IF EXISTS `g_employee_deductions`;
CREATE TABLE IF NOT EXISTS `g_employee_deductions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `employee_id` varchar(240) CHARACTER SET latin1 NOT NULL,
  `department_section_id` varchar(240) COLLATE utf8_unicode_ci NOT NULL,
  `employment_status_id` varchar(240) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(200) CHARACTER SET latin1 NOT NULL,
  `remarks` text CHARACTER SET latin1 NOT NULL,
  `amount` double NOT NULL,
  `payroll_period_id` int(11) NOT NULL,
  `apply_to_all_employee` varchar(10) CHARACTER SET latin1 NOT NULL,
  `status` varchar(20) CHARACTER SET latin1 NOT NULL,
  `is_taxable` varchar(10) CHARACTER SET latin1 NOT NULL,
  `is_archive` varchar(10) CHARACTER SET latin1 NOT NULL,
  `date_created` varchar(30) CHARACTER SET latin1 NOT NULL,
  `is_moved_deduction` int(11) NOT NULL COMMENT '0 = No, 1 = Yes',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_dependent`
--

DROP TABLE IF EXISTS `g_employee_dependent`;
CREATE TABLE IF NOT EXISTS `g_employee_dependent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `relationship` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_details_history`
--

DROP TABLE IF EXISTS `g_employee_details_history`;
CREATE TABLE IF NOT EXISTS `g_employee_details_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(30) NOT NULL,
  `employee_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `modified_by` bigint(30) NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `history_date` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `date_modified` datetime NOT NULL,
  `is_archive` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No' COMMENT 'Yes / No',
  PRIMARY KEY (`id`),
  KEY `firstname-lastname` (`id`),
  KEY `id-first-last-code` (`id`,`employee_code`),
  KEY `employee_code` (`employee_code`),
  KEY `id` (`id`,`employee_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_direct_deposit`
--

DROP TABLE IF EXISTS `g_employee_direct_deposit`;
CREATE TABLE IF NOT EXISTS `g_employee_direct_deposit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `bank_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `account` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `account_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'checking / savings',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_dynamic_field`
--

DROP TABLE IF EXISTS `g_employee_dynamic_field`;
CREATE TABLE IF NOT EXISTS `g_employee_dynamic_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `settings_employee_field_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `screen` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_earnings`
--

DROP TABLE IF EXISTS `g_employee_earnings`;
CREATE TABLE IF NOT EXISTS `g_employee_earnings` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `amount` float(11,2) DEFAULT NULL,
  `payroll_period_id` int(11) NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `is_taxable` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `is_archive` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `object_id` int(11) NOT NULL,
  `applied_to` int(10) NOT NULL,
  `earning_type` int(11) NOT NULL COMMENT '1 = percentage / 2 = amount',
  `percentage` float(11,2) NOT NULL,
  `percentage_multiplier` int(11) NOT NULL COMMENT '1 = monthly / 2 = daily',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `object_description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `company_structure_id` (`company_structure_id`),
  KEY `payroll_period_id` (`payroll_period_id`),
  KEY `id` (`id`,`company_structure_id`,`payroll_period_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_education`
--

DROP TABLE IF EXISTS `g_employee_education`;
CREATE TABLE IF NOT EXISTS `g_employee_education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `institute` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `course` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `year` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `gpa_score` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `attainment` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_emergency_contact`
--

DROP TABLE IF EXISTS `g_employee_emergency_contact`;
CREATE TABLE IF NOT EXISTS `g_employee_emergency_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `person` varchar(160) COLLATE utf8_unicode_ci NOT NULL,
  `relationship` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `home_telephone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `work_telephone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(280) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_extend_contract`
--

DROP TABLE IF EXISTS `g_employee_extend_contract`;
CREATE TABLE IF NOT EXISTS `g_employee_extend_contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `is_done` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `end_date` (`end_date`),
  KEY `start_date` (`start_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_fixed_contributions`
--

DROP TABLE IF EXISTS `g_employee_fixed_contributions`;
CREATE TABLE IF NOT EXISTS `g_employee_fixed_contributions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `type` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `ee_amount` float(11,2) NOT NULL,
  `er_amount` float(11,2) NOT NULL,
  `is_activated` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_group_schedule`
--

DROP TABLE IF EXISTS `g_employee_group_schedule`;
CREATE TABLE IF NOT EXISTS `g_employee_group_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_group_id` int(11) NOT NULL COMMENT 'g_employee.id OR g_company_structure.id ',
  `schedule_group_id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `date_start` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `date_end` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `employee_group` int(1) NOT NULL COMMENT '1 = Employee; 2 = Group',
  PRIMARY KEY (`id`),
  KEY `schedule_id` (`schedule_id`),
  KEY `employee_group_id` (`employee_group_id`),
  KEY `schedule_group_id` (`schedule_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This is template schedule' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_job_history`
--

DROP TABLE IF EXISTS `g_employee_job_history`;
CREATE TABLE IF NOT EXISTS `g_employee_job_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `employment_status` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Part Time, Full Time etc',
  `start_date` date NOT NULL,
  `end_date` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `job_id` (`job_id`),
  KEY `employee_id` (`employee_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `g_employee_job_history`
--

INSERT INTO `g_employee_job_history` (`id`, `employee_id`, `job_id`, `name`, `employment_status`, `start_date`, `end_date`) VALUES
(1, 1, 2, 'Human Resource Manager', 'Full Time', '2013-01-01', '');

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_language`
--

DROP TABLE IF EXISTS `g_employee_language`;
CREATE TABLE IF NOT EXISTS `g_employee_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `language` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fluency` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'writing, speaking,reading',
  `competency` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'poor,basic,good, mother tongue',
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_leave_available`
--

DROP TABLE IF EXISTS `g_employee_leave_available`;
CREATE TABLE IF NOT EXISTS `g_employee_leave_available` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `leave_id` int(11) NOT NULL,
  `no_of_days_alloted` float NOT NULL,
  `no_of_days_available` float NOT NULL,
  `no_of_days_used` float NOT NULL,
  `covered_year` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_leave_credit_history`
--

DROP TABLE IF EXISTS `g_employee_leave_credit_history`;
CREATE TABLE IF NOT EXISTS `g_employee_leave_credit_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leave_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `credits_added` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_leave_credit_tracking`
--

DROP TABLE IF EXISTS `g_employee_leave_credit_tracking`;
CREATE TABLE IF NOT EXISTS `g_employee_leave_credit_tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(10) NOT NULL,
  `leave_id` int(11) NOT NULL,
  `credit` int(10) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_leave_request`
--

DROP TABLE IF EXISTS `g_employee_leave_request`;
CREATE TABLE IF NOT EXISTS `g_employee_leave_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL DEFAULT '1',
  `employee_id` int(11) NOT NULL,
  `leave_id` int(11) NOT NULL,
  `date_applied` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `time_applied` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `date_start` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `date_end` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `apply_half_day_date_start` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `apply_half_day_date_end` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `leave_comments` text COLLATE utf8_unicode_ci NOT NULL,
  `is_approved` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Pending / Approved / Disapproved',
  `is_paid` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `is_archive` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Yes / No',
  PRIMARY KEY (`id`),
  KEY `emp_id-date_start-date_end` (`employee_id`,`date_start`,`date_end`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_license`
--

DROP TABLE IF EXISTS `g_employee_license`;
CREATE TABLE IF NOT EXISTS `g_employee_license` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `license_type` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Engineer License etc',
  `license_number` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `issued_date` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `expiry_date` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(164) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_loan`
--

DROP TABLE IF EXISTS `g_employee_loan`;
CREATE TABLE IF NOT EXISTS `g_employee_loan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `loan_type_id` int(11) NOT NULL,
  `employee_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `loan_title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `interest_rate` varchar(50) CHARACTER SET latin1 NOT NULL,
  `loan_amount` double(11,2) NOT NULL,
  `amount_paid` double(11,2) NOT NULL,
  `months_to_pay` int(11) NOT NULL,
  `deduction_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` varchar(50) CHARACTER SET latin1 NOT NULL,
  `end_date` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `total_amount_to_pay` double(11,2) NOT NULL,
  `deduction_per_period` double(11,2) NOT NULL,
  `status` varchar(20) CHARACTER SET latin1 NOT NULL,
  `is_lock` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `is_archive` varchar(10) CHARACTER SET latin1 NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_loan_details`
--

DROP TABLE IF EXISTS `g_employee_loan_details`;
CREATE TABLE IF NOT EXISTS `g_employee_loan_details` (
  `id` int(11) NOT NULL,
  `company_structure_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `loan_id` int(11) NOT NULL,
  `date_of_payment` varchar(10) CHARACTER SET latin1 NOT NULL,
  `amount` double NOT NULL,
  `amount_paid` double NOT NULL,
  `is_paid` varchar(10) CHARACTER SET latin1 NOT NULL,
  `remarks` text CHARACTER SET latin1 NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_loan_payment_breakdown`
--

DROP TABLE IF EXISTS `g_employee_loan_payment_breakdown`;
CREATE TABLE IF NOT EXISTS `g_employee_loan_payment_breakdown` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loan_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `loan_payment_id` int(11) NOT NULL,
  `reference_number` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `amount_paid` double NOT NULL,
  `date_paid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_loan_payment_history`
--

DROP TABLE IF EXISTS `g_employee_loan_payment_history`;
CREATE TABLE IF NOT EXISTS `g_employee_loan_payment_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `employee_loan_id` int(11) NOT NULL,
  `deduction_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `reference_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `balance` double NOT NULL,
  `amount_paid` double NOT NULL,
  `date_paid` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_loan_payment_schedule`
--

DROP TABLE IF EXISTS `g_employee_loan_payment_schedule`;
CREATE TABLE IF NOT EXISTS `g_employee_loan_payment_schedule` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(11) NOT NULL,
  `loan_id` int(11) NOT NULL,
  `reference_number` varchar(50) NOT NULL,
  `loan_payment_scheduled_date` varchar(50) NOT NULL,
  `amount_to_pay` double(11,2) NOT NULL,
  `amount_paid` double(11,2) NOT NULL,
  `date_paid` varchar(50) NOT NULL,
  `remarks` varchar(180) NOT NULL,
  `is_lock` varchar(5) NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_make_up_schedule_request`
--

DROP TABLE IF EXISTS `g_employee_make_up_schedule_request`;
CREATE TABLE IF NOT EXISTS `g_employee_make_up_schedule_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `date_applied` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `date_from` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `date_to` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `start_time` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `end_time` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `is_approved` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `is_archive` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_membership`
--

DROP TABLE IF EXISTS `g_employee_membership`;
CREATE TABLE IF NOT EXISTS `g_employee_membership` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `membership_type_id` int(11) NOT NULL,
  `membership_id` int(11) NOT NULL,
  `subscription_ownership` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'company, individual',
  `subscription_amount` double NOT NULL,
  `commence_date` date NOT NULL,
  `renewal_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_memo`
--

DROP TABLE IF EXISTS `g_employee_memo`;
CREATE TABLE IF NOT EXISTS `g_employee_memo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `memo_id` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `memo` text COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(164) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_offense` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `offense_description` text COLLATE utf8_unicode_ci NOT NULL,
  `remarks` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_official_business_request`
--

DROP TABLE IF EXISTS `g_employee_official_business_request`;
CREATE TABLE IF NOT EXISTS `g_employee_official_business_request` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date_applied` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `date_start` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `date_end` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `is_approved` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `is_archive` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id-date` (`employee_id`,`date_start`,`date_end`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_overtime`
--

DROP TABLE IF EXISTS `g_employee_overtime`;
CREATE TABLE IF NOT EXISTS `g_employee_overtime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `date` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `time_in` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `time_out` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `date_in` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `date_out` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Pending, Approved, Disapproved',
  `is_archived` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id-date` (`employee_id`,`date`),
  KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_overtime_rates`
--

DROP TABLE IF EXISTS `g_employee_overtime_rates`;
CREATE TABLE IF NOT EXISTS `g_employee_overtime_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `ot_rate` float(11,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_overtime_request`
--

DROP TABLE IF EXISTS `g_employee_overtime_request`;
CREATE TABLE IF NOT EXISTS `g_employee_overtime_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date_attendance` date NOT NULL,
  `date_applied` datetime NOT NULL,
  `date_start` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `date_end` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `time_in` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `time_out` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `overtime_comments` text COLLATE utf8_unicode_ci NOT NULL,
  `is_approved` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Pending / Approved / Disapproved',
  `is_archive` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Yes / No',
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Not used. Deprecated' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_payable`
--

DROP TABLE IF EXISTS `g_employee_payable`;
CREATE TABLE IF NOT EXISTS `g_employee_payable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `balance_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_amount` double(15,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_payable_history`
--

DROP TABLE IF EXISTS `g_employee_payable_history`;
CREATE TABLE IF NOT EXISTS `g_employee_payable_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_payable_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `amount_paid` double(15,2) NOT NULL,
  `date_paid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_payslip`
--

DROP TABLE IF EXISTS `g_employee_payslip`;
CREATE TABLE IF NOT EXISTS `g_employee_payslip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `period_start` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `period_end` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `payout_date` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `basic_pay` double(15,2) NOT NULL,
  `gross_pay` double(15,2) NOT NULL,
  `total_earnings` decimal(10,2) DEFAULT NULL,
  `total_deductions` decimal(10,2) DEFAULT NULL,
  `net_pay` double(15,2) NOT NULL,
  `taxable` double(15,2) NOT NULL,
  `non_taxable` double(15,2) NOT NULL,
  `withheld_tax` float(15,2) NOT NULL,
  `month_13th` double(15,2) NOT NULL,
  `sss` double(15,2) NOT NULL,
  `pagibig` double(15,2) NOT NULL,
  `philhealth` double(15,2) NOT NULL,
  `earnings` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'Initial earnings like overtime, late, basic pay',
  `other_earnings` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'other earnings to be added manually',
  `deductions` longtext COLLATE utf8_unicode_ci NOT NULL,
  `other_deductions` longtext COLLATE utf8_unicode_ci NOT NULL,
  `labels` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `overtime` double NOT NULL,
  `number_of_declared_dependents` int(11) NOT NULL,
  `taxable_benefits` double(15,2) NOT NULL,
  `non_taxable_benefits` double(15,2) NOT NULL,
  `tardiness_amount` double(15,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `period_start-end` (`period_start`,`period_end`,`employee_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_performance`
--

DROP TABLE IF EXISTS `g_employee_performance`;
CREATE TABLE IF NOT EXISTS `g_employee_performance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `performance_id` int(11) NOT NULL,
  `performance_title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `employee_id` int(11) NOT NULL,
  `reviewer_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `position` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` date NOT NULL,
  `period_from` date NOT NULL,
  `period_to` date NOT NULL,
  `due_date` date NOT NULL,
  `summary` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'being reviewed, pending',
  `kpi` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_request`
--

DROP TABLE IF EXISTS `g_employee_request`;
CREATE TABLE IF NOT EXISTS `g_employee_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `settings_request_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `request_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_time` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `end_time` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT '0 = Pending / 1 = Approve / -1 = Disapprove',
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_request_approvers`
--

DROP TABLE IF EXISTS `g_employee_request_approvers`;
CREATE TABLE IF NOT EXISTS `g_employee_request_approvers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Generic / Overtime / Leave / Rest Day',
  `request_type_id` int(11) NOT NULL,
  `position_employee_id` int(11) NOT NULL,
  `type` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Employee Id / Position Id / Department Id',
  `level` int(11) NOT NULL COMMENT '0 = Override',
  `override_level` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Pending / Approved / Disapproved',
  `remarks` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Approver''s Remarks',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_requirements`
--

DROP TABLE IF EXISTS `g_employee_requirements`;
CREATE TABLE IF NOT EXISTS `g_employee_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `requirements` text COLLATE utf8_unicode_ci NOT NULL,
  `is_complete` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `date_updated` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_restday`
--

DROP TABLE IF EXISTS `g_employee_restday`;
CREATE TABLE IF NOT EXISTS `g_employee_restday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `date` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `time_in` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `time_out` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id-date` (`employee_id`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This is a restday schedule NOT the actual restday work' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_rest_day_request`
--

DROP TABLE IF EXISTS `g_employee_rest_day_request`;
CREATE TABLE IF NOT EXISTS `g_employee_rest_day_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `date_applied` datetime NOT NULL,
  `date_start` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `date_end` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `rest_day_comments` text COLLATE utf8_unicode_ci NOT NULL,
  `is_approved` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `is_archive` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_schedule_specific`
--

DROP TABLE IF EXISTS `g_employee_schedule_specific`;
CREATE TABLE IF NOT EXISTS `g_employee_schedule_specific` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `date_start` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `date_end` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `time_in` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `time_out` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id-date_start` (`employee_id`,`date_start`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_skills`
--

DROP TABLE IF EXISTS `g_employee_skills`;
CREATE TABLE IF NOT EXISTS `g_employee_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `skill` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `years_experience` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_status_history`
--

DROP TABLE IF EXISTS `g_employee_status_history`;
CREATE TABLE IF NOT EXISTS `g_employee_status_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `employee_status_id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `start_date` varchar(10) NOT NULL,
  `end_date` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_subdivision_history`
--

DROP TABLE IF EXISTS `g_employee_subdivision_history`;
CREATE TABLE IF NOT EXISTS `g_employee_subdivision_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `company_structure_id` int(11) NOT NULL COMMENT 'branch,division, department, team',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Finance Department, Production Team',
  `type` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Department, Group, Team etc',
  `start_date` date NOT NULL,
  `end_date` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'if null means current',
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `company_structure_id` (`company_structure_id`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `g_employee_subdivision_history`
--

INSERT INTO `g_employee_subdivision_history` (`id`, `employee_id`, `company_structure_id`, `name`, `type`, `start_date`, `end_date`) VALUES
(1, 1, 5, 'Human Resources', 'Department', '2013-01-01', '');

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_supervisor`
--

DROP TABLE IF EXISTS `g_employee_supervisor`;
CREATE TABLE IF NOT EXISTS `g_employee_supervisor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `supervisor_id` int(11) NOT NULL COMMENT 'employee_id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_tags`
--

DROP TABLE IF EXISTS `g_employee_tags`;
CREATE TABLE IF NOT EXISTS `g_employee_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `tags` text COLLATE utf8_unicode_ci NOT NULL,
  `is_archive` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_training`
--

DROP TABLE IF EXISTS `g_employee_training`;
CREATE TABLE IF NOT EXISTS `g_employee_training` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `cost` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_undertime_request`
--

DROP TABLE IF EXISTS `g_employee_undertime_request`;
CREATE TABLE IF NOT EXISTS `g_employee_undertime_request` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date_applied` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_undertime` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `time_out` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `is_approved` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `is_archive` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_user`
--

DROP TABLE IF EXISTS `g_employee_user`;
CREATE TABLE IF NOT EXISTS `g_employee_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `employee_id` bigint(20) NOT NULL,
  `username` varchar(80) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role_id` int(11) NOT NULL,
  `date_created` varchar(80) NOT NULL,
  `last_modified` varchar(80) NOT NULL,
  `is_archive` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `g_employee_user`
--

INSERT INTO `g_employee_user` (`id`, `company_structure_id`, `employee_id`, `username`, `password`, `role_id`, `date_created`, `last_modified`, `is_archive`) VALUES
(1, 1, 1, 'admin', 'pyAatYGqkByFsiOTn-NhfKI5UHygF3VyOIXhGbC-U5k', 1, '2017-05-09 00:00:00', '', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `g_employee_work_experience`
--

DROP TABLE IF EXISTS `g_employee_work_experience`;
CREATE TABLE IF NOT EXISTS `g_employee_work_experience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `company` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `job_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_error_attendance`
--

DROP TABLE IF EXISTS `g_error_attendance`;
CREATE TABLE IF NOT EXISTS `g_error_attendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `employee_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_attendance` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `is_fixed` tinyint(1) NOT NULL,
  `error_type_id` tinyint(10) NOT NULL COMMENT 'ERROR_INVALID_EMPLOYEE = 1; ERROR_INVALID_TIME = 2; ERROR_INVALID_OT = 3; ERROR_INVALID_DATE = 4; ERROR_NO_OUT = 5; ERROR_NO_IN = 6;',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_error_leave`
--

DROP TABLE IF EXISTS `g_error_leave`;
CREATE TABLE IF NOT EXISTS `g_error_leave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `employee_code` int(11) NOT NULL,
  `employee_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `date_applied` varchar(20) CHARACTER SET latin1 NOT NULL,
  `date_start` varchar(15) CHARACTER SET latin1 NOT NULL,
  `date_end` varchar(15) CHARACTER SET latin1 NOT NULL,
  `message` text CHARACTER SET latin1 NOT NULL,
  `is_fixed` varchar(10) CHARACTER SET latin1 NOT NULL DEFAULT 'No' COMMENT 'Yes / No',
  `error_type_id` int(11) NOT NULL COMMENT '1 = EMPLOYEE_DOES_NOT_EXISTS, 2 = INVALID_START_END_DATE',
  PRIMARY KEY (`id`),
  KEY `default_log` (`employee_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_error_overtime`
--

DROP TABLE IF EXISTS `g_error_overtime`;
CREATE TABLE IF NOT EXISTS `g_error_overtime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `employee_code` int(11) NOT NULL,
  `employee_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `date_attendance` varchar(20) CHARACTER SET latin1 NOT NULL,
  `time_in` varchar(15) CHARACTER SET latin1 NOT NULL,
  `time_out` varchar(15) CHARACTER SET latin1 NOT NULL,
  `message` text CHARACTER SET latin1 NOT NULL,
  `is_fixed` varchar(10) CHARACTER SET latin1 NOT NULL DEFAULT 'No' COMMENT 'Yes / No',
  `error_type_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `default_log` (`employee_code`),
  KEY `employee_id,date_attendance` (`employee_id`,`date_attendance`),
  KEY `date_attendance` (`date_attendance`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_error_payslip`
--

DROP TABLE IF EXISTS `g_error_payslip`;
CREATE TABLE IF NOT EXISTS `g_error_payslip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `is_fixed` tinyint(1) NOT NULL,
  `error_type_id` int(11) NOT NULL COMMENT '1 = ERROR_NO_SALARY, 2 = ERROR_NO_ATTENDANCE',
  `date_logged` date NOT NULL,
  `time_logged` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_exam`
--

DROP TABLE IF EXISTS `g_exam`;
CREATE TABLE IF NOT EXISTS `g_exam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `applicable_to_job` text COLLATE utf8_unicode_ci NOT NULL,
  `apply_to_all_jobs` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `passing_percentage` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `time_duration` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'days:hours:minutes',
  `created_by` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_exam_choices`
--

DROP TABLE IF EXISTS `g_exam_choices`;
CREATE TABLE IF NOT EXISTS `g_exam_choices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_question_id` int(11) NOT NULL,
  `choices` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `order_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_exam_question`
--

DROP TABLE IF EXISTS `g_exam_question`;
CREATE TABLE IF NOT EXISTS `g_exam_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `answer` text COLLATE utf8_unicode_ci NOT NULL,
  `order_by` int(11) NOT NULL,
  `type` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_excluded_employee_deduction`
--

DROP TABLE IF EXISTS `g_excluded_employee_deduction`;
CREATE TABLE IF NOT EXISTS `g_excluded_employee_deduction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) NOT NULL,
  `payroll_period_id` int(11) NOT NULL,
  `new_payroll_period_id` int(11) NOT NULL,
  `variable_name` varchar(90) NOT NULL,
  `amount` float NOT NULL,
  `action` varchar(50) NOT NULL,
  `date_created` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_fp_attendance_log`
--

DROP TABLE IF EXISTS `g_fp_attendance_log`;
CREATE TABLE IF NOT EXISTS `g_fp_attendance_log` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `employee_code` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `employee_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `time` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `remarks` varchar(128) CHARACTER SET latin1 NOT NULL,
  `sync` int(11) NOT NULL DEFAULT '1',
  `is_transferred` int(1) unsigned zerofill NOT NULL COMMENT '1 = transferred / 0 = new data',
  `employee_device_id` int(11) NOT NULL COMMENT 'for anviz device',
  PRIMARY KEY (`id`),
  KEY `code,date,type` (`employee_code`,`date`,`type`),
  KEY `date` (`date`),
  KEY `employee_code` (`employee_code`),
  KEY `code-date-time` (`employee_code`,`date`,`time`),
  KEY `date-time` (`date`,`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_fp_attendance_summary`
--

DROP TABLE IF EXISTS `g_fp_attendance_summary`;
CREATE TABLE IF NOT EXISTS `g_fp_attendance_summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `employee_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'for biometrics',
  `actual_date_in` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'for biometrics',
  `actual_time_in` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `actual_date_out` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `actual_time_out` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `actual_total_hours_worked` varchar(16) COLLATE utf8_unicode_ci NOT NULL COMMENT 'compute by biometrics',
  `done` int(11) NOT NULL COMMENT 'for biometrics',
  `sync` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_fp_fingerprint`
--

DROP TABLE IF EXISTS `g_fp_fingerprint`;
CREATE TABLE IF NOT EXISTS `g_fp_fingerprint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `employee_code` varchar(64) CHARACTER SET latin1 NOT NULL COMMENT 'employee_code example 100203-023',
  `name` varchar(64) CHARACTER SET latin1 NOT NULL,
  `template` longblob NOT NULL,
  `finger` varchar(64) CHARACTER SET latin1 NOT NULL,
  `integer_representation` int(11) NOT NULL,
  `sync` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_group_restday`
--

DROP TABLE IF EXISTS `g_group_restday`;
CREATE TABLE IF NOT EXISTS `g_group_restday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL COMMENT 'department / section / group id',
  `date` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_holiday`
--

DROP TABLE IF EXISTS `g_holiday`;
CREATE TABLE IF NOT EXISTS `g_holiday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `public_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `holiday_type` tinyint(1) NOT NULL COMMENT '1 = legal, 2 = special',
  `holiday_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `holiday_month` tinyint(12) NOT NULL,
  `holiday_day` tinyint(31) NOT NULL,
  `holiday_year` int(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `month-day-year` (`holiday_month`,`holiday_day`,`holiday_year`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `g_holiday`
--

INSERT INTO `g_holiday` (`id`, `public_id`, `holiday_type`, `holiday_title`, `holiday_month`, `holiday_day`, `holiday_year`) VALUES
(1, '59116b6978277', 1, 'New Year''s Day', 1, 1, 2017),
(2, '59116b6978660', 2, 'Chinese New Year', 1, 3, 2017),
(3, '59116b6978a48', 2, 'EDSA Revolution Anniversary', 2, 25, 2017),
(4, '59116b6978a48', 1, 'Good Friday', 4, 18, 2017),
(5, '59116b6978a48', 1, 'Maundry Thursday', 4, 17, 2017),
(6, '59116b6978e30', 1, 'Day of Valour', 4, 9, 2017),
(7, '59116b6978e30', 2, 'Black Saturday', 4, 19, 2017),
(8, '59116b6978e30', 1, 'Labor Day', 5, 1, 2017),
(9, '59116b6978e30', 1, 'Independence Day', 6, 12, 2017),
(10, '59116b6978e30', 1, 'Eid''l Fitr', 7, 28, 2017),
(11, '59116b6979218', 2, 'Ninoy Aquino Day', 8, 21, 2017),
(12, '59116b6979218', 1, 'National Heroes'' Day', 8, 25, 2017),
(13, '59116b6979218', 1, 'Eidul Adha', 10, 5, 2017),
(14, '59116b6979218', 1, 'Bonifacio Day', 11, 30, 2017),
(15, '59116b6979600', 2, 'All Saints'' Day', 11, 1, 2017),
(16, '59116b6979600', 2, 'All Souls'' Day', 11, 2, 2017),
(17, '59116b6979600', 1, 'Rizal Day', 12, 30, 2017),
(18, '59116b6979600', 2, 'Christmas Eve', 12, 24, 2017),
(19, '59116b6979600', 2, 'Last Day of the Year', 12, 31, 2017),
(20, '59116b69799e8', 1, 'Christmas Day', 12, 25, 2017);

-- --------------------------------------------------------

--
-- Table structure for table `g_holiday_branch`
--

DROP TABLE IF EXISTS `g_holiday_branch`;
CREATE TABLE IF NOT EXISTS `g_holiday_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `holiday_id` int(11) NOT NULL,
  `company_branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_incentive_leave_history`
--

DROP TABLE IF EXISTS `g_incentive_leave_history`;
CREATE TABLE IF NOT EXISTS `g_incentive_leave_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `month_number` int(2) NOT NULL,
  `year` int(11) NOT NULL,
  `total_given` int(5) NOT NULL,
  `date_process` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_job`
--

DROP TABLE IF EXISTS `g_job`;
CREATE TABLE IF NOT EXISTS `g_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `job_specification_id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `g_job`
--

INSERT INTO `g_job` (`id`, `company_structure_id`, `job_specification_id`, `title`, `is_active`) VALUES
(1, 1, 0, 'CEO', 1),
(2, 1, 0, 'Human Resource Manager', 1),
(3, 1, 0, 'HR Supervisor', 1),
(4, 1, 0, 'HR Assistant', 1),
(5, 1, 0, 'Admin Assistant', 1),
(6, 1, 0, 'Finance Manager', 1),
(7, 1, 0, 'Finance Analyst', 1),
(8, 1, 0, 'Production Manager', 1),
(9, 1, 0, 'Production Supervisor', 1),
(10, 1, 0, 'QA Manager', 1),
(11, 1, 0, 'Jr. QA Supervisor', 1),
(12, 1, 0, 'Manager', 1),
(13, 1, 0, 'Staff', 1),
(14, 1, 0, 'IT', 1),
(15, 1, 0, 'Sales Executive', 1);

-- --------------------------------------------------------

--
-- Table structure for table `g_job_application_event`
--

DROP TABLE IF EXISTS `g_job_application_event`;
CREATE TABLE IF NOT EXISTS `g_job_application_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `date_time_created` datetime NOT NULL,
  `created_by` int(11) NOT NULL COMMENT 'employee_id',
  `hiring_manager_id` int(11) DEFAULT NULL,
  `date_time_event` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'on march 6, 3:00pm',
  `event_type` int(11) NOT NULL COMMENT 'Application Submitted: 0, Interview: 1, Job Offered: 2, Offer Declined: 3, Rejected: 4, Hired: 5',
  `application_status_id` int(11) NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `remarks` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `applicant_id` (`applicant_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_job_employment_status`
--

DROP TABLE IF EXISTS `g_job_employment_status`;
CREATE TABLE IF NOT EXISTS `g_job_employment_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `employment_status_id` int(11) NOT NULL,
  `employment_status` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_job_salary_rate`
--

DROP TABLE IF EXISTS `g_job_salary_rate`;
CREATE TABLE IF NOT EXISTS `g_job_salary_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `job_level` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'senior programmer, beginner programmer etc',
  `minimum_salary` double DEFAULT NULL,
  `maximum_salary` double DEFAULT NULL,
  `step_salary` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `g_job_salary_rate`
--

INSERT INTO `g_job_salary_rate` (`id`, `company_structure_id`, `job_level`, `minimum_salary`, `maximum_salary`, `step_salary`) VALUES
(1, 1, 'Officials and managers', 15000, 100000, 0),
(2, 1, 'Professionals', 12000, 50000, 0),
(3, 1, 'Technicians', 10000, 20000, 0),
(4, 1, 'Sales', 10000, 18000, 0),
(5, 1, 'Office and clerical', 12000, 18000, 0),
(6, 1, 'Craft Workers (skilled)', 12000, 18000, 0),
(7, 1, 'Operatives (semiskilled)', 10000, 18000, 0),
(8, 1, 'Laborers (unskilled)', 10000, 14000, 0),
(9, 1, 'Service workers', 10000, 14000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `g_job_specification`
--

DROP TABLE IF EXISTS `g_job_specification`;
CREATE TABLE IF NOT EXISTS `g_job_specification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `duties` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_job_vacancy`
--

DROP TABLE IF EXISTS `g_job_vacancy`;
CREATE TABLE IF NOT EXISTS `g_job_vacancy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `job_description` text COLLATE utf8_unicode_ci NOT NULL,
  `hiring_manager_id` int(11) NOT NULL COMMENT 'employee_id',
  `job_title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `hiring_manager_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `publication_date` date NOT NULL,
  `advertisement_end` date NOT NULL,
  `is_active` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_leave`
--

DROP TABLE IF EXISTS `g_leave`;
CREATE TABLE IF NOT EXISTS `g_leave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `default_credit` int(11) NOT NULL,
  `is_paid` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `gl_is_archive` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `g_leave`
--

INSERT INTO `g_leave` (`id`, `company_structure_id`, `name`, `type`, `default_credit`, `is_paid`, `gl_is_archive`, `is_default`) VALUES
(1, 1, 'Sick Leave', '', 0, 'Yes', 'No', 'No'),
(2, 1, 'Vacation Leave', '', 0, 'Yes', 'No', 'No'),
(3, 1, 'Bereavement Leave', '', 0, 'Yes', 'No', 'No'),
(4, 1, 'Maternity Leave', '', 0, 'Yes', 'No', 'No'),
(5, 1, 'Emergency Leave', '', 0, 'Yes', 'No', 'No'),
(6, 1, 'Paternity Leave', '', 0, 'Yes', 'No', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `g_loan_deduction_type`
--

DROP TABLE IF EXISTS `g_loan_deduction_type`;
CREATE TABLE IF NOT EXISTS `g_loan_deduction_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `deduction_type` varchar(180) CHARACTER SET latin1 NOT NULL,
  `is_archive` varchar(10) CHARACTER SET latin1 NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `g_loan_deduction_type`
--

INSERT INTO `g_loan_deduction_type` (`id`, `company_structure_id`, `deduction_type`, `is_archive`, `date_created`) VALUES
(1, 1, 'Bi-monthly', 'No', '2017-05-09 09:10:31'),
(2, 1, 'Monthly', 'No', '2017-05-09 09:10:31'),
(3, 1, 'Weekly', 'No', '2017-05-09 09:10:31'),
(4, 1, 'Daily', 'No', '2017-05-09 09:10:31'),
(5, 1, 'Quarterly', 'No', '2017-05-09 09:10:31');

-- --------------------------------------------------------

--
-- Table structure for table `g_loan_type`
--

DROP TABLE IF EXISTS `g_loan_type`;
CREATE TABLE IF NOT EXISTS `g_loan_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `loan_type` varchar(180) CHARACTER SET latin1 NOT NULL,
  `is_archive` varchar(10) CHARACTER SET latin1 NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `g_loan_type`
--

INSERT INTO `g_loan_type` (`id`, `company_structure_id`, `loan_type`, `is_archive`, `date_created`) VALUES
(1, 1, 'Salary Loan', 'No', '2017-05-09 09:10:31'),
(2, 1, 'Cash Advance', 'No', '2017-05-09 09:10:31'),
(3, 1, 'Pagibig Loan', 'No', '2017-05-09 09:10:31'),
(4, 1, 'SSS Loan', 'No', '2017-05-09 09:10:31'),
(5, 1, 'Others', 'No', '2017-05-09 09:10:31');

-- --------------------------------------------------------

--
-- Table structure for table `g_net_taxable_table`
--

DROP TABLE IF EXISTS `g_net_taxable_table`;
CREATE TABLE IF NOT EXISTS `g_net_taxable_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `over` float NOT NULL,
  `not_over` float NOT NULL,
  `amount` float NOT NULL,
  `rate_percentage` float NOT NULL,
  `excess_over` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `g_net_taxable_table`
--

INSERT INTO `g_net_taxable_table` (`id`, `over`, `not_over`, `amount`, `rate_percentage`, `excess_over`) VALUES
(1, 0, 10000, 0, 0.05, 0),
(2, 10000, 30000, 500, 0.1, 10000),
(3, 30000, 70000, 2500, 0.15, 30000),
(4, 70000, 140000, 8500, 0.2, 70000),
(5, 140000, 250000, 22500, 0.25, 140000),
(6, 250000, 500000, 50000, 0.3, 250000),
(7, 500000, 10000000, 125000, 0.32, 500000);

-- --------------------------------------------------------

--
-- Table structure for table `g_notifications`
--

DROP TABLE IF EXISTS `g_notifications`;
CREATE TABLE IF NOT EXISTS `g_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` varchar(150) NOT NULL,
  `description` varchar(180) NOT NULL,
  `status` varchar(20) NOT NULL,
  `item` int(11) NOT NULL,
  `date_modified` varchar(50) NOT NULL,
  `date_created` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `g_notifications`
--

INSERT INTO `g_notifications` (`id`, `event_type`, `description`, `status`, `item`, `date_modified`, `date_created`) VALUES
(2, 'No Salary Rate', 'Employee has no salary rate yet.', 'New', 0, '2017-05-09 09:11:01', '2017-05-09 09:11:01'),
(3, 'End of Contract', 'Employee reached end of contract this month.', 'New', 0, '2017-05-09 09:11:01', '2017-05-09 09:11:01'),
(4, 'No Department', 'Employee has no assigned department yet.', 'New', 0, '2017-05-09 09:11:01', '2017-05-09 09:11:01'),
(5, 'No Job Title', 'Employee has no Job Title yet.', 'New', 0, '2017-05-09 09:11:01', '2017-05-09 09:11:01'),
(6, 'No Employment Status', 'Employee has no employment status.', 'New', 0, '2017-05-09 09:11:01', '2017-05-09 09:11:01'),
(7, 'No Employee Status', 'Employee has no employee status.', 'New', 1, '2017-05-09 09:11:01', '2017-05-09 09:11:01'),
(8, 'Tardiness', 'Employee is late today.', 'New', 0, '2017-05-09 09:11:01', '2017-05-09 09:11:01'),
(9, 'Incomplete DTR', 'Employee with incomplete DTR this month.', 'New', 0, '2017-05-09 09:11:01', '2017-05-09 09:11:01'),
(10, 'Employee with incorrect shift', 'Employee with incorrect shift.', 'New', 0, '2017-05-09 09:11:01', '2017-05-09 09:11:01'),
(11, 'Employee with undertime', 'Employee with undertime.', 'New', 0, '2017-05-09 09:11:01', '2017-05-09 09:11:01'),
(12, 'No Bank Account', 'Employee has no bank account.', 'New', 1, '2017-05-09 09:11:02', '2017-05-09 09:11:02'),
(13, 'Upcoming Birthday', 'Employee with upcoming birthday.', 'New', 0, '2017-05-09 09:11:02', '2017-05-09 09:11:02'),
(14, 'Birthday Today', 'Employee with birthday today.', 'New', 0, '2017-05-09 09:11:02', '2017-05-09 09:11:02');

-- --------------------------------------------------------

--
-- Table structure for table `g_overtime_allowance`
--

DROP TABLE IF EXISTS `g_overtime_allowance`;
CREATE TABLE IF NOT EXISTS `g_overtime_allowance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `object_id` bigint(20) NOT NULL,
  `object_type` varchar(5) NOT NULL,
  `applied_day_type` text NOT NULL,
  `ot_allowance` float NOT NULL,
  `multiplier` int(11) NOT NULL,
  `max_ot_allowance` float NOT NULL,
  `date_start` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `description_day_type` text NOT NULL,
  `date_created` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_pagibig_table`
--

DROP TABLE IF EXISTS `g_pagibig_table`;
CREATE TABLE IF NOT EXISTS `g_pagibig_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `salary_from` float NOT NULL,
  `salary_to` float NOT NULL,
  `multiplier_employee` float NOT NULL,
  `multiplier_employer` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `g_pagibig_table`
--

INSERT INTO `g_pagibig_table` (`id`, `company_structure_id`, `salary_from`, `salary_to`, `multiplier_employee`, `multiplier_employer`) VALUES
(1, 1, 0, 1499.99, 0.01, 0.02),
(2, 1, 1500, 999999, 0.02, 0.02);

-- --------------------------------------------------------

--
-- Table structure for table `g_payroll_variables`
--

DROP TABLE IF EXISTS `g_payroll_variables`;
CREATE TABLE IF NOT EXISTS `g_payroll_variables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number_of_days` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `g_payroll_variables`
--

INSERT INTO `g_payroll_variables` (`id`, `number_of_days`) VALUES
(1, 26.17);

-- --------------------------------------------------------

--
-- Table structure for table `g_payslip_deductions`
--

DROP TABLE IF EXISTS `g_payslip_deductions`;
CREATE TABLE IF NOT EXISTS `g_payslip_deductions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deduction_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='DEPRECATED!' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_payslip_earnings`
--

DROP TABLE IF EXISTS `g_payslip_earnings`;
CREATE TABLE IF NOT EXISTS `g_payslip_earnings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `earning_type` int(10) NOT NULL COMMENT '0=normal, 1=adjustment, 2=allowance, 3=bonus, 4=advance',
  `earning_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='DEPRECATED!' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_payslip_template`
--

DROP TABLE IF EXISTS `g_payslip_template`;
CREATE TABLE IF NOT EXISTS `g_payslip_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(320) NOT NULL,
  `is_default` varchar(5) NOT NULL COMMENT 'Yes or No',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `g_payslip_template`
--

INSERT INTO `g_payslip_template` (`id`, `template_name`, `is_default`) VALUES
(1, 'Template 01', 'No'),
(2, 'Template 02', 'No'),
(3, 'Template 03', 'No'),
(4, 'Template 04', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `g_performance`
--

DROP TABLE IF EXISTS `g_performance`;
CREATE TABLE IF NOT EXISTS `g_performance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `title` varchar(124) COLLATE utf8_unicode_ci NOT NULL,
  `job_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `date_created` date NOT NULL,
  `created_by` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `is_archive` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_performance_indicator`
--

DROP TABLE IF EXISTS `g_performance_indicator`;
CREATE TABLE IF NOT EXISTS `g_performance_indicator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `performance_id` int(11) NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(230) COLLATE utf8_unicode_ci NOT NULL,
  `rate_min` int(11) NOT NULL,
  `rate_max` int(11) NOT NULL,
  `rate_default` int(11) NOT NULL,
  `order_by` int(11) NOT NULL,
  `is_active` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_philhealth`
--

DROP TABLE IF EXISTS `g_philhealth`;
CREATE TABLE IF NOT EXISTS `g_philhealth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(258) COLLATE utf8_unicode_ci NOT NULL,
  `effectivity_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_philhealth_table_rate`
--

DROP TABLE IF EXISTS `g_philhealth_table_rate`;
CREATE TABLE IF NOT EXISTS `g_philhealth_table_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `philhealth_id` int(11) NOT NULL,
  `monthly_salary_bracket` float NOT NULL,
  `from_salary` float NOT NULL,
  `to_salary` float NOT NULL,
  `salary_base` float NOT NULL,
  `total_monthly_contribution` float NOT NULL,
  `employee_share` float NOT NULL COMMENT 'Employee Share',
  `employer_share` float NOT NULL COMMENT 'Employer Share',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_requests`
--

DROP TABLE IF EXISTS `g_requests`;
CREATE TABLE IF NOT EXISTS `g_requests` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `requestor_employee_id` bigint(20) DEFAULT NULL,
  `request_id` bigint(20) DEFAULT NULL,
  `request_type` varchar(10) DEFAULT NULL,
  `approver_employee_id` bigint(20) DEFAULT NULL,
  `approver_name` varchar(180) DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL,
  `is_lock` varchar(10) DEFAULT NULL,
  `remarks` text,
  `action_date` varchar(180) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `requestor_employee_id` (`requestor_employee_id`,`request_id`,`request_type`,`approver_employee_id`,`status`),
  KEY `approver_name` (`approver_name`),
  KEY `request_type` (`request_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_request_approvers`
--

DROP TABLE IF EXISTS `g_request_approvers`;
CREATE TABLE IF NOT EXISTS `g_request_approvers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `approvers_name` text,
  `requestors_name` text,
  `date_created` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_request_approvers_level`
--

DROP TABLE IF EXISTS `g_request_approvers_level`;
CREATE TABLE IF NOT EXISTS `g_request_approvers_level` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `request_approvers_id` bigint(20) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `employee_name` varchar(160) DEFAULT NULL,
  `level` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_name` (`employee_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_request_approvers_requestors`
--

DROP TABLE IF EXISTS `g_request_approvers_requestors`;
CREATE TABLE IF NOT EXISTS `g_request_approvers_requestors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `request_approvers_id` bigint(20) DEFAULT NULL,
  `employee_department_group_id` bigint(20) DEFAULT NULL,
  `employee_department_group` varchar(5) DEFAULT NULL,
  `description` varchar(160) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `request_approvers_id` (`request_approvers_id`),
  KEY `employee_department_group` (`employee_department_group`),
  KEY `description` (`description`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_roles`
--

DROP TABLE IF EXISTS `g_roles`;
CREATE TABLE IF NOT EXISTS `g_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(180) NOT NULL,
  `is_archive` varchar(10) NOT NULL,
  `date_created` varchar(50) NOT NULL,
  `last_modified` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `g_roles`
--

INSERT INTO `g_roles` (`id`, `name`, `description`, `is_archive`, `date_created`, `last_modified`) VALUES
(1, 'HR Staff', 'Default role', 'No', '2017-05-09 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `g_role_actions`
--

DROP TABLE IF EXISTS `g_role_actions`;
CREATE TABLE IF NOT EXISTS `g_role_actions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `parent_module` varchar(30) NOT NULL,
  `module` varchar(80) NOT NULL,
  `action` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `g_role_actions`
--

INSERT INTO `g_role_actions` (`id`, `role_id`, `parent_module`, `module`, `action`) VALUES
(1, 1, 'hr', 'schedule', 'View and Edit'),
(2, 1, 'hr', 'attendance', 'Can Access'),
(3, 1, 'hr', 'attendance_daily_time_record', 'View and Edit'),
(4, 1, 'hr', 'attendance_overtime', 'View and Edit'),
(5, 1, 'hr', 'attendance_leave', 'View and Edit'),
(6, 1, 'hr', 'official_business', 'View and Edit'),
(7, 1, 'hr', 'attendance_timesheet', 'View and Edit'),
(8, 1, 'hr', 'reports', 'No Access'),
(9, 1, 'hr', 'settings', 'No Access'),
(10, 1, 'hr', 'employees', 'No Access'),
(11, 1, 'hr', 'employee_access', 'Both'),
(12, 1, 'employee', 'employee_dashboard', 'Can Access'),
(13, 1, 'employee', 'employee_overtime', 'Can Access'),
(14, 1, 'employee', 'employee_leave', 'Can Access'),
(15, 1, 'employee', 'employee_official_business', 'Can Access'),
(16, 1, 'employee', 'employee_reports', 'Can Access'),
(17, 1, 'employee', 'employee_payslip', 'Can Access'),
(18, 1, 'employee', 'employee_profile', 'Can Access');

-- --------------------------------------------------------

--
-- Table structure for table `g_salary_cycle`
--

DROP TABLE IF EXISTS `g_salary_cycle`;
CREATE TABLE IF NOT EXISTS `g_salary_cycle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cycle_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cut_offs` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'serialized from array',
  `is_default` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='deprecated!!' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_schedule`
--

DROP TABLE IF EXISTS `g_schedule`;
CREATE TABLE IF NOT EXISTS `g_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `public_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `schedule_group_id` int(11) NOT NULL,
  `schedule_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `grace_period` int(11) NOT NULL,
  `working_days` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'mon, tue, wed, thu, fri, sat, sun',
  `time_in` time NOT NULL,
  `time_out` time NOT NULL,
  `is_default` int(1) NOT NULL COMMENT '1 = yes, 0 = no',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `g_schedule`
--

INSERT INTO `g_schedule` (`id`, `public_id`, `schedule_group_id`, `schedule_name`, `grace_period`, `working_days`, `time_in`, `time_out`, `is_default`) VALUES
(1, '59116b694176b', 1, 'Default', 10, 'mon,tue,wed,thu,fri', '08:00:00', '17:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `g_schedule_group`
--

DROP TABLE IF EXISTS `g_schedule_group`;
CREATE TABLE IF NOT EXISTS `g_schedule_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `public_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `schedule_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `grace_period` int(11) NOT NULL,
  `effectivity_date` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `end_date` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` int(1) NOT NULL COMMENT '0 = no, 1 = yes',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `g_schedule_group`
--

INSERT INTO `g_schedule_group` (`id`, `public_id`, `schedule_name`, `grace_period`, `effectivity_date`, `end_date`, `is_default`) VALUES
(1, '59116b693d8ea', 'Default', 10, '2013-01-01', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_application_status`
--

DROP TABLE IF EXISTS `g_settings_application_status`;
CREATE TABLE IF NOT EXISTS `g_settings_application_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '	Application Submitted 	1st Interview 	2nd Interview 	Offer Job 	no response  	Declined offer 	Failed_exam 	Backout 	Reject 	Hired',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `g_settings_application_status`
--

INSERT INTO `g_settings_application_status` (`id`, `company_structure_id`, `status`) VALUES
(1, 1, 'Application'),
(2, 1, 'Submitted'),
(3, 1, '1st Interview'),
(4, 1, '2nd Interview'),
(5, 1, 'Offer Job'),
(6, 1, 'Declined offer'),
(7, 1, 'Failed Exam'),
(8, 1, 'Backout'),
(9, 1, 'Reject'),
(10, 1, 'Hired');

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_company_benefits`
--

DROP TABLE IF EXISTS `g_settings_company_benefits`;
CREATE TABLE IF NOT EXISTS `g_settings_company_benefits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `benefit_code` varchar(100) CHARACTER SET latin1 NOT NULL,
  `benefit_name` varchar(180) CHARACTER SET latin1 NOT NULL,
  `benefit_description` text CHARACTER SET latin1 NOT NULL,
  `benefit_type` varchar(30) CHARACTER SET latin1 NOT NULL,
  `benefit_amount` double NOT NULL,
  `is_archived` varchar(10) CHARACTER SET latin1 NOT NULL,
  `is_taxable` varchar(10) CHARACTER SET latin1 NOT NULL,
  `date_created` varchar(150) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_deduction_breakdown`
--

DROP TABLE IF EXISTS `g_settings_deduction_breakdown`;
CREATE TABLE IF NOT EXISTS `g_settings_deduction_breakdown` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET latin1 NOT NULL,
  `breakdown` varchar(50) CHARACTER SET latin1 NOT NULL,
  `is_active` varchar(10) CHARACTER SET latin1 NOT NULL,
  `is_taxable` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No' COMMENT 'Yes / No',
  `salary_credit` int(2) NOT NULL DEFAULT '0' COMMENT '0 = basic pay / 1 = gross pay',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `g_settings_deduction_breakdown`
--

INSERT INTO `g_settings_deduction_breakdown` (`id`, `name`, `breakdown`, `is_active`, `is_taxable`, `salary_credit`) VALUES
(1, 'SSS', '50:50', 'Yes', '', 0),
(2, 'HDMF', '50:50', 'Yes', '', 0),
(3, 'Phil Health', '50:50', 'Yes', '', 0),
(4, 'TAX-BIR', '100:100', 'Yes', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_default_leave`
--

DROP TABLE IF EXISTS `g_settings_default_leave`;
CREATE TABLE IF NOT EXISTS `g_settings_default_leave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `leave_type_id` int(11) NOT NULL,
  `number_of_days_default` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_dependent_relationship`
--

DROP TABLE IF EXISTS `g_settings_dependent_relationship`;
CREATE TABLE IF NOT EXISTS `g_settings_dependent_relationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `relationship` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'child, wife etc',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `g_settings_dependent_relationship`
--

INSERT INTO `g_settings_dependent_relationship` (`id`, `company_structure_id`, `relationship`) VALUES
(1, 1, 'Husband'),
(2, 1, 'Wife'),
(3, 1, 'Cousin'),
(4, 1, 'Friend');

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_employee_benefits`
--

DROP TABLE IF EXISTS `g_settings_employee_benefits`;
CREATE TABLE IF NOT EXISTS `g_settings_employee_benefits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) CHARACTER SET latin1 NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `description` varchar(200) CHARACTER SET latin1 NOT NULL,
  `amount` double NOT NULL,
  `is_taxable` varchar(5) CHARACTER SET latin1 NOT NULL,
  `is_archive` varchar(5) CHARACTER SET latin1 NOT NULL,
  `date_created` varchar(80) CHARACTER SET latin1 NOT NULL,
  `date_last_modified` varchar(80) CHARACTER SET latin1 NOT NULL,
  `cutoff` int(1) DEFAULT NULL,
  `multiplied_by` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `g_settings_employee_benefits`
--

INSERT INTO `g_settings_employee_benefits` (`id`, `code`, `name`, `description`, `amount`, `is_taxable`, `is_archive`, `date_created`, `date_last_modified`, `cutoff`, `multiplied_by`) VALUES
(1, 'MEAL', 'Meal Allowance', 'Meal Allowance', 0, 'No', 'No', '2017-05-09 09:10:36', '', 1, ''),
(2, 'TRANSPO', 'Transpo Allowance', 'Transportation Allowance', 0, 'No', 'No', '2017-05-09 09:10:36', '', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_employee_field`
--

DROP TABLE IF EXISTS `g_settings_employee_field`;
CREATE TABLE IF NOT EXISTS `g_settings_employee_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `screen` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `default` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_employee_status`
--

DROP TABLE IF EXISTS `g_settings_employee_status`;
CREATE TABLE IF NOT EXISTS `g_settings_employee_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET latin1 NOT NULL,
  `is_archive` varchar(10) CHARACTER SET latin1 NOT NULL,
  `date_created` varchar(180) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `g_settings_employee_status`
--

INSERT INTO `g_settings_employee_status` (`id`, `company_structure_id`, `name`, `is_archive`, `date_created`) VALUES
(1, 1, 'ACTIVE', 'No', '2017-05-09'),
(2, 1, 'RESIGNED', 'No', '2017-05-09'),
(3, 1, 'TERMINATED', 'No', '2017-05-09'),
(4, 1, 'END OF CONTRACT', 'No', '2017-05-09');

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_employment_status`
--

DROP TABLE IF EXISTS `g_settings_employment_status`;
CREATE TABLE IF NOT EXISTS `g_settings_employment_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `code` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Full Time Permanent, Full Time Contract, Full Time Internship',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `g_settings_employment_status`
--

INSERT INTO `g_settings_employment_status` (`id`, `company_structure_id`, `code`, `status`) VALUES
(1, 1, 'FT', 'Full Time'),
(2, 1, 'PT', 'Part Time'),
(3, 1, 'REG', 'Regular'),
(4, 1, 'PROB', 'Probationary');

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_grace_period`
--

DROP TABLE IF EXISTS `g_settings_grace_period`;
CREATE TABLE IF NOT EXISTS `g_settings_grace_period` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1 NOT NULL,
  `number_minute_default` double NOT NULL,
  `is_archive` int(11) NOT NULL DEFAULT '0' COMMENT '0=No, 1=yes',
  `is_default` int(11) NOT NULL DEFAULT '0' COMMENT '0=No, 1=yes',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `g_settings_grace_period`
--

INSERT INTO `g_settings_grace_period` (`id`, `company_structure_id`, `title`, `description`, `number_minute_default`, `is_archive`, `is_default`) VALUES
(1, 1, 'Default', 'Default', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_holiday`
--

DROP TABLE IF EXISTS `g_settings_holiday`;
CREATE TABLE IF NOT EXISTS `g_settings_holiday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `holiday_type` tinyint(1) NOT NULL COMMENT '1 = legal, 2 = special',
  `holiday_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `holiday_month` tinyint(12) NOT NULL,
  `holiday_day` tinyint(31) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=45 ;

--
-- Dumping data for table `g_settings_holiday`
--

INSERT INTO `g_settings_holiday` (`id`, `holiday_type`, `holiday_title`, `holiday_month`, `holiday_day`) VALUES
(29, 1, 'Labor Day', 5, 1),
(25, 1, 'New Year''s Day', 1, 1),
(28, 1, 'Good Friday', 4, 18),
(27, 1, 'Maundry Thursday', 4, 17),
(26, 1, 'Day of Valour', 4, 9),
(30, 1, 'Independence Day', 6, 12),
(31, 1, 'Eid''l Fitr', 7, 28),
(32, 1, 'National Heroes'' Day', 8, 25),
(33, 1, 'Eidul Adha', 10, 5),
(34, 1, 'Bonifacio Day', 11, 30),
(35, 1, 'Christmas Day', 12, 25),
(36, 1, 'Rizal Day', 12, 30),
(37, 2, 'Chinese New Year', 1, 31),
(38, 2, 'Black Saturday', 4, 19),
(39, 2, 'Ninoy Aquino Day', 8, 21),
(40, 2, 'All Saints'' Day', 11, 1),
(41, 2, 'All Souls'' Day', 11, 2),
(42, 2, 'Christmas Eve', 12, 24),
(43, 2, 'Last Day of the Year', 12, 31),
(44, 2, 'EDSA Revolution Anniversary', 2, 25);

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_language`
--

DROP TABLE IF EXISTS `g_settings_language`;
CREATE TABLE IF NOT EXISTS `g_settings_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `language` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

--
-- Dumping data for table `g_settings_language`
--

INSERT INTO `g_settings_language` (`id`, `company_structure_id`, `language`) VALUES
(1, 1, 'Albanian'),
(2, 1, 'Arabic'),
(3, 1, 'Bulgarian'),
(4, 1, 'Cantonese'),
(5, 1, 'Cantonese'),
(6, 1, 'Chinese, Mandarin'),
(7, 1, 'Danish'),
(8, 1, 'Dutch'),
(9, 1, 'Egyptian Arabic'),
(10, 1, 'English'),
(11, 1, 'French'),
(12, 1, 'German'),
(13, 1, 'Greek'),
(14, 1, 'Hebrew'),
(15, 1, 'Hindi'),
(16, 1, 'Indonesian'),
(17, 1, 'Iraqw'),
(18, 1, 'Irish'),
(19, 1, 'Italian'),
(20, 1, 'Japanese'),
(21, 1, 'Jewish Babylonian Aramaic'),
(22, 1, 'Korean'),
(23, 1, 'Kurdish'),
(24, 1, 'Latin'),
(25, 1, 'Lithuanian'),
(26, 1, 'Luxembourgish'),
(27, 1, 'Macedonian'),
(28, 1, 'Maltese'),
(29, 1, 'Mandarin'),
(30, 1, 'Mongolian'),
(31, 1, 'Nepali'),
(32, 1, 'Polish'),
(33, 1, 'Portuguese'),
(34, 1, 'Romanian'),
(35, 1, 'Russian'),
(36, 1, 'Samoan'),
(37, 1, 'Slovak'),
(38, 1, 'Spanish'),
(39, 1, 'Tagalog'),
(40, 1, 'Thai'),
(41, 1, 'Tibetan'),
(42, 1, 'Turkish'),
(43, 1, 'Ukrainian'),
(44, 1, 'Valencian'),
(45, 1, 'Venetian'),
(46, 1, 'Welsh');

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_leave_credit`
--

DROP TABLE IF EXISTS `g_settings_leave_credit`;
CREATE TABLE IF NOT EXISTS `g_settings_leave_credit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employment_years` varchar(5) NOT NULL,
  `default_credit` int(11) NOT NULL,
  `leave_id` int(11) NOT NULL COMMENT 'from ''Leave Type'' table',
  `employment_status_id` int(11) NOT NULL COMMENT 'from ''Employment Status'' table',
  `is_archived` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_leave_general`
--

DROP TABLE IF EXISTS `g_settings_leave_general`;
CREATE TABLE IF NOT EXISTS `g_settings_leave_general` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `convert_leave_criteria` int(2) NOT NULL,
  `leave_id` int(2) NOT NULL COMMENT 'from ''g_leave'' table',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `g_settings_leave_general`
--

INSERT INTO `g_settings_leave_general` (`id`, `convert_leave_criteria`, `leave_id`) VALUES
(1, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_license`
--

DROP TABLE IF EXISTS `g_settings_license`;
CREATE TABLE IF NOT EXISTS `g_settings_license` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `license_type` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'engineer, doctor',
  `description` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `g_settings_license`
--

INSERT INTO `g_settings_license` (`id`, `company_structure_id`, `license_type`, `description`) VALUES
(1, 1, 'Driver''s License', 'Driver''s License'),
(2, 1, 'Pharmacist PRC License', 'Pharmacist PRC License'),
(3, 1, 'Doctor''s License', 'Doctor''s License');

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_location`
--

DROP TABLE IF EXISTS `g_settings_location`;
CREATE TABLE IF NOT EXISTS `g_settings_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'PH',
  `location` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Philippine',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=115 ;

--
-- Dumping data for table `g_settings_location`
--

INSERT INTO `g_settings_location` (`id`, `company_structure_id`, `code`, `location`) VALUES
(1, 1, 'PH', 'Philippines'),
(2, 1, 'AU', 'Australia'),
(3, 1, 'AF', 'Afghanistan'),
(4, 1, 'AL', 'Albania'),
(5, 1, 'DZ', 'Algeria'),
(6, 1, 'AS', 'American Samoa'),
(7, 1, 'AD', 'Andorra'),
(8, 1, 'AO', 'Angola'),
(9, 1, 'AI', 'Anguilla'),
(10, 1, 'AQ', 'Antarctica'),
(11, 1, 'AG', 'Antigua and Barbuda'),
(12, 1, 'AR', 'Argentina'),
(13, 1, 'AM', 'Armenia'),
(14, 1, 'AW', 'Aruba'),
(15, 1, 'AT', 'Austria'),
(16, 1, 'AZ', 'Azerbaijan'),
(17, 1, 'BS', 'Bahamas'),
(18, 1, 'BH', 'Bahrain'),
(19, 1, 'BD', 'Bangladesh'),
(20, 1, 'BB', 'Barbados'),
(21, 1, 'BY', 'Belarus'),
(22, 1, 'BE', 'Belgium'),
(23, 1, 'BZ', 'Belize'),
(24, 1, 'BJ', 'Benin'),
(25, 1, 'BM', 'Bermuda'),
(26, 1, 'BT', 'Bhutan'),
(27, 1, 'BO', 'Bolivia'),
(28, 1, 'BA', 'Bosnia and Herzegovina'),
(29, 1, 'BW', 'Botswana'),
(30, 1, 'BR', 'Brazil'),
(31, 1, 'IO', 'British Indian Ocean Territory'),
(32, 1, 'VG', 'British Virgin Islands'),
(33, 1, 'BN', 'Brunei'),
(34, 1, 'BG', 'Bulgaria'),
(35, 1, 'BF', 'Burkina Faso'),
(36, 1, 'MM', 'Burma (Myanmar)'),
(37, 1, 'BI', 'Burundi'),
(38, 1, 'KH', 'Cambodia'),
(39, 1, 'CM', 'Cameroon'),
(40, 1, 'CA', 'Canada'),
(41, 1, 'CV', 'Cape Verde'),
(42, 1, 'KY', 'Cayman Islands'),
(43, 1, 'CF', 'Central African Republic'),
(44, 1, 'TD', 'Chad'),
(45, 1, 'CL', 'Chile'),
(46, 1, 'CN', 'China'),
(47, 1, 'CX', 'Christmas Island'),
(48, 1, 'CC', 'Cocos (Keeling) Islands'),
(49, 1, 'CO', 'Colombia'),
(50, 1, 'KM', 'Comoros'),
(51, 1, 'CK', 'Cook Islands'),
(52, 1, 'CR', 'Costa Rica'),
(53, 1, 'HR', 'Croatia'),
(54, 1, 'CU', 'Cuba'),
(55, 1, 'CY', 'Cyprus'),
(56, 1, 'CZ', 'Czech Republic'),
(57, 1, 'CD', 'Democratic Republic of the Con'),
(58, 1, 'DK', 'Denmark'),
(59, 1, 'DJ', 'Djibouti'),
(60, 1, 'DM', 'Dominica'),
(61, 1, 'DO', 'Dominican Republic'),
(62, 1, 'EC', 'Ecuador'),
(63, 1, 'EG', 'Egypt'),
(64, 1, 'SV', 'El Salvador'),
(65, 1, 'GQ', 'Equatorial Guinea'),
(66, 1, 'ER', 'Eritrea'),
(67, 1, 'EE', 'Estonia'),
(68, 1, 'ET', 'Ethiopia'),
(69, 1, 'FK', 'Falkland Islands'),
(70, 1, 'FO', 'Faroe Islands'),
(71, 1, 'FJ', 'Fiji'),
(72, 1, 'FI', 'Finland'),
(73, 1, 'FR', 'France'),
(74, 1, 'PF', 'French Polynesia'),
(75, 1, 'GA', 'Gabon'),
(76, 1, 'GM', 'Gambia'),
(77, 1, 'GE', 'Georgia'),
(78, 1, 'DE', 'Germany'),
(79, 1, 'GH', 'Ghana'),
(80, 1, 'GI', 'Gibraltar'),
(81, 1, 'GR', 'Greece'),
(82, 1, 'GL', 'Greenland'),
(83, 1, 'GD', 'Grenada'),
(84, 1, 'GU', 'Guam'),
(85, 1, 'GT', 'Guatemala'),
(86, 1, 'GN', 'Guinea'),
(87, 1, 'GW', 'Guinea-Bissau'),
(88, 1, 'GY', 'Guyana'),
(89, 1, 'HT', 'Haiti'),
(90, 1, 'VA', 'Holy See (Vatican City)'),
(91, 1, 'HN', 'Honduras'),
(92, 1, 'HK', 'Hong Kong'),
(93, 1, 'HU', 'Hungary'),
(94, 1, 'IS', 'Iceland'),
(95, 1, 'IN', 'India'),
(96, 1, 'ID', 'Indonesia'),
(97, 1, 'IR', 'Iran'),
(98, 1, 'IQ', 'Iraq'),
(99, 1, 'IE', 'Ireland'),
(100, 1, 'IM', 'Isle of Man'),
(101, 1, 'IL', 'Israel'),
(102, 1, 'IT', 'Italy'),
(103, 1, 'CI', 'Ivory Coast'),
(104, 1, 'JM', 'Jamaica'),
(105, 1, 'JP', 'Japan'),
(106, 1, 'JE', 'Jersey'),
(107, 1, 'JO', 'Jordan'),
(108, 1, 'KZ', 'Kazakhstan'),
(109, 1, 'KE', 'Kenya'),
(110, 1, 'KI', 'Kiribati'),
(111, 1, 'KW', 'Kuwait'),
(112, 1, 'KG', 'Kyrgyzstan'),
(113, 1, 'LA', 'Laos'),
(114, 1, 'LV', 'Latvia');

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_membership`
--

DROP TABLE IF EXISTS `g_settings_membership`;
CREATE TABLE IF NOT EXISTS `g_settings_membership` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `membership_type_id` int(11) NOT NULL,
  `membership` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'house loan etc.',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_membership_type`
--

DROP TABLE IF EXISTS `g_settings_membership_type`;
CREATE TABLE IF NOT EXISTS `g_settings_membership_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'pagibig, sss etc',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_memo`
--

DROP TABLE IF EXISTS `g_settings_memo`;
CREATE TABLE IF NOT EXISTS `g_settings_memo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET latin1 NOT NULL,
  `content` text CHARACTER SET latin1 NOT NULL,
  `created_by` varchar(255) CHARACTER SET latin1 NOT NULL,
  `is_archive` varchar(10) CHARACTER SET latin1 NOT NULL,
  `date_created` varchar(110) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `g_settings_memo`
--

INSERT INTO `g_settings_memo` (`id`, `title`, `content`, `created_by`, `is_archive`, `date_created`) VALUES
(1, 'Termination', 'Sample content for Termination', 'HR', 'No', '2017-05-09'),
(2, 'Tardiness', 'Sample content for Tardiness', 'HR', 'No', '2017-05-09');

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_pay_period`
--

DROP TABLE IF EXISTS `g_settings_pay_period`;
CREATE TABLE IF NOT EXISTS `g_settings_pay_period` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `pay_period_code` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `pay_period_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cut_off` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `payout_day` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ex: 15,end',
  `is_default` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `g_settings_pay_period`
--

INSERT INTO `g_settings_pay_period` (`id`, `company_structure_id`, `pay_period_code`, `pay_period_name`, `cut_off`, `payout_day`, `is_default`) VALUES
(1, 1, 'BMO', 'Bi-Monthly', '1-15,16-31', '20,5', 1);

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_policy`
--

DROP TABLE IF EXISTS `g_settings_policy`;
CREATE TABLE IF NOT EXISTS `g_settings_policy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy` varchar(255) CHARACTER SET latin1 NOT NULL,
  `description` varchar(255) CHARACTER SET latin1 NOT NULL,
  `is_active` varchar(3) CHARACTER SET latin1 NOT NULL COMMENT '''Yes'' or ''No''',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `g_settings_policy`
--

INSERT INTO `g_settings_policy` (`id`, `policy`, `description`, `is_active`) VALUES
(1, 'File OT when late', 'File OT when late', 'Yes'),
(2, 'File LEAVE', 'File LEAVE', 'Yes'),
(3, 'File OT when later cccc', 'Settings for filing of OT when late cccc ccc', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_request`
--

DROP TABLE IF EXISTS `g_settings_request`;
CREATE TABLE IF NOT EXISTS `g_settings_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `request_type` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `applied_to_departments` varchar(240) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Array of department id',
  `applied_to_positions` varchar(240) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Array of position id',
  `applied_to_employees` varchar(240) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Array of employee id',
  `applied_to_description` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Text value of joined applied position and employee id',
  `is_active` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT '1 = Active / 0 = InActive',
  `is_archive` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT '1 = Archive / 0 = Is not Archive',
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_request_approvers`
--

DROP TABLE IF EXISTS `g_settings_request_approvers`;
CREATE TABLE IF NOT EXISTS `g_settings_request_approvers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `settings_request_id` int(11) NOT NULL,
  `position_employee_id` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Employee Id / Position Id ',
  `level` int(11) NOT NULL,
  `override_level` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_requirements`
--

DROP TABLE IF EXISTS `g_settings_requirements`;
CREATE TABLE IF NOT EXISTS `g_settings_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `title` varchar(180) NOT NULL,
  `is_archive` varchar(10) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `g_settings_requirements`
--

INSERT INTO `g_settings_requirements` (`id`, `company_structure_id`, `title`, `is_archive`, `date_created`) VALUES
(1, 1, 'SSS', 'No', '2017-05-09 00:00:00'),
(2, 1, 'Pagibig', 'No', '2017-05-09 00:00:00'),
(3, 1, '2x2 Picture', 'No', '2017-05-09 00:00:00'),
(4, 1, 'NBI Clearance', 'No', '2017-05-09 00:00:00'),
(5, 1, 'Police Clearance', 'No', '2017-05-09 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_salutation`
--

DROP TABLE IF EXISTS `g_settings_salutation`;
CREATE TABLE IF NOT EXISTS `g_settings_salutation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `salutation` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(124) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `g_settings_salutation`
--

INSERT INTO `g_settings_salutation` (`id`, `company_structure_id`, `salutation`, `description`) VALUES
(1, 1, 'Mr', ''),
(2, 1, 'Mrs', ''),
(3, 1, 'Ms', ''),
(4, 1, 'Dr', '');

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_skills`
--

DROP TABLE IF EXISTS `g_settings_skills`;
CREATE TABLE IF NOT EXISTS `g_settings_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `skill` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'swimming, cooking, driving',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `g_settings_skills`
--

INSERT INTO `g_settings_skills` (`id`, `company_structure_id`, `skill`) VALUES
(1, 1, 'Analytical/Research'),
(2, 1, 'Computer/Technical Literacy'),
(3, 1, 'Flexibility/Adaptability/Managing Multiple Priorities'),
(4, 1, 'Leadership/Management'),
(5, 1, 'Planning/Organizing'),
(6, 1, 'Teamwork'),
(7, 1, 'Self-Motivated/Ability to Work With Little or No Supervision');

-- --------------------------------------------------------

--
-- Table structure for table `g_settings_subdivision_type`
--

DROP TABLE IF EXISTS `g_settings_subdivision_type`;
CREATE TABLE IF NOT EXISTS `g_settings_subdivision_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'branch,division, department, team',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `g_settings_subdivision_type`
--

INSERT INTO `g_settings_subdivision_type` (`id`, `company_structure_id`, `type`) VALUES
(1, 1, 'Team'),
(2, 1, 'Group');

-- --------------------------------------------------------

--
-- Table structure for table `g_sprint_variables`
--

DROP TABLE IF EXISTS `g_sprint_variables`;
CREATE TABLE IF NOT EXISTS `g_sprint_variables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `variable_name` varchar(100) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `custom_value_a` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `g_sprint_variables`
--

INSERT INTO `g_sprint_variables` (`id`, `variable_name`, `value`, `custom_value_a`) VALUES
(1, 'default_total_working_days', '261', ''),
(2, 'ceta', '12.5', ''),
(3, 'sea', '13', ''),
(4, 'minimum_rate', '310', ''),
(5, 'night_shift_hour', '22:00:00 to 06:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `g_sss`
--

DROP TABLE IF EXISTS `g_sss`;
CREATE TABLE IF NOT EXISTS `g_sss` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(258) CHARACTER SET latin1 NOT NULL,
  `effectivity_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `g_sss`
--

INSERT INTO `g_sss` (`id`, `description`, `effectivity_date`) VALUES
(1, 'test', '2010-07-07'),
(2, 'sdfsf', '2012-07-10');

-- --------------------------------------------------------

--
-- Table structure for table `g_sss_table_rate`
--

DROP TABLE IF EXISTS `g_sss_table_rate`;
CREATE TABLE IF NOT EXISTS `g_sss_table_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sss_table_id` int(11) NOT NULL,
  `from_salary` float NOT NULL,
  `to_salary` float NOT NULL,
  `monthly_salary_credit` float NOT NULL,
  `ss_er` float NOT NULL COMMENT 'Social Security ER',
  `ss_ee` float NOT NULL COMMENT 'Social Security EE',
  `ss_total` float NOT NULL COMMENT 'Total',
  `company_ec` float NOT NULL,
  `tc_er` float NOT NULL COMMENT 'Total Contribution ER',
  `tc_ee` float NOT NULL COMMENT 'Total Contribution EE',
  `tc_total` float NOT NULL COMMENT 'Total',
  `total_contribution` float NOT NULL COMMENT 'Total Contribution',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_system_modules`
--

DROP TABLE IF EXISTS `g_system_modules`;
CREATE TABLE IF NOT EXISTS `g_system_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `owner_email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(240) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_tax_table`
--

DROP TABLE IF EXISTS `g_tax_table`;
CREATE TABLE IF NOT EXISTS `g_tax_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `pay_frequency` varchar(200) CHARACTER SET latin1 NOT NULL COMMENT 'Monthly / Semi Monthly',
  `status` varchar(200) CHARACTER SET latin1 NOT NULL,
  `d0` float NOT NULL,
  `d1` float NOT NULL,
  `d2` float NOT NULL,
  `d3` float NOT NULL,
  `d4` float NOT NULL,
  `d5` float NOT NULL,
  `d6` float NOT NULL,
  `d7` float NOT NULL,
  `d8` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_user`
--

DROP TABLE IF EXISTS `g_user`;
CREATE TABLE IF NOT EXISTS `g_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `employment_status` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'terminated, regular',
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'email address',
  `hash` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `receive_notification` int(11) NOT NULL COMMENT '0/1',
  `date_entered` date NOT NULL,
  `date_modified` date NOT NULL,
  `modified_user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_user_group`
--

DROP TABLE IF EXISTS `g_user_group`;
CREATE TABLE IF NOT EXISTS `g_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_structure_id` int(11) NOT NULL,
  `group_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `g_yearly_bonus_release_dates`
--

DROP TABLE IF EXISTS `g_yearly_bonus_release_dates`;
CREATE TABLE IF NOT EXISTS `g_yearly_bonus_release_dates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `amount` float(11,2) NOT NULL,
  `taxable_amount` float(11,2) NOT NULL,
  `tax` float(11,2) NOT NULL,
  `percentage` float(11,2) NOT NULL,
  `total_bonus_amount` float(11,2) NOT NULL,
  `deducted_amount` float(11,2) NOT NULL,
  `year_released` int(11) NOT NULL,
  `month_start` int(2) NOT NULL,
  `month_end` int(2) NOT NULL,
  `cutoff_start_date` date NOT NULL,
  `cutoff_end_date` date NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pbcattbl`
--

DROP TABLE IF EXISTS `pbcattbl`;
CREATE TABLE IF NOT EXISTS `pbcattbl` (
  `pbt_tnam` char(193) CHARACTER SET latin1 NOT NULL,
  `pbt_tid` int(11) DEFAULT NULL,
  `pbt_ownr` char(193) CHARACTER SET latin1 NOT NULL,
  `pbd_fhgt` smallint(6) DEFAULT NULL,
  `pbd_fwgt` smallint(6) DEFAULT NULL,
  `pbd_fitl` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `pbd_funl` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `pbd_fchr` smallint(6) DEFAULT NULL,
  `pbd_fptc` smallint(6) DEFAULT NULL,
  `pbd_ffce` char(18) CHARACTER SET latin1 DEFAULT NULL,
  `pbh_fhgt` smallint(6) DEFAULT NULL,
  `pbh_fwgt` smallint(6) DEFAULT NULL,
  `pbh_fitl` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `pbh_funl` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `pbh_fchr` smallint(6) DEFAULT NULL,
  `pbh_fptc` smallint(6) DEFAULT NULL,
  `pbh_ffce` char(18) CHARACTER SET latin1 DEFAULT NULL,
  `pbl_fhgt` smallint(6) DEFAULT NULL,
  `pbl_fwgt` smallint(6) DEFAULT NULL,
  `pbl_fitl` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `pbl_funl` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `pbl_fchr` smallint(6) DEFAULT NULL,
  `pbl_fptc` smallint(6) DEFAULT NULL,
  `pbl_ffce` char(18) CHARACTER SET latin1 DEFAULT NULL,
  `pbt_cmnt` varchar(254) CHARACTER SET latin1 DEFAULT NULL,
  UNIQUE KEY `pbcatt_x` (`pbt_tnam`,`pbt_ownr`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `p_philhealth`
--

DROP TABLE IF EXISTS `p_philhealth`;
CREATE TABLE IF NOT EXISTS `p_philhealth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salary_base` float(10,2) NOT NULL,
  `salary_bracket` smallint(6) NOT NULL,
  `from_salary` decimal(15,2) NOT NULL,
  `to_salary` decimal(15,2) NOT NULL,
  `monthly_contribution` float(10,2) NOT NULL,
  `employee_share` float(10,2) NOT NULL,
  `company_share` float(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Dumping data for table `p_philhealth`
--

INSERT INTO `p_philhealth` (`id`, `salary_base`, `salary_bracket`, `from_salary`, `to_salary`, `monthly_contribution`, `employee_share`, `company_share`) VALUES
(1, 8000.00, 1, 0.00, 8999.99, 200.00, 100.00, 100.00),
(2, 9000.00, 2, 9000.00, 9999.99, 225.00, 112.50, 112.50),
(3, 10000.00, 3, 10000.00, 10999.99, 250.00, 125.00, 125.00),
(4, 11000.00, 4, 11000.00, 11999.99, 275.00, 137.50, 137.50),
(5, 12000.00, 5, 12000.00, 12999.99, 300.00, 150.00, 150.00),
(6, 13000.00, 6, 13000.00, 13999.99, 325.00, 162.50, 162.50),
(7, 14000.00, 7, 14000.00, 14999.99, 350.00, 175.00, 175.00),
(8, 15000.00, 8, 15000.00, 15999.99, 375.00, 187.50, 187.50),
(9, 16000.00, 9, 16000.00, 16999.99, 400.00, 200.00, 200.00),
(10, 17000.00, 10, 17000.00, 17999.99, 425.00, 212.50, 212.50),
(11, 18000.00, 11, 18000.00, 18999.99, 450.00, 225.00, 225.00),
(12, 19000.00, 12, 19000.00, 19999.99, 475.00, 237.50, 237.50),
(13, 20000.00, 13, 20000.00, 20999.99, 500.00, 250.00, 250.00),
(14, 21000.00, 14, 21000.00, 21999.99, 525.00, 262.50, 262.50),
(15, 22000.00, 15, 22000.00, 22999.99, 550.00, 275.00, 275.00),
(16, 23000.00, 16, 23000.00, 23999.99, 575.00, 287.50, 287.50),
(17, 24000.00, 17, 24000.00, 24999.99, 600.00, 300.00, 300.00),
(18, 25000.00, 18, 25000.00, 25999.99, 625.00, 312.50, 312.50),
(19, 26000.00, 19, 26000.00, 26999.99, 650.00, 325.00, 325.00),
(20, 27000.00, 20, 27000.00, 27999.99, 675.00, 337.50, 337.50),
(21, 28000.00, 21, 28000.00, 28999.99, 700.00, 350.00, 350.00),
(22, 29000.00, 22, 29000.00, 29999.99, 725.00, 362.50, 362.50),
(23, 30000.00, 23, 30000.00, 30999.99, 750.00, 375.00, 375.00),
(24, 31000.00, 24, 31000.00, 31999.99, 775.00, 387.50, 387.50),
(25, 32000.00, 25, 32000.00, 32999.99, 800.00, 400.00, 400.00),
(26, 33000.00, 26, 33000.00, 33999.99, 825.00, 412.50, 412.50),
(27, 34000.00, 27, 34000.00, 34999.99, 850.00, 425.00, 425.00),
(28, 35000.00, 28, 35000.00, 9999999.99, 875.00, 437.50, 437.50);

-- --------------------------------------------------------

--
-- Table structure for table `p_sss`
--

DROP TABLE IF EXISTS `p_sss`;
CREATE TABLE IF NOT EXISTS `p_sss` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monthly_salary_credit` float(10,2) NOT NULL,
  `from_salary` float(10,2) NOT NULL,
  `to_salary` float(10,2) NOT NULL,
  `employee_share` float(10,2) NOT NULL,
  `company_share` float(10,2) NOT NULL,
  `company_ec` float(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Dumping data for table `p_sss`
--

INSERT INTO `p_sss` (`id`, `monthly_salary_credit`, `from_salary`, `to_salary`, `employee_share`, `company_share`, `company_ec`) VALUES
(1, 1000.00, 0.00, 1249.99, 36.30, 83.70, 10.00),
(2, 1500.00, 1250.00, 1749.99, 54.50, 120.50, 10.00),
(3, 2000.00, 1750.00, 2249.99, 72.70, 157.30, 10.00),
(4, 2500.00, 2250.00, 2749.99, 90.80, 194.20, 10.00),
(5, 3000.00, 2750.00, 3249.99, 109.00, 231.00, 10.00),
(6, 3500.00, 3250.00, 3749.99, 127.20, 267.80, 10.00),
(7, 4000.00, 3750.00, 4249.99, 145.30, 304.70, 10.00),
(8, 4500.00, 4250.00, 4749.99, 163.50, 341.50, 10.00),
(9, 5000.00, 4750.00, 5249.99, 181.70, 378.30, 10.00),
(10, 5500.00, 5250.00, 5749.99, 199.80, 415.20, 10.00),
(11, 6000.00, 5750.00, 6249.99, 218.00, 452.00, 10.00),
(12, 6500.00, 6250.00, 6749.99, 236.20, 488.80, 10.00),
(13, 7000.00, 6750.00, 7249.99, 254.30, 525.70, 10.00),
(14, 7500.00, 7250.00, 7749.99, 272.50, 562.50, 10.00),
(15, 8000.00, 7750.00, 8249.99, 290.70, 599.30, 10.00),
(16, 8500.00, 8250.00, 8749.99, 308.80, 636.20, 10.00),
(17, 9000.00, 8750.00, 9249.99, 327.00, 673.00, 10.00),
(18, 9500.00, 9250.00, 9749.99, 345.20, 709.80, 10.00),
(19, 10000.00, 9750.00, 10250.00, 363.30, 746.70, 10.00),
(20, 10500.00, 10250.00, 10750.00, 381.50, 783.50, 10.00),
(21, 11000.00, 10750.00, 11250.00, 399.70, 820.30, 10.00),
(22, 11500.00, 11250.00, 11750.00, 417.80, 857.20, 10.00),
(23, 12000.00, 11750.00, 12250.00, 436.00, 894.00, 10.00),
(24, 12500.00, 12250.00, 12750.00, 454.20, 930.80, 10.00),
(25, 13000.00, 12750.00, 13250.00, 472.30, 967.70, 10.00),
(26, 13500.00, 13250.00, 13750.00, 490.50, 1004.50, 10.00),
(27, 14000.00, 13750.00, 14250.00, 508.70, 1041.30, 10.00),
(28, 14500.00, 14250.00, 14750.00, 526.80, 1078.20, 10.00),
(29, 15000.00, 14750.00, 15250.00, 545.00, 1135.00, 30.00),
(30, 15500.00, 15250.00, 15750.00, 563.20, 1171.80, 30.00),
(31, 16000.00, 15750.00, 999999.00, 581.30, 1208.70, 30.00);

-- --------------------------------------------------------

--
-- Table structure for table `tmp_employee_payslip`
--

DROP TABLE IF EXISTS `tmp_employee_payslip`;
CREATE TABLE IF NOT EXISTS `tmp_employee_payslip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `field` varchar(180) NOT NULL,
  `amount` float NOT NULL,
  `created` datetime NOT NULL,
  `year` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
