<?php 
function get_controller()
{

	$path = ($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : $_SERVER['PHP_SELF'];
	if (BASE_FOLDER != '/')
	{
		$path = str_replace(BASE_FOLDER, '', ($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : $_SERVER['PHP_SELF']);
	}
	$first_character = $path[0];
	$last_character = $path[strlen($path) - 1];
	$path = ($first_character == '/') ? substr($path, 1) : $path; // remove first character if it is '/'
	$path = ($last_character == '/') ? substr($path, 0, strlen($path) - 1) : $path ; // remove last character if it is '/'

	// get the controller from URI
	list($controller) = explode('/', $path);
	//$controller = ($controller == INDEX_PAGE || $controller == '') ? DEFAULT_CONTROLLER : $controller . $suffix ;
	return $controller;
}

?>