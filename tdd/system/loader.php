<?php
class Loader
{
	protected $controller_object;
	public static $scriptstyle_to_load;
	public static $script_code;
	public static $script_top_code;
	public static $script_file;
	public static $script_to_include;

	public function __construct($controller_object)
	{
		$this->controller_object = $controller_object;
	}
	


	/**
	 * Include application library without initializing
	 *
	 * @param mixed $class
	 */
	public static function appMainLibrary($class)
	{
		$path = $_SERVER['DOCUMENT_ROOT'] . MAIN_FOLDER .'application/libraries' . LIBRARY_FOLDER;
		if (is_array($class))
		{
			
			foreach ($class AS $c)
			{
				$class = $c;
				if (!file_exists($path . "$c" . EXT))
				{
					exit("<b>$c" . EXT . "</b> not found in " . $path);
				}
				require_once $path . "$c" . EXT;
			}
		}
		else
		{
	
			if (!file_exists($path . "$class" . EXT))
			{
				exit("<b>$class" . EXT . "</b> not found in " . $path);
			}
			
			require_once $path."$class" . EXT;

		}
		
	}	
	
	
	/**
	 * Include config file located in <application folder>/config/
	 *
	 * @param string $file
	 */
	public static function appMainConfig($file)
	{
	
		//APP_PATH . "config/$file" . EXT
	
		$path =  $_SERVER['DOCUMENT_ROOT'] . MAIN_FOLDER .'application/config/'.$file.EXT;				

		if (!file_exists($path))
		{
			exit("<b>$file" . EXT . "</b> not found in " . MAIN_FOLDER . "config/");
		}
		require_once $path;
	}
	
	
	
	/**
	 * Include system library without initializing
	 *
	 * @param mixed $class
	 */
	public static function sysLibrary($class)
	{
		if (is_array($class))
		{
			foreach ($class as $c)
			{
				$class = strtolower($c);
				if (!file_exists(SYS_PATH . "libraries/$c" . EXT))
				{
					exit("<b>$c" . EXT . "</b> not found in " . SYS_PATH . "libraries/");
				}
				include_once SYS_PATH . "libraries/$c" . EXT;
			}
		}
		else
		{
			//$class = ucfirst($class);
			$class = strtolower($class);
			if (!file_exists(SYS_PATH . "libraries/$class" . EXT))
			{
				exit("<b>$class" . EXT . "</b> not found in " . SYS_PATH . "libraries/");
			}
			include_once SYS_PATH . "libraries/$class" . EXT;
		}
	}


	/**
	 * Include webgine file
	 *
	 * @param string $file
	 */
	public static function webgine($file)
	{
		if (!file_exists(TDD_SYSTEM_PATH . "$file" . EXT))
		{
			exit("<b>$file" . EXT . "</b> not found in " . TDD_SYSTEM_PATH." folder");		
		}
		require_once TDD_SYSTEM_PATH . "$file" . EXT;
	}
	
	public static function tddgine($file) {
		
		if (!file_exists(TDD_ENGINE_PATH . "$file" . EXT))
		{
			exit("<b>$file" . EXT . "</b> not found in " . TDD_ENGINE_PATH." folder");		
		}
		require_once TDD_ENGINE_PATH . "$file" . EXT;
	}
	
	
	public static function config($file) {
		
		if (!file_exists(TDD_CONFIG_PATH . "$file" . EXT))
		{
			exit("<b>$file" . EXT . "</b> not found in " . TDD_CONFIG_PATH." folder");		
		}
		require_once TDD_CONFIG_PATH . "$file" . EXT;
	}
	


}

?>