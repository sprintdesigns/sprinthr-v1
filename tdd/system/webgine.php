<?php
require_once TDD_SYSTEM_PATH . '/loader.php';
Loader::appMainConfig('constants');
Loader::appMainConfig('config_company');
Loader::appMainConfig('config');
Loader::config('database');
Loader::webgine('base');
Loader::webgine('model');
Loader::tddgine('autorun');

Loader::appMainLibrary('class_subfolder_loader');

if (DB_DATABASE != '')
{
	//connect to database
	$cnt = mysql_connect(DB_HOST, DB_USERNAME, DB_PASSWORD) or die(mysql_error());
	mysql_select_db(DB_DATABASE, $cnt) or die(mysql_error());
	mysql_query ('SET NAMES UTF8');
}

$c = get_controller();
?>
<a href="<?php echo BASE_FOLDER; ?>simpletest/docs/en/unit_test_documentation.html" target="_blank">Documentation</a> | <a href="http://simpletest.org/api/" target="_blank">Reference</a>
<?php
require_once 'test/'.$c.EXT;
?>