<?php
error_reporting(1);
define("BASE_PATH", str_replace("\\", "/", realpath(dirname(__FILE__))).'/');

class Payroll_Computation extends UnitTestCase {    
	function testCase04_Late_Daily() {                
		$employee_code = 'AAA';
		$employee_type = "Daily";
		$salary 	   = 456;
		$dependents	   = 2;

        $c = G_Company_Factory::get();
        $c->addCutoffPeriodByDate('2014-05-06');

        //Create Employee
        $c->hireEmployee($employee_code, 'Rossel', 'Nava', 'Nava', '1985-11-01', 'Female', 'Married', 
        		$dependents, '2014-01-01', 'Software Engineer', 'Software Engineer', 'Regular', $salary, $employee_type);
        $e = G_Employee_Finder::findByEmployeeCode($employee_code);

        //Add Schedule
        $schedule_name = 'Schedule: Test Case - May1';
        $working_days  = 'mon,tue,wed,thu,fri';
        $time_in       = '08:00:00';
        $time_out      = '17:00:00';

        //TODO: CREATE USABLE USAGE OF SCHEDULE
        $group = new G_Schedule_Group;
        $group->setName($schedule_name);
        $group->setEffectivityDate('2014-05-01');		
		$group->setGracePeriod(0);	
        $group_id = $group->save();
        $group->setId($group_id);
        
        $s = new G_Schedule;
        $s->setName($schedule_name);
        $s->setWorkingDays($working_days);
        $s->setTimeIn($time_in);
        $s->setTimeOut($time_out);
        $s->setScheduleGroupId($group_id);
        $schedule_id = $s->save();               
        $s->setId($schedule_id);
        $s->saveToScheduleGroup($group);
        $s->assignToEmployeeWithGroup($e, '2014-05-01', '2014-05-30'); //Assign to employee   

        $date = '2014-05-06';
        $e->goToWork($date, '08:30:00', '17:00:00');

        $date = '2014-05-07';
        $e->goToWork($date, '08:30:00', '17:00:00');

        $date = '2014-05-08';
        $e->goToWork($date, '08:00:00', '17:00:00');

        $date = '2014-05-09';
        $e->goToWork($date, '08:00:00', '17:00:00');

        $date = '2014-05-12';
        $e->goToWork($date, '08:03:00', '17:00:00');

        $date = '2014-05-13';
        $e->goToWork($date, '08:03:00', '17:00:00');

        $date = '2014-05-14';
        $e->goToWork($date, '08:03:00', '17:00:00');

        $date = '2014-05-15';
        $e->goToWork($date, '08:00:00', '17:00:00');

        $date = '2014-05-16';
        $e->goToWork($date, '08:45:00', '17:00:00');

        $date = '2014-05-19';
        $e->goToWork($date, '08:05:00', '17:00:00');

        $date = '2014-05-20';
        $e->goToWork($date, '08:05:00', '17:00:00');


        //Payslip
		$p = $c->generatePayslipByEmployee($e, '2014', 5, 1);

		//echo '<pre>';
		//print_r($p);

		$sss = $p->getSss();
		$phealth = $p->getPhilhealth();
		$pagibig = $p->getPagibig();
		$tax = $p->getWithheldTax();
		$net_pay = $p->getNetPay();

		//$ph = new G_Payslip_Helper($p);
		//$late_amount = $ph->getValue('late_amount');

		//$this->assertEqual($sss, 166.70);
		//$this->assertEqual($phealth, 100);
		//$this->assertEqual($pagibig, 97.97);
		//$this->assertEqual($tax, 110.39);
		//$this->assertEqual($late_amount, 117.42);
		//$this->assertEqual($net_pay, 4306.10);

        //Delete schedule after the test
		$g = G_Schedule_Group_Finder::findById($group_id);  
        $g->deleteAll(); 
	}
	
	function testCase01_Daily() {
		$file = BASE_PATH . "attendance/dtr_instance18.xlsx";
        G_Attendance_Helper::importTimesheet($file);

		$e = G_Employee_Finder::findByEmployeeCode('GC006'); // Daily
		$c = G_Company_Factory::get();
		$p = $c->generatePayslipByEmployee($e, '2014', 9, 1);

        ///Utilities::displayArray($p);

		$sss     = $p->getSss();
		$phealth = $p->getPhilhealth();
		$pagibig = $p->getPagibig();
		$tax     = $p->getWithheldTax();
		$net_pay = $p->getNetPay();


		//echo '<pre>';
		//print_r($p);

		$this->assertEqual($sss, 150);
		$this->assertEqual($phealth, 100);
		$this->assertEqual($pagibig, 100);
		$this->assertEqual($tax, 74.73);
		$this->assertEqual($net_pay, 5047.27);
	}

	/*function testCase02_Monthly() {
		$file = BASE_PATH . "attendance/dtr_instance19.xlsx";
        G_Attendance_Helper::importTimesheet($file);

		$e = G_Employee_Finder::findByEmployeeCode('GC005'); // Monthly
		$c = G_Company_Factory::get();
		$p = $c->generatePayslipByEmployee($e, '2014', 9, 1);

		$sss = $p->getSss();
		$phealth = $p->getPhilhealth();
		$pagibig = $p->getPagibig();
		$tax = $p->getWithheldTax();
		$net_pay = $p->getNetPay();

		$this->assertEqual($sss, 400);
		$this->assertEqual($phealth, 150);
		$this->assertEqual($pagibig, 100);
		$this->assertEqual($tax, 97.53);
		$this->assertEqual($net_pay, 5252.47);
	}*/

	function testCase03_Holiday() {
		$employee_code = 'GC007';
		$employee_type = "Monthly";
		$salary 	   = 12000;
		$dependents	   = 2;

        $c = G_Company_Factory::get();

        //Add Holidays
        $h1 = $c->addHoliday('Holiday1', 2014, 4, 7, G_Holiday::LEGAL);
        $h2 = $c->addHoliday('Holiday2', 2014, 4, 8, G_Holiday::SPECIAL);
        $h3 = $c->addHoliday('Holiday3', 2014, 4, 14, G_Holiday::LEGAL);
        $h4 = $c->addHoliday('Holiday4', 2014, 4, 17, G_Holiday::SPECIAL);  

        //Create Employee
        $c->hireEmployee($employee_code, 'Kem', 'Kem', 'Kem', '1985-11-01', 'Female', 'Married', 
        		$dependents, '2014-01-01', 'Software Engineer', 'Software Engineer', 'Regular', $salary, $employee_type);
        $e = G_Employee_Finder::findByEmployeeCode($employee_code);

        //Add Schedule
        $schedule_name = 'Schedule: Test Case';
        $working_days  = 'mon,tue,wed,thu,fri';
        $time_in       = '08:00:00';
        $time_out      = '17:00:00';

        //TODO: CREATE USABLE USAGE OF SCHEDULE
        $group = new G_Schedule_Group;
        $group->setName($schedule_name);
        $group->setEffectivityDate('2014-04-01');		
		$group->setGracePeriod(0);	
        $group_id = $group->save();
        $group->setId($group_id);
        
        $s = new G_Schedule;
        $s->setName($schedule_name);
        $s->setWorkingDays($working_days);
        $s->setTimeIn($time_in);
        $s->setTimeOut($time_out);
        $s->setScheduleGroupId($group_id);
        $schedule_id = $s->save();               
        $s->setId($schedule_id);
        $s->saveToScheduleGroup($group);
        $s->assignToEmployeeWithGroup($e, '2014-04-01', '2014-04-30'); //Assign to employee    

        //DTR
		$file = BASE_PATH . "attendance/dtr_instance20.xlsx";
        G_Attendance_Helper::importTimesheet($file);     

        //Payslip
		$c = G_Company_Factory::get();
		$p = $c->generatePayslipByEmployee($e, '2014', 4, 1);

		$sss = $p->getSss();
		$phealth = $p->getPhilhealth();
		$pagibig = $p->getPagibig();
		$tax = $p->getWithheldTax();
		$net_pay = $p->getNetPay();

		$this->assertEqual($sss, 400);
		$this->assertEqual($phealth, 150);
		$this->assertEqual($pagibig, 100);
		$this->assertEqual($tax, 429.43);
		$this->assertEqual($net_pay, 7029.85);

		//DELETE RECORDS

		//Delete schedule after the test
		$g = G_Schedule_Group_Finder::findById($group_id);  
        $g->deleteAll(); 

        //Delete holidays
        $h1 = G_Holiday_Finder::findById($h1);
        $h1->delete();

        $h2 = G_Holiday_Finder::findById($h2);
        $h2->delete();

        $h3 = G_Holiday_Finder::findById($h3);
        $h3->delete();

        $h4 = G_Holiday_Finder::findById($h4);
        $h4->delete();
	}

	function testCase04_Late_Monthly() {
		$employee_code = 'GC007';
		$employee_type = "Monthly";
		$salary 	   = 12000;
		$dependents	   = 2;

        $c = G_Company_Factory::get();
        $c->addCutoffPeriodByDate('2014-05-06');

        //Create Employee
        $c->hireEmployee($employee_code, 'Kem', 'Kem', 'Kem', '1985-11-01', 'Female', 'Married', 
        		$dependents, '2014-01-01', 'Software Engineer', 'Software Engineer', 'Regular', $salary, $employee_type);
        $e = G_Employee_Finder::findByEmployeeCode($employee_code);

        //Add Schedule
        $schedule_name = 'Schedule: Test Case - May';
        $working_days  = 'mon,tue,wed,thu,fri';
        $time_in       = '08:00:00';
        $time_out      = '17:00:00';

        //TODO: CREATE USABLE USAGE OF SCHEDULE
        $group = new G_Schedule_Group;
        $group->setName($schedule_name);
        $group->setEffectivityDate('2014-05-01');		
		$group->setGracePeriod(0);	
        $group_id = $group->save();
        $group->setId($group_id);
        
        $s = new G_Schedule;
        $s->setName($schedule_name);
        $s->setWorkingDays($working_days);
        $s->setTimeIn($time_in);
        $s->setTimeOut($time_out);
        $s->setScheduleGroupId($group_id);
        $schedule_id = $s->save();               
        $s->setId($schedule_id);
        $s->saveToScheduleGroup($group);
        $s->assignToEmployeeWithGroup($e, '2014-05-01', '2014-05-30'); //Assign to employee   

        $date = '2014-05-06';
        $e->goToWork($date, '08:30:00', '17:00:00');

        $date = '2014-05-07';
        $e->goToWork($date, '08:30:00', '17:00:00');

        $date = '2014-05-08';
        $e->goToWork($date, '08:00:00', '17:00:00');

        $date = '2014-05-09';
        $e->goToWork($date, '08:00:00', '17:00:00');

        $date = '2014-05-12';
        $e->goToWork($date, '08:03:00', '17:00:00');

        $date = '2014-05-13';
        $e->goToWork($date, '08:03:00', '17:00:00');

        $date = '2014-05-14';
        $e->goToWork($date, '08:03:00', '17:00:00');

        $date = '2014-05-15';
        $e->goToWork($date, '08:00:00', '17:00:00');

        $date = '2014-05-16';
        $e->goToWork($date, '08:45:00', '17:00:00');

        $date = '2014-05-19';
        $e->goToWork($date, '08:05:00', '17:00:00');

        $date = '2014-05-20';
        $e->goToWork($date, '08:05:00', '17:00:00');

         //Payslip
		$p = $c->generatePayslipByEmployee($e, '2014', 5, 1);

		$sss = $p->getSss();
		$phealth = $p->getPhilhealth();
		$pagibig = $p->getPagibig();
		$tax = $p->getWithheldTax();
		$net_pay = $p->getNetPay();

		$ph = new G_Payslip_Helper($p);
		$late_amount = $ph->getValue('late_amount');

		$this->assertEqual($sss, 400);
		$this->assertEqual($phealth, 150);
		$this->assertEqual($pagibig, 100);
		$this->assertEqual($tax, 85.72);
		$this->assertEqual($late_amount, 118.07);
		$this->assertEqual($net_pay, 5146.21);

        //Delete schedule after the test
		$g = G_Schedule_Group_Finder::findById($group_id);  
        $g->deleteAll(); 
	}	
}

class Payroll extends UnitTestCase {

    function testCase01()
	{      
	    /**
	     * Morning Shift : Monthly
	     */
        //echo "<pre>";
        
		$employee_type = "Monthly";
		$rate 		   = 12000;
        
        //Create Employee
        $c = G_Company_Factory::get();
        $e = G_Employee_Finder::findByEmployeeCode('2014-GLEENT-TESTCASE-A');
        if( empty($e) ) {
            $c->hireEmployee('2014-GLEENT-TESTCASE-A', 'Jongjong', 'Jang', 'Jing', '1985-11-01', 'Male', 'Married',
                0, '2014-01-01', 'Software Engineer', 'Software Engineer', 'Regular', $rate, $employee_type);
            $e = G_Employee_Finder::findByEmployeeCode('2014-GLEENT-TESTCASE-A');
        }

        //Schedule
        $schedule_name = 'Schedule : Test Case 01';
        $working_days  = 'mon,tue,wed,thu,fri,sat';
        $time_in       = '08:00:00';
        $time_out      = '17:00:00';
        $restday       = array('2014-01-05','2014-01-12');
        $holiday_ids   = array();
        $holiday_dates = array(
                0 => array(
                        "date" => '2014-01-09',
                        "title" => 'Test Case Holiday',
                        "type" => 1 // 1 = Legal / 2 = Special
                    )
            );
        
        //Attendance dummy db
        $attendance_start_date = '2014-01-01';
        $attendance_end_date   = '2014-01-15';
           
        $attendance = array(
        	0 => array(
        			"date" => '2014-01-01',
        			"time_in" => '08:45:00',
        			"time_out" => '19:45:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '19:45:00'
        		),
        	1 => array(
        			"date" => '2014-01-02',
        			"time_in" => '08:00:00',
                    "time_out" => '19:45:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '19:45:00'
        		),
        	2 => array(
        			"date" => '2014-01-03',
   		            "time_in" => '08:00:00',
                    "time_out" => '19:45:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '19:45:00'
                    
        		),        	
            3 => array(
        			"date" => '2014-01-04',
        			"time_in" => '08:00:00',
                    "time_out" => '19:45:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '19:45:00'
        		),
            4 => array(
        			"date" => '2014-01-05',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            5 => array(
        			"date" => '2014-01-06',
        			"time_in" => '08:00:00',
                    "time_out" => '19:45:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '19:45:00'
        		),
            6 => array(
        			"date" => '2014-01-07',
        			"time_in" => '08:00:00',
                    "time_out" => '19:45:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '19:45:00'
        		),
            7 => array(
        			"date" => '2014-01-08',
        			"time_in" => '08:00:00',
                    "time_out" => '19:45:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '19:45:00'
        		), 
            8 => array(
        			"date" => '2014-01-09',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            9 => array(
        			"date" => '2014-01-10',
        			"time_in" => '08:00:00',
                    "time_out" => '19:45:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '19:45:00'
        		),
            10 => array(
        			"date" => '2014-01-11',
        			"time_in" => '08:00:00',
                    "time_out" => '19:45:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '19:45:00'
        		), 
            11 => array(
        			"date" => '2014-01-12',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            12 => array(
        			"date" => '2014-01-13',
        			"time_in" => '08:00:00',
                    "time_out" => '19:45:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '19:45:00'
        		),
            13 => array(
        			"date" => '2014-01-14',
        			"time_in" => '08:00:00',
                    "time_out" => '19:45:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '19:45:00'
        		),
            14 => array(
        			"date" => '2014-01-15',
        			"time_in" => '08:00:00',
                    "time_out" => '19:45:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '19:45:00'
        		)
        );

        //Holiday
        foreach($holiday_dates as $holiday){
            $day   = date("d",strtotime($holiday['date']));
            $month = date("m",strtotime($holiday['date']));
            $year  = date("Y",strtotime($holiday['date']));
            $hol   = G_Holiday_Finder::findByMonthDayYear($month,$day,$year);

            if( empty($hol) ){                
                $hol = new G_Holiday();
                $hol->setTitle($holiday['title']);
                $hol->setMonth($month);
                $hol->setDay($day);
                $hol->setType($holiday['type']);
                $hol->setYear($year);
                $holiday_ids[] = $hol->save(); //Store to array, will remove/delete holiday after assert equal
            }            
        }             
        //Schedule
        $s = G_Schedule_Finder::findByScheduleName($schedule_name);          
        if( empty($s) ){            
            $group = new G_Schedule_Group;
            $group->setName($schedule_name);
            $group->setEffectivityDate('2012-10-25');		
			$group->setGracePeriod(0);	
            $group_id = $group->save();
            $group->setId($group_id);
            
            $s = new G_Schedule;
            $s->setName($schedule_name);
            $s->setWorkingDays($working_days);
            $s->setTimeIn($time_in);
            $s->setTimeOut($time_out);
            $s->setScheduleGroupId($group_id);
            $schedule_id = $s->save();               
            $s->setId($schedule_id);
            $s->saveToScheduleGroup($group);
            $s->assignToEmployeeWithGroup($e, '2012-10-25', '2020-10-30');//Assign to employee
                
            //Create RD
            $r = new G_Restday;
            $r->setEmployeeId($e->getId());
            $r->setTimeIn($time_in);
            $r->setTimeOut($time_out);    
            foreach($restday as $rd){
                $r->setDate($rd);                            
                $r->save();   
            }
            
            $s = G_Schedule_Finder::findById($schedule_id);

        }else{            
            //$s->assignToEmployee($e, '2012-10-25', '2020-10-30');//Assign to employee        
        }

        //Attendance
        foreach($attendance as $a){
        	$date        = date("Y-m-d",strtotime($a['date']));
        	$time_in     = date("H:i:s",strtotime($a['time_in']));
        	$time_out    = date("H:i:s",strtotime($a['time_out']));
            
            $ot_time_in  = date("H:i:s",strtotime($a['ot_time_in']));
        	$ot_time_out = date("H:i:s",strtotime($a['ot_time_out']));
            $ot_reason   = "Sample Test Case01";
            
            $e->goToWork($date, $time_in, $time_out);//Record attendance
            if( $a['ot_time_in'] != '00:00:00' && $a['ot_time_out'] != '00:00:00' ){                
                $e->addApprovedOvertime($date,$ot_time_in,$ot_time_out,$ot_reason); //Record Overtime
            }      
        }
        
        $e_attendance = $e->getAttendance($attendance_start_date,$attendance_end_date);
        //print_r($e_attendance);
        //exit;
        
        foreach($e_attendance as $a){
            $timesheet = $a->getTimeSheet();
            if( !$a->isRestday() && !$a->isHoliday() ){
                $total_hrs_worked += $timesheet->getTotalHoursWorked() - $timesheet->getTotalOvertimeHours();
            }
            
            //Display IN/OUT               
            $is_holiday = $a->isHoliday() ? 'Yes' : 'No';   
            $is_restday = $a->isRestday() ? 'Yes' : 'No';      
            //echo "Date : " . $a->getDate() . " / Time IN : " . $timesheet->getTimeIn() . " / Time OUT : " . $timesheet->getTimeOut() . " / Holiday : {$is_holiday} / Restday : {$is_restday} <br />";            
        }
        
        $c      = G_Company_Factory::get();
        $period = G_Cutoff_Period_Finder::findByPeriod($attendance_start_date, $attendance_end_date);

        if( empty($period) ){
            $cutoff_start = "2014-01-01";
            $cutoff_end   = "2014-01-15";

            $year         = date("Y", strtotime($cutoff_start));
            $payout_date  = date("Y-m-d", strtotime("+2 month",strtotime($cutoff_end)));
            $day_start    = date("d",strtotime($cutoff_start));

            if( $day_start <= 15 ){
                $salary_cycle    = 1;
                $salary_cycle_id = 2;
            }else{
                $salary_cycle    = 2;
                $salary_cycle_id = 2;
            }

            $period = new G_Cutoff_Period();
            $period->setYearTag($year);
            $period->setStartDate($cutoff_start);
            $period->setEndDate($cutoff_end);
            $period->setPayoutDate($payout_date);
            $period->setCutoffNumber($salary_cycle);
            $period->setSalaryCycleId($salary_cycle_id);
            $period->setIsPayrollGenerated("");
            $period->setIsLock("No");
            $c_last_id = $period->save();
            $period->setId($c_last_id);
        }

        $p      = $c->generatePayslipByEmployee($e, '2014', '1', '1');
        
        $labels       = $p->getLabels();
        $valid_labels = array('Monthly Rate','Salary Type','Salary Rate','Daily Rate','Hourly Rate','Present Days with Pay','Restday OT Hours','Restday OT Amount','Regular OT Hours','Regular OT Amount','Regular NS Hours','Regular NS Amount','Regular NS OT Hours','Regular NS OT Amount','Restday Hours','Restday Amount','Restday OT Hours','Restday OT Amount','Restday NS Hours','Restday NS Amount','Restday NS OT Hours','Restday NS OT Amount','Rest Day Special Hours','Rest Day Special Amount','Rest Day Special OT Hours','Rest Day Special OT Amount','Rest Day Special NS Hours','Rest Day Special NS Amount','Rest Day Legal Hours','Rest Day Legal Amount','Holiday Special Hours','Holiday Special Amount','Holiday Special OT Hours','Holiday Special OT Amount','Holiday Legal Hours','Holiday Legal Amount','Holiday Legal OT Hours','Holiday Legal OT Hours','Holiday Legal OT Amount');
        $data         = array();
        
        foreach($labels as $l){
            if( in_array($l->getLabel(),$valid_labels) ){
                $data[$l->getLabel()] = number_format($l->getValue(),2);
            }
        }
        
        //Remove holidays
        foreach($holiday_ids as $id){
            $hol = new G_Holiday();
            $hol->setId($id);
            $hol->delete();
        }

        $data['Total Regular HRS Worked'] = $total_hrs_worked;
       
        //print_r($p);
        //print_r($data);
        
        $this->assertEqual($data['Daily Rate'], 458.54);    
        $this->assertEqual($data['Hourly Rate'], 57.32);
        $this->assertEqual($data['Total Regular HRS Worked'], 88);
        $this->assertEqual($data['Regular NS Hours'], 0);
        $this->assertEqual($data['Regular NS Amount'], 0);
        
        $this->assertEqual($data['Restday Hours'], 16);
        $this->assertEqual($data['Restday Amount'], 1,192.2);
        $this->assertEqual($data['Restday NS Hours'], 0);
        $this->assertEqual($data['Restday NS Amount'], 0);
        $this->assertEqual($data['Rest Day Special Hours'], 0);
        $this->assertEqual($data['Rest Day Special Amount'], 0);
        $this->assertEqual($data['Rest Day Special NS Hours'], 0);
        $this->assertEqual($data['Rest Day Special NS Amount'], 0);
        $this->assertEqual($data['Rest Day Legal Hours'], 0);
        $this->assertEqual($data['Rest Day Legal Amount'], 0);
        
        $this->assertEqual($data['Holiday Special Hours'], 0);
        $this->assertEqual($data['Holiday Special Amount'], 0);
        $this->assertEqual($data['Holiday Legal Hours'], 16);
        $this->assertEqual($data['Holiday Legal Amount'], 917.08);
        
        $this->assertEqual($data['Regular OT Hours'], 30.25);
        $this->assertEqual($data['Regular OT Amount'], 2,167.32);
        $this->assertEqual($data['Regular NS OT Hours'], 0);
        $this->assertEqual($data['Regular NS OT Amount'], 0);
        $this->assertEqual($data['Restday OT Hours'], 0);
        $this->assertEqual($data['Restday OT Amount'], 0);
        $this->assertEqual($data['Restday NS OT Hours'], 0);
        $this->assertEqual($data['Restday NS OT Amount'], 0);
        $this->assertEqual($data['Rest Day Special OT Hours'], 0);
        $this->assertEqual($data['Rest Day Special OT Amount'], 0);
        $this->assertEqual($data['HHoliday Special OT Hours'], 0);
        $this->assertEqual($data['Holiday Special OT Amount'], 0);
        $this->assertEqual($data['Holiday Legal OT Hours'], 2.75);
        $this->assertEqual($data['Holiday Legal OT Amount'], 409.82);
	}	
    
    function testCase02()
	{ 
        /**
	     * Morning Shift : Monthly
	     */
         
        echo "<pre>";
        
		$employee_type = "Monthly";
		$rate 		   = 14000;

        //Create Employee
        $c = G_Company_Factory::get();
        $e = G_Employee_Finder::findByEmployeeCode('2014-GLEENT-TESTCASE-B');
        if( empty($e) ) {
            $c->hireEmployee('2014-GLEENT-TESTCASE-B', 'Jingjing', 'Jong', 'Jong', '1985-11-01', 'Male', 'Married',
                0, '2014-01-01', 'Software Engineer', 'Software Engineer', 'Regular', $rate, $employee_type);
            $e = G_Employee_Finder::findByEmployeeCode('2014-GLEENT-TESTCASE-B');
        }

        //Schedule
        $schedule_name = 'Schedule : Test Case 02';
        $working_days  = 'mon,tue,wed,thu,fri,sat';
        $time_in       = '08:00:00';
        $time_out      = '17:00:00';
        $restday       = array('2014-01-05','2014-01-12');
        $holiday_ids   = array();
        $holiday_dates = array(
                0 => array(
                        "date" => '2014-01-09',
                        "title" => 'Test Case Holiday',
                        "type" => 1 // 1 = Legal / 2 = Special
                    )
            );
        
        //Attendance dummy db
        $attendance_start_date = '2014-01-01';
        $attendance_end_date   = '2014-01-15';
           
        $attendance = array(
        	0 => array(
        			"date" => '2014-01-01',
        			"time_in" => '00:00:00',
        			"time_out" => '00:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
        	1 => array(
        			"date" => '2014-01-02',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
        	2 => array(
        			"date" => '2014-01-03',
   		            "time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
                    
        		),        	
            3 => array(
        			"date" => '2014-01-04',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            4 => array(
        			"date" => '2014-01-05',
        			"time_in" => '00:00:00',
                    "time_out" => '00:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            5 => array(
        			"date" => '2014-01-06',
        			"time_in" => '08:00:00',
                    "time_out" => '21:45:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '21:45:00'
        		),
            6 => array(
        			"date" => '2014-01-07',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            7 => array(
        			"date" => '2014-01-08',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		), 
            8 => array(
        			"date" => '2014-01-09',
        			"time_in" => '00:00:00',
                    "time_out" => '00:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            9 => array(
        			"date" => '2014-01-10',
        			"time_in" => '08:00:00',
                    "time_out" => '20:45:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '20:45:00'
        		),
            10 => array(
        			"date" => '2014-01-11',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		), 
            11 => array(
        			"date" => '2014-01-12',
        			"time_in" => '00:00:00',
                    "time_out" => '00:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            12 => array(
        			"date" => '2014-01-13',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            13 => array(
        			"date" => '2014-01-14',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            14 => array(
        			"date" => '2014-01-15',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		)
        );

        //Holiday       
        foreach($holiday_dates as $holiday){
            $day   = date("d",strtotime($holiday['date']));
            $month = date("m",strtotime($holiday['date']));
            $year  = date("Y",strtotime($holiday['date']));
            $hol = G_Holiday_Finder::findByMonthDayYear($month,$day,$year);
            if( empty($hol) ){                
                $hol = new G_Holiday();
                $hol->setTitle($holiday['title']);
                $hol->setMonth($month);
                $hol->setDay($day);
                $hol->setType($holiday['type']);
                $hol->setYear($year);
                $holiday_ids[] = $hol->save(); //Store to array, will remove/delete holiday after assert equal
            }
        }   
        
        //Schedule
        $s = G_Schedule_Finder::findByScheduleName($schedule_name);  
        if( empty($s) ){            
            $group = new G_Schedule_Group;
            $group->setName($schedule_name);
            $group->setEffectivityDate('2012-10-25');		
			$group->setGracePeriod(0);	
            $group_id = $group->save();
            $group->setId($group_id);
            
            $s = new G_Schedule;
            $s->setName($schedule_name);
            $s->setWorkingDays($working_days);
            $s->setTimeIn($time_in);
            $s->setTimeOut($time_out);
            $s->setScheduleGroupId($group_id);
            $schedule_id = $s->save();               
            $s->setId($schedule_id);
            $s->saveToScheduleGroup($group);
            $s->assignToEmployeeWithGroup($e, '2012-10-25', '2020-10-30');//Assign to employee
            
            //Create RD
            $r = new G_Restday;
            $r->setEmployeeId($e->getId());
            $r->setTimeIn($time_in);
            $r->setTimeOut($time_out);    
            foreach($restday as $rd){
                $r->setDate($rd);                            
                $r->save();   
            }
            
            $s = G_Schedule_Finder::findById($schedule_id);
            
        }else{
            //$s->assignToEmployee($e, '2012-10-25', '2020-10-30');//Assign to employee        
        }

        //Attendance
        foreach($attendance as $a){
        	$date        = date("Y-m-d",strtotime($a['date']));
        	$time_in     = date("H:i:s",strtotime($a['time_in']));
        	$time_out    = date("H:i:s",strtotime($a['time_out']));
            
            $ot_time_in  = date("H:i:s",strtotime($a['ot_time_in']));
        	$ot_time_out = date("H:i:s",strtotime($a['ot_time_out']));
            $ot_reason   = "Sample Test Case02";
            
            $e->goToWork($date, $time_in, $time_out);//Record attendance
            if( $a['ot_time_in'] != '00:00:00' && $a['ot_time_out'] != '00:00:00' ){                            
                $e->addApprovedOvertime($date,$ot_time_in,$ot_time_out,$ot_reason); //Record Overtime
            }               
        }
        
        $e_attendance = $e->getAttendance($attendance_start_date,$attendance_end_date);
               
        foreach($e_attendance as $a){
            $timesheet = $a->getTimeSheet();
            if( !$a->isRestday() && !$a->isHoliday() ){
               $total_hrs_worked += $timesheet->getTotalHoursWorked() - $timesheet->getTotalOvertimeHours();
            } 
            
            //Display IN/OUT   
            $is_holiday = $a->isHoliday() ? 'Yes' : 'No';
            $is_restday = $a->isRestday() ? 'Yes' : 'No';      
            //echo "Date : " . $a->getDate() . " / Time IN : " . $timesheet->getTimeIn() . " / Time OUT : " . $timesheet->getTimeOut() . " / Holiday : {$is_holiday} / Restday : {$is_restday} <br />";           
        }
        
        $c      = G_Company_Factory::get();
        $period = G_Cutoff_Period_Finder::findByPeriod($attendance_start_date, $attendance_end_date);

        if( empty($period) ){
            $cutoff_start = "2014-01-01";
            $cutoff_end   = "2014-01-15";

            $year        = date("Y", strtotime($cutoff_start));
            $payout_date = date("Y-m-d", strtotime("+2 month",strtotime($cutoff_end)));
            $day_start   = date("d",strtotime($cutoff_start));

            if( $day_start <= 15 ){
                $salary_cycle    = 1;
                $salary_cycle_id = 2;
            }else{
                $salary_cycle    = 2;
                $salary_cycle_id = 2;
            }

            $period = new G_Cutoff_Period();
            $period->setYearTag($year);
            $period->setStartDate($cutoff_start);
            $period->setEndDate($cutoff_end);
            $period->setPayoutDate($payout_date);
            $period->setCutoffNumber($salary_cycle);
            $period->setSalaryCycleId($salary_cycle_id);
            $period->setIsPayrollGenerated("");
            $period->setIsLock("No");
            $c_last_id = $period->save();
            $period->setId($c_last_id);
        }
        
        $p = $c->generatePayslipByEmployee($e, '2014', '1', '1');
        
        $labels       = $p->getLabels();
        $valid_labels = array('Monthly Rate','Salary Type','Salary Rate','Daily Rate','Hourly Rate','Present Days with Pay','Restday OT Hours','Restday OT Amount','Regular OT Hours','Regular OT Amount','Regular NS Hours','Regular NS Amount','Regular NS OT Hours','Regular NS OT Amount','Restday Hours','Restday Amount','Restday OT Hours','Restday OT Amount','Restday NS Hours','Restday NS Amount','Restday NS OT Hours','Restday NS OT Amount','Rest Day Special Hours','Rest Day Special Amount','Rest Day Special OT Hours','Rest Day Special OT Amount','Rest Day Special NS Hours','Rest Day Special NS Amount','Rest Day Legal Hours','Rest Day Legal Amount','Holiday Special Hours','Holiday Special Amount','Holiday Special OT Hours','Holiday Special OT Amount','Holiday Legal Hours','Holiday Legal Amount','Holiday Legal OT Hours','Holiday Legal OT Hours','Holiday Legal OT Amount');
        $data         = array();

        //Remove holidays
        foreach($holiday_ids as $id){            
            $hol = new G_Holiday();
            $hol->setId($id);
            $hol->delete();
        }
        
        foreach($labels as $l){
            if( in_array($l->getLabel(),$valid_labels) ){
                $data[$l->getLabel()] = number_format($l->getValue(),2);
            }
        }
        
        $data['Total Regular HRS Worked'] = $total_hrs_worked;
        
        //print_r($data);
        
        $this->assertEqual($data['Daily Rate'], 534.96);    
        $this->assertEqual($data['Hourly Rate'], 66.87);
        $this->assertEqual($data['Total Regular HRS Worked'], 88);    
        $this->assertEqual($data['Regular NS Hours'], 0);
        $this->assertEqual($data['Regular NS Amount'], 0);
        
        $this->assertEqual($data['Restday Hours'], 0);
        $this->assertEqual($data['Restday Amount'], 0);
        $this->assertEqual($data['Restday NS Hours'], 0);
        $this->assertEqual($data['Restday NS Amount'], 0);
        $this->assertEqual($data['Rest Day Special Hours'], 0);
        $this->assertEqual($data['Rest Day Special Amount'], 0);
        $this->assertEqual($data['Rest Day Special NS Hours'], 0);
        $this->assertEqual($data['Rest Day Special NS Amount'], 0);
        $this->assertEqual($data['Rest Day Legal Hours'], 0);
        $this->assertEqual($data['Rest Day Legal Amount'], 0);
        
        $this->assertEqual($data['Holiday Special Hours'], 0);
        $this->assertEqual($data['Holiday Special Amount'], 0);
        $this->assertEqual($data['Holiday Legal Hours'], 0);
        $this->assertEqual($data['Holiday Legal Amount'], 0);
        
        $this->assertEqual($data['Regular OT Hours'], 8.5);
        $this->assertEqual($data['Regular OT Amount'], 710.50);        
        $this->assertEqual($data['Regular NS OT Hours'], 0);
        $this->assertEqual($data['Regular NS OT Amount'], 0);        
        $this->assertEqual($data['Restday OT Hours'], 0);
        $this->assertEqual($data['Restday OT Amount'], 0);        
        $this->assertEqual($data['Restday NS OT Hours'], 0);
        $this->assertEqual($data['Restday NS OT Amount'], 0);        
        $this->assertEqual($data['Rest Day Special OT Hours'], 0);
        $this->assertEqual($data['Rest Day Special OT Amount'], 0);
        $this->assertEqual($data['Holiday Special OT Hours'], 0);
        $this->assertEqual($data['Holiday Special OT Amount'], 0);
        $this->assertEqual($data['Holiday Legal OT Hours'], 0);
        $this->assertEqual($data['Holiday Legal OT Amount'], 0);
	}	
    
    function testCase03()
	{ 
        /**
	     * Night Shift : Monthly
	     */
         
        //echo "<pre>";
        
		$employee_type = "Monthly";
		$rate 		   = 12000;

        //Create Employee
        $c = G_Company_Factory::get();
        $e = G_Employee_Finder::findByEmployeeCode('2014-GLEENT-TESTCASE-C');
        if( empty($e) ) {
            $c->hireEmployee('2014-GLEENT-TESTCASE-C', 'Jungjung', 'Jang', 'Jang', '1985-11-01', 'Male', 'Married',
                0, '2014-01-01', 'Software Engineer', 'Software Engineer', 'Regular', $rate, $employee_type);
            $e = G_Employee_Finder::findByEmployeeCode('2014-GLEENT-TESTCASE-C');
        }

        //Schedule
        $schedule_name = 'Schedule : Test Case 03';
        $working_days  = 'mon,tue,wed,thu,fri';
        $time_in       = '22:00:00';
        $time_out      = '06:00:00';
        $restday       = array('2014-01-04','2014-01-05','2014-01-11','2014-01-12');
        $holiday_ids   = array();
        $holiday_dates = array(
                0 => array(
                        "date" => '2014-01-09',
                        "title" => 'Test Case Holiday',
                        "type" => 1 // 1 = Legal / 2 = Special
                    )
            );

        //Attendance dummy db
        $attendance_start_date = '2014-01-01';
        $attendance_end_date   = '2014-01-15';
           
        $attendance = array(
        	0 => array(
        			"date" => '2014-01-01',
        			"time_in" => '00:00:00',
        			"time_out" => '00:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
        	1 => array(
        			"date" => '2014-01-02',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
        	2 => array(
        			"date" => '2014-01-03',
   		            "time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),        	
            3 => array(
        			"date" => '2014-01-04',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            4 => array(
        			"date" => '2014-01-05',
        			"time_in" => '00:00:00',
                    "time_out" => '00:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            5 => array(
        			"date" => '2014-01-06',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            6 => array(
        			"date" => '2014-01-07',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            7 => array(
        			"date" => '2014-01-08',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		), 
            8 => array(
        			"date" => '2014-01-09',
        			"time_in" => '00:00:00',
                    "time_out" => '00:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            9 => array(
        			"date" => '2014-01-10',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            10 => array(
        			"date" => '2014-01-11',
        			"time_in" => '00:00:00',
                    "time_out" => '00:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		), 
            11 => array(
        			"date" => '2014-01-12',
        			"time_in" => '00:00:00',
                    "time_out" => '00:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            12 => array(
        			"date" => '2014-01-13',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            13 => array(
        			"date" => '2014-01-14',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            14 => array(
        			"date" => '2014-01-15',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		)
        );

        //Holiday
        foreach($holiday_dates as $holiday){
            $day   = date("d",strtotime($holiday['date']));
            $month = date("m",strtotime($holiday['date']));
            $year  = date("Y",strtotime($holiday['date']));
            $hol = G_Holiday_Finder::findByMonthDayYear($month,$day,$year);
            if( empty($hol) ){
                $hol = new G_Holiday();
                $hol->setTitle($holiday['title']);
                $hol->setMonth($month);
                $hol->setDay($day);
                $hol->setType($holiday['type']);
                $hol->setYear($year);
                $holiday_ids[] = $hol->save(); //Store to array, will remove/delete holiday after assert equal
            }
        }   
        
        //Schedule
        $s = G_Schedule_Finder::findByScheduleName($schedule_name);  
        if( empty($s) ){
            $group = new G_Schedule_Group;
            $group->setName($schedule_name);
            $group->setEffectivityDate('2012-10-25');		
			$group->setGracePeriod(0);	
            $group_id = $group->save();
            $group->setId($group_id);
            
            $s = new G_Schedule;
            $s->setName($schedule_name);
            $s->setWorkingDays($working_days);
            $s->setTimeIn($time_in);
            $s->setTimeOut($time_out);
            $s->setScheduleGroupId($group_id);
            $schedule_id = $s->save();               
            $s->setId($schedule_id);
            $s->saveToScheduleGroup($group);
            $s->assignToEmployeeWithGroup($e, '2012-10-25', '2020-10-30');//Assign to employee
        
            //Create RD
            $r = new G_Restday;
            $r->setEmployeeId($e->getId());
            $r->setTimeIn($time_in);
            $r->setTimeOut($time_out);    
            foreach($restday as $rd){
                $r->setDate($rd);                            
                $r->save();   
            }
            
            $s = G_Schedule_Finder::findById($schedule_id);

        }else{            
            //$s->assignToEmployee($e, '2012-10-25', '2020-10-30');//Assign to employee        
        }
        
        //Attendance
        foreach($attendance as $a){
        	$date        = date("Y-m-d",strtotime($a['date']));
        	$time_in     = date("H:i:s",strtotime($a['time_in']));
        	$time_out    = date("H:i:s",strtotime($a['time_out']));            
            $ot_time_in  = date("H:i:s",strtotime($a['ot_time_in']));
        	$ot_time_out = date("H:i:s",strtotime($a['ot_time_out']));
            $ot_reason   = "Sample Test Case03";
            
            $e->goToWork($date, $time_in, $time_out);//Record attendance
            if( $a['ot_time_in'] != '00:00:00' && $a['ot_time_out'] != '00:00:00' ){                
                $e->addApprovedOvertime($date,$ot_time_in,$ot_time_out,$ot_reason); //Record Overtime
            }        
        }
        
        $e_attendance = $e->getAttendance($attendance_start_date,$attendance_end_date);
        
        //print_r($e_attendance);
        //exit;
        
        foreach($e_attendance as $a){
            $timesheet = $a->getTimeSheet();
            if( !$a->isRestday() && !$a->isHoliday() ){
                $total_hrs_worked += $timesheet->getTotalHoursWorked() - $timesheet->getTotalOvertimeHours();
            }
            
            //Display IN/OUT   
            $is_holiday = $a->isHoliday() ? 'Yes' : 'No';
            $is_restday = $a->isRestday() ? 'Yes' : 'No';      
            //echo "Date : " . $a->getDate() . " / Time IN : " . $timesheet->getTimeIn() . " / Time OUT : " . $timesheet->getTimeOut() . " / Holiday : {$is_holiday} / Restday : {$is_restday} <br />";                
        }
        
        $c      = G_Company_Factory::get();
        $period = G_Cutoff_Period_Finder::findByPeriod($attendance_start_date, $attendance_end_date);

        if( empty($period) ){
            $cutoff_start = "2014-01-01";
            $cutoff_end   = "2014-01-15";

            $year        = date("Y", strtotime($cutoff_start));
            $payout_date = date("Y-m-d", strtotime("+2 month",strtotime($cutoff_end)));
            $day_start   = date("d",strtotime($cutoff_start));

            if( $day_start <= 15 ){
                $salary_cycle    = 1;
                $salary_cycle_id = 2;
            }else{
                $salary_cycle    = 2;
                $salary_cycle_id = 2;
            }

            $period = new G_Cutoff_Period();
            $period->setYearTag($year);
            $period->setStartDate($cutoff_start);
            $period->setEndDate($cutoff_end);
            $period->setPayoutDate($payout_date);
            $period->setCutoffNumber($salary_cycle);
            $period->setSalaryCycleId($salary_cycle_id);
            $period->setIsPayrollGenerated("");
            $period->setIsLock("No");
            $c_last_id = $period->save();
            $period->setId($c_last_id);
        }
        
        $p      = $c->generatePayslipByEmployee($e, '2014', '1', '1');
        
        $labels       = $p->getLabels();
        $valid_labels = array('Monthly Rate','Salary Type','Salary Rate','Daily Rate','Hourly Rate','Present Days with Pay','Restday OT Hours','Restday OT Amount','Regular OT Hours','Regular OT Amount','Regular NS Hours','Regular NS Amount','Regular NS OT Hours','Regular NS OT Amount','Restday Hours','Restday Amount','Restday OT Hours','Restday OT Amount','Restday NS Hours','Restday NS Amount','Restday NS OT Hours','Restday NS OT Amount','Rest Day Special Hours','Rest Day Special Amount','Rest Day Special OT Hours','Rest Day Special OT Amount','Rest Day Special NS Hours','Rest Day Special NS Amount','Rest Day Legal Hours','Rest Day Legal Amount','Holiday Special Hours','Holiday Special Amount','Holiday Special OT Hours','Holiday Special OT Amount','Holiday Legal Hours','Holiday Legal Amount','Holiday Legal OT Hours','Holiday Legal OT Hours','Holiday Legal OT Amount');
        $data         = array();
        
        //Remove holidays
        foreach($holiday_ids as $id){
            $hol = new G_Holiday();
            $hol->setId($id);
            $hol->delete();
        }

        foreach($labels as $l){
            if( in_array($l->getLabel(),$valid_labels) ){
                $data[$l->getLabel()] = number_format($l->getValue(),2);
            }
        }
        
        $data['Total Regular HRS Worked'] = $total_hrs_worked;
        
        //print_r($p);
        //print_r($data);
        
        $this->assertEqual($data['Daily Rate'], 458.54);    
        $this->assertEqual($data['Hourly Rate'], 57.32);
        
        $this->assertEqual($data['Total Regular HRS Worked'], 72);
        $this->assertEqual($data['Regular NS Hours'], 72);
        $this->assertEqual($data['Regular NS Amount'], 412.65);
        
        $this->assertEqual($data['Restday Hours'], 8);
        $this->assertEqual($data['Restday Amount'], 596.10);
        $this->assertEqual($data['Restday NS Hours'], 8);
        $this->assertEqual($data['Restday NS Amount'], 59.61);
        $this->assertEqual($data['Rest Day Special Hours'], 0);
        $this->assertEqual($data['Rest Day Special Amount'], 0);
        $this->assertEqual($data['Rest Day Special NS Hours'], 0);
        $this->assertEqual($data['Rest Day Special NS Amount'], 0);
        $this->assertEqual($data['Rest Day Legal Hours'], 0);
        $this->assertEqual($data['Rest Day Legal Amount'], 0);
        
        $this->assertEqual($data['Holiday Special Hours'], 0);
        $this->assertEqual($data['Holiday Special Amount'], 0);
        $this->assertEqual($data['Holiday Legal Hours'], 0);
        $this->assertEqual($data['Holiday Legal Amount'], 0);
            
        $this->assertEqual($data['Regular OT Hours'], 0);
        $this->assertEqual($data['Regular OT Amount'], 0);
        $this->assertEqual($data['Regular NS OT Hours'], 0);
        $this->assertEqual($data['Regular NS OT Amount'], 0);
        $this->assertEqual($data['Restday OT Hours'], 0);
        $this->assertEqual($data['Restday OT Amount'], 0);
        $this->assertEqual($data['Restday NS OT Hours'], 0);
        $this->assertEqual($data['Restday NS OT Amount'], 0);
        $this->assertEqual($data['Rest Day Special OT Hours'], 0);
        $this->assertEqual($data['Rest Day Special OT Amount'], 0);
        $this->assertEqual($data['HHoliday Special OT Hours'], 0);
        $this->assertEqual($data['Holiday Special OT Amount'], 0);
        $this->assertEqual($data['Holiday Legal OT Hours'], 0);
        $this->assertEqual($data['Holiday Legal OT Amount'], 0);
	}
    
    function testCase04()
	{
	    /**
	     * Morning Shift : Daily
	     */
        //echo "<pre>";
        
		$employee_type = "Daily";
		$rate 		   = 456;

        //Create Employee
        $c = G_Company_Factory::get();
        $e = G_Employee_Finder::findByEmployeeCode('2014-GLEENT-TESTCASE-D');
        if( empty($e) ) {
            $c->hireEmployee('2014-GLEENT-TESTCASE-D', 'Jengjeng', 'Jeng', 'Jing', '1985-11-01', 'Male', 'Married',
                0, '2014-01-01', 'Software Engineer', 'Software Engineer', 'Regular', $rate, $employee_type);
            $e = G_Employee_Finder::findByEmployeeCode('2014-GLEENT-TESTCASE-D');
        }

        //Schedule
        $schedule_name = 'Schedule : Test Case 04';
        $working_days  = 'mon,tue,wed,thu,fri,sat';
        $time_in       = '08:00:00';
        $time_out      = '17:00:00';
        $restday       = array('2014-01-05','2014-01-12');
        $holiday_ids   = array();
        $holiday_dates = array(
                0 => array(
                        "date" => '2014-01-09',
                        "title" => 'Test Case Holiday',
                        "type" => 1 // 1 = Legal / 2 = Special
                    )
            );

        //Attendance dummy db
        $attendance_start_date = '2014-01-01';
        $attendance_end_date   = '2014-01-15';
           
        $attendance = array(
        	0 => array(
        			"date" => '2014-01-01',
        			"time_in" => '08:00:00',
        			"time_out" => '19:45:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '19:45:00'
        		),
        	1 => array(
        			"date" => '2014-01-02',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
        	2 => array(
        			"date" => '2014-01-03',
   		            "time_in" => '08:00:00',
                    "time_out" => '19:15:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '19:15:00'
                    
        		),        	
            3 => array(
        			"date" => '2014-01-04',
        			"time_in" => '08:00:00',
                    "time_out" => '21:20:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '21:20:00'
        		),
            4 => array(
        			"date" => '2014-01-05',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            5 => array(
        			"date" => '2014-01-06',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            6 => array(
        			"date" => '2014-01-07',
        			"time_in" => '08:00:00',
                    "time_out" => '20:43:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '20:43:00'
        		),
            7 => array(
        			"date" => '2014-01-08',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		), 
            8 => array(
        			"date" => '2014-01-09',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            9 => array(
        			"date" => '2014-01-10',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            10 => array(
        			"date" => '2014-01-11',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		), 
            11 => array(
        			"date" => '2014-01-12',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            12 => array(
        			"date" => '2014-01-13',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            13 => array(
        			"date" => '2014-01-14',
        			"time_in" => '08:00:00',
                    "time_out" => '18:25:00',
                    "ot_time_in" => '17:00:00',
                    "ot_time_out" => '18:25:00'
        		),
            14 => array(
        			"date" => '2014-01-15',
        			"time_in" => '08:00:00',
                    "time_out" => '17:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		)
        );

        //Holiday
        foreach($holiday_dates as $holiday){
            $day   = date("d",strtotime($holiday['date']));
            $month = date("m",strtotime($holiday['date']));
            $year  = date("Y",strtotime($holiday['date']));
            $hol = G_Holiday_Finder::findByMonthDayYear($month,$day,$year);
            if( empty($hol) ){
                $hol = new G_Holiday();
                $hol->setTitle($holiday['title']);
                $hol->setMonth($month);
                $hol->setDay($day);
                $hol->setType($holiday['type']);
                $hol->setYear($year);
                $holiday_ids[] = $hol->save(); //Store to array, will remove/delete holiday after assert equal
            }
        }   
        
        //Schedule
        $s = G_Schedule_Finder::findByScheduleName($schedule_name);  
        if( empty($s) ){
            $group = new G_Schedule_Group;
            $group->setName($schedule_name);
            $group->setEffectivityDate('2012-10-25');		
			$group->setGracePeriod(0);	
            $group_id = $group->save();
            $group->setId($group_id);
            
            $s = new G_Schedule;
            $s->setName($schedule_name);
            $s->setWorkingDays($working_days);
            $s->setTimeIn($time_in);
            $s->setTimeOut($time_out);
            $s->setScheduleGroupId($group_id);
            $schedule_id = $s->save();               
            $s->setId($schedule_id);
            $s->saveToScheduleGroup($group);
            $s->assignToEmployeeWithGroup($e, '2012-10-25', '2020-10-30');//Assign to employee
                
            //Create RD
            $r = new G_Restday;
            $r->setEmployeeId($e->getId());
            $r->setTimeIn($time_in);
            $r->setTimeOut($time_out);    
            foreach($restday as $rd){
                $r->setDate($rd);                            
                $r->save();   
            }
            
            $s = G_Schedule_Finder::findById($schedule_id);

        }else{            
            //$s->assignToEmployee($e, '2012-10-25', '2020-10-30');//Assign to employee        
        }

        //Attendance
        foreach($attendance as $a){
        	$date        = date("Y-m-d",strtotime($a['date']));
        	$time_in     = date("H:i:s",strtotime($a['time_in']));
        	$time_out    = date("H:i:s",strtotime($a['time_out']));
            
            $ot_time_in  = date("H:i:s",strtotime($a['ot_time_in']));
        	$ot_time_out = date("H:i:s",strtotime($a['ot_time_out']));
            $ot_reason   = "Sample Test Case01";
            
            $e->goToWork($date, $time_in, $time_out);//Record attendance
            if( $a['ot_time_in'] != '00:00:00' && $a['ot_time_out'] != '00:00:00' ){                
                $e->addApprovedOvertime($date,$ot_time_in,$ot_time_out,$ot_reason); //Record Overtime
            }      
        }
        
        $e_attendance = $e->getAttendance($attendance_start_date,$attendance_end_date);
        //print_r($e_attendance);
        //exit;
        
        foreach($e_attendance as $a){
            $timesheet = $a->getTimeSheet();
            if( !$a->isRestday() && !$a->isHoliday() ){
                $total_hrs_worked += $timesheet->getTotalHoursWorked() - $timesheet->getTotalOvertimeHours();
            }
            
            //Display IN/OUT   
            $is_holiday = $a->isHoliday() ? 'Yes' : 'No';
            $is_restday = $a->isRestday() ? 'Yes' : 'No';      
            //echo "Date : " . $a->getDate() . " / Time IN : " . $timesheet->getTimeIn() . " / Time OUT : " . $timesheet->getTimeOut() . " / Holiday : {$is_holiday} / Restday : {$is_restday} <br />";            
        }
        
        $c      = G_Company_Factory::get();
        $period = G_Cutoff_Period_Finder::findByPeriod($attendance_start_date, $attendance_end_date);

        if( empty($period) ){
            $cutoff_start = "2014-01-01";
            $cutoff_end   = "2014-01-15";

            $year        = date("Y", strtotime($cutoff_start));
            $payout_date = date("Y-m-d", strtotime("+2 month",strtotime($cutoff_end)));
            $day_start   = date("d",strtotime($cutoff_start));

            if( $day_start <= 15 ){
                $salary_cycle    = 1;
                $salary_cycle_id = 2;
            }else{
                $salary_cycle    = 2;
                $salary_cycle_id = 2;
            }

            $period = new G_Cutoff_Period();
            $period->setYearTag($year);
            $period->setStartDate($cutoff_start);
            $period->setEndDate($cutoff_end);
            $period->setPayoutDate($payout_date);
            $period->setCutoffNumber($salary_cycle);
            $period->setSalaryCycleId($salary_cycle_id);
            $period->setIsPayrollGenerated("");
            $period->setIsLock("No");
            $c_last_id = $period->save();
            $period->setId($c_last_id);
        }

        $p      = $c->generatePayslipByEmployee($e, '2014', '1', '1');
        
        $labels       = $p->getLabels();
        $valid_labels = array('Monthly Rate','Salary Type','Salary Rate','Daily Rate','Hourly Rate','Present Days with Pay','Restday OT Hours','Restday OT Amount','Regular OT Hours','Regular OT Amount','Regular NS Hours','Regular NS Amount','Regular NS OT Hours','Regular NS OT Amount','Restday Hours','Restday Amount','Restday OT Hours','Restday OT Amount','Restday NS Hours','Restday NS Amount','Restday NS OT Hours','Restday NS OT Amount','Rest Day Special Hours','Rest Day Special Amount','Rest Day Special OT Hours','Rest Day Special OT Amount','Rest Day Special NS Hours','Rest Day Special NS Amount','Rest Day Legal Hours','Rest Day Legal Amount','Holiday Special Hours','Holiday Special Amount','Holiday Special OT Hours','Holiday Special OT Amount','Holiday Legal Hours','Holiday Legal Amount','Holiday Legal OT Hours','Holiday Legal OT Hours','Holiday Legal OT Amount');
        $data         = array();
        
        //Remove holidays
        foreach($holiday_ids as $id){
            $hol = new G_Holiday();
            $hol->setId($id);
            $hol->delete();
        }

        foreach($labels as $l){
            if( in_array($l->getLabel(),$valid_labels) ){
                $data[$l->getLabel()] = number_format($l->getValue(),2);
            }
        }
        
        $data['Total Regular HRS Worked'] = number_format($total_hrs_worked,2);
       
        //print_r($p);
        //print_r($data);
        
        $this->assertEqual($data['Daily Rate'], 456.00);    
        $this->assertEqual($data['Hourly Rate'], 57.00);
        
        $this->assertEqual($data['Total Regular HRS Worked'], 88.00);    
        $this->assertEqual($data['Regular NS Hours'], 0);
        $this->assertEqual($data['Regular NS Amount'], 0);
       
        $this->assertEqual($data['Holiday Legal Hours'], 16);
        $this->assertEqual($data['Holiday Legal Amount'], 1,824.00);
        $this->assertEqual($data['Holiday Special Hours'], 0);
        $this->assertEqual($data['Holiday Special Amount'], 0);
        
        $this->assertEqual($data['Restday Hours'], 16);
        $this->assertEqual($data['Restday Amount'], 1,185.60);        
        $this->assertEqual($data['Rest Day Special Hours'], 0);
        $this->assertEqual($data['Rest Day Special Amount'], 0);
        $this->assertEqual($data['Rest Day Legal Hours'], 0);
        $this->assertEqual($data['Rest Day Legal Amount'], 0);
        $this->assertEqual($data['Rest Day Special NS Hours'], 0);
        $this->assertEqual($data['Rest Day Special NS Amount'], 0);
        $this->assertEqual($data['Restday NS Hours'], 0);
        $this->assertEqual($data['Restday NS Amount'], 0);
        
        $this->assertEqual($data['Regular OT Hours'], 11.72);
        $this->assertEqual($data['Regular OT Amount'], 834.81);
        $this->assertEqual($data['Regular NS OT Hours'], 0);
        $this->assertEqual($data['Regular NS OT Amount'], 0);
        $this->assertEqual($data['Restday OT Hours'], 0);
        $this->assertEqual($data['Restday OT Amount'], 0);       
        $this->assertEqual($data['Restday NS OT Hours'], 0);
        $this->assertEqual($data['Restday NS OT Amount'], 0);
        $this->assertEqual($data['Rest Day Special OT Hours'], 0);
        $this->assertEqual($data['Rest Day Special OT Amount'], 0);
        $this->assertEqual($data['Holiday Special OT Hours'], 0);
        $this->assertEqual($data['Holiday Special OT Amount'], 0);
        $this->assertEqual($data['Holiday Legal OT Hours'], 2.75);
        $this->assertEqual($data['Holiday Legal OT Amount'], 407.55);
	}
    
    function testCase05()
	{ 
        /**
	     * Night Shift : Daily
	     */
         
        //echo "<pre>";
        
		$employee_type = "Daily";
		$rate 		   = 456;

        //Create Employee
        $c = G_Company_Factory::get();
        $e = G_Employee_Finder::findByEmployeeCode('2014-GLEENT-TESTCASE-E');
        if( empty($e) ) {
            $c->hireEmployee('2014-GLEENT-TESTCASE-E', 'Jungjung', 'Jang', 'Jang', '1985-11-01', 'Male', 'Married',
                0, '2014-01-01', 'Software Engineer', 'Software Engineer', 'Regular', $rate, $employee_type);
            $e = G_Employee_Finder::findByEmployeeCode('2014-GLEENT-TESTCASE-E');
        }

        //Schedule
        $schedule_name = 'Schedule : Test Case 05';
        $working_days  = 'mon,tue,wed,thu,fri';
        $time_in       = '22:00:00';
        $time_out      = '06:00:00';
        $restday       = array('2014-01-04','2014-01-05','2014-01-11','2014-01-12');
        $holiday_ids   = array();
        $holiday_dates = array(
                0 => array(
                        "date" => '2014-01-09',
                        "title" => 'Test Case Holiday',
                        "type" => 1 // 1 = Legal / 2 = Special
                    )
            );

        //Attendance dummy db
        $attendance_start_date = '2014-01-01';
        $attendance_end_date   = '2014-01-15';
           
        $attendance = array(
        	0 => array(
        			"date" => '2014-01-01',
        			"time_in" => '00:00:00',
        			"time_out" => '00:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
        	1 => array(
        			"date" => '2014-01-02',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
        	2 => array(
        			"date" => '2014-01-03',
   		            "time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),        	
            3 => array(
        			"date" => '2014-01-04',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            4 => array(
        			"date" => '2014-01-05',
        			"time_in" => '00:00:00',
                    "time_out" => '00:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            5 => array(
        			"date" => '2014-01-06',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            6 => array(
        			"date" => '2014-01-07',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            7 => array(
        			"date" => '2014-01-08',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		), 
            8 => array(
        			"date" => '2014-01-09',
        			"time_in" => '00:00:00',
                    "time_out" => '00:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            9 => array(
        			"date" => '2014-01-10',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            10 => array(
        			"date" => '2014-01-11',
        			"time_in" => '00:00:00',
                    "time_out" => '00:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		), 
            11 => array(
        			"date" => '2014-01-12',
        			"time_in" => '00:00:00',
                    "time_out" => '00:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            12 => array(
        			"date" => '2014-01-13',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            13 => array(
        			"date" => '2014-01-14',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		),
            14 => array(
        			"date" => '2014-01-15',
        			"time_in" => '22:00:00',
                    "time_out" => '06:00:00',
                    "ot_time_in" => '00:00:00',
                    "ot_time_out" => '00:00:00'
        		)
        );
    
        //Holiday
        foreach($holiday_dates as $holiday){
            $day   = date("d",strtotime($holiday['date']));
            $month = date("m",strtotime($holiday['date']));
            $year  = date("Y",strtotime($holiday['date']));
            $hol = G_Holiday_Finder::findByMonthDayYear($month,$day,$year);
            if( empty($hol) ){
                $hol = new G_Holiday();
                $hol->setTitle($holiday['title']);
                $hol->setMonth($month);
                $hol->setDay($day);
                $hol->setType($holiday['type']);
                $hol->setYear($year);
                $holiday_ids[] = $hol->save(); //Store to array, will remove/delete holiday after assert equal
            }
        }   
        
        //Schedule
        $s = G_Schedule_Finder::findByScheduleName($schedule_name);  
        if( empty($s) ){
            $group = new G_Schedule_Group;
            $group->setName($schedule_name);
            $group->setEffectivityDate('2012-10-25');		
			$group->setGracePeriod(0);	
            $group_id = $group->save();
            $group->setId($group_id);
            
            $s = new G_Schedule;
            $s->setName($schedule_name);
            $s->setWorkingDays($working_days);
            $s->setTimeIn($time_in);
            $s->setTimeOut($time_out);
            $s->setScheduleGroupId($group_id);
            $schedule_id = $s->save();               
            $s->setId($schedule_id);
            $s->saveToScheduleGroup($group);
            $s->assignToEmployeeWithGroup($e, '2012-10-25', '2020-10-30');//Assign to employee
        
            //Create RD
            $r = new G_Restday;
            $r->setEmployeeId($e->getId());
            $r->setTimeIn($time_in);
            $r->setTimeOut($time_out);    
            foreach($restday as $rd){
                $r->setDate($rd);                            
                $r->save();   
            }
            
            $s = G_Schedule_Finder::findById($schedule_id);

        }else{            
            //$s->assignToEmployee($e, '2012-10-25', '2020-10-30');//Assign to employee        
        }
        
        //Attendance
        foreach($attendance as $a){
        	$date        = date("Y-m-d",strtotime($a['date']));
        	$time_in     = date("H:i:s",strtotime($a['time_in']));
        	$time_out    = date("H:i:s",strtotime($a['time_out']));            
            $ot_time_in  = date("H:i:s",strtotime($a['ot_time_in']));
        	$ot_time_out = date("H:i:s",strtotime($a['ot_time_out']));
            $ot_reason   = "Sample Test Case03";
            
            $e->goToWork($date, $time_in, $time_out);//Record attendance
            if( $a['ot_time_in'] != '00:00:00' && $a['ot_time_out'] != '00:00:00' ){                
                $e->addApprovedOvertime($date,$ot_time_in,$ot_time_out,$ot_reason); //Record Overtime
            }        
        }
        
        $e_attendance = $e->getAttendance($attendance_start_date,$attendance_end_date);
        
        //print_r($e_attendance);
        //exit;
        
        foreach($e_attendance as $a){
            $timesheet = $a->getTimeSheet();
            if( !$a->isRestday() && !$a->isHoliday() ){
                $total_hrs_worked += $timesheet->getTotalHoursWorked() - $timesheet->getTotalOvertimeHours();
            }
            
            //Display IN/OUT   
            $is_holiday = $a->isHoliday() ? 'Yes' : 'No';
            $is_restday = $a->isRestday() ? 'Yes' : 'No';      
            //echo "Date : " . $a->getDate() . " / Time IN : " . $timesheet->getTimeIn() . " / Time OUT : " . $timesheet->getTimeOut() . " / Holiday : {$is_holiday} / Restday : {$is_restday} <br />";                
        }
        
        $c      = G_Company_Factory::get();
        $period = G_Cutoff_Period_Finder::findByPeriod($attendance_start_date, $attendance_end_date);

        if( empty($period) ){
            $cutoff_start = "2014-01-01";
            $cutoff_end   = "2014-01-15";

            $year        = date("Y", strtotime($cutoff_start));
            $payout_date = date("Y-m-d", strtotime("+2 month",strtotime($cutoff_end)));
            $day_start   = date("d",strtotime($cutoff_start));

            if( $day_start <= 15 ){
                $salary_cycle    = 1;
                $salary_cycle_id = 2;
            }else{
                $salary_cycle    = 2;
                $salary_cycle_id = 2;
            }

            $period = new G_Cutoff_Period();
            $period->setYearTag($year);
            $period->setStartDate($cutoff_start);
            $period->setEndDate($cutoff_end);
            $period->setPayoutDate($payout_date);
            $period->setCutoffNumber($salary_cycle);
            $period->setSalaryCycleId($salary_cycle_id);
            $period->setIsPayrollGenerated("");
            $period->setIsLock("No");
            $c_last_id = $period->save();
            $period->setId($c_last_id);
        }
        
        $p      = $c->generatePayslipByEmployee($e, '2014', '1', '1');
        
        $labels       = $p->getLabels();
        $valid_labels = array('Monthly Rate','Salary Type','Salary Rate','Daily Rate','Hourly Rate','Present Days with Pay','Restday OT Hours','Restday OT Amount','Regular OT Hours','Regular OT Amount','Regular NS Hours','Regular NS Amount','Regular NS OT Hours','Regular NS OT Amount','Restday Hours','Restday Amount','Restday OT Hours','Restday OT Amount','Restday NS Hours','Restday NS Amount','Restday NS OT Hours','Restday NS OT Amount','Rest Day Special Hours','Rest Day Special Amount','Rest Day Special OT Hours','Rest Day Special OT Amount','Rest Day Special NS Hours','Rest Day Special NS Amount','Rest Day Legal Hours','Rest Day Legal Amount','Holiday Special Hours','Holiday Special Amount','Holiday Special OT Hours','Holiday Special OT Amount','Holiday Legal Hours','Holiday Legal Amount','Holiday Legal OT Hours','Holiday Legal OT Hours','Holiday Legal OT Amount');
        $data         = array();

        //Remove holidays
        foreach($holiday_ids as $id){
            $hol = new G_Holiday();
            $hol->setId($id);
            $hol->delete();
        }
        
        foreach($labels as $l){
            if( in_array($l->getLabel(),$valid_labels) ){
                $data[$l->getLabel()] = number_format($l->getValue(),2);
            }
        }
        
        $data['Total Regular HRS Worked'] = $total_hrs_worked;
        
        //print_r($p);
        //print_r($data);
        
        $this->assertEqual($data['Daily Rate'], 456.00);    
        $this->assertEqual($data['Hourly Rate'], 57.00);
        
        $this->assertEqual($data['Total Regular HRS Worked'], 72);
        $this->assertEqual($data['Regular NS Hours'], 72);
        $this->assertEqual($data['Regular NS Amount'], 410.40);
        
        $this->assertEqual($data['Restday Hours'], 8);
        $this->assertEqual($data['Restday Amount'], 592.80);
        $this->assertEqual($data['Restday NS Hours'], 8);
        $this->assertEqual($data['Restday NS Amount'], 59.28);
        $this->assertEqual($data['Rest Day Special Hours'], 0);
        $this->assertEqual($data['Rest Day Special Amount'], 0);
        $this->assertEqual($data['Rest Day Special NS Hours'], 0);
        $this->assertEqual($data['Rest Day Special NS Amount'], 0);
        $this->assertEqual($data['Rest Day Legal Hours'], 0);
        $this->assertEqual($data['Rest Day Legal Amount'], 0);
        
        $this->assertEqual($data['Holiday Special Hours'], 0);
        $this->assertEqual($data['Holiday Special Amount'], 0);
        $this->assertEqual($data['Holiday Legal Hours'], 0);
        $this->assertEqual($data['Holiday Legal Amount'], 0);
            
        $this->assertEqual($data['Regular OT Hours'], 0);
        $this->assertEqual($data['Regular OT Amount'], 0);
        $this->assertEqual($data['Regular NS OT Hours'], 0);
        $this->assertEqual($data['Regular NS OT Amount'], 0);
        $this->assertEqual($data['Restday OT Hours'], 0);
        $this->assertEqual($data['Restday OT Amount'], 0);
        $this->assertEqual($data['Restday NS OT Hours'], 0);
        $this->assertEqual($data['Restday NS OT Amount'], 0);
        $this->assertEqual($data['Rest Day Special OT Hours'], 0);
        $this->assertEqual($data['Rest Day Special OT Amount'], 0);
        $this->assertEqual($data['HHoliday Special OT Hours'], 0);
        $this->assertEqual($data['Holiday Special OT Amount'], 0);
        $this->assertEqual($data['Holiday Legal OT Hours'], 0);
        $this->assertEqual($data['Holiday Legal OT Amount'], 0);
	}
}
?>